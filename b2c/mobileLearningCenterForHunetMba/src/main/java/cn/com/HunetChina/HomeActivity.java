package cn.com.HunetChina;

import cn.co.HunetLib.Company.Company;
import cn.com.HunetChina.TabClassLoader.CompanySeq;

import android.os.Bundle;

public class HomeActivity extends cn.co.HunetLib.HomeActivity
{
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
	    super.onCreate(savedInstanceState);
	    
	    int company_seq = getCompany().getSeq();
	    
		if(company_seq == CompanySeq.SLS_PROWAY){
			setSelectTab(Company.eTAB_CLASSROOM);
		}
	}
}
