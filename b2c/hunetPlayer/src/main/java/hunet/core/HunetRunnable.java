package hunet.core;


public class HunetRunnable implements Runnable 
{
	private IHunetRunnableEventListener m_EventListener;
	private int 	m_nId;
	private Object 	m_objTag;
	
	public int GetId() { return m_nId; }
	
	public HunetRunnable(int nId)
	{
		m_nId = nId;
	}
	
	public void SetValue(Object tag)
	{
		m_objTag = tag;
	}
	
	public void SetEventListener(IHunetRunnableEventListener a_eventListener)
	{
		m_EventListener = a_eventListener;
	}
	
	@Override
	public void run() 
	{		
		if(m_EventListener != null)
			m_EventListener.OnRun(m_nId, m_objTag);
	}
}
