package hunet.net;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by albert on 2016. 1. 21..
 */
public class HttpData2 {

    public static String getData(String uri) throws IOException {
        StringBuilder sb=new StringBuilder();
        BufferedReader reader= null;

        try {
            URL url = new URL(uri);
            HttpURLConnection con= (HttpURLConnection) url.openConnection();
            reader= new BufferedReader(new InputStreamReader(con.getInputStream()));

            String line;
            while ((line=reader.readLine()) !=null) {

                sb.append(line+ "\n");

            }


        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        finally {
            if(reader != null){
                reader.close();
            }
        }

        return sb.toString();
    }


}
