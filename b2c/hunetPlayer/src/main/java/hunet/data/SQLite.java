package hunet.data;

import hunet.library.Utilities;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;

public class SQLite extends SQLiteOpenHelper 
{
	public final static String eLOGIN_TABLE_NM 				= "MLC_LOGIN";
	public final static String eACCESS_TABLE_NM 			= "MLC_ACCESS";
	public final static String eALARM_TABLE_NM 				= "MLC_ALARM";
	public final static String eSTUDYPROGRESS_TABLE_NM		= "MLC_STUDYPROGRESS";
	public final static String eDOWN_TAKE_COURSE_TABLE_NM	= "MLC_DOWN_TAKE_COURSE";
	public final static String eDOWN_STUDY_INDEX_TABLE_NM	= "MLC_DOWN_STUDY_INDEX";
	//public final static String eSTUDYPROGRESS_SANGSANG_TABLE_NM		= "MLC_STUDYPROGRESS_SANGSANG";
	public final static String eDOWN_TAKE_SANGSANG_TABLE_NM	= "MLC_DOWN_SANGSANG";
	public final static String eDOWN_STUDY_SANGSANG_TABLE_NM = "MLC_DOWN_STUDY_SANGSANG";
	public final static String eDOWNLOAD_CENTER_SETTINGS    = "MLC_DOWNLOAD_CENTER_SETTINGS";
	public final static String eAPP_EVENT_TABLE_NM			= "MLC_APP_EVENT";
	public final static String eAPP_SETTINGS_TABLE_NM		= "MLC_APP_SETTINGS";
	// 진도 동기화 못한 데이터
	public final static String TABLE_MLC_PROGRESS_SYNC_FAILED_LIST = "MLC_PROGRESS_SYNC_FAILED_LIST";
		
	public final static int DB_VERSION						= 19;
	
	private String CREATE_TABLE_APP_SETTINGS = "CREATE TABLE " + eAPP_SETTINGS_TABLE_NM + " ("
			+ "company_seq int"
			+ ", enable_sangsang_player_speedbar TEXT"
			+ ")";
	
	/**
	 * 로그인
	 */
	protected String CREATE_TABLE_QUERY = "CREATE TABLE MLC_LOGIN ("
		+ "id TEXT PRIMARY KEY, " + "pwd TEXT, "		
		+ "auto_login TEXT, "
		+ "save_id TEXT, "
		+ "user_nm TEXT, mobile_nm TEXT, email_nm TEXT, userType_cd TEXT, loginYn TEXT)";
	protected String DROP_TABLE_QUERY = "DROP TABLE MLC_LOGIN";
	
	/**
	 * 다운로드 센터 세팅
	 */
	protected String CREATE_TABLE_DOWNLOAD_CENTER_SETTINGS_QUERY = "CREATE TABLE MLC_DOWNLOAD_CENTER_SETTINGS ("
		+ "company_seq int, "		
		+ "tab1_text TEXT, "
		+ "tab2_text TEXT, "
		+ "only_imagine_yn TEXT)";
	protected String DROP_TABLE_DOWNLOAD_CENTER_SETTINGS_QUERY = "DROP TABLE MLC_DOWNLOAD_CENTER_SETTINGS";
	
	
	/**
	 * 3G 접속
	 */
	protected String CREATE_TABLE_ACCESS_QUERY = "CREATE TABLE MLC_ACCESS ("
		+ "id TEXT PRIMARY KEY, "
		+ "access_text TEXT, "
		+ "access_mov TEXT, "
		+ "push_type TEXT, "
		+ "external_memory TEXT)";
	protected String DROP_TABLE_ACCESS_QUERY = "DROP TABLE MLC_ACCESS";
	
	/**
	 * 알람
	 */
	protected String CREATE_TABLE_ALARM_QUERY = "CREATE TABLE MLC_ALARM ("
		+ "id TEXT PRIMARY KEY, "
		+ "use TEXT, "
		+ "week TEXT, "
		+ "hour TEXT, "
		+ "minute TEXT) ";
	protected String DROP_TABLE_ALARM_QUERY = "DROP TABLE MLC_ALARM";
	
	private String CREATE_TABLE_STUDYPROGRESS = "CREATE TABLE " + eSTUDYPROGRESS_TABLE_NM + " ("
			+ "study_progress_seq integer primary key autoincrement, url TEXT, try_count integer, reg_id TEXT, reg_date TEXT) ";
	
	private String CREATE_TABLE_DOWN_TAKE_COURSE = "CREATE TABLE " + eDOWN_TAKE_COURSE_TABLE_NM + " ("
			+ "take_course_seq integer primary key" 
			+ ", course_cd TEXT"
			+ ", course_nm TEXT"
			+ ", study_end_date TEXT) ";
	
	private String CREATE_TABLE_DOWN_STUDY_INDEX = "CREATE TABLE " + eDOWN_STUDY_INDEX_TABLE_NM + " ("
			+ "take_course_seq integer"
			+ ", chapter_no TEXT"
			+ ", index_no integer" 
			+ ", index_nm TEXT"
			+ ", max_mark_sec integer"
			+ ", max_view_sec integer" 
			+ ", view_index integer"
			+ ", study_sec integer" 
			+ ", last_view_sec integer"
			+ ", full_path TEXT"
			+ ", external_memory TEXT"
			+ ")";
	
	private String CREATE_TABLE_DOWN_TAKE_SANGSANG = "CREATE TABLE " + eDOWN_TAKE_SANGSANG_TABLE_NM + " ("
			+ "goods_id TEXT primary key" 
			+ ", goods_nm TEXT"				
			+ ", study_end_date TEXT) ";
	
	private String CREATE_TABLE_DOWN_STUDY_SANGSANG = "CREATE TABLE " + eDOWN_STUDY_SANGSANG_TABLE_NM + " ("
			+ "company_seq integer"
			+ ", contract_no integer"
			+ ", user_id TEXT"
			+ ", goods_id TEXT"
			+ ", contents_seq integer" 
			+ ", contents_nm TEXT"
			+ ", view_sec integer"
			+ ", view_sec_mobile integer"
			+ ", last_view_sec integer"
			+ ", view_no integer"
			+ ", contents_view_sec integer"
			+ ", se_goods_id TEXT"
			+ ", full_path TEXT"
			+ ", external_memory TEXT"
			+ ", last_position_sec integer"
			+ ") ";
	
	private String CREATE_TABLE_APP_EVENT = "CREATE TABLE " + eAPP_EVENT_TABLE_NM + " ("
			+ "event_seq integer primary key autoincrement, url TEXT, reg_date TEXT, try_count integer, max_count TEXT) ";
	
	private String CREATE_TABLE_PROGRESS_SYNC_FAILED_LIST = "CREATE TABLE " + TABLE_MLC_PROGRESS_SYNC_FAILED_LIST + " (" +
			"seq integer primary key autoincrement" +
			", url TEXT" +
			", params TEXT" +
			", reg_date TEXT" +
			")";
	
	public SQLite(Context context)
	{
		super(context, "HUNET_MBA.db", null, DB_VERSION);
	}
	
	@Override
	public void onCreate(SQLiteDatabase db)
	{
		if(ExistsTable(db, eLOGIN_TABLE_NM) == false)
			db.execSQL(CREATE_TABLE_QUERY);
		
		if(ExistsTable(db, eACCESS_TABLE_NM) == false)
			db.execSQL(CREATE_TABLE_ACCESS_QUERY);
		
		if(ExistsTable(db, eALARM_TABLE_NM) == false)
			db.execSQL(CREATE_TABLE_ALARM_QUERY);	
		
		if(ExistsTable(db, eSTUDYPROGRESS_TABLE_NM) == false)
			db.execSQL(CREATE_TABLE_STUDYPROGRESS);
		
		if(ExistsTable(db, eDOWN_TAKE_COURSE_TABLE_NM) == false)
			db.execSQL(CREATE_TABLE_DOWN_TAKE_COURSE);
		
		if(ExistsTable(db, eDOWN_STUDY_INDEX_TABLE_NM) == false)
			db.execSQL(CREATE_TABLE_DOWN_STUDY_INDEX);
		
		if(ExistsTable(db, eDOWN_TAKE_SANGSANG_TABLE_NM) == false)
			db.execSQL(CREATE_TABLE_DOWN_TAKE_SANGSANG);
		
		if(ExistsTable(db, eDOWN_STUDY_SANGSANG_TABLE_NM) == false)
			db.execSQL(CREATE_TABLE_DOWN_STUDY_SANGSANG);
		
		if(ExistsTable(db, eDOWNLOAD_CENTER_SETTINGS) == false)
			db.execSQL(CREATE_TABLE_DOWNLOAD_CENTER_SETTINGS_QUERY);
		
		if(ExistsTable(db, eAPP_EVENT_TABLE_NM) == false)
			db.execSQL(CREATE_TABLE_APP_EVENT);
		
		if(ExistsTable(db, eAPP_SETTINGS_TABLE_NM) == false)
			db.execSQL(CREATE_TABLE_APP_SETTINGS);
		
		if (!ExistsTable(db, TABLE_MLC_PROGRESS_SYNC_FAILED_LIST))
			db.execSQL(CREATE_TABLE_PROGRESS_SYNC_FAILED_LIST);
	}
	
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
	{
		onCreate(db);
		try
		{
			if(oldVersion < 11)
				db.execSQL("ALTER TABLE "+ eDOWN_STUDY_INDEX_TABLE_NM +" ADD 'study_sec' integer");
			
			if(oldVersion < 13)
				db.execSQL("ALTER TABLE "+ eACCESS_TABLE_NM +" ADD 'push_type' TEXT");
			
			if(oldVersion < 14)
				db.execSQL("ALTER TABLE "+ eDOWN_STUDY_INDEX_TABLE_NM +" ADD 'last_view_sec' integer");

			if (oldVersion < 16)
				db.execSQL(CREATE_TABLE_APP_SETTINGS);
			
			if (oldVersion < 17) {
				if (!ExistsTable(db, TABLE_MLC_PROGRESS_SYNC_FAILED_LIST))
					db.execSQL(CREATE_TABLE_PROGRESS_SYNC_FAILED_LIST);
			}
			
			if (oldVersion < 18) {
				db.execSQL("ALTER TABLE "+ eACCESS_TABLE_NM +" ADD 'external_memory' TEXT");
				studyIndexTableUpgrade(db, eDOWN_STUDY_INDEX_TABLE_NM);
				studySangSangTableUpgrade(db, eDOWN_STUDY_SANGSANG_TABLE_NM);
			}

			if(oldVersion < 19) {
				db.execSQL("ALTER TABLE "+ eDOWN_STUDY_SANGSANG_TABLE_NM +" ADD 'last_position_sec' integer");
			}

		}
		catch(Exception ex){}
	}
	
	private boolean ExistsTable(SQLiteDatabase db, String table_nm)
	{
		Cursor cursor 		= db.rawQuery("SELECT name FROM sqlite_master WHERE type = 'table' and name = '" + table_nm + "'", null);
		String strTableNm 	= "";
		
		if(cursor.moveToFirst())
			strTableNm = cursor.getString(0);
		
		return "".equals(strTableNm) == false;
	}
	
	private boolean studyIndexTableUpgrade(SQLiteDatabase db, String table_nm)
	{
		Cursor cursor = null;
		ArrayList<Bundle> bundles = new ArrayList<Bundle>();
		String downPath 	= Utilities.GetExternalDir();
		
		cursor = db.rawQuery("SELECT * FROM " + table_nm , null);
		
		if(cursor == null)
			return false;
		
		while(cursor.moveToNext()){
			Bundle bundleData = new Bundle();
			bundleData.putInt("take_course_seq", cursor.getInt(0));
			bundleData.putString("chapter_no", cursor.getString(1));
			bundleData.putInt("index_no", cursor.getInt(2));
			bundleData.putString("index_nm", cursor.getString(3));
			bundleData.putInt("max_mark_sec", cursor.getInt(4));
			bundleData.putInt("max_view_sec", cursor.getInt(5));
			bundleData.putInt("view_index", cursor.getInt(6));
			bundleData.putInt("study_sec", cursor.getInt(7));
			bundleData.putInt("last_view_sec", cursor.getInt(8));
			bundleData.putString("full_path", "");
			bundleData.putString("external_memory", "");
			bundles.add(bundleData);
		}

		cursor.close();
		db.execSQL("DROP TABLE IF EXISTS " + table_nm);
		
		if(ExistsTable(db, table_nm) == false)
			db.execSQL(CREATE_TABLE_DOWN_STUDY_INDEX);
		
		for(Bundle bundleData : bundles){
			ContentValues values = new ContentValues();
			
			values.put("take_course_seq", bundleData.getInt("take_course_seq"));
			values.put("chapter_no", bundleData.getString("chapter_no"));
			values.put("index_no", bundleData.getInt("index_no"));
			values.put("index_nm", bundleData.getString("index_nm"));
			values.put("max_mark_sec", bundleData.getInt("max_mark_sec"));
			values.put("max_view_sec", bundleData.getInt("max_view_sec"));
			values.put("view_index", bundleData.getInt("view_index"));
			values.put("study_sec", bundleData.getInt("study_sec"));
			values.put("last_view_sec", bundleData.getInt("last_view_sec"));
			values.put("full_path", String.format("%s%d_%s.mp4", downPath, bundleData.getInt("take_course_seq"), bundleData.getString("chapter_no")));
			values.put("external_memory", "0"); // 0:hunet_mlc, 1: 내부저장소, 2: 외부저장소
			
			db.insert(table_nm, null, values);
		}
		
		return true;
	}
	
	private boolean studySangSangTableUpgrade(SQLiteDatabase db, String table_nm)
	{
		Cursor cursor = null;
		ArrayList<Bundle> bundles = new ArrayList<Bundle>();
		String downPath 	= Utilities.GetExternalDir();
		
		cursor = db.rawQuery("SELECT * FROM " + table_nm , null);
		
		if(cursor == null)
			return false;
		
		while(cursor.moveToNext()){
			Bundle bundleData = new Bundle();
			bundleData.putInt("company_seq", cursor.getInt(0));
			bundleData.putInt("contract_no", cursor.getInt(1));
			bundleData.putString("user_id", cursor.getString(2));
			bundleData.putString("goods_id", cursor.getString(3));
			bundleData.putInt("contents_seq", cursor.getInt(4));
			bundleData.putString("contents_nm", cursor.getString(5));
			bundleData.putInt("view_sec", cursor.getInt(6));
			bundleData.putInt("view_sec_mobile", cursor.getInt(7));
			bundleData.putInt("last_view_sec", cursor.getInt(8));
			bundleData.putInt("view_no", cursor.getInt(9));
			bundleData.putInt("contents_view_sec", cursor.getInt(10));
			bundleData.putString("se_goods_id", cursor.getString(11));
			bundleData.putString("full_path", "");
			bundleData.putString("external_memory", "");
			bundles.add(bundleData);
		}

		cursor.close();
		db.execSQL("DROP TABLE IF EXISTS " + table_nm);
		
		if(ExistsTable(db, table_nm) == false)
			db.execSQL(CREATE_TABLE_DOWN_STUDY_SANGSANG);
		
		for(Bundle bundleData : bundles){
			ContentValues values = new ContentValues();
			
			values.put("company_seq", bundleData.getInt("company_seq"));
			values.put("contract_no", bundleData.getInt("contract_no"));
			values.put("user_id", bundleData.getString("user_id"));
			values.put("goods_id", bundleData.getString("goods_id"));
			values.put("contents_seq", bundleData.getInt("contents_seq"));
			values.put("contents_nm", bundleData.getString("contents_nm"));
			values.put("view_sec", bundleData.getInt("view_sec"));
			values.put("view_sec_mobile", bundleData.getInt("view_sec_mobile"));
			values.put("last_view_sec", bundleData.getInt("last_view_sec"));
			values.put("view_no", bundleData.getInt("view_no"));
			values.put("contents_view_sec", bundleData.getInt("contents_view_sec"));
			values.put("se_goods_id", bundleData.getString("se_goods_id"));
			values.put("full_path", String.format("%s%s_%d.mp4", downPath, bundleData.getString("user_id"), bundleData.getInt("contents_seq")));
			values.put("external_memory", "0"); // 0:hunet_mlc, 1: 내부저장소, 2: 외부저장소
			
			db.insert(table_nm, null, values);
		}
		
		return true;
	}

}

