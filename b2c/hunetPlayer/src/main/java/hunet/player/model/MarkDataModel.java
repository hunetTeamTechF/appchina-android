package hunet.player.model;

public class MarkDataModel
{
	public int 		index 		= 0;
	public int 		markNo 		= 0;
	public String 	markNm 		= "";
	public int 		markValue 	= 0;
	public String 	pptUrl 		= "";
	public int 		realFrameNo = 0;
	public String 	progressYn 	= "";
}
