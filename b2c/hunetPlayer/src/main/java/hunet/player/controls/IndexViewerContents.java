package hunet.player.controls;




import hunet.library.hunetplayer.R;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

public class IndexViewerContents extends LinearLayout {
	public TextView markValue;
	public TextView markNm;	
	public IndexViewerContents(Context context, AttributeSet attrs) 
	{
		super(context, attrs);
		
		LayoutInflater li = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		li.inflate(R.layout.library_player_index_viewer_contents, this, true);		
		markValue =(TextView)findViewById(R.id.markValue);		
		markNm =(TextView)findViewById(R.id.markNm);
	}
	public void SetView(String value, String name)
	{
		markValue.setText(value);
		markValue.setTextSize(18);
		markNm.setText(name);
		markNm.setTextSize(18);		
	}
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) 
	{
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);	
	}
	
	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b)
	{
		super.onLayout(changed, l, t, r, b);
	}
}
