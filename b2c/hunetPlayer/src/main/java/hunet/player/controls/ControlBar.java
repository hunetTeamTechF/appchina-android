package hunet.player.controls;

import hunet.controls.AutoSizeTextView;
import hunet.data.SQLiteManager;
import hunet.drm.models.DownloadCenterSettingsModel;
import hunet.library.Utilities;
import hunet.library.hunetplayer.R;
import hunet.player.interfaces.IControlBarEventListener;
import hunet.player.interfaces.ISpeedEventListener;
import hunet.player.interfaces.IVolumeEventListener;

import java.util.Timer;
import java.util.TimerTask;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

public class ControlBar extends LinearLayout implements IVolumeEventListener,
		ISpeedEventListener, android.view.View.OnClickListener,
		View.OnTouchListener, OnSeekBarChangeListener {
	private IControlBarEventListener eventListener = null;
	private boolean hasPpt = false;

	private LinearLayout leftPanel = null;
	private LinearLayout centerPanel = null;
	private LinearLayout volumeBarMain = null;
	private LinearLayout speedBarMain = null;
	private LinearLayout controlBarTop = null;
	private LinearLayout controlBarBottom = null;
	private LinearLayout topBar = null;
	private VolumeBar volumeBar = null;
	private SpeedBar speedBar = null;
	private ImageButton btnComplete;
	public AutoSizeTextView txtIndexNm;
	private ImageButton btnScreenRate;
	private LinearLayout mediaRateMain = null;
	private TextView txtTotalTime = null;
	private ImageButton btnIndexFull = null;
	private ImageButton btnPTFull = null;
	private TextView txtTimePosition = null;
	private ImageButton btnSound = null;
	private ImageButton btnMediaPlay = null;
	private ImageButton btnMediaBack = null;
	private ImageButton btnMediaForward = null;
	private ImageButton btnRepeatPlay = null;
	private AutoSizeTextView txtMediaRate = null;
	private SeekBar seekBar = null;
	private Timer timer = new Timer();
	private TimerTask timerTask = null;
	private long lastTouchTime = 0;
	private int repeatStatus = 0;
	private boolean isStarbucks = false;
	private boolean isSlifc = false;
	
	private final static int SLIFC_COMPANY_SEQ	= 17970;
	
	public ControlBar(Context context, AttributeSet attrs) {
		super(context, attrs);
		LayoutInflater li = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		li.inflate(R.layout.library_player_control_bar, this, true);
		isStarbucks ="kr.co.hunet.mobilelearningcenterforstartbucks".equals(context.getApplicationContext().getPackageName().toLowerCase());
		
		SQLiteManager m_SQLiteManager = new hunet.data.SQLiteManager(context);
		DownloadCenterSettingsModel model = m_SQLiteManager.GetDownloadCenterSettings();
		if(model.company_seq == SLIFC_COMPANY_SEQ)
			isSlifc = true;
		
		timer = new Timer();
		InflateControl();
		SetEvent();
		InitControl();
	}

	private void InflateControl() {
		leftPanel = (LinearLayout) findViewById(R.id.leftPanel);
		centerPanel = (LinearLayout) findViewById(R.id.centerPanel);
		btnSound = (ImageButton) findViewById(R.id.btnSound);
		btnMediaPlay = (ImageButton) findViewById(R.id.btnMediaPlay);
		btnMediaBack = (ImageButton) findViewById(R.id.btnMediaBack);
		btnMediaForward = (ImageButton) findViewById(R.id.btnMediaForward);
		btnIndexFull = (ImageButton) findViewById(R.id.btnIndexFull);
		btnPTFull = (ImageButton) findViewById(R.id.btnPTFull);
		txtTotalTime = (TextView) findViewById(R.id.txtTotalTime);
		txtTimePosition = (TextView) findViewById(R.id.txtTimePosition);
		volumeBarMain = (LinearLayout) findViewById(R.id.volumebar_main);
		volumeBar = (VolumeBar) findViewById(R.id.volumebar);
		speedBar = (SpeedBar) findViewById(R.id.speedbar);
		speedBarMain = (LinearLayout) findViewById(R.id.speedbar_main);
		controlBarTop = (LinearLayout) findViewById(R.id.controlbar_top);
		controlBarBottom = (LinearLayout) findViewById(R.id.controlbar_bottom);
		topBar = (LinearLayout) findViewById(R.id.topBar);
		seekBar = (SeekBar) findViewById(R.id.sbBar);
		txtMediaRate = (AutoSizeTextView) findViewById(R.id.txtMediaRate);
		mediaRateMain = (LinearLayout) findViewById(R.id.mediaRateMain);
		btnComplete = (ImageButton) findViewById(R.id.btnComplete);
		txtIndexNm = (AutoSizeTextView) findViewById(R.id.txtIndexNm);
		btnScreenRate = (ImageButton) findViewById(R.id.btnScreenRate);
		btnRepeatPlay = (ImageButton) findViewById(R.id.btnRepeatPlay);
	}

	private void SetEvent() {

		txtMediaRate.setOnClickListener(this);
		btnSound.setOnClickListener(this);
		btnMediaPlay.setOnClickListener(this);
		btnMediaBack.setOnClickListener(this);
		btnMediaForward.setOnClickListener(this);
		speedBar.setOnClickListener(this);
		speedBar.SetEventListener(this);
		btnIndexFull.setOnClickListener(this);
		btnPTFull.setOnClickListener(this);

		btnMediaPlay.setOnTouchListener(this);
		btnMediaBack.setOnTouchListener(this);
		btnMediaForward.setOnTouchListener(this);

		btnSound.setOnTouchListener(this);
		controlBarTop.setOnTouchListener(this);
		volumeBar.SetEventListener(this);
		seekBar.setOnSeekBarChangeListener(this);

		btnComplete.setOnClickListener(this);
		btnScreenRate.setOnClickListener(this);
		btnRepeatPlay.setOnClickListener(this);
	}

	private void InitControl() {
		btnMediaPlay.setTag("play");
		btnIndexFull.setTag("off");
		btnPTFull.setTag("off");
		btnScreenRate.setTag("rate");
		btnRepeatPlay.setTag("0");

		seekBar.setMax(0);
		controlBarBottom.setVisibility(View.GONE);
		topBar.setVisibility(View.GONE);
		if(isStarbucks || isSlifc)
			mediaRateMain.setVisibility(View.GONE);
	}

	private Runnable OnTouchClick = new Runnable() {
		public void run() {
			if (controlBarBottom.getVisibility() == View.VISIBLE)
				Show(false);
			else {
				Show(true);
				// StartTimer();
			}
		}
	};

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		volumeBar.onWindowFocusChanged(hasFocus);
		// m_SpeedBar.onWindowFocusChanged(hasFocus);
	}

	private void OnTimerEvent(Timer timer) {
		controlBarBottom.post(new Runnable() {
			public void run() {
				Hide();
			}
		});
	}

	public void SetEventListener(IControlBarEventListener a_EventListener) {
		eventListener = a_EventListener;
	}

	public void InitControlBar(boolean hasPpt, String title) {
		this.hasPpt = hasPpt;
		HideVolume();
		HideSpeed();
		txtTimePosition.setText("00:00");
		txtTotalTime.setText("00:00");
		if (title.length() < 28)// padding
		{
			for (int i = 28 - title.length(); i > 0; i--)
				title += "&nbsp";
		}

		// txtIndexNm.setText(Html.fromHtml(String.format("<b>%s</b>", title)));
		// txtIndexNm.setTextSize(18);
		seekBar.setProgress(0);
		SetPlay(true);

		if (hasPpt) {
			SetPptFull(false);
			btnIndexFull.setVisibility(View.VISIBLE);
			btnPTFull.setVisibility(View.VISIBLE);

		} else {
			leftPanel.setVisibility(View.GONE);
			centerPanel.setLayoutParams(new LinearLayout.LayoutParams(0,
					LayoutParams.FILL_PARENT, 74.501f));
		}
		// seekBar.getProgressDrawable().setBounds(bounds);
	}

	public void SetPptFull(boolean bIsFull) {
		if (hasPpt == false)
			return;

		if (bIsFull) {
			btnPTFull.setTag("on");
			btnPTFull.setBackgroundDrawable(getResources().getDrawable(
					R.drawable.l_btn_ppt_new));
		} else {
			btnPTFull.setTag("off");
			btnPTFull.setBackgroundDrawable(getResources().getDrawable(
					R.drawable.l_btn_ppt_new));
		}
	}
	
	public void HideSpeedLayout() {
		mediaRateMain.setVisibility(View.GONE);
	}
	
	public void ShowSpeedLayout() {
		if(!isStarbucks && !isSlifc)
			mediaRateMain.setVisibility(View.VISIBLE);
	}

	private void HideSpeed() {
		speedBar.setVisibility(View.INVISIBLE);
	}

	public void ShowSpeed() {
		Show();
		/*
		 * Rect tempRect = new Rect();
		 * txtMediaRate.getGlobalVisibleRect(tempRect);
		 * 
		 * LinearLayout.LayoutParams layoutParams = new
		 * LinearLayout.LayoutParams( LinearLayout.LayoutParams.FILL_PARENT,
		 * LinearLayout.LayoutParams.FILL_PARENT);
		 * layoutParams.setMargins(tempRect.left + 10, tempRect.top - 150, 0,
		 * 0); speedBarMain.setLayoutParams(layoutParams);
		 * speedBar.setVisibility(View.VISIBLE);
		 */
		Rect tempRect = new Rect();
		mediaRateMain.getGlobalVisibleRect(tempRect);

		int nAddLeft = (tempRect.width() - speedBar.getWidth()) / 2;

		speedBarMain.setPadding(tempRect.left + nAddLeft, tempRect.top
				- speedBar.getHeight(), 0, 0);
		speedBar.setVisibility(View.GONE);// 갤럭시노트대응
		speedBar.setVisibility(View.VISIBLE);
	}

	public void Pause() {
		SetPlay(false);
		if (eventListener != null)
			eventListener.OnClickPause(this);
	}

	public void Play() {
		SetPlay(true);
		if (eventListener != null)
			eventListener.OnClickPlay(this);
	}

	public void SetPlay(boolean bIsPlay) {
		if (bIsPlay) {
			btnMediaPlay.setTag("pause");
			btnMediaPlay.setBackgroundDrawable(getResources().getDrawable(
					R.drawable.l_btn_pause));
		} else {
			btnMediaPlay.setTag("play");
			btnMediaPlay.setBackgroundDrawable(getResources().getDrawable(
					R.drawable.l_btn_play));
		}
	}

	public void SetMediaRateText(float rate) {
		txtMediaRate.setText(String.format("倍数x%1.1f", rate));
	}

	public void SetTotalTimeText(String strText) {
		txtTotalTime.setText(strText);
	}

	public void SetTimeText(String strText) {
		txtTimePosition.setText(strText);
	}

	public void SetVolume(int nPercent) {
		if (nPercent > 100)
			nPercent = 100;
		else if (nPercent < 0)
			nPercent = 0;

		volumeBar.SetValue(nPercent);
	}

	public void ShowVolume() {
		Show();

		Rect tempRect = new Rect();
		btnSound.getGlobalVisibleRect(tempRect);

		int nAddLeft = (tempRect.width() - volumeBar.getWidth()) / 2;

		volumeBarMain.setPadding(tempRect.left + nAddLeft, tempRect.top
				- volumeBar.getHeight(), 0, 0);
		volumeBar.setVisibility(View.GONE);// 갤럭시노트대응
		volumeBar.setVisibility(View.VISIBLE);
	}

	public void HideVolume() {
		volumeBar.setVisibility(View.INVISIBLE);
	}

	public void SetProgress(int progress) {
		seekBar.setProgress(progress);
	}

	public int GetSeekBarMax() {
		return seekBar.getMax();
	}

	public void SetSeekBarMax(int max) {
		seekBar.setMax(max);
	}

	/*
	 * public void StartTimer() { StopTimer();
	 * 
	 * timerTask = new TimerTask() {
	 * 
	 * @Override public void run() { OnTimerEvent(timer); } };
	 * 
	 * timer.schedule(timerTask, 5000); }
	 * 
	 * public void StopTimer() { if (timerTask == null) return;
	 * 
	 * timerTask.cancel(); timerTask = null; }
	 */

	public void Show() {
		if (controlBarBottom.getVisibility() == View.VISIBLE)
			return;

		Animation ani = new TranslateAnimation(0, 0, 100, 0);
		ani.setDuration(500);
		controlBarBottom.setAnimation(ani);
		controlBarBottom.setVisibility(View.VISIBLE);
		Animation ani2 = new TranslateAnimation(0, 0, -10, 0);
		ani2.setDuration(500);
		topBar.setAnimation(ani2);
		topBar.setVisibility(View.VISIBLE);
	}

	public void Show(boolean show) {
		if (show)
			Show();
		else
			Hide();
	}

	public void Hide() {
		if (controlBarBottom.getVisibility() == View.GONE)
			return;

		Animation ani = new TranslateAnimation(0, 0, 0, 100);
		ani.setDuration(500);
		controlBarBottom.setAnimation(ani);
		controlBarBottom.setVisibility(View.GONE);
		volumeBar.setVisibility(View.INVISIBLE);
		speedBar.setVisibility(View.INVISIBLE);
		/*
		 * Animation ani2 = new TranslateAnimation(0, 0, 0, -50);
		 * ani2.setDuration(300); topBar.setAnimation(ani2);
		 */
		topBar.setVisibility(View.INVISIBLE);
	}

	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		super.onLayout(changed, l, t, r, b);
	}

	public void OnChangeVolume(VolumeBar v, int nValue) {
		if (eventListener != null)
			eventListener.OnChangeVolume(this, nValue);
	}

	public void onClick(View v) {
		if (R.id.btnSound == v.getId()) {
			if (eventListener != null
					&& eventListener.OnClickSound(this) == false)
				return;

			if (volumeBar.getVisibility() == View.INVISIBLE)
				ShowVolume();
			else
				HideVolume();
		} else if (R.id.txtMediaRate == v.getId()) {
			if (eventListener != null
					&& eventListener.OnClickSound(this) == false)
				return;

			if (speedBar.getVisibility() == View.INVISIBLE)
				ShowSpeed();
			else
				HideSpeed();
		} else if (R.id.btnMediaPlay == v.getId()) {
			if ("play".equals(btnMediaPlay.getTag().toString())) {
				if (eventListener != null
						&& eventListener.OnClickPlay(this) == false)
					return;

				btnMediaPlay.setTag("pause");
				btnMediaPlay.setBackgroundDrawable(getResources().getDrawable(
						R.drawable.l_btn_pause));
			} else {
				if (eventListener != null
						&& eventListener.OnClickPause(this) == false)
					return;

				btnMediaPlay.setTag("play");
				btnMediaPlay.setBackgroundDrawable(getResources().getDrawable(
						R.drawable.l_btn_play));
			}
		} else if (R.id.btnMediaBack == v.getId()) {

			if (eventListener != null
					&& eventListener.OnClickBack(this) == false)
				return;
		} else if (R.id.btnMediaForward == v.getId()) {

			if (eventListener != null
					&& eventListener.OnClickForward(this) == false)
				return;
//		} else if (R.id.btnMediaSpeedUp == v.getId()) {
//
//			if (eventListener != null
//					&& eventListener.OnClickSpeedUp(this) == false)
//				return;
//		} else if (R.id.btnMediaSpeedDown == v.getId()) {
//
//			if (eventListener != null
//					&& eventListener.OnClickSpeedDown(this) == false)
//				return;
		} else if (R.id.btnIndexFull == v.getId()) {
			if ("on".equals(btnIndexFull.getTag().toString())) {
				if (eventListener != null
						&& eventListener.OnClickIndexFull(this, false) == false)
					return;

				btnIndexFull.setTag("off");
				btnIndexFull.setBackgroundDrawable(getResources().getDrawable(
						R.drawable.l_btn_index));
			} else {
				if (eventListener != null
						&& eventListener.OnClickIndexFull(this, true) == false)
					return;

				btnIndexFull.setTag("on");
				btnIndexFull.setBackgroundDrawable(getResources().getDrawable(
						R.drawable.l_btn_index));

				SetPptFull(false);
			}
		} else if (R.id.btnPTFull == v.getId()) {
			if ("on".equals(btnPTFull.getTag().toString())) {
				if (eventListener != null
						&& eventListener.OnClickPptFull(this, false) == false)
					return;

				btnPTFull.setTag("off");
				btnPTFull.setBackgroundDrawable(getResources().getDrawable(
						R.drawable.l_btn_ppt_new));
			} else {
				if (eventListener != null
						&& eventListener.OnClickPptFull(this, true) == false)
					return;

				btnPTFull.setTag("on");
				btnPTFull.setBackgroundDrawable(getResources().getDrawable(
						R.drawable.l_btn_ppt_new));
			}
		} else if (R.id.btnComplete == v.getId()) {
			eventListener.OnClickComplete(this);
		} else if (R.id.btnScreenRate == v.getId()) {
			if ("rate".equals(btnScreenRate.getTag().toString())) {
				btnScreenRate.setTag("fit");
				btnScreenRate
						.setBackgroundResource(R.drawable.l_btn_top_bar_movie_rate);
				eventListener.OnClickScreenFit(true);
			} else {
				btnScreenRate.setTag("rate");
				btnScreenRate
						.setBackgroundResource(R.drawable.l_btn_top_bar_screen_fit);
				eventListener.OnClickScreenFit(false);
			}
		} else if (R.id.btnRepeatPlay == v.getId()) {
			if ("0".equals(btnRepeatPlay.getTag().toString())) {
				btnRepeatPlay.setTag("1");
				btnRepeatPlay.setBackgroundResource(R.drawable.l_auto_play1);
			} else if ("1".equals(btnRepeatPlay.getTag().toString())) {
				btnRepeatPlay.setTag("2");
				btnRepeatPlay.setBackgroundResource(R.drawable.l_auto_play2);
			} else if ("2".equals(btnRepeatPlay.getTag().toString())) {
				btnRepeatPlay.setTag("0");
				btnRepeatPlay.setBackgroundResource(R.drawable.l_auto_play0);
			}
			eventListener.OnClickRepeat(btnRepeatPlay.getTag().toString());
		}

	}

	public boolean onTouch(View v, MotionEvent event) {
		int nAction = event.getAction();
		int nId = v.getId();

		if (nId == R.id.controlbar_top) {
			long thisTime = System.currentTimeMillis();

			if (thisTime - lastTouchTime >= 250) {
				lastTouchTime = thisTime;
				this.postDelayed(OnTouchClick, 300);
				return false;
			}

			this.removeCallbacks(OnTouchClick);
			return false;
		}

		/*
		 * if (nAction == MotionEvent.ACTION_UP) StartTimer(); else if (nAction
		 * == MotionEvent.ACTION_DOWN) { Show(true); StopTimer(); }
		 */

		return false;
	}

	public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
		SetTimeText(Utilities.GetTimeString(progress * 1000, false));
	}

	public void onStartTrackingTouch(SeekBar seekBar) {
		// Show(true);
		// StopTimer();

		if (eventListener != null)
			eventListener.OnStartTrackingTouch(this, seekBar.getProgress());
	}

	public void onStopTrackingTouch(SeekBar seekBar) {
		// StartTimer();

		if (eventListener != null)
			eventListener.OnStopTrackingTouch(this, seekBar.getProgress());
	}

	@Override
	public boolean OnSpeedDown() {
		if (eventListener != null)
			eventListener.OnClickSpeedDown(this);
		return false;
	}

	@Override
	public boolean OnSpeedUp() {
		if (eventListener != null)
			eventListener.OnClickSpeedUp(this);
		return false;
	}

}