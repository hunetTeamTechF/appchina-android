package hunet.player.service;

import hunet.data.SQLiteManager;
import hunet.drm.models.StudyProgressModel;
import hunet.drm.models.StudyProgressSyncFailedModel;
import hunet.library.Utilities;
import hunet.net.AsyncHttpRequestData;
import hunet.net.HttpData;
import hunet.net.IAsyncHttpRequestData;

import java.util.Date;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

public class SyncProgressService extends Service implements Runnable, IAsyncHttpRequestData
{	
	private SQLiteManager sqlManager = null;
	private AsyncHttpRequestData asyncHttpReq = null;
	private Thread httpSender = null;
	private int state = 0;
	private final int RUNNING = 0;
	private final int SUSPENDED = 1;
	private final int STOPPED = 2;
	
	@Override
	public void onCreate()
	{
		super.onCreate();	
		sqlManager = new SQLiteManager(this);
		asyncHttpReq = new AsyncHttpRequestData();
		asyncHttpReq.SetEventListener(this);
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId)
	{	
		Log.d("HunetStudyProgress", "onStartCommand");
		if(httpSender == null)
		{
			httpSender = new Thread(this, "ajoumba_send_progress");
			httpSender.start();
		}
		
		setState(RUNNING);
		
		return super.onStartCommand(intent, flags, startId);
	}
	
	public synchronized void setState(int state)
	{
		this.state = state;
		
		if(state == RUNNING)
			notify();
		else
			httpSender.interrupt();
	}
	
	public synchronized boolean checkState()
	{
		while(state == SUSPENDED)
		{
			try	{
				wait();
			} catch(Exception e) { }
		}
		
		return state == STOPPED;
	}
	
	@Override
	public void run() 
	{		
		boolean bIsSend = true;
		
		while(true)
		{
			if(checkState())
				break;
			
			StudyProgressModel model = sqlManager.GetStudyProgress();
						
			if(model.study_progress_seq > 0)
			{				
				if(model.try_count > 0)
				{
					try 
					{
						Thread.sleep(5000);
					} catch (Exception e) {}
				}
				
				bIsSend = asyncHttpReq.Request(1, model.url, "", model);
			}
			else
				bIsSend = true;
			
			if(bIsSend)
				setState(SUSPENDED);
			else
			{
				try 
				{
					Thread.sleep(1000);
				} catch (Exception e) {}
			}
		}
	}
	
	@Override
	public IBinder onBind(Intent intent) {
	
		return null;
	}

	@Override
	public void onDestroy()
	{
		super.onDestroy();
		
		setState(STOPPED);
		httpSender = null;
	}

	@Override
	public void AsyncRequestDataResult(HttpData result) {
		String isSuccess = result.GetJsonValue("IsSuccess");
		StudyProgressModel model = (StudyProgressModel)result.tag;
		
		if("YES".equals(isSuccess)) {
			sqlManager.DeleteStudyProgress(model.study_progress_seq);
		} else {					
			if(model.try_count >= 8) {
				StudyProgressSyncFailedModel failedModel = new StudyProgressSyncFailedModel();
				failedModel.url = model.url;
				failedModel.params = "";
				failedModel.reg_date = Utilities.GetFormattedDateString(new Date(), "yyyy-MM-dd HH:mm:ss");
				sqlManager.setStudyProgressFailedInfo(failedModel);
				
				sqlManager.DeleteStudyProgress(model.study_progress_seq);
				Toast.makeText(this, "因网络状态不稳定，不能正常保存学习进度。建议您在舒畅的网络环境里学习1.", Toast.LENGTH_LONG).show();
			} else {
				sqlManager.UpdateStudyProgress(model.study_progress_seq);
			}
		}

		setState(RUNNING);
	}
}
