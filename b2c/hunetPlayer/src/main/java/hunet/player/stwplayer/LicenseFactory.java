package hunet.player.stwplayer;

import java.util.Hashtable;

public class LicenseFactory {

	static Hashtable<String, String> license = new Hashtable<String, String>();
	static {
			SetLicense();	
	}
	private static void SetLicense() {
		license.put("kr.co.hunet.*", "NONE");
	}
	public static String GetLicense(String packageName)
	{
		if(license.containsKey(packageName))
			return license.get(packageName);
		
		return "";
	}

}
