package hunet.drm.download;

import hunet.drm.models.StudyIndexModel;
import hunet.drm.models.StudySangSangModel;
import hunet.library.hunetplayer.R;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.view.View;

public class CourseIndexLayout extends LinearLayout implements View.OnClickListener
{
	private ICourseIndexEventListener eventListener;
	public TextView 			index;
	private TextView 			indexNm;
	private StudyIndexModel downStudyIndexModel;
	private StudySangSangModel downStudySangSangModel;
	private ImageButton			btnPlay;
	private ImageButton 		btnSync;
	private ImageButton 		btnRemove;
	
	public CourseIndexLayout(Context context)
	{
		super(context);
		InitControl(context);
	}
	
	public CourseIndexLayout(Context context, AttributeSet attrs) 
	{
		super(context, attrs);
		InitControl(context);
	}

	private void InitControl(Context context)
	{
		LayoutInflater li = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		li.inflate(R.layout.library_course_index_layout, this, true);
		
		index 		= (TextView)findViewById(R.id.tvIndex);
		indexNm 	= (TextView)findViewById(R.id.tvIndexNm);
		btnPlay		= (ImageButton)findViewById(R.id.ibtnPlay);
		btnSync		= (ImageButton)findViewById(R.id.ibtnSync);
		btnRemove	= (ImageButton)findViewById(R.id.ibtnRemove);
		
		btnPlay.setOnClickListener(this);
		btnSync.setOnClickListener(this);
		btnRemove.setOnClickListener(this);
	}
	
	public void SetEventListener(ICourseIndexEventListener a_EventListener)
	{
		eventListener = a_EventListener;
	}
	
	public void SetDownIndexInfo(StudyIndexModel model)
	{
		downStudyIndexModel = model;

		index.setText(model.view_index + "课时");
		indexNm.setText(model.index_nm);
	}
	
	public void SetDownSangSangInfo(StudySangSangModel model)
	{
		downStudySangSangModel = model;

		//m_tvIndex.setText(model.view_index + "차시");
		indexNm.setText(model.contents_nm);
	}
	
	public StudyIndexModel GetDownStudyIndexModel()
	{
		return downStudyIndexModel;
	}
	
	public StudySangSangModel GetDownStudySangSangModel()
	{
		return downStudySangSangModel;
	}
	
	public void SetIndex(String strText) 	{ index.setText(strText); }
	public void SetIndexNm(String strText) 	{ indexNm.setText(strText); }

	@Override
	public void onClick(View view) 
	{
		if(eventListener == null)
			return;
		
		if(view.getId() ==R.id.ibtnPlay )
			eventListener.OnClickPlay(this);
		else if(view.getId() ==R.id.ibtnSync )
			eventListener.OnClickSync(this);
		else if(view.getId() ==R.id.ibtnRemove )
			eventListener.OnClickRemove(this);		
	}
}
