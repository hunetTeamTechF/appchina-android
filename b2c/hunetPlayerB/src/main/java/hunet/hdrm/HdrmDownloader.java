package hunet.hdrm;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map.Entry;

public class HdrmDownloader {
	private static final String TAG = "HdrmDownloader";
	private Callback callback = null;
	private String dataDir = null;
	private Context context = null;
	private Activity activity = null;

	private String downloadUrl;
	private String downloadFilePath;
	public boolean isDownloadCanceled;
	public boolean isContinueDownload;

	private HashMap<String, String> hashMap;
	private AsyncDownloadTask task = null;
	public static final int BYTES_BUFFER_SIZE = 256 * 1024;
	private long currentUrlFileSize = 0;
	private long currentLocalFileSize = 0;

	private BroadcastReceiver completeReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			Toast.makeText(context, "다운로드가 완료되었습니다.", Toast.LENGTH_SHORT)
					.show();
		}

	};

	public HdrmDownloader(Activity activity)
			throws HdrmDownloader.CreateFailedException {
		this.context = activity.getApplicationContext();
		this.activity = activity;
		this.dataDir = context.getFilesDir().getAbsolutePath();
		hashMap = new HashMap<String, String>();
		isDownloadCanceled = false;
		isContinueDownload = false;

		Log.i("HdrmDownloader", ">>>>> " + this.dataDir);
	}

	 public void dispose()
	 {
	 	activity.unregisterReceiver(completeReceiver);
	 }

	public void setCallback(Callback callback) {
		this.callback = callback;
	}

	public void setMovieUrl(String movieUrl) {
		if ((movieUrl == null) || (movieUrl.equals(""))) {
			return;
		}
		if (!movieUrl.startsWith("http://")) {
			return;
		}
		downloadUrl = movieUrl.replace("http://m2.", "http://w.").replace("http://m.", "http://w.");
	}

	public String movieUrl() {
		return downloadUrl;
	}

	public void setFileLocation(String location) {
		if ((location == null) || (location.equals(""))) {
			return;
		}
		downloadFilePath = location;
	}

	public String fileLocation() {
		return downloadFilePath;
	}

	public void setDrmInfo(String startDate, String endDate, String drmKey) {
		if ((startDate == null) || (endDate == null) || (drmKey == null)) {
			return;
		}
	}

	public void prepareDownload() {
		Log.i(TAG, "prepareDownload");
		hashMap.put(downloadUrl, downloadFilePath);

		startDownload();
	}

	public void startDownload() {
		Log.i(TAG, "startDownload");

		if (isOnline()) {
			task = new AsyncDownloadTask();
			task.execute();
		}
		else {
			Toast.makeText(activity.getBaseContext(), "인터넷 연결을 확인해 주십시오.", Toast.LENGTH_LONG).show();
		}
	}

	private void continueDownload() {
		Log.i(TAG, "continueDownload");
		isContinueDownload = true;
		new android.os.Handler().postDelayed(
				new Runnable() {
					public void run() {
						if (isContinueDownload) {
							if (!isDownloadCanceled && isOnline() && sizeOnServer() > 0 && sizeOnServer() > sizeDownloaded()) {
								startDownload();
							}
							continueDownload();
						}
					}
				}, 3000);
	}

	public boolean isOnline() { // network 연결 상태 확인
		try {

			ConnectivityManager conMan = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);

			NetworkInfo.State wifi = conMan.getNetworkInfo(1).getState(); // wifi
			if (wifi == NetworkInfo.State.CONNECTED || wifi == NetworkInfo.State.CONNECTING) {
				return true;
			}

			NetworkInfo.State mobile = conMan.getNetworkInfo(0).getState(); // mobile ConnectivityManager.TYPE_MOBILE
			if (mobile == NetworkInfo.State.CONNECTED || mobile == NetworkInfo.State.CONNECTING) {
				return true;
			}

		} catch (NullPointerException e) {
			return false;
		}

		return false;
	}

	public void cancelDownload() {
		isDownloadCanceled = true;
		task.cancel(true);
		canceledCallback(1);
//		cancelPrepareCallback(1);
	}

	public long getCurrentUrlFileSize() {
		return currentUrlFileSize;
	}

	public void setCurrentUrlFileSize(long currentUrlFileSize) {
		this.currentUrlFileSize = currentUrlFileSize;
	}

	public long getCurrentLocalFileSize() {
		return currentLocalFileSize;
	}

	public void setCurrentLocalFileSize(long currentLocalFileSize) {
		this.currentLocalFileSize = currentLocalFileSize;
	}

	public long sizeOnServer() {
		return currentUrlFileSize;
	}

	public long sizeDownloaded() {
		return currentLocalFileSize;
	}

	public long sizeRemained() {
		return currentUrlFileSize - currentLocalFileSize;
	}

	public static boolean deleteDRM(String fileLocation, String dataDir) {
		if ((fileLocation == null) || (dataDir == null)
				|| (fileLocation.equals(""))) {
			return false;
		}
		// To Do Impl...
		return true;
	}

	public static boolean updateDRM(String fileLocation, String dataDir,
			String newFileLocation, String startDate, String endDate,
			String drmKey) {
		if ((fileLocation == null) || (dataDir == null)
				|| (newFileLocation == null) || (startDate == null)
				|| (endDate == null) || (drmKey == null)) {
			return false;
		}
		if ((fileLocation.equals("")) || (newFileLocation.equals(""))) {
			return false;
		}
		// To Do Impl...
		return true;
	}

	public static boolean updateDRM(String fileLocation, String dataDir,
			String newFileLocation, String startDate, String endDate,
			String drmKey, boolean bForce, boolean bDeleteExisting) {
		if ((fileLocation == null) || (dataDir == null)
				|| (newFileLocation == null) || (startDate == null)
				|| (endDate == null) || (drmKey == null)) {
			return false;
		}
		if ((fileLocation.equals("")) || (newFileLocation.equals(""))) {
			return false;
		}
		// To Do Impl...
		return true;
	}

	public static String getDRMKey(String fileLocation, String dataDir) {
		if ((fileLocation == null) || (dataDir == null)
				|| (fileLocation.equals(""))) {
			return null;
		}
		// return nativeGetDRMKey(fileLocation, dataDir);
		return "";
	}

	private void startedCallback(long sizeOnServer) {
		isContinueDownload = false;
		if (this.callback != null) {
			this.callback.started(sizeOnServer);
		}
	}

	private void completedCallback(long sizeDownloaded) {
		if (this.callback != null) {
			this.callback.completed(sizeDownloaded);
		}
	}

	private void cancelPrepareCallback(int failedReason) {
		if (this.callback != null) {
			this.callback.cancelPrepare(failedReason);
		}
	}

	private void canceledCallback(int failedReason) {
		if (this.callback != null) {
			this.callback.canceled(failedReason);
		}
	}

	private void cannotDownloadCallback(int failedReason) {
		if (this.callback != null) {
			this.callback.cannotDownload(failedReason);
		}
		cancelDownload();
		Toast.makeText(activity.getBaseContext(), "다운로드 할 수 없는 파일입니다.", Toast.LENGTH_LONG).show();
	}

	private void progressCallback(long sizeOnServer, long sizeDownloaded,
			long sizeRemained) {
		if (this.callback != null) {
			this.callback.progress(sizeOnServer, sizeDownloaded, sizeRemained);
		}
	}

	public static abstract interface Callback {
		public abstract void started(long paramLong);

		public abstract void completed(long paramLong);

		public abstract void cancelPrepare(int paramInt);

		public abstract void canceled(int paramInt);

		public abstract void cannotDownload(int paramInt);

		public abstract void progress(long paramLong1, long paramLong2,
				long paramLong3);
	}

	public class CreateFailedException extends Exception {
		private static final long serialVersionUID = -1893022740644560398L;

		public CreateFailedException() {
		}
	}

	protected HashMap<String, String> getTargetFiles() {
		return hashMap;
	}

	protected int getConnectTimeout() {
		return 3000;
	}

	protected int getReadTimeout() {
		return 3000;
	}

	protected RemoteViews getProgressView(int currentNumFile,
			int totalNumFiles, long currentReceivedBytes, int totalNumBytes) {
		// RemoteViews contentView = new RemoteViews(getPackageName(),
		// R.layout.progress);
		// contentView.setImageViewResource(R.id.image, R.drawable.icon);
		// contentView.setTextViewText(R.id.text,
		// String.format("Progress (%d / %d)", currentNumFile, totalNumFiles));
		// contentView.setProgressBar(R.id.progress, 100, 100 *
		// currentReceivedBytes / totalNumBytes, false);
		// return contentView;

		Log.i(TAG, String.format("Progress (%d / %d)", currentNumFile,
				totalNumFiles));

		return null;
	}

	protected void showNotification(String ticker, String title, String content) {
		// Notification notification = new Notification(getNotificationIcon(),
		// ticker, System.currentTimeMillis());
		// PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
		// new Intent(this, getIntentForLatestInfo()),
		// Intent.FLAG_ACTIVITY_CLEAR_TOP);
		// notification.setLatestEventInfo(getApplicationContext(), title,
		// content, contentIntent);
		// notification.flags = getNotificationFlag();
		//
		// notificationManager.notify(SERVICE_ID, notification);

		Log.i(TAG, String.format("showNotification : %s", title));
	}

	protected void showNotification(RemoteViews remoteView, String ticker) {
	}

	protected String getStringByteSize(long size) {
		if (size > 1024 * 1024) // mega
		{
			return String.format("%.1f MB", size / (float) (1024 * 1024));
		} else if (size > 1024) // kilo
		{
			return String.format("%.1f KB", size / 1024.0f);
		} else {
			return String.format("%d B");
		}
	}

	private class AsyncDownloadTask extends AsyncTask<Void, Void, Void> {
		private int successCount;
		private int numTotalFiles;
		private HashMap<String, String> targetFiles = null;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			successCount = 0;

			targetFiles = getTargetFiles();
			numTotalFiles = targetFiles.size();
			isDownloadCanceled = false;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.os.AsyncTask#doInBackground(Params[])
		 */
		@Override
		protected Void doInBackground(Void... params) {
			String remoteFilepath, localFilepath;
			for (Entry<String, String> entry : targetFiles.entrySet()) {
				if (isDownloadCanceled) {
					return null;
				}

				remoteFilepath = entry.getKey();
				localFilepath = entry.getValue();

				Log.v(TAG, "downloading: '" + remoteFilepath + "' => '" + localFilepath + "'");

				doDownload(remoteFilepath, localFilepath);
			}
			return null;
		}

		protected void doDownload(String remoteFilepath, String localFilepath) {
			if (isDownloadCanceled) {
				return;
			}

			long totalBytesRead = 0;
			long remorteFileSize = 0;
			long localFileSize = 0;
			RandomAccessFile output = null;
			BufferedInputStream bis = null;

			try {
				URL url = new URL(remoteFilepath);
				URLConnection connection = url.openConnection();
				remorteFileSize = url.openConnection().getContentLength();
				// 다운로드 받을 서버파일 용량
				setCurrentUrlFileSize(remorteFileSize);

				// 다운로드 시작 콜백
				startedCallback(remorteFileSize);

				long fromStart = HdrmManager.getHdrmContinueDownloadPosition(localFilepath);
				if (fromStart >= remorteFileSize) {
					return;
				}
				else if (fromStart > 0) {
					connection.setRequestProperty("Range", "bytes=" + String.valueOf(fromStart) + '-');
					connection.connect();
				}

				output = new RandomAccessFile(localFilepath, "rw");
				localFileSize = output.length();
				output.seek(localFileSize);

				int loopCount = 0;
				if (remorteFileSize > 0) {
					connection.setConnectTimeout(getConnectTimeout());
					connection.setReadTimeout(getReadTimeout());

					bis = new BufferedInputStream(connection.getInputStream());

					totalBytesRead = localFileSize;

					byte[] bytes = new byte[BYTES_BUFFER_SIZE];
					int readBytesLength = 0;
					while (!isDownloadCanceled && (readBytesLength = bis.read(bytes)) != -1) {
						if (totalBytesRead == 0) {
							HdrmManager.writeCurrentHdrmEncryptedBytes(bytes, readBytesLength, output, context, remorteFileSize);
						} else {
							output.write(bytes, 0, readBytesLength);
						}
						totalBytesRead += readBytesLength;

						// 다운로드 받은 로컬파일 용량
						setCurrentLocalFileSize(totalBytesRead);

						if (loopCount++ % 20 == 0) {
							// 다운로드 진행율 콜백
							progressCallback(remorteFileSize, totalBytesRead, 0);
						}
					}

					successCount++;
				} else {
					Log.e(TAG, "file size unknown for remote file: " + remoteFilepath);
					cannotDownloadCallback(1);
				}
			} catch (Exception e) {
				Log.e(TAG, "doDownload Error : " + e.toString());
			}
			finally {
				try {
					if (output != null) {
						output.close();
					}
					if (bis != null) {
						bis.close();
					}
				} catch (IOException ex) {
					ex.printStackTrace();
				}
			}
		}

		@Override
		protected void onCancelled() {
			super.onCancelled();
			Log.i(TAG, "download task cancelled");
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);

			if (!isDownloadCanceled && sizeOnServer() > 0 && sizeOnServer() > sizeDownloaded()) {
				continueDownload();
				Log.v(TAG, "다운로드 실패");
			}
			else {
				// 다운로드 완료 콜백
				completedCallback(sizeDownloaded());
				Log.v(TAG, "다운로드 성공");
			}

			Log.v(TAG, "download task finished");
		}
	}
}
