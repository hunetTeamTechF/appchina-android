package hunet.domain;

public class DomainAddress {
	public static boolean IS_STAGING				= false;

	public static String getUrlMLC() {
		return IS_STAGING ? "http://test.mlc.hunet.co.kr" : "http://mlc.hunet.co.kr";
	}
	
	public static String getUrlApps() {
		return IS_STAGING ? "http://test.apps.hunet.co.kr" : "http://apps.hunet.co.kr";
	}
}
