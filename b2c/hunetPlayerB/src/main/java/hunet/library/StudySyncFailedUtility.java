package hunet.library;

import hunet.data.SQLiteManager;
import hunet.drm.models.StudyProgressSyncFailedModel;
import hunet.net.AsyncHttpRequestData;
import hunet.net.HttpData;
import hunet.net.IAsyncHttpRequestData;
import android.content.Context;

public class StudySyncFailedUtility {

	private SQLiteManager sqlManager;
	
	/**
	 * 진도 데이터를 전송하지 못한 목록을 재전송 합니다.
	 */
	public void doSendFailedProgressData(Context context) {
		sqlManager = new SQLiteManager(context);
		
		AsyncHttpRequestData request = new AsyncHttpRequestData();
		request.SetEventListener(new IAsyncHttpRequestData() {
			
			@Override
			public void AsyncRequestDataResult(HttpData result) {
				String isSuccess = result.GetJsonValue("IsSuccess");
				int resultCode = Integer.parseInt(result.GetJsonValue("ResultCode", "0"));
				StudyProgressSyncFailedModel model = (StudyProgressSyncFailedModel) result.tag;

				if("YES".equals(isSuccess) || resultCode == 1)
					sqlManager.removeStudyProgressFailedInfo(model.seq);
			}
		});
		
		// 동기화 하지 못한 진도가 있을 경우 일괄 전송 하며
		// 실패시 다음 앱 실행 시 재시도 함
		for (StudyProgressSyncFailedModel model : sqlManager.getStudyProgressFailedInfoList())
			request.Request(0, model.url, model.params, model);
	}
}
