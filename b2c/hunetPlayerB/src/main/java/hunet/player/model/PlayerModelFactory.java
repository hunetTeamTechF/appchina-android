package hunet.player.model;

import android.app.Activity;

public class PlayerModelFactory 
{
	private static PlayerModelFactory instance = new PlayerModelFactory();
	public static PlayerModelFactory Instance(){
		return instance;		
	}
	
	public PlayerModelBase Get(Activity activity,String type)
	{
		PlayerModelBase result;
		if ("local".equals(type))					// 온라인과정 다운로드 학습
			result = new PlayerOGModel(activity);
		else if ("localSangSang".equals(type))		// 상상마루 다운로드 학습
			result = new PlayerOSModel(activity);
		else if ("sangsangWeb".equals(type))		// 상상마루 스트리밍
			result = new PlayerWSModel(activity);
		else if ("innumWeb".equals(type))			// 인문학당 스트리밍
			result = new PlayerWIModel(activity);
		else if ("htmlWeb".equals(type))			// HTML 모듈화과정 스트리밍
			result = new PlayerWHModel(activity);
		else if ("edubankWeb".equals(type))			// 학점은행 스트리밍
			result = new PlayerWEModel(activity);
		else										// 일반 스트리밍 (온라인 스트리밍)
			result = new PlayerWGModel(activity);
		return result;		
	}
}
