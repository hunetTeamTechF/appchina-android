package hunet.player.stwplayer;

import java.util.Hashtable;

public class LicenseFactory {

	static Hashtable<String, String> license = new Hashtable<String, String>();
	static {
			SetLicense();	
	}
	private static void SetLicense() {
		license.put("kr.co.hunet.MobileLearningCenterForPoscomba", "4be099c9bb2148edb60032a87fb447ef");
		license.put("kr.co.hunet.MobileLearningCenterForBusiness", "37a4a54614d54859abc78f53622bf31e");
		license.put("kr.co.hunet.MobileLearningCenterForStartbucks", "9a00d7db2cc9481fa784246e936ff8f0");
		license.put("kr.co.hunet.MobileLearningCenterForKDBLife", "548a5b9994cb4d39965b2ee6fffc2007");
		license.put("kr.co.hunet.mlearning.android", "c6569c2fd1f04ca7823a36064de5ebe1");
		license.put("kr.co.hunet.HunetEduBank", "d79e9ebc55514debadfcb1884a76411f");
		license.put("kr.co.hunet.HunetChina", "7fad71176fa74a24a8a377c9f86a983d");
		license.put("kr.co.hunet.MobileLearningCenterForSisul", "c409947650f04b14b1393990075ba5b3");		
		license.put("kr.co.hunet.MobileLearningCenterForMospa2014", "e2aa12a3ff7849a793f13647e618a331");
		license.put("kr.co.hunet.MobileLearningCenterForPolice2014", "93e558939a8b40288d0c9d820f9a2468");
		license.put("kr.co.hunet.MobileLearningCenterForPresident2014", "8a02ae48681346e3a0cbf034a4f58a42");
		license.put("kr.co.hunet.MobileLearningCenterForMospa1", "8b6333a483304f13b0ea8d09324a8dcb");
		license.put("kr.co.hunet.MobileLearningCenterForMospaFamily", "6c33c19f6c8245609a709b019f13a83e");
		license.put("kr.co.hunet.MobileLearningCenterForPcaLife", "f409e02e376b474886788258e5202466");
		license.put("kr.co.hunet.MobileLearningCenterForEkb", "a16d24a3548e4b90b10f5ce37869c3d5");
		license.put("kr.co.hunet.MobileLearningCenterForEmart", "a9aae3f4173a4f6ca9acbfd96874a496");
		license.put("kr.co.hunet.MobileLearningCenterForPoscoEnc", "2ab90f3f90a64e2a81ebc49872cdd0f6");
		license.put("kr.co.hunet.MobileLearningCenterForKyoboLife", "7e57390a53f24f3494d9a8f6de40070c");
		license.put("kr.co.hunet.MobileLearningCenterForAllianzlife", "6617a25edc0148c080ef65c38ad68424");
		license.put("kr.co.hunet.MobileLearningCenterForCmc", "a20bc40d332a4777b5e1153b48f5320d");
//		license.put("kr.co.hunet.MobileLearningCenterForCjHellovision", "69704d21e8204be6a8cee2a4bd20b123");
		license.put("kr.co.hunet.MobileLearningCenterForDunkinSchool", "381c0241c26e4c859bdc6df1ecfc5ad3");
		license.put("kr.co.hunet.MobileLearningCenterForBaskinSchool", "00caafe2168c4b6ab7a06fbce9ba47e6");
		license.put("kr.co.hunet.MobileLearningCenterForKBStar", "ba5086a30e17425da258eebb826bfa08");
		license.put("kr.co.hunet.MobileLearningCenterForSejung", "a789f226166e4addb0809d39a1305de5");
		license.put("kr.co.hunet.MobileLearningCenterForGlobalkepco", "58a7c54a212a4a788621665d70bf83be");
		license.put("kr.co.hunet.MobileLearningCenterForKepcofamily", "3d7f2c40b9bc4463a6e05c625a8ca62a");
		license.put("kr.co.hunet.MobileLearningCenterForMyangel", "04fe1332085f458b99c014b883d6a935");
		license.put("kr.co.hunet.MobileLearningCenterForSangSangStart", "35579637467e490ea0f79063ead11c64");
//		license.put("kr.co.hunet.MobileLearningCenterForKorail", "3e6d1b06f079462fafb9b798ee91eef4");
		license.put("kr.co.hunet.MobileLearningCenterForKhnp", "7606aa7f0f8d4c15aa001036c78a7fea");
		license.put("kr.co.hunet.MobileLearningCenterForSmartCj", "4c6b07c6b2cd4bb195d722fe89e71256");
		license.put("kr.co.hunet.MobileLearningCenterForCoway", "467782f455e44b4f8828b9684402d9f4");
		license.put("kr.co.hunet.MobileLearningCenterForHyundaiDepartmentPartner", "77f383eab1f14ad8be5c6356d25246da");
		license.put("kr.co.hunet.MobileLearningCenterForEhyundaiPartner", "135cd2436e9a477ba962f96f2030485d");
		license.put("kr.co.hunet.MobileLearningCenterForRenk", "4d9f08a43b114ac9a8b6806a695241fc");
		license.put("kr.co.hunet.MobileLearningCenterForKiki", "52cdbafff0c3435ea9780ddd104f2c1a");
		license.put("kr.co.hunet.MobileLearningCenterForCjStaff", "c1eef4dadc634444b444e214e483f763");
		license.put("kr.co.hunet.MobileLearningCenterForHiteJinro", "6d864df365904b028fd7c1f04ec376bb");
		license.put("kr.co.hunet.MobileLearningCenterForElcakr", "897e0cbf4fb64b0989beafe88f1cc15d");
		license.put("kr.co.hunet.MobileLearningCenterForOlympus", "ea4e28d22eb44a078aed5397802cb2a4");
		license.put("kr.co.hunet.MobileLearningCenterForDaesang", "d9d86bd18b254c5aaea463fedb8b56d4");
		license.put("kr.co.hunet.MobileLearningCenterForScourt", "ab6bac1da1c64ae683090c7bc5fb266d");
		license.put("hunet.hunetplayer", "2d5e769c16b34727a12a1f9a94f8edc0");
		license.put("kr.co.hunet.MobileLearningCenterForKogasTech", "b605c8c9c1e445b3acefa298f135aee9");
		license.put("kr.co.hunet.MobileLearningCenterForKotra", "59e47a05ffc4480c87608f7777c1a527");
		license.put("kr.co.hunet.MobileLearningCenterForSknservice", "8dc895c9fd624713a8d7ee4162c4d2dd");
		license.put("kr.co.hunet.MobileLearningCenterForCKD", "76081c6aa5f74352b2999e934b76758c");
		license.put("kr.co.hunet.MobileLearningCenterForNHLife", "edbb741b1baf4ad48098a13c12d66e4d");
		license.put("kr.co.hunet.MobileLearningCenterForKsan", "642c8599ce724d888e4cc4442005486b");
		license.put("kr.co.hunet.MobileLearningCenterForBrk", "34258a8eb36040f390d4fcba52f4e911");
		license.put("kr.co.hunet.MobileLearningCenterForKBCard", "71cc2736a5f54c319a1ca5262a4c2384");
		license.put("kr.co.hunet.MobileLearningCenterForSeoulmilk", "7cfa76394a19401cb8611d5333d91807");
		license.put("kr.co.hunet.MobileLearningCenterForGG", "f8e55b15f3c742eba9f09781166f1a32");
		license.put("kr.co.hunet.MobileLearningCenterForSlifc", "a681fdd7413a4311a61c3ecc7bb2545d");
		license.put("kr.co.hunet.MobileLearningCenterForSejungF", "a2eed31b213c48d2a9f5371f13bbd9e7");
		license.put("kr.co.hunet.MobileLearningCenterForSisulFamily", "af2aae7ed0e84981869956239d787209");
		license.put("kr.co.hunet.MobileLearningCenterForNepes", "e9e2adcf17c04d1cbe0d3f29eec0a65d");
		license.put("kr.co.hunet.MobileLearningCenterForNps", "166fa8c6fb31424ab31602672bcd9f0d");
		license.put("kr.co.hunet.MobileLearningCenterForSnuh", "b888147e9e6441678e97c696599f1943");
		license.put("kr.co.hunet.MobileLearningCenterForKorad", "7a73a5d210d64257b9276844347c6918");
		license.put("kr.co.hunet.MobileLearningCenterForSamsungLife", "69af305203154306aa6e38eb212b594e");
		license.put("kr.co.hunet.MobileLearningCenterForTkgl", "5410f2feb976406389bfb73e5a5791c1");
		license.put("kr.co.hunet.MobileLearningCenterForPantech", "72eb6a320e024a0c978bcef23aaf9ac7");
		license.put("kr.co.hunet.ktng", "b863d9855c3c439e829a53258b1cb82e");
		license.put("kr.co.hunet.kgc", "22bf35e2ba304b6e8a41f82f7b8750e4");
		license.put("cn.com.HunetChina", "ecf28edd5ba9433a8ad59e606b62627d");
	}
	public static String GetLicense(String packageName)
	{
		if(license.containsKey(packageName))
			return license.get(packageName);
		
		return "";
	}

}
