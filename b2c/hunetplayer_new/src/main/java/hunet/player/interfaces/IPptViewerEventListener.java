package hunet.player.interfaces;

import hunet.player.controls.VolumeBar;
import android.view.MotionEvent;
import android.view.View;

public interface IPptViewerEventListener 
{
	public boolean OnPptLeftMove();
	public boolean OnPptRightMove();
	public boolean OnPptPlay();
	public boolean OnPptClose();
}
