package hunet.player.model;

import java.util.ArrayList;



import hunet.data.*;
import hunet.player.interfaces.IPlayerStudyDataEventListener;
import hunet.player.model.restriction.ItemType;
import hunet.player.model.restriction.RestrictionEntity;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

public abstract class PlayerModelBase 
{
	protected IPlayerStudyDataEventListener m_EventListener = null;
	protected SQLiteManager	m_SqlManager;
	protected Intent				m_StudyProgressService;
	protected Activity				m_ParentActivity;	
	public MarkDataModel  m_CurMarkData;	
	public ArrayList<MarkDataModel> m_arMarkList;	
	public String title = "";

	public static ArrayList<RestrictionEntity> restrictionEntities;

	public PlayerModelBase(Activity activity)
	{
		Log.d("PlayerStudyData","PlayerStudyData");
		m_ParentActivity		= activity;
		m_SqlManager			= new SQLiteManager(activity);
		m_StudyProgressService 	= new Intent(activity, hunet.player.service.SyncProgressService.class);
	}
	
	public void SetEventListener(IPlayerStudyDataEventListener listner)
	{
		this.m_EventListener = listner;		
	}

	protected RestrictionEntity getRestrictionEntity(String key, ItemType itemType) {
		RestrictionEntity entity = null;

		if (PlayerModelBase.restrictionEntities != null) {
			for(RestrictionEntity e : PlayerModelBase.restrictionEntities) {
				if (e.itemType != itemType)
					continue;

				if (e.key.equalsIgnoreCase(key)) {
					entity = e;
					break;
				}
			}
		}

		return entity;
	}

	public abstract boolean CheckProgressRestriction();
	
	public abstract void SaveProgress(int lastViewSec, int studySec);
	
	public abstract int GetLastViewSec();
	
	public abstract String GetPlayerUrl();
	
	public abstract boolean CheckSendProgress(int lastViewSec, int studySec);
	
	public abstract boolean HasPpt();
	
	public abstract boolean ShowSpeedControl();

	public abstract boolean ShowCompletedButton();

	public abstract boolean CheckOverlapStudyRestriction();

	public abstract boolean IsOverlapStudy();
}
