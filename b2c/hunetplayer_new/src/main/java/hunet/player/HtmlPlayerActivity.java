package hunet.player;



import hunet.library.hunetplayer.R;
import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.Uri;
import android.os.Bundle;
import android.widget.VideoView;

public class HtmlPlayerActivity extends Activity  {
	private VideoView vv;
	private String url;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);	
		this.setContentView(R.layout.library_html_player);
	}
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if(vv == null){
			vv = (VideoView)findViewById(R.id.vv);
			Intent intent = getIntent();
		    Bundle bundle = intent.getExtras();
		    url = bundle.getString("url");		    
		    
			vv.setOnCompletionListener(new OnCompletionListener() {
				
				@Override
				public void onCompletion(MediaPlayer mp) {
					// TODO Auto-generated method stub
					Intent result = new Intent();
					result.putExtra("completed", true);
					setResult(Activity.RESULT_OK,result);
				}
			});
			vv.setVideoURI(Uri.parse(url));
			vv.start();
		}
	}
	

	
	
}
