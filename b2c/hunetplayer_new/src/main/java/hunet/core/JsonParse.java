package hunet.core;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JsonParse 
{
	private JSONObject jsonObj = null;
		
	public void SetData(String strData)
	{	
		try {
			jsonObj = new JSONObject(strData);
		} catch (JSONException e) { e.printStackTrace(); }
	}
	
	public String GetJsonValue(String strKey)
	{
		if(jsonObj == null)
			return "";
		
		String strValue = "";
				
		try 
		{
			strValue = jsonObj.getString(strKey);
		} 
		catch (JSONException e) {  }
		
		return strValue;
	}
	
	public String GetJsonValue(String strKey, String strDefault)
	{
		if(jsonObj == null)
			return strDefault;
		
		String strValue = "";
				
		try 
		{
			strValue = jsonObj.getString(strKey);
		} 
		catch (JSONException e) { e.printStackTrace(); }
		
		if("".equals(strValue))
			strValue = strDefault;
		
		return strValue;
	}
	
	public String GetJsonValue(JSONObject json, String strKey, String strDefault)
	{
		if(json == null)
			return strDefault;
		
		String strValue = "";
		
		try 
		{
			strValue = json.getString(strKey);
		} 
		catch (JSONException e) { e.printStackTrace(); }
		
		if("".equals(strValue))
			strValue = strDefault;
		
		return strValue;
	}
	
	public JSONArray GetJsonArray(String strKey)
	{
		if(jsonObj == null)
			return null;
		
		JSONArray jsonArray = null;
		
		try 
		{
			jsonArray = jsonObj.getJSONArray(strKey);
		} 
		catch (JSONException e) { return null; }
		
		return jsonArray;
	}
}
