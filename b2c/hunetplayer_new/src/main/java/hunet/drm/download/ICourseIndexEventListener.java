package hunet.drm.download;

public interface ICourseIndexEventListener 
{
	public void OnClickPlay(CourseIndexLayout object);
	public void OnClickSync(CourseIndexLayout object);
	public void OnClickRemove(CourseIndexLayout object);
}
