package hunet.drm.download;

import hunet.data.SQLiteManager;
import hunet.drm.models.DownloadCenterSettingsModel;
import hunet.drm.models.TakeCourseModel;
import hunet.drm.models.TakeSangSangModel;
import hunet.library.MemoryStatus;
import hunet.library.Utilities;
import hunet.library.hunetplayer.R;
import hunet.library.hunetplayer.R.string;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

public class DownloadCourseActivity extends Activity implements OnClickListener {
	private SQLiteManager sqlManager;
	private ImageButton btnPre;
	private LinearLayout llCourseList;
	private TextView tvMemoryStatus;
	private MemoryStatus memoryStatus;	
	private LinearLayout tabLayout;
	private TextView txtLeftTab;
	private TextView txtRightTab;
	private int selectedTabIndex = 0;
	private DownloadCenterSettingsModel model;
	
	private final static int SEOULMILK_COMPANY_SEQ		= 6928;
	private final static int SAMSUNGLIFE_COMPANY_SEQ	= 314;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.library_download_course_activity);

		Utilities.changeTitlebarColor(this, R.id.download_course_fl, R.id.header_text_color);
		Init();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		Refresh();
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}

	protected void Init()
	{
		sqlManager = new SQLiteManager(this);
		model = sqlManager.GetDownloadCenterSettings();
		btnPre = (ImageButton) findViewById(R.id.btnPre);
		llCourseList = (LinearLayout) findViewById(R.id.llCourseList);
		tvMemoryStatus = (TextView) findViewById(R.id.tvMemoryStatus);
		memoryStatus = new MemoryStatus();

		tabLayout = (LinearLayout) findViewById(R.id.tab);
		txtLeftTab = (TextView) findViewById(R.id.txtLeftTab);
		txtRightTab = (TextView) findViewById(R.id.txtRightTab);

		txtLeftTab.setSelected(true);
		txtLeftTab.setText(model.tab1_text != null && model.tab1_text.length() >0 ? model.tab1_text : getString(string.title_download_center_tab_left));
		txtRightTab.setSelected(false);
		txtRightTab.setText(model.tab2_text != null && model.tab2_text.length() >0 ? model.tab2_text : getString(string.title_download_center_tab_right));
		btnPre.setOnClickListener(this);

		/*
		 * model.only_imagine_yn 프로퍼티 값
		 * Y : 상상마루 전용
		 * N : 온라인 전용
		 * B : 둘다 사용
		 */
		
		if("B".equalsIgnoreCase(model.only_imagine_yn)){
			txtLeftTab.setOnClickListener(this);
			txtRightTab.setOnClickListener(this);
			
			selectedTabIndex = 0;
		}
		else{
			txtLeftTab.setVisibility(View.GONE);
			txtRightTab.setVisibility(View.GONE);

			if("N".equalsIgnoreCase(model.only_imagine_yn))		
				selectedTabIndex = 0;
			
			if("Y".equalsIgnoreCase(model.only_imagine_yn))
				selectedTabIndex = 1;
			
			tabLayout.setVisibility(View.GONE);
		}

		if(model.company_seq == SEOULMILK_COMPANY_SEQ || model.company_seq == SAMSUNGLIFE_COMPANY_SEQ) {
			txtLeftTab.setVisibility(View.GONE);
			txtRightTab.setVisibility(View.GONE);
			tabLayout.setVisibility(View.GONE);
			selectedTabIndex = 1;
		}

	}

	public void BindCourse() {
		llCourseList.removeAllViews();

		ArrayList<TakeCourseModel> modelList = sqlManager.GetDownTakeCourseDataList();
		int nLength = modelList.size();
		for (int nIndex = 0; nIndex < nLength; nIndex++) {
			CourseLayout courseLayout = new CourseLayout(this);
			TakeCourseModel model = modelList.get(nIndex);
			int nIndexCount = sqlManager.GetDownStudyIndexDataCount(model.take_course_seq);

			if (nIndexCount > 0)
				courseLayout.setOnClickListener(this);

			courseLayout.SetDownTakeCourseInfo(model, nIndexCount);

			llCourseList.addView(courseLayout);
		}
	}

	public void BindSangSang() {
		llCourseList.removeAllViews();
		ArrayList<TakeSangSangModel> sangSangModelList = sqlManager.GetDownTakeSangSangDataList();
		int snagSangLength = sangSangModelList.size();
		for (int nIndex = 0; nIndex < snagSangLength; nIndex++) {
			CourseLayout courseLayout = new CourseLayout(this);
			courseLayout.type = "sangsang";
			TakeSangSangModel model = sangSangModelList.get(nIndex);
			int nIndexCount = sqlManager.GetDownStudySangSangDataCount(model.goods_id);

			if (nIndexCount > 0)
				courseLayout.setOnClickListener(this);

			courseLayout.SetDownTakeSangSangInfo(model, nIndexCount);

			llCourseList.addView(courseLayout);
		}
	}

	protected void Refresh() {
		long externalTotalMemory = memoryStatus.getTotalExternalMemorySize();
		long externalAvailableMemory = memoryStatus
				.getAvailableExternalMemorySize();

		tvMemoryStatus.setText(String.format("총 공간 : %s, 남은 용량 : %s",
				memoryStatus.getFileSizeFormat(externalTotalMemory),
				memoryStatus.getFileSizeFormat(externalAvailableMemory)));

		if(selectedTabIndex == 0)
			BindCourse();
		else
			BindSangSang();
	}

	@Override
	public void onClick(View view) {
		if (view instanceof CourseLayout) {
			CourseLayout courseLayout = (CourseLayout) view;

			if(courseLayout.type == "sangsang") {
				Intent intent = new Intent(DownloadCourseActivity.this, DownloadSangSangContentsActivity.class);
				intent.putExtra("goodsId", courseLayout.getDownTakeSangSangModel().goods_id);
				startActivity(intent);
			} else {
				Intent intent = new Intent(DownloadCourseActivity.this, DownloadCourseIndexActivity.class);
				intent.putExtra("take_course_seq", courseLayout.getDownTakeCourseModel().take_course_seq);
				startActivity(intent);
			}

			return;
		}

		if (view.getId() == R.id.btnPre) {
			finish();
		} else if(view.getId() == R.id.txtLeftTab) {
			txtLeftTab.setSelected(true);
			txtRightTab.setSelected(false);
			selectedTabIndex = 0;
			BindCourse();			
		} else if(view.getId() == R.id.txtRightTab) {
			txtLeftTab.setSelected(false);
			txtRightTab.setSelected(true);
			selectedTabIndex = 1;
			BindSangSang();
		}
	}
}
