package hunet.drm.download;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import hunet.data.SQLiteManager;
import hunet.domain.DomainAddress;
import hunet.drm.models.StudyIndexModel;
import hunet.drm.models.TakeCourseModel;
import hunet.library.DownloadStudySyncUtility;
import hunet.library.NetworkUtility;
import hunet.library.Utilities;
import hunet.library.hunetplayer.R;
import hunet.net.AsyncHttpRequestData;
import hunet.net.HttpData;
import hunet.net.IAsyncHttpRequestData;

public class DownloadCourseIndexActivity extends Activity implements
        OnClickListener
        , ICourseIndexEventListener
        , IAsyncHttpRequestData {
    private static String userId = "";
    private ProgressDialog m_LoadingDialog = null;
    //	private String externalDir = Environment.getExternalStorageDirectory() + "/hunet_mlc/";
    private AsyncHttpRequestData m_AsyncReqData;
    private SQLiteManager m_SqlManager;
    private ImageButton m_btnPre;
    private LinearLayout m_llCourseIndexList;
    private TextView m_tvCourseTitle;
    private TextView m_tvCourseDescription;
    private int takeCourseSeq;

    private StudyIndexModel selectedModel;
    private TakeCourseModel downTakeCourseModel;
    private boolean bDownloadAfterPlayerStart = false;
    private boolean isPlay = true;
    private String studyDate = "";

    private final int eSYNC_PLAYER_BEFORE_START = 1;
    private final int eSYNC_PLAYER_ENDED = 2;
    private final int eSYNC_PROGRESS = 3;

    protected NetworkUtility networkUtil;
    private static Activity currentActivity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.library_download_course_index_activity);

        Utilities.changeTitlebarColor(this, R.id.download_course_index_fl, R.id.header_text_color);

        m_btnPre = (ImageButton) findViewById(R.id.btnPre);
        m_llCourseIndexList = (LinearLayout) findViewById(R.id.llCourseIndexList);
        m_tvCourseTitle = (TextView) findViewById(R.id.tvCourseTitle);
        m_tvCourseDescription = (TextView) findViewById(R.id.tvCourseDescription);
        m_SqlManager = new SQLiteManager(this);
        selectedModel = new StudyIndexModel();
        m_AsyncReqData = new AsyncHttpRequestData();

        m_btnPre.setOnClickListener(this);
        m_AsyncReqData.SetEventListener(this);
        //currentAct = this;

        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            takeCourseSeq = bundle.getInt("take_course_seq", 0);
            if (userId == "")
                userId = bundle.getString("userId");
        }

        downTakeCourseModel = m_SqlManager.GetDownTakeCourseData(takeCourseSeq);
        currentActivity = this;
        networkUtil = new NetworkUtility(this);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    private long GetRemainStudyDate(String studyEndDate) {
        String[] arDate = studyEndDate.split("-");

        if (arDate.length != 3)
            return 0;

        int year = Integer.parseInt(arDate[0]);
        int month = Integer.parseInt(arDate[1]) - 1;
        int day = Integer.parseInt(arDate[2]);

        Calendar curCal = Calendar.getInstance();
        Calendar studyEndCal = Calendar.getInstance();
        studyEndCal.set(year, month, day);

        long remainDay = ((studyEndCal.getTime().getTime() - curCal.getTime().getTime()) / 86400000); // 1000 * 60 * 60 * 24

        return remainDay;
    }

    private void Refresh() {
        m_tvCourseTitle.setText(downTakeCourseModel.course_nm);

        long day = GetRemainStudyDate(downTakeCourseModel.study_end_date);
        String strText = String.format("&middot; 학습기간 : %s까지&nbsp;%s"
                , downTakeCourseModel.study_end_date, day <= 0 ? "<font color=#ee0000>(학습 기간 종료)</font>" : "<font color=#b37e4d>(" + day + "일 남음)</font>");

        m_tvCourseDescription.setText(Html.fromHtml(strText));

        int nLength = m_llCourseIndexList.getChildCount();

        for (int nIndex = nLength - 1; nIndex >= 0; nIndex--) {
            CourseIndexLayout courseIndexLayout = (CourseIndexLayout) m_llCourseIndexList.getChildAt(nIndex);
            courseIndexLayout.SetEventListener(null);
            m_llCourseIndexList.removeViewAt(nIndex);
        }

        TakeCourseModel takeCourseModel = m_SqlManager.GetDownTakeCourseData(takeCourseSeq);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String strCurTime = sdf.format(new Date());
        studyDate = takeCourseModel == null ? "" : takeCourseModel.study_end_date;

        if ("".equals(studyDate) || strCurTime.compareTo(takeCourseModel.study_end_date) >= 0)
            isPlay = false;

        ArrayList<StudyIndexModel> modelList = m_SqlManager.GetDownStudyIndexDataList(takeCourseSeq);
        nLength = modelList.size();

        if (!Utilities.getExternalMemoryCheck(getApplicationContext())) {
            int nIndex = 0;
            int nSize = modelList.size();
            if (nSize > 0) {
                while (true) {
                    if (nIndex > (nSize - 1)) {
                        break;
                    }
                    StudyIndexModel model = modelList.get(nIndex);
                    if (TextUtils.equals(model.external_memory, "2")) {
                        modelList.remove(nIndex);
                        nSize--;
                        continue;
                    }
                    nIndex++;

                }
                nLength = modelList.size();
            }
        }

        for (int nIndex = 0; nIndex < nLength; nIndex++) {
            CourseIndexLayout courseIndexLayout = new CourseIndexLayout(this);
            StudyIndexModel model = modelList.get(nIndex);

            if (selectedModel.take_course_seq == model.take_course_seq && selectedModel.chapter_no == model.chapter_no)
                selectedModel = model;

            courseIndexLayout.SetDownIndexInfo(model);
            courseIndexLayout.SetEventListener(this);
            m_llCourseIndexList.addView(courseIndexLayout);
        }

        if (nLength == 0) {
            Toast.makeText(this, "차시 목록이 존재하지 않습니다.", Toast.LENGTH_LONG).show();
            DoBack();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        networkUtil.setNetworkChangeEventListener(new NetworkUtility.NetworkChangeEventListener() {

            @Override
            public void onChangeNetworkStatus(int status, int prevStatus) {

                if (prevStatus == NetworkUtility.NETWORK_STATUS_OFFLINE) {
                    if (status != NetworkUtility.NETWORK_STATUS_OFFLINE) {
                        // 오프라인에서 전환되는 경우
                        DownloadStudySyncUtility downsyncUtil = new DownloadStudySyncUtility();
                        downsyncUtil.doSendFailedProgressData(currentActivity);
                    }
                }
            }
        });

        networkUtil.startMonitoring();

//		AccessModel accessModel = m_SqlManager.GetAccessData();
//		if(TextUtils.equals(accessModel.external_memory, "1")){
//			if(Utilities.getExternalMemoryCheck(getApplicationContext())){
//				accessModel.external_memory = "1";
//			}else{
//				accessModel.external_memory = "0";
//			}
//		}
//		m_SqlManager.SetAccessData(accessModel);

        Refresh();

        if (bDownloadAfterPlayerStart == false)
            return;

        bDownloadAfterPlayerStart = false;
        SendProgressSync(eSYNC_PLAYER_ENDED, selectedModel);
    }

    @Override
    protected void onPause() {
        networkUtil.stopMonitoring();
        super.onPause();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                DoBack();
                break;
        }

        return super.onKeyDown(keyCode, event);
    }

    private void SendProgressSync(int eSendType, StudyIndexModel model) {
        StudyIndexModel studyIndexModel = null;

        if (TextUtils.isEmpty(model.external_memory)) {
            studyIndexModel = m_SqlManager.GetDownStudyIndexData(model.take_course_seq, model.chapter_no);
        } else {
            studyIndexModel = m_SqlManager.GetDownStudyIndexData(model.take_course_seq, model.chapter_no, model.external_memory);
        }

        String strReqUrl = DomainAddress.getUrlApps() + "/Hunet_Player/Android.aspx";
        String strReqParam = String.format("action=ProgressSyncNew&courseCd=%s&takeCourseSeq=%d&chapterNo=%s&studySec=%d&deviceNm=android&regId=%s&lastStudySec=%d"
                , downTakeCourseModel.course_cd, model.take_course_seq
                , model.chapter_no, studyIndexModel.study_sec, userId,
                //studyIndexModel.max_view_sec
                (studyIndexModel.max_view_sec > studyIndexModel.last_view_sec ? studyIndexModel.max_view_sec : studyIndexModel.last_view_sec)
        );

        m_AsyncReqData.Request(eSendType, strReqUrl, strReqParam, null);
    }

    private void UpdateDownStudyIndexData(int take_course_seq, String chapter_no, int max_view_sec, int study_sec, String external_memory) {
        StudyIndexModel studyIndexModel = new StudyIndexModel();
        studyIndexModel.take_course_seq = take_course_seq;
        studyIndexModel.chapter_no = chapter_no;
        studyIndexModel.max_view_sec = max_view_sec;
        studyIndexModel.study_sec = study_sec;
        studyIndexModel.external_memory = external_memory;

        m_SqlManager.SetDownStudyIndexData(studyIndexModel);
    }

    private void DoBack() {
        finish();
    }

    @Override
    public void onClick(View view) {
        Intent intent = null;

        if (view.getId() == R.id.btnPre)
            DoBack();

        if (intent == null)
            return;

        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
        finish();
    }

    @Override
    public void OnClickPlay(CourseIndexLayout object) {
        StudyIndexModel model = object.GetDownStudyIndexModel();

        if (isPlay == false) {
            Toast.makeText(this, "학습 기간이 종료되어 학습을 할 수 없습니다. 삭제 후 다시 다운 받아주세요.", Toast.LENGTH_LONG).show();
            return;
        }

//		String 	downFileName = "";
//		if(model.external_memory.equalsIgnoreCase("0")){
//			String externalDir = Environment.getExternalStorageDirectory() + "/hunet_mlc/";
//			downFileName = String.format("%s%d_%s.mp4", externalDir, model.take_course_seq, model.chapter_no);
//		}else{
//			downFileName = Utilities.getDownloadPullPath(getApplicationContext(), 
//							String.valueOf(model.take_course_seq), model.chapter_no);
//		}

        String downFileName = model.full_path;
        File file = new File(downFileName);

        if (file.exists() == false) {
            Toast.makeText(this, "파일이 존재 하지 않습니다. 확인 후 다시 시도해 주세요.", Toast.LENGTH_LONG).show();
            return;
        }

        selectedModel.take_course_seq = model.take_course_seq;
        selectedModel.chapter_no = model.chapter_no;
        selectedModel.index_no = model.index_no;
        selectedModel.index_nm = model.index_nm;
        selectedModel.max_mark_sec = model.max_mark_sec;
        selectedModel.max_view_sec = model.max_view_sec;
        selectedModel.view_index = model.view_index;
        selectedModel.full_path = model.full_path;
        selectedModel.external_memory = model.external_memory;

        ShowDialog(this, "정보 구성중입니다.\n잠시만 기다려 주세요.", false);
        SendProgressSync(eSYNC_PLAYER_BEFORE_START, model);
    }

    @Override
    public void OnClickSync(CourseIndexLayout object) {
        selectedModel = object.GetDownStudyIndexModel();

        new AlertDialog.Builder(this).setTitle("동기화 확인").setMessage(getString(R.string.download_course_sync_confirm_message, selectedModel.view_index, selectedModel.index_nm))
                .setPositiveButton("확인", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ShowDialog(DownloadCourseIndexActivity.this, "동기화 진행중 입니다.\n\n잠시만 기다려주세요.", false);
                        SendProgressSync(eSYNC_PROGRESS, selectedModel);
                    }
                })
                .setNegativeButton("취소", null).show();
    }

    public void ShowDialog(Context context, String msg, boolean cancelable) {
        if (m_LoadingDialog != null)
            return;

        m_LoadingDialog = new ProgressDialog(context);
        m_LoadingDialog.setMessage("".equals(msg) ? "로딩하는 중입니다..." : msg);
        m_LoadingDialog.setIndeterminate(true);
        m_LoadingDialog.setCancelable(cancelable);
        m_LoadingDialog.show();
    }

    public void HideDialog() {
        if (m_LoadingDialog != null) {
            try {
                m_LoadingDialog.dismiss();
                m_LoadingDialog = null;
            } catch (Exception e) {
            }
        }
    }

    @Override
    public void OnClickRemove(CourseIndexLayout object) {
        final StudyIndexModel model = object.GetDownStudyIndexModel();

        new AlertDialog.Builder(this).setTitle("삭제 확인").setMessage(String.format("%d차시 %s\n동영상을 삭제 하시겠습니까?", model.view_index, model.index_nm))
                .setPositiveButton("삭제", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
//	    			String 	strFilePath = "";
//	    			if(model.external_memory.equalsIgnoreCase("0")){
//	    				String externalDir = Environment.getExternalStorageDirectory() + "/hunet_mlc/";
//	    				strFilePath = String.format("%s%d_%s.mp4", externalDir, model.take_course_seq, model.chapter_no);
//	    			}else{
//	    				strFilePath = Utilities.getDownloadPullPath(getApplicationContext(), 
//	    								String.valueOf(model.take_course_seq), model.chapter_no);
//	    			}
//	    			
                            String strFilePath = model.full_path;

                            File file = new File(strFilePath);
                            if (file.exists())
                                file.delete();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        m_SqlManager.DeleteDownStudyIndexData(model.take_course_seq, model.chapter_no, model.external_memory);
                        int nCount = m_SqlManager.GetDownStudyIndexDataCount(model.take_course_seq);

                        Toast.makeText(DownloadCourseIndexActivity.this, "삭제가 완료 되었습니다.", Toast.LENGTH_LONG).show();

                        if (nCount > 0) {
                            Refresh();
                            return;
                        }

                        m_SqlManager.DeleteDownTakeCourseData(model.take_course_seq);
                        DoBack();
                    }
                })
                .setNegativeButton("취소", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                }).show();
    }

    @Override
    public void AsyncRequestDataResult(HttpData result) {

        if ("YES".equals(result.GetJsonValue("IsSuccess"))) {
            UpdateDownStudyIndexData(selectedModel.take_course_seq, selectedModel.chapter_no
                    , Integer.parseInt(result.GetJsonValue("max_study_sec", "0")), 0, selectedModel.external_memory);
        }

        switch (result.id) {
            case eSYNC_PROGRESS: {
                HideDialog();
                if ("YES".equals(result.GetJsonValue("IsSuccess")))
                    Toast.makeText(this, "동기화가 완료 되었습니다.", Toast.LENGTH_LONG).show();
                else
                    Toast.makeText(this, "동기화가 실패 하였습니다.\n잠시 후 다시 시도해 주세요.", Toast.LENGTH_LONG).show();
            }
            break;
            case eSYNC_PLAYER_BEFORE_START: {
                bDownloadAfterPlayerStart = true;
                HideDialog();

                Intent intent = new Intent(this, hunet.player.stwplayer.HunetStwPlayerActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                intent.putExtra("playerType", "local");
                intent.putExtra("take_course_seq", selectedModel.take_course_seq);
                intent.putExtra("chapter_no", selectedModel.chapter_no);
                intent.putExtra("indexNm", selectedModel.index_nm);
                intent.putExtra("max_mark_sec", selectedModel.max_mark_sec);
                intent.putExtra("full_path", selectedModel.full_path);
                intent.putExtra("external_memory", selectedModel.external_memory);

                startActivity(intent);
            }
            break;
            case eSYNC_PLAYER_ENDED: {

            }
            break;
        }
    }

}
