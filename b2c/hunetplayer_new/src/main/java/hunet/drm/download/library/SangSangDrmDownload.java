
package hunet.drm.download.library;

import hunet.data.SQLiteManager;
import hunet.domain.DomainAddress;
import hunet.drm.models.AccessModel;
import hunet.drm.models.StudyIndexModel;
import hunet.drm.models.StudySangSangModel;
import hunet.drm.models.TakeSangSangModel;
import hunet.library.MemoryStatus;
import hunet.library.PlayerMessageUtility;
import hunet.library.Utilities;
import hunet.library.hunetplayer.R;
import hunet.net.AsyncHttpRequestData;
import hunet.net.HttpData;
import hunet.net.IAsyncHttpRequestData;
import hunet.player.stwplayer.LicenseFactory;

import java.io.File;

import net.stway.sniperplayer.SniperDownloader;
import net.stway.sniperplayer.SniperDownloader.CreateFailedException;
import net.stway.sniperplayer.SniperPlayerUtil;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.WebView;
import android.widget.Toast;

public class SangSangDrmDownload implements IAsyncHttpRequestData {

	protected static final String LOG_TAG = "FileDown";
	private final String libraryKey;
	private SQLiteManager sqlManager;
	private TakeSangSangModel takeSangSangModel;
	private StudySangSangModel studySangSangModel;
	private SniperDownloader stwDownloader;
	private ProgressDialog progressDialog;
	private ProgressDialog loadingDialog = null;
	private AsyncHttpRequestData asyncReqData;
	private final int downloadInfo = 1; // 다운로드 정보 가져 오기
	private final int downloadCompleted = 2; // 다운로드 정보 저장하기
	private String goodsId = "";
	private String SeGoodsId = "";
	private int contentsSeq = 0;
	private int contractNo = 0;
	private Activity activity;
	private String userId = "";
	private String companySeq = "0";
	private WebView wv;
	private boolean isSizeChecked = false;

	public SangSangDrmDownload(Activity activity, WebView wv, String userId,
			String companySeq) {
		// TODO Auto-generated constructor stub
		String packageName = activity.getApplicationContext().getPackageName();
		libraryKey = LicenseFactory.GetLicense(packageName);
		this.activity = activity;
		this.wv = wv;
		sqlManager = new SQLiteManager(activity.getApplicationContext());
		takeSangSangModel = new TakeSangSangModel();
		studySangSangModel = new StudySangSangModel();
		asyncReqData = new AsyncHttpRequestData();
		asyncReqData.SetEventListener(this);
		this.userId = userId;
		this.companySeq = companySeq;
	}

	public void ShowDialog(String msg, boolean cancelable) {
		if (loadingDialog != null)
			return;

		loadingDialog = new ProgressDialog(activity);
		loadingDialog.setMessage("".equals(msg) ? "로딩하는 중입니다..." : msg);
		loadingDialog.setIndeterminate(true);
		loadingDialog.setCancelable(cancelable);
		loadingDialog.show();
	}

	public void HideDialog() {
		if (loadingDialog != null) {
			try {
				loadingDialog.dismiss();
				loadingDialog = null;
			} catch (Exception e) {
			}
		}
	}

	private void PlaySangSang(final String url) {
		String[] temp = url.replace("sangsangplayer://", "").split("/");
		final Intent intent = new Intent(activity, hunet.player.stwplayer.HunetStwPlayerActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
		intent.putExtra("contractNo", Integer.parseInt(temp[0]));
		intent.putExtra("contentsSeq", Integer.parseInt(temp[1]));
		intent.putExtra("goodsId", temp[2]);
		intent.putExtra("userId", userId);
		intent.putExtra("companySeq", companySeq);
		intent.putExtra("playerType", "sangsangWeb");
		activity.startActivity(intent);

	}

	public void ConfirmDownload(final String url) {
		int network = PlayerMessageUtility.getNetworkSettings(activity);
		String message = PlayerMessageUtility.getDownloadMessage(activity, network);

		switch (network) {
		case PlayerMessageUtility.NETWORK_WIFI:
			DownloadStep1(url);
			break;
		case PlayerMessageUtility.NETWORK_OFFLINE:
			new AlertDialog.Builder(this.activity)
			.setTitle("알림")
			.setMessage(message)
			.setPositiveButton("확인", null).show();
			break;
		case PlayerMessageUtility.NETWORK_ALARM_OFF_MOVIE_OFF:
			new AlertDialog.Builder(this.activity)
			.setTitle("알림")
			.setMessage(message)
			.setPositiveButton("확인", null).show();
			break;
		case PlayerMessageUtility.NETWORK_ALARM_OFF_MOVIE_ON:
			DownloadStep1(url);
			break;
		case PlayerMessageUtility.NETWORK_ALARM_ON_MOVIE_OFF:
			new AlertDialog.Builder(this.activity)
			.setTitle("알림")
			.setMessage(message)
			.setPositiveButton("확인", null).show();
			break;
		case PlayerMessageUtility.NETWORK_ALARM_ON_MOVIE_ON:
			new AlertDialog.Builder(this.activity)
			.setTitle("알림")
			.setMessage(message)
			.setPositiveButton("확인", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					DownloadStep1(url);
				}
			}).show();
			break;
		}
		
	}

	public void DownloadPlay(String url) {
		String[] temp = url.replace("sangsangdownloadplay://", "").split("/");

		int contentsSeq = 0;
		String goodsId = "";
		int contractNo = 0;
		String companySeq = "0";

		try {
			contractNo = Integer.parseInt(temp[0]);
			contentsSeq = Integer.parseInt(temp[1]);
			goodsId = temp[2];
			companySeq = this.companySeq;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		StudySangSangModel model = sqlManager.GetDownStudySangSangData(goodsId, contentsSeq);
		
		if(!TextUtils.equals(model.external_memory, "0")){
			AccessModel accessModel = sqlManager.GetAccessData();
			if("1".equals(accessModel.external_memory)){ // 외부 메모리 사용
				if(Utilities.getExternalMemoryCheck(activity.getApplicationContext())){
					model = sqlManager.GetDownStudySangSangData(goodsId, contentsSeq, "2");
				}else{
					activity.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							Toast.makeText(activity.getApplicationContext(), "이동식 메모리가 제거 되었습니다..", Toast.LENGTH_LONG).show();
						}
					});
					model = sqlManager.GetDownStudySangSangData(goodsId, contentsSeq, "1");
				}
			}else{
				model = sqlManager.GetDownStudySangSangData(goodsId, contentsSeq, "1");
			}
		}

		if ("".equals(model.goods_id)) {
			ConfirmDownload("sangsangdownload://"
					+ url.replace("sangsangdownloadplay://", ""));
			return;
		}

//		String 	downFileName = "";
//		if(model.external_memory.equalsIgnoreCase("0")){
//			String externalDir = Environment.getExternalStorageDirectory() + "/hunet_mlc/";
//			downFileName = String.format("%s%s_%d.mp4", externalDir, userId, contentsSeq);
//		}else{
//			downFileName = Utilities.getDownloadPullPath(activity.getApplicationContext(), 
//							userId, String.valueOf(contentsSeq));
//		}
		String 	downFileName = model.full_path;
		File file = new File(downFileName);

		if (file.exists() == false) {
			ConfirmDownload("sangsangdownload://"
					+ url.replace("sangsangdownloadplay://", ""));
			return;
		}

		Intent intent = getDownloadPlayIntent(activity, temp);
		activity.startActivity(intent);
	}
	
	public Intent getDownloadPlayIntent(Activity activity, String[] goodsInfo) {
		Intent intent = new Intent(activity, hunet.drm.download.DownloadCourseActivity.class);
		intent.putExtra("goodsId", goodsInfo[2]);
		
		return intent;
	}

	private void DownloadStep1(String url) {
		String[] temp = url.replace("sangsangdownload://", "").split("/");

		contractNo = Integer.parseInt(temp[0]);
		contentsSeq = Integer.parseInt(temp[1]);
		goodsId = temp[2];
		if (temp.length <= 3 || (temp.length > 3 && "".equals(temp[3])))
			SeGoodsId = goodsId;
		else
			SeGoodsId = temp[3];

		String targetUrl = DomainAddress.getUrlMLC() + "/App/JLog.aspx";
		String param = String.format("type=21&company_seq=%s&uid=%s&contract_no=%s&contents_seq=%s&goods_id=%s&se_goods_id=%s",
						companySeq, userId, contractNo, contentsSeq, goodsId, SeGoodsId);
		ShowDialog(activity.getString(R.string.download_msg_request_info), true);
		asyncReqData.Request(downloadInfo, targetUrl, param, null);
	}

	private void DownloadStep2() {
		String targetUrl = DomainAddress.getUrlApps() + "/Hunet_Player/Android.aspx";
		String param = String
				.format("action=SangSangDrmDownloadComplete&goodsId=%s&contentsSeq=%s&companySeq=%s&contractNo=%s&downloadType=2&deviceNm=android&regId=%s",
						studySangSangModel.goods_id,
						studySangSangModel.contents_seq,
						studySangSangModel.company_seq,
						studySangSangModel.contract_no, userId);

		asyncReqData.Request(downloadCompleted, targetUrl, param, null);

	}

	private void CreateProgressDialog() {
		if (progressDialog != null)
			return;

		progressDialog = new ProgressDialog(activity);
		progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		progressDialog.setTitle("다운로드");
		progressDialog.setProgress(0);
		progressDialog.setMax(0);
		progressDialog.setCancelable(false);
		progressDialog.setCanceledOnTouchOutside(false);

		progressDialog.setButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						CloseProgressDialog();

						if (stwDownloader != null)
							stwDownloader.cancelDownload();
					}
				});
	}

	private void CloseProgressDialog() {
		if (progressDialog == null)
			return;

		progressDialog.dismiss();
		progressDialog = null;
	}

	private SniperDownloader.Callback callback = new SniperDownloader.Callback() {

		@Override
		public void canDownload(long arg0) {
			Log.i(LOG_TAG, "canDownload!!!");
			stwDownloader.startDownload();
		}

		@Override
		public void canceled(int failedReason) {
			// TODO Auto-generated method stub
			Log.i(LOG_TAG, "cannotContinue!!!");
			Toast.makeText(activity, "다운로드가 취소 되었습니다.", Toast.LENGTH_LONG)
					.show();
			return;
		}

		@Override
		public void cannotContinue(int failedReaso) {
			// TODO Auto-generated method stub
			Log.i(LOG_TAG, "cannotContinue!!!");
		}

		@Override
		public void cannotDownload(int failedReason) {
			// TODO Auto-generated method stub
			Log.i(LOG_TAG, "cannotDownload!!!");
		}

		@Override
		public void completed(long sizeDownloaded) {
			Log.i(LOG_TAG, "completed!!!");
			CloseProgressDialog();
			if (stwDownloader.canceled()) {
				try {
					File file = new File(studySangSangModel.full_path);
				
					if (file.exists())
						file.delete();
					
				
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			DownloadStep2();
		}

		@Override
		public void queryOverwrite(long sizeOnServer, long sizeDownloaded) {
			// TODO Auto-generated method stub
			stwDownloader.startDownload();
		}

		@Override
		public void queryOverwriteOrContinue(long arg0, long arg1) {
			// TODO Auto-generated method stub
			stwDownloader.startContinueDownload();
		}

		@Override
		public void started(final long sizeOnServer) {
			// TODO Auto-generated method stub
			activity.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					if (progressDialog == null)
						return;
					if (!progressDialog.isShowing()) {
						final MemoryStatus m_MemoryStatus = new MemoryStatus();
//						long availableSize = m_MemoryStatus
//								.getAvailableExternalMemorySize();
						final long availableSize = m_MemoryStatus.getAvailableMemorySize(activity.getApplicationContext(), sqlManager);

						int tempSize = (int)sizeOnServer;
						Log.i(LOG_TAG, "availableSize!!! : "+ availableSize + " sizeOnServer : " + sizeOnServer + " tempSize: " + tempSize);

						if (availableSize <= sizeOnServer) {
							stwDownloader.cancelDownload();
							
							activity.runOnUiThread(new Runnable() {
								@Override
								public void run() {
									new AlertDialog.Builder(activity)
									.setTitle("저장 공간 부족 확인")
									.setMessage(
											String.format(
													"현재 선택하신 파일의 용량은 %s이며, 저장 가능한 공간은 %s 입니다.\n\n저장 공간을 확보 하신 후 다시 시도해 주세요.",
													m_MemoryStatus
															.getFileSizeFormat(sizeOnServer),
													m_MemoryStatus
															.getFileSizeFormat(availableSize)))
									.setPositiveButton("확인", null).show();
								}
							});

							return;
						}
						HideDialog();
						progressDialog.setMax((int) sizeOnServer);
						progressDialog.show();

					}
				}
			});
		}

		@Override
		public void progress(final long sizeOnServer,
				final long sizeDownloaded, final long speed) {

			activity.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					if (progressDialog == null)
						return;
					progressDialog.setProgress((int) sizeDownloaded);
				}
			});
		}

	};

	@Override
	public void AsyncRequestDataResult(HttpData result) {
		switch (result.id) {

		case downloadInfo: {

			takeSangSangModel.goods_id = SeGoodsId;
			takeSangSangModel.goods_nm = result.GetJsonValue("goods_nm");
			takeSangSangModel.study_end_date = result.GetJsonValue("study_end_date");

			studySangSangModel.user_id = userId;
			studySangSangModel.goods_id = goodsId;
			studySangSangModel.company_seq = Integer.parseInt(companySeq);
			studySangSangModel.contents_nm = result.GetJsonValue("contents_nm");
			studySangSangModel.contents_seq = contentsSeq;
			studySangSangModel.contents_view_sec = Integer.parseInt(result.GetJsonValue("contents_view_sec", "0"));
			studySangSangModel.last_view_sec = Integer.parseInt(result.GetJsonValue("last_view_sec", "0"));
			studySangSangModel.view_sec = Integer.parseInt(result.GetJsonValue("view_sec", "0"));
			studySangSangModel.view_no = Integer.parseInt(result.GetJsonValue("view_no", "0"));
			studySangSangModel.contents_view_sec = Integer.parseInt(result.GetJsonValue("contents_sec", "0"));
			studySangSangModel.view_sec_mobile = Integer.parseInt(result.GetJsonValue("view_sec_mobile", "0"));
			studySangSangModel.se_goods_id = SeGoodsId;
			studySangSangModel.contract_no = contractNo;
			
			String downFileName = Utilities.getDownloadPullPath(activity.getApplicationContext(), 
					studySangSangModel.user_id, String.valueOf(studySangSangModel.contents_seq));

			studySangSangModel.full_path = downFileName;
			
			String downHttpUrl = result
					.GetJsonValue("url")
					.replace("http://hunet2.hscdn.com/hunetvod/_definst_/mp4:Mvod/", "http://m.hunet.hscdn.com/hunet/Mvod/")
					.replace("/playlist.m3u8", "")
					.replace("m.hunet", "m2.hunet");

			CreateProgressDialog();
			progressDialog.setMessage(String.format("%s", result.GetJsonValue("contents_nm")));
			try {

				String udid = SniperPlayerUtil.readDeviceId(this.activity) + libraryKey;
				stwDownloader = new SniperDownloader(this.activity, libraryKey,
						udid, true);
			} catch (CreateFailedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			stwDownloader.setCallback(this.callback);
			stwDownloader.setMovieUrl(downHttpUrl);
			stwDownloader.setFileLocation(downFileName);

			String drmKey = String.format("<drmKey><UserID value=\"%s\" /><MovieID value=\"%s\" /><PayID value=\"%d\" /></drmKey>", userId, SeGoodsId, contractNo);
			String startDate = "2014-1-1";
			String endDate = takeSangSangModel.study_end_date;

			stwDownloader.setDrmInfo(startDate, endDate, drmKey);
			stwDownloader.prepareDownload();
		}
			break;
		case downloadCompleted: {
			HideDialog();
			// downStudyIndexModel.max_view_sec =
			// Integer.parseInt(data.GetJsonValue("max_study_sec", "0"));
			
			AccessModel accessModel = sqlManager.GetAccessData();
			if("1".equals(accessModel.external_memory)){ // 외부 메모리 사용
				if(Utilities.getExternalMemoryCheck(activity.getApplicationContext())){
					studySangSangModel.external_memory = "2";
				}else{
					studySangSangModel.external_memory = "1";
				}
			}else{
				studySangSangModel.external_memory = "1";
			}

			sqlManager.SetDownTakeSangSangData(takeSangSangModel);
			sqlManager.SetDownStudySangSangData(studySangSangModel);

			Toast.makeText(activity, "다운로드가 완료 되었습니다.", Toast.LENGTH_LONG).show();

			wv.reload();
		}
			break;
		}

	}

}
