package hunet.library;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface.OnClickListener;

public class ConfirmDialog {

	public static void ConfirmDialog(Activity activity, String title, String message, String positive, String negative, OnClickListener listener)
	{	
		AlertDialog.Builder dialog = new AlertDialog.Builder(activity);
		dialog.setTitle(title).setMessage(message)
        .setPositiveButton("".equals(positive) ? "확인" : positive, listener)
        .setNegativeButton("".equals(negative) ? "취소" : negative, listener)
        .show();
	}

}
