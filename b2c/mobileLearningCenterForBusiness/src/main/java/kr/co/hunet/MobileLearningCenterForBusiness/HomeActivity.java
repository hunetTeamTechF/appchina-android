package kr.co.hunet.MobileLearningCenterForBusiness;

import kr.co.HunetLib.Company.Company;
import kr.co.hunet.MobileLearningCenterForBusiness.TabClassLoader.CompanySeq;
import android.os.Bundle;

public class HomeActivity extends kr.co.HunetLib.HomeActivity
{
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
	    super.onCreate(savedInstanceState);
	    
	    int company_seq = getCompany().getSeq();
	    
		if(company_seq == CompanySeq.SLS_PROWAY){
			setSelectTab(Company.eTAB_CLASSROOM);
		}
	}
}
