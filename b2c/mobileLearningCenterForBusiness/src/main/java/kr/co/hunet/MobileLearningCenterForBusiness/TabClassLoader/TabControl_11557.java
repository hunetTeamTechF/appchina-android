package kr.co.hunet.MobileLearningCenterForBusiness.TabClassLoader;

import kr.co.HunetLib.Company.Company;
import kr.co.HunetLib.Control.TabControl;
import kr.co.hunet.MobileLearningCenterForBusiness.R;
import android.widget.ImageButton;
import android.widget.LinearLayout;

public class TabControl_11557 extends TabControl 
{
	public TabControl_11557() {
		// Background, Icon Color, Text Color, Selected Icon Color, Selected Text Color
		super("#fff5f5f5", "#ff888888", "#ff888888", "#ff0000ff", "#ff0000ff");
	}
	
	@Override
	public void InitTab(int nSelectTab) {
		selected_tab = nSelectTab;

		InitTabContainer();		

		/*탭메뉴*/
		ImageButton img_tab_1 = new ImageButton(tab_container.getContext());
		ImageButton img_tab_2 = new ImageButton(tab_container.getContext());

		/*탭메뉴 설정*/
		img_tab_1.setLayoutParams(new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 1));
		img_tab_2.setLayoutParams(new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 1));

		img_tab_1.setBackgroundResource(nSelectTab == Company.eTAB_CLASSROOM 			? R.drawable.tab1_11557_on : R.drawable.tab1_11557);
		img_tab_2.setBackgroundResource(nSelectTab >= Company.eTAB_MORE					? R.drawable.tab2_11557_on : R.drawable.tab2_11557);

		img_tab_1.setTag(Company.eTAB_CLASSROOM);
		img_tab_2.setTag(Company.eTAB_MORE);


		img_tab_1.setOnClickListener(this);
		img_tab_2.setOnClickListener(this);
		
		tab_container.addView(img_tab_1);
		//tab_container.addView(img_tab_2);
	}
}
