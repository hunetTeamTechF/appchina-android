package cn.co.HunetLib.Common;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebView;

public class HunetWebChromeClient extends WebChromeClient 
{
	//Javascript alert 호출 시 실행
	@Override
	public boolean onJsAlert(WebView view, String url, String message, final JsResult result)
	{	   
		//AlertDialog 생성
		new AlertDialog.Builder(view.getContext()).setMessage(message).setPositiveButton("确认",
				new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which) 
			{
				result.confirm();  
			}
		}).setCancelable(false).create().show();
		return true;
	}

	@Override
	public boolean onJsConfirm(WebView view, String url, String message, final JsResult result) {
		//AlertDialog 생성
		new AlertDialog.Builder(view.getContext()).setMessage(message).setPositiveButton("确认",
				new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which) 
			{
				result.confirm();  
			}
		})
		.setNegativeButton("取消", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				result.cancel();
			}
		})
		.setCancelable(false).create().show();
		return true;
	}
} 
