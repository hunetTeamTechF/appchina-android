package cn.co.HunetLib.Common;

import java.util.Enumeration;
import java.util.Hashtable;

import cn.co.HunetLib.R;
import cn.co.HunetLib.Company.Company;
import cn.co.HunetLib.Interface.IHunetReceiverEventListener;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Vibrator;

public class HunetReceiver extends BroadcastReceiver 
{	
	private static Hashtable<String, IHunetReceiverEventListener> m_htReceiver = new Hashtable<String, IHunetReceiverEventListener>();
	
	public static void AddEventListner(String strKey, IHunetReceiverEventListener eventListener)
	{
		if(m_htReceiver.containsKey(strKey))
			return;
		
		m_htReceiver.put(strKey, eventListener);
	}
	
	public static void AddEventListner(String strKey, IHunetReceiverEventListener eventListener, boolean bIsRemove)
	{
		if(m_htReceiver.containsKey(strKey))
		{
			if(bIsRemove)
				m_htReceiver.remove(strKey);
			else
				return;
		}
		
		m_htReceiver.put(strKey, eventListener);
	}
	
	public static void RemoveEventListener(String strKey)
	{
		if(m_htReceiver.containsKey(strKey))
			m_htReceiver.remove(strKey);
	}
	
	protected void AlarmSetting(Context context, Intent intent)
	{
		HunetAlarmManager alarmManager = new HunetAlarmManager(context); 
		alarmManager.RunAlarm();
	}
	
	protected void NotifyAlaram(Context context, Intent intent)
	{
		Company company = AppMain.CompanyInfo;
		
		Vibrator vib = (Vibrator)context.getSystemService(Context.VIBRATOR_SERVICE);
		vib.vibrate(2000);
		
		Intent loginIntent = new Intent(context, company.getNextActivity(company.eNEXT_ACTIVITY_NOTIFY_ALARM));
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		NotificationManager nm = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
		PendingIntent sender = PendingIntent.getActivity(context, 0, loginIntent, PendingIntent.FLAG_ONE_SHOT);
		Notification noti = new Notification(R.drawable.icon_app, "학습시간 알림", System.currentTimeMillis());
		
		noti.setLatestEventInfo(context, "학습시간 알림", "모바일  학습을 할 시간입니다.", sender);
		
		noti.defaults 	|= Notification.DEFAULT_SOUND;
		noti.defaults 	|= Notification.DEFAULT_VIBRATE;
		noti.flags 		|= Notification.FLAG_AUTO_CANCEL;
		
		nm.notify(AppMain.eNOTIFICATION_ALARM, noti);
	}
	
	protected void NetworkChange(Context context, Intent intent)
	{
		AppMain.setNetworkChangeYn("Y");
	}
	
	@Override
	public void onReceive(Context context, Intent intent) 
	{
		String strAction = intent.getAction();		
		
		if(AppMain.eACTION_BOOT_COMPLETED.equals(strAction))
		{
			AlarmSetting(context, intent);
		} 
		else if(AppMain.eACTION_ALARM_SETTING.equals(strAction))
		{
			NotifyAlaram(context, intent);
		}
		else if(AppMain.eACTION_NETWORK_CHANGE.equals(strAction))
		{
			NetworkChange(context, intent);
		}
		else if(Intent.ACTION_SCREEN_OFF.equals(strAction))
		{
			
		}
		
		Enumeration<String> recevers 	= m_htReceiver.keys();
		String 				strKey 		= "";
		
		while(recevers.hasMoreElements())
		{
			strKey = recevers.nextElement();
			IHunetReceiverEventListener listener = m_htReceiver.get(strKey);
			listener.OnHunetReceiverEvent(strAction);
		}
	}
}
