package cn.co.HunetLib;

import java.net.URLDecoder;

import cn.co.HunetLib.Base.BaseActivity;
import cn.co.HunetLib.Common.HunetWebChromeClient;
import cn.co.HunetLib.R;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

public class MyStudyWebtoonActivity extends BaseActivity
{	
	private long lastTouchTime = -1;              //더블클릭 인지 변수
	String webtoonUrl;
	WebView mWebview;
	LinearLayout ll_mystudywebtoon_back;
	FrameLayout frame_tool_bar;
	
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.mystudywebtoon);
	    	    
		ll_mystudywebtoon_back 	= (LinearLayout) findViewById(R.id.more_ll_mystudywebtoon_back);
		mWebview 				= (WebView) findViewById(R.id.wv_webtoon);
	    frame_tool_bar 			= (FrameLayout) findViewById(R.id.frame_tool_bar);
		
		ll_mystudywebtoon_back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		
		InitWebView(mWebview);
		
		Intent intent = getIntent();
	    Bundle bundle = intent.getExtras();
	    
	    webtoonUrl = URLDecoder.decode(this.getBundleValue(bundle, "webtoonUrl", ""));
	}
	
	@Override
	protected void onFirstResume() 
	{
		if("".equals(webtoonUrl))
		{
			ShowToast("잘못된 경로 입니다. 확인 후 다시 시도해주세요.");
			finish();
			return;
		}
		
		mWebview.loadUrl("http://study.hunet.co.kr/" + webtoonUrl);
	}
	
	@Override
	protected void onResume() 
	{
		super.onResume();	    
	}
	
	@SuppressLint("SetJavaScriptEnabled")
	protected void InitWebView(WebView webview)
	{	
		webview.setHorizontalScrollBarEnabled(false);
		webview.getSettings().setJavaScriptEnabled(true);
		webview.getSettings().setBuiltInZoomControls(true);
		webview.getSettings().setSupportZoom(true);
		webview.setWebChromeClient(new HunetWebChromeClient());
		webview.setOnTouchListener(new OnTouchListener()
		{
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if(event.getAction() == MotionEvent.ACTION_DOWN){
					long thisTime = System.currentTimeMillis();
					
					if(thisTime -lastTouchTime < 250){//double touch
						lastTouchTime = -1;
				
						 if(frame_tool_bar.getVisibility()== View.VISIBLE)
						 {
							 frame_tool_bar.setVisibility(View.GONE);					 
						 }
						 else
						 {
							 frame_tool_bar.setVisibility(View.VISIBLE);								 
						 }
					}
					else
					{
						lastTouchTime = thisTime;
					}
				}
				lastTouchTime = System.currentTimeMillis();
				return false;
			}
		});
		
		webview.setWebViewClient(new WebViewClient() 
		{
			@Override
			public void onPageStarted(WebView view, String url, Bitmap favicon) 
			{
				ShowDialog(MyStudyWebtoonActivity.this, "", true);
				super.onPageStarted(view, url, favicon);
			}
			
			@Override
			public void onPageFinished(WebView view, String url) 
			{
				HideDialog();
				super.onPageFinished(view, url);
			}
			
			@Override
		    public boolean shouldOverrideUrlLoading(WebView view, String url) 
			{					
				String tempUrl = url.toLowerCase();
				
		        return false;
	    	}
			
			@Override
			public void onReceivedError(WebView view, int errorCode, String description, String failingUrl)
			{					
							
			}
		});
	}
}
