package cn.co.HunetLib;

import cn.co.HunetLib.Common.UrlAction;
import cn.co.HunetLib.Company.ViewModel;
import cn.co.HunetLib.Interface.ITabControlEventListener;
import cn.co.HunetLib.SQLite.SQLiteManager;
import hunet.library.Utilities;

import java.net.URLDecoder;

import cn.co.HunetLib.Base.TabActivity;
import cn.co.HunetLib.Common.HunetWebChromeClient;
import cn.co.HunetLib.Interface.IWebViewEventListener;
import cn.co.HunetLib.R;

import org.apache.http.util.EncodingUtils;

import android.annotation.SuppressLint;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.webkit.SslErrorHandler;
import android.net.http.SslError;

public class UserTabWebviewActivity extends TabActivity implements ITabControlEventListener
{
	private IWebViewEventListener m_WebViewListener;
	private FrameLayout 	m_mainHeader;
	private LinearLayout 	m_mainHeaderPriv;
	private TextView 		m_tvTitle;
	private WebView 		m_WebView;
	private LinearLayout 	m_TabMenu;
	private String 			m_LoadUrl = "";
	private String 			m_NextUrl = "";
	private String			m_LoadMethod = "";
	private int 			m_SelectTab = 0;
	private UrlAction m_UrlAction;
	private SQLiteManager m_SQLiteManager;

	
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.usertabwebview_activity);
	    
	    Utilities.changeTitlebarColor(this, R.id.mainHeader, R.id.header_text_color);
	    
	    m_mainHeader		= (FrameLayout)findViewById(R.id.mainHeader);
	    m_mainHeaderPriv	= (LinearLayout)findViewById(R.id.mainHeaderPriv);
	    m_tvTitle			= (TextView)findViewById(R.id.header_text_color);
	    m_WebView 			= (WebView)findViewById(R.id.mainWebView);
	    m_TabMenu 			= (LinearLayout)findViewById(R.id.mainTabMenu);
	    m_UrlAction 		= getCompany().getUrlAction();
	    m_SQLiteManager		= new SQLiteManager(this);
	    
	    InitWebView(m_WebView);
	    
	    Bundle bundle = getIntent().getExtras();
	    m_SelectTab   = Integer.parseInt(this.getBundleValue(bundle, "TAB_NUMBER", "0"));
	    m_LoadUrl 	  = URLDecoder.decode(this.getBundleValue(bundle, "LOAD_URL", ""));
	    m_NextUrl	  = this.getBundleValue(bundle, "NEXT_URL", "");
	    m_LoadMethod  = this.getBundleValue(bundle, "METHOD", "get");

	    // SCREEN_ORIENTATION 파라메터 값은 아래와 같이 사용
    	// ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
    	// ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
    	// ActivityInfo.SCREEN_ORIENTATION_SENSOR
	    int orientation = Integer.parseInt(getBundleValue(bundle, "SCREEN_ORIENTATION", String.valueOf(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)));
    	setRequestedOrientation(orientation);
	    
	    m_UrlAction.Init(this, m_WebView);	    
	    m_UrlAction.setTabId(m_SelectTab);
	    
	    if("".equals(m_NextUrl) == false)
	    	m_WebView.setVisibility(View.INVISIBLE);
	    
	    if("Y".equals(getBundleValue(bundle, "SHOW_HEADER", "N")))
	    {
	    	m_mainHeader.setVisibility(View.VISIBLE);
	    	
	    	if (!"".equals(getBundleValue(bundle, "HEADER_TITLE", "")))
		    	m_tvTitle.setText(getBundleValue(bundle, "HEADER_TITLE", ""));
	    	else {
		    	ViewModel model = getCompany().getViewModel(m_SelectTab);
	    		m_tvTitle.setText(model == null ? "" : model.Title);
	    	}
	    }
	    
	    if("Y".equals(getBundleValue(bundle, "SHOW_TAB", "N")))
	    {
	    	CreateTabControl(m_TabMenu);
	    	setSelectTab(m_SelectTab);
	    	m_TabMenu.setVisibility(View.VISIBLE);
	    }
	    
	    m_mainHeaderPriv.setOnClickListener(new OnClickListener() 
	    { 
	    	@Override
	    	public void onClick(View v) { finish(); }
	    });
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}
	
	@Override
	protected void onFirstResume()
	{
		String url = m_LoadUrl;
		
		if("".equals(url))
			url = getCompany().getUrl(getCompany().getTabUrlType(m_SelectTab));
		
		if("".equals(url))
			return;
		
		setLoadUrl(url);
	}
	
	public void setWebViewEventListener(IWebViewEventListener webViewListener)
	{
		m_WebViewListener = webViewListener;
	}
	
	protected void setLoadUrl(String url)
	{
		if ("post".equalsIgnoreCase(m_LoadMethod)) {
			Uri postUri = Uri.parse(url);
			String redirectURL = String.valueOf(postUri).replace("?" + postUri.getQuery(), "");
			m_WebView.postUrl(redirectURL, EncodingUtils.getBytes(postUri.getQuery(), "UTF-8"));
		} else {
			m_WebView.loadUrl(url);
		}
	}
	
	@SuppressLint("SetJavaScriptEnabled")
	protected void InitWebView(WebView webview)
	{	
		WebSettings setting = webview.getSettings();
		setting.setUserAgentString(setting.getUserAgentString() + " HunetAndroid ");
		setting.setJavaScriptEnabled(true);

		setting.setUseWideViewPort(true);
		setting.setLoadWithOverviewMode(true);

		webview.addJavascriptInterface(this, "ScriptBridge");
		
		webview.setHorizontalScrollBarEnabled(false);
		webview.setVerticalScrollBarEnabled(false);

		webview.setWebChromeClient(new HunetWebChromeClient());
		webview.setWebViewClient(new WebViewClient() 
		{
			@Override
			public void onPageStarted(WebView view, String url, Bitmap favicon) 
			{
				ShowDialog(UserTabWebviewActivity.this, "", true);
				
				if(m_WebViewListener != null)
					m_WebViewListener.OnPageStarted(view, url, favicon);
				
				super.onPageStarted(view, url, favicon);
			}
			
			@Override
			public void onPageFinished(WebView view, String url) 
			{
				HideDialog();
				
				if(m_WebViewListener != null)
					m_WebViewListener.OnPageFinished(view, url);
							
				super.onPageFinished(view, url);
				
				if("".equals(m_NextUrl))
				{
					m_WebView.setVisibility(View.VISIBLE);
					return;
				}
				
				m_WebView.clearHistory();
				m_WebView.loadUrl(m_NextUrl);
				m_NextUrl = "";
			}
			
			@Override
		    public boolean shouldOverrideUrlLoading(WebView view, String url) 
			{					
				String tempUrl = url.toLowerCase();
				
				if(m_WebViewListener != null && m_WebViewListener.OnShouldOverrideUrlLoading(view, tempUrl))
					return true;
				
				if(m_UrlAction.CheckUrl(url))
					return true;
				
		        return false;
	    	}
			
			@Override
			public void onReceivedError(WebView view, int errorCode, String description, String failingUrl)
			{					
				if(m_WebViewListener != null)
					m_WebViewListener.OnReceivedError(view, errorCode, description, failingUrl);				
			}
			
			@Override
			 public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {   
			     handler.proceed(); // SSL 에러가 발생해도 계속 진행!   
			}
		});
	}
	
	@Override 
	public boolean onKeyDown(int keyCode, KeyEvent event) 
	{
		switch(keyCode)
		{
		case KeyEvent.KEYCODE_BACK:
			{
				if(m_WebView != null && m_WebView.canGoBack())
				{
					m_WebView.goBack();
					return true;
				}
			}
			break;
		}
		
		return super.onKeyDown(keyCode, event);
	}
	
	@Override
	protected void onDestroy()
	{
		super.onDestroy();
	}
	
	/**
	 * 웹페이지에서 비밀번호 변경 성공시 호출하는 메서드
	 * @param newPwd 변경된 비밀번호
	 */
	public void changePassword(String newPwd) {
		getCompany().LoginModel.pwd = newPwd;
		m_SQLiteManager.SetLoginData(getCompany().LoginModel);
		finish();
	}
	
	/**
	 * 앱 설치 여부를 확인합니다.
	 * @param packageName 패키지 명
	 * @return 설치 여부
	 */
	public boolean existsInstalledApp(String packageName) {
		return getPackageManager().getLaunchIntentForPackage(packageName) != null;
	}
}
