package cn.co.HunetLib.Common;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class HunetNetwork 
{
	/***
	 * WIFI 3G 연결 모두 확인
	 * @param context
	 * @return
	 */
	public static boolean connectAllConfirm(Context context)
	{
		try
		{
			if(connect3GConfirm(context) || connectWifiConfirm(context) || connectWimaxConfirm(context))
				return true;
			else
				return false;
		}
		catch (Exception e) {
			// TODO: handle exception
			try
			{
				if(connectWifiConfirm(context))
					return true;
				else
					return false;
			}
			catch (Exception e2) {
				// TODO: handle exception
				return false;
			}
		}
	}
	
	/***
	 * 3G망 연결 확인
	 * @param context
	 * @return
	 */
	public static boolean connect3GConfirm(Context context)
	{
		ConnectivityManager cm =  (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		
		try
		{
			NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
			
			if(mobile.isConnected())
				return true;
			else
				return false;
		}
		catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	}
	
	/**
	 * WIFI 연결 확인
	 * @param context
	 * @return
	 */
	public static boolean connectWifiConfirm(Context context)
	{
		ConnectivityManager cm =  (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		
		try
		{
			NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		
			if(wifi.isConnected())
				return true;
			else
				return false;
		}
		catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	}
	
	/**
	 * 4G 연결 확인
	 * @param context
	 * @return
	 */
	public static boolean connectWimaxConfirm(Context context)
	{
		ConnectivityManager cm =  (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

		try
		{
			NetworkInfo wimax = cm.getNetworkInfo(ConnectivityManager.TYPE_WIMAX);
			
			if(wimax.isConnected()) return true;
			else return false;
		}
		catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	}
	
	public static int GetCurrentUseNetwork(Context context)
	{
		if(connectWifiConfirm(context))
			return 1;
			
		if(connect3GConfirm(context))
			return 2;
		
		if(connectWimaxConfirm(context))
			return 3;
		
		return 0;
	}
}
