package cn.co.HunetLib.OldPlayer;

import org.json.JSONException;
import org.json.JSONObject;

import cn.co.HunetLib.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaPlayer.OnVideoSizeChangedListener;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.Gallery;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.SeekBar.OnSeekBarChangeListener;

/**
 * 강의관련 HTTP 통신. 플레이정보
 * 작업자 : 장형일
 * 작업일 : 2011.11
 * mlc 상상마루 플레이어 인앱 수정. 2011.11.04
 * mlc 플레이어 호환성 작업. 2012.02.20
 */
public class HunetSangSangVideoActivity extends Activity {

	private FrameLayout totalFrame;               //플레이어 전체 프레임레이아웃
	private VideoViewPlayer video;                //ㄴ영상 레이아웃
	
	//컨트롤
	private LinearLayout vcontrol;                //컨트롤 레이아웃
	private ImageButton btnMediaPlay;             //ㄴ재생
	private ImageButton btnMediaBack;             //ㄴ뒤로
	private ImageButton btnMediaForward;          //ㄴ앞으로
	private ImageButton btnSound;                 //ㄴ사운드
	
	private SeekBar seekBar = null;               //ㄴ슬라이드
	private TextView txtTimePosition = null;      //ㄴ슬라이드 시간
	private TextView txtTotalTime;                //ㄴ영상전체시간
	
	private LinearLayout layoutSound;             //사운드
	private LinearLayout volume_bar;              //ㄴ볼륨바 레이아웃
    private LinearLayout volume_ctr;              //ㄴ볼륨포인트 레이아웃
    private ImageView volume_point;               //ㄴ볼륨포인트
	
	private int mPosition=0;                      //영상 위치 마크값
	private int touchXPos = 0;                    //터치시, X 좌표
	private int touchYPos = 0;                    //터치시, Y 좌표
	
	TranslateAnimation ani = null;
	public static Dialog loading;                 //영상 로딩 메세지용 변수
	
	private SangSangMaruProgressThread progressThread = null;
	private DisplayMetrics metrics = new DisplayMetrics();
	
	//변수
	private String contents_url = "";             //영상 url 
	private int lastViewSec = 0;                  //최종 진도 위치
	private int viewSec = 0;                      //총 시청 시간
	private int viewSecMobile = 0;                //총 모바일 시청 시간.
	
	private int contractNo;                       //계약번호
	private int contentsSeq;                      //컨텐츠 번호
	private String goodsId;                          //상품번호
	
	Hunet_http_Data_Cls data = new Hunet_http_Data_Cls();
	
	int movWidth;
	int movHeight;
	
	int laterViewSec;    //다음 재생 위치, 앱 비활성화시 셋팅. 다시 활성화시 이어보기
	
	void SetZOrderByVideo()
    {
    	vcontrol.bringToFront();
    	layoutSound.bringToFront();
    }
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	
	    // TODO Auto-generated method stub
	    setContentView(R.layout.sangsangvideo);
	    
	    SetLayout();
	    SetInit();      //초기화
        SetMovInfo();    //영상정보 설정
        SetVideoFull();
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		SetControl();   //컨트롤 셋팅
		
		if(laterViewSec > 0) //비활성화 후 다시 활성화 시 이어보기
		{
			video.seekTo(laterViewSec);
		}
	}
	
    @Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		
		try {
			laterViewSec = video.getCurrentPosition();
			progressThread.stop();
		}
		catch (Exception e) {
			// TODO: handle exception
		}
	}
    
    protected void SetLayout() {
    	
    	loading = HunetAlertMethod.AlertDialogForIng(this, 0);
    	
    	if(totalFrame == null)
			totalFrame = (FrameLayout) findViewById(R.id.totalFrame);
		if(video == null)
			video = (VideoViewPlayer) findViewById(R.id.VideoViewPlayer);
		if(vcontrol == null)
			vcontrol = (LinearLayout) findViewById(R.id.vcontrol);
		if(btnMediaPlay == null)
			btnMediaPlay = (ImageButton) findViewById(R.id.btnMediaPlay);
		if(btnMediaBack == null)
			btnMediaBack = (ImageButton) findViewById(R.id.btnMediaBack);
		if(btnMediaForward == null)
			btnMediaForward = (ImageButton) findViewById(R.id.btnMediaForward);
		if(btnSound == null)
			btnSound = (ImageButton) findViewById(R.id.btnSound);
		if(txtTimePosition == null)
			txtTimePosition = (TextView) findViewById(R.id.txtTimePosition);
		if(txtTotalTime == null)
			txtTotalTime = (TextView) findViewById(R.id.txtTotalTime);
		if(seekBar == null)
        	seekBar = (SeekBar) findViewById(R.id.sbBar);
		if(layoutSound == null)
			layoutSound = (LinearLayout) findViewById(R.id.layoutSound);
		if(volume_bar == null)
			volume_bar = (LinearLayout) findViewById(R.id.volume_bar);
		if(volume_ctr == null)
			volume_ctr = (LinearLayout) findViewById(R.id.volume_ctr);
		if(volume_point == null)
			volume_point = (ImageView) findViewById(R.id.volume_point);
		
    }
	
	protected void SetInit()
    {
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
		
    	Intent intent = getIntent();
	    Bundle bundle = intent.getExtras();
	    
	    contractNo = bundle.getInt("contractNo");
	    contentsSeq = bundle.getInt("contentsSeq");
	    goodsId = bundle.getString("goodsId");
    }
	
	/***
     * 동영상 컨텐츠 정보, 진도 정보
     */
    protected void SetMovInfo()
    {
    	String success = "";
    	
    	try {
    		JSONObject MovJObj = data.SelectContentsInfo(contractNo, contentsSeq, goodsId);
    		
			success = MovJObj.getString("IsSuccess");
			viewSec = MovJObj.getInt("view_sec");
			viewSecMobile = MovJObj.getInt("view_sec_mobile");
			lastViewSec = MovJObj.getInt("last_view_sec");
			contents_url = MovJObj.getString("url");
			//contents_url = contents_url.replace("http", "rtsp").replace(":1935", "").replace("/smil:", "").replace("hunetvodM_Learning", "hunetvod/mp4://M_Learning").replace("hunetvod/M_Learning", "hunetvod/mp4://M_Learning").replace(".smil", "").replace("/playlist.m3u8", "");
//			if(!contents_url.contains("_definst_")) contents_url = contents_url.replace("mp4://", "_definst_/mp4:");
//			contents_url = contents_url.replace("http", "rtsp").replace("/playlist.m3u8", "");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
			success = "NO";
			contents_url = "";
			viewSec = 0;
			lastViewSec = 0;
		}
    	
		if(success.equals("YES"))
    	{
			SetVideo();     //동영상 셋팅
    	}
    	else
    	{
    		HunetAlertMethod.AlertMessage("동영상 재생을 위한 정보가 없습니다.", this, "재생");
    	}
    }
	
    /***
     * 영상 부
     */
    protected void SetVideo()
    {
    	loading.show();
    	
        Uri uri = Uri.parse(contents_url);
    
        video.setVideoURI(uri);
        video.requestFocus();
        
        //영상 정보 셋팅
        video.setOnPreparedListener(new OnPreparedListener() {
			
			@Override
			public void onPrepared(MediaPlayer mp) {
				// TODO Auto-generated method stub
				
				if(video.getDuration() == 0) //기기 특성상 0으로 인식 시.(예:젤리빈 OS 사용 기기)
				{
					mp.setOnVideoSizeChangedListener(new OnVideoSizeChangedListener() {

						@Override
						public void onVideoSizeChanged(MediaPlayer arg0, int arg1,
								int arg2) {
							// TODO Auto-generated method stub
	
							if(lastViewSec > 0)
							{
								int tmpSec = lastViewSec*1000;
								
								if(tmpSec >= arg0.getDuration())
									tmpSec = 0;
								
								video.seekTo(tmpSec);
							}
							
							loading.dismiss();
							seekBar.setMax(arg0.getDuration());
							txtTotalTime.setText(HunetCommonMethod.MarkToTimeString(arg0.getDuration()));
						}
					});
				}
				else
				{
					if(lastViewSec > 0)
					{
						int tmpSec = lastViewSec*1000;
						
						if(tmpSec >= video.getDuration())
							tmpSec = 0;
						
						video.seekTo(tmpSec);
					}
					
					loading.dismiss();
					seekBar.setMax(video.getDuration());
					txtTotalTime.setText(HunetCommonMethod.MarkToTimeString(video.getDuration()));
				}
				
				vcontrol.setVisibility(View.VISIBLE);
				video.start();
			}
		});
        
        //영상 끝, 자동종료
        video.setOnCompletionListener(new OnCompletionListener() {

			@Override
			public void onCompletion(MediaPlayer mp) {
				// TODO Auto-generated method stub
				
				progressThread.lastUpdate();
				finish();
			}
        	
        });

        if(progressThread == null)
			progressThread = new SangSangMaruProgressThread(video, txtTimePosition, seekBar, vcontrol, layoutSound,contractNo, contentsSeq, goodsId, viewSec, viewSecMobile, this);
		
		if(progressThread.isAlive() == false){
			progressThread.start();
		}
    }

	/***
     * 컨트롤 부
     */
    protected void SetControl()
    {
    	txtTimePosition.setText("00:00:00");
    	
    	totalFrame.setOnTouchListener(new OnTouchListener() {
        	
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				
				if(event.getAction() == MotionEvent.ACTION_DOWN)
				{
					touchXPos = (int)event.getX();
					touchYPos = (int)event.getY();
					SetCTLVisible();
				}
				else if(event.getAction() == MotionEvent.ACTION_MOVE)
				{
					int moveXPos = (int)event.getX();
					int moveYPos = (int)event.getY();
					
					if(touchYPos - moveYPos < 2 || touchYPos - moveYPos > -2)
					{
						int currentVolum = getVolume();
						
						if(moveXPos - touchXPos > 40 && currentVolum < 15)
						{
							currentVolum = currentVolum + 1;
							SetVolume(currentVolum);
						}
						else if(moveXPos - touchXPos < -40 && currentVolum > 0)
						{
							currentVolum = currentVolum - 1;
							SetVolume(currentVolum);
						}
						
						touchXPos = moveXPos;
						touchYPos = moveYPos;
					}
				}
				
				return true;
			}
		});
        
    	seekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
					video.seekTo(seekBar.getProgress());
					video.start();
			}
			
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
				video.pause();
			}
			
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				// TODO Auto-generated method stub
			}
		});
        
        /*play button*/
        btnMediaPlay.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_pause));
        btnMediaPlay.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
 				if(video.isPlaying())
				{
					video.pause();
					btnMediaPlay.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_play));
				}
 				else
 				{
					video.start();
					btnMediaPlay.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_pause));
				}
			}
		});
        btnMediaPlay.setOnTouchListener(new OnTouchListener() {
        	
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				
				if(video.isPlaying())
				{
					btnMediaPlay.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_pause_on));
				}
 				else
 				{
					btnMediaPlay.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_play_on));
				}
					
				return false;
			}
		});
        
        /*back*/
        btnMediaBack.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_rewind));
    	btnMediaBack.setOnClickListener(new OnClickListener() {
    		
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
								
 				if(video.isPlaying()){
 				     mPosition = video.getCurrentPosition();
 				     video.seekTo(mPosition-10000);
 				}
 				else {
 				     mPosition = video.getCurrentPosition();
 				     video.seekTo(mPosition-10000);
 				}
			}
		});
    	btnMediaBack.setOnTouchListener(new OnTouchListener() {
        	
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				
				if(event.getAction() == MotionEvent.ACTION_DOWN)
					btnMediaBack.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_rewind_on));
				else
					btnMediaBack.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_rewind));
					
				return false;
			}
		});
    	
    	/*Forward*/
    	btnMediaForward.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_forward));
    	btnMediaForward.setOnClickListener(new OnClickListener() {
    		
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
								
 				if(video.isPlaying()){
			      mPosition = video.getCurrentPosition();
			      video.seekTo(mPosition+10000);
			    }
			    else if(!video.isPlaying() || video.isPlaying()==false){
			      mPosition = video.getCurrentPosition();
			      video.seekTo(mPosition+10000);
 				}
			}
		});
    	
    	btnMediaForward.setOnTouchListener(new OnTouchListener() {
        	
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				
				if(event.getAction() == MotionEvent.ACTION_DOWN)
					btnMediaForward.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_forward_on));
				else
					btnMediaForward.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_forward));
					
				return false;
			}
		});
    	
    	btnSound.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_sound));
    	btnSound.setOnClickListener(new OnClickListener() {
    		
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(layoutSound.getVisibility() == View.INVISIBLE)
				{
					if(vcontrol.getVisibility() == View.INVISIBLE)
						vcontrol.setVisibility(View.VISIBLE);
					
					layoutSound.setVisibility(View.VISIBLE);
					SetVolumeMov(); //볼륨 셋팅
				}
				else
					layoutSound.setVisibility(View.INVISIBLE);
			}
		});
    	btnSound.setOnTouchListener(new OnTouchListener() {
        	
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				
				if(event.getAction() == MotionEvent.ACTION_DOWN)
					btnSound.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_sound_on));
				else
					btnSound.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_sound));
					
				return false;
			}
		});
    	
    	//해상도 720 이상 일 시(갤럭시 탭 10.1), 소리조절 버튼 안 보여준다. 레이아웃 공용으로 조건 잡기 어려움으로 인해..
    	if(getDiviceDisplayHeight() > 720) btnSound.setVisibility(View.INVISIBLE);
    }
    
    /***
     * 재생 컨트롤 보여주기, 안보여주기
     */
    protected void SetCTLVisible()
    {
    	SetZOrderByVideo();
    	
    	if(vcontrol.getVisibility() == View.INVISIBLE){
    		ani = new TranslateAnimation(0,0,100,0);
            ani.setDuration(500);
            vcontrol.setAnimation(ani);
			vcontrol.setVisibility(View.VISIBLE);
		}
		else{
			ani = new TranslateAnimation(0,0,0,100);
	        ani.setDuration(500);
	        vcontrol.setAnimation(ani);
			vcontrol.setVisibility(View.INVISIBLE);
			layoutSound.setVisibility(View.INVISIBLE);
		}
    }
    
    /***
     * 영상 확대
     */
    protected void SetVideoFull()
    {
		video.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
		
		movWidth = video.getWidth();
		movHeight = video.getHeight();
		video.setVideoAspect(getDiviceDisplayWidth(), getDiviceDisplayHeight());
    }
    
    /***
	 * 동영상 볼륨(미디어 볼륨 조절)
	 */
	private void SetVolumeMov()
	{
		//현재볼륨, 볼륨 포인터 위치 설정
		volume_ctr.setPadding(0, 0, 0, getVolumePointY());
		setVolumeLayout(getVolume());
		
		//볼륨 포인터 이동 시
		volume_point.setOnTouchListener(
				new OnTouchListener() {

					@Override
					public boolean onTouch(View v, MotionEvent event) {
						// TODO Auto-generated method stub

						int keyAction = event.getAction();
						int y = (int)event.getY();
						switch (keyAction){
							case MotionEvent.ACTION_MOVE:
								
								int sel_volume = 0;  //설정 볼륨;
								
								//위치 = 레이아웃 paddingLeft
								int max = volume_bar.getHeight() - (volume_bar.getHeight() / 16);                               //맥스 범위
								int point_y = volume_ctr.getPaddingBottom(); //현재 위치
								int move_y = 0;                              //이동할 위치
								
								if(point_y > max) move_y = point_y;
								else move_y = point_y - y;
								
								if(move_y < 0) move_y = 0;
								
								if(move_y > max) move_y = max;
								
								sel_volume = getVolume(move_y);
								
								volume_ctr.setPadding(0, 0, 0, move_y);
								setVolumeLayout(sel_volume);
								SetVolume(sel_volume);
								
								break;
						}

						return true;
					}
				}
		);
	}
	
	protected void SetVolume(int currentVolum)
    {
    	((AudioManager)getSystemService(AUDIO_SERVICE)).
		setStreamVolume(AudioManager.STREAM_MUSIC, currentVolum, AudioManager.FLAG_SHOW_UI);
    }
    
    protected int getVolume()
    {
    	return ((AudioManager)getSystemService(AUDIO_SERVICE)).getStreamVolume(AudioManager.STREAM_MUSIC);
    }
	
	/***
	 * 볼륨 포인터 위치 구하기
	 * @return
	 */
	private int getVolumePointY()
	{
		int rtnResult = 0;
		int current_vol = getVolume();
		
		if(current_vol == 15)
			rtnResult = volume_bar.getHeight() - (volume_bar.getHeight() / 16);
		else
			rtnResult = (volume_bar.getHeight() / 16) * current_vol;
		
		return rtnResult;
	}
	
	/***
	 * 포인터 위치에 따른 볼륨 구하기
	 * @param move_x
	 * @return
	 */
	private int getVolume(int move_x)
	{
		int rtnResult = 0;
		
		rtnResult = Math.round(move_x / (volume_bar.getHeight() / 16));
		
		return rtnResult;
	}
	
	/***
	 * 볼륨 포인터 이동시, 레이아웃 셋팅
	 */
	private void setVolumeLayout(int volume)
	{
		if(volume_bar.getChildCount() > 0)
			volume_bar.removeAllViews();

		if(volume > 14)
		{
			ImageView ivTop = new ImageView(this);
			
			ivTop.setLayoutParams(new Gallery.LayoutParams(LayoutParams.WRAP_CONTENT, 4));
			ivTop.setBackgroundResource(R.drawable.green_top);
			volume_bar.addView(ivTop);
		}
		
		for(int i=1; i<=volume; i++)
		{
			ImageView ivBar = new ImageView(this);
			
			if(i == 15)
				ivBar.setLayoutParams(new Gallery.LayoutParams(LayoutParams.WRAP_CONTENT, 7));
			else
				ivBar.setLayoutParams(new Gallery.LayoutParams(LayoutParams.WRAP_CONTENT, ((volume_bar.getHeight() - (volume_bar.getHeight() / 16) - 11) / 13)));
			ivBar.setBackgroundResource(R.drawable.green_bar);
			volume_bar.addView(ivBar);
		}
		
		if(volume > 0)
		{
			ImageView ivBottom = new ImageView(this);
			
			ivBottom.setLayoutParams(new Gallery.LayoutParams(LayoutParams.WRAP_CONTENT, 4));
			ivBottom.setBackgroundResource(R.drawable.green_bottom);
			volume_bar.addView(ivBottom);
		}
	}
	
	//가로
	public int getDiviceDisplayWidth()
	{
		Display d = ((WindowManager)this.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();		
		return d.getWidth();
	}
	//세로
	public int getDiviceDisplayHeight()
	{
		Display d = ((WindowManager)this.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
		
		return d.getHeight();
	}
	
	/***
	 * 키 선택 이벤트
	 */
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		switch(keyCode)
		{
			case KeyEvent.KEYCODE_BACK:
				new AlertDialog.Builder(HunetSangSangVideoActivity.this)
				   .setTitle("플레이어 종료")
				   .setMessage("플레이어을 종료 하시겠습니까?")
				.setPositiveButton("종료", new DialogInterface.OnClickListener() {
						 						@Override
												public void onClick(DialogInterface dialog, int which) {
						 							
						 							progressThread.lastUpdate();
						 							finish();
						 						}
			          						})
			    .setNegativeButton("취소", new DialogInterface.OnClickListener() {
			          							@Override
												public void onClick(DialogInterface dialog, int which) {
			          							}
			    })
			    .show();
				return true;
			default:
				return false;
		}
	}
	
	@Override
	protected void onDestroy()
	{
		if(progressThread != null)
			progressThread.Finish();
		
		super.onDestroy();
	}
}
