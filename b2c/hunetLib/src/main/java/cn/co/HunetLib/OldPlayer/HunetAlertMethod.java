package cn.co.HunetLib.OldPlayer;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;



/**
 * @author hyoungil
 * 알림 매소드 클래스
 * 작업자 : 장형일
 * 작업일 : 2011.05
 */
public class HunetAlertMethod {
	
	protected boolean blConnect;
	protected static Activity _act;
	protected static Intent _intent;
	
	public HunetAlertMethod() {
		
	}
	
	
	/**
	 * 에러메시지
	 * @param message
	 * @param act
	 * @param title
	 */
	public static void AlertMessage(String message, Activity act, String title) {
		 
        AlertDialog.Builder builder=new AlertDialog.Builder(act);

        builder.setTitle(title).setMessage(message).setPositiveButton("确认", null).show();
	}
	
	/**
	 * 에러메시지
	 * @param message
	 * @param act
	 * @param title
	 */
	public static void AlertMessage(String message, Context context, String title) {
		 
        AlertDialog.Builder builder=new AlertDialog.Builder(context);

        builder.setTitle(title).setMessage(message).setPositiveButton("确认", null).show();
	}
	
	/***
	 * 로그인메시지
	 * @param message
	 * @param act
	 * @param title
	 */
	public static void AlertLoginMessage(Activity act, Intent intent) {
		_act = act;
		_intent = intent; 
		
        AlertDialog.Builder builder=new AlertDialog.Builder(act);

        builder.setTitle("登录").setMessage("请先登录！")
        	.setPositiveButton("确认", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					//_act.finish();
					_act.startActivity(_intent);
				}})
			.setNeutralButton("取消", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {				
			}}).show();
	}
	
	/**
	 * 
	 * @param act
	 * @param intent
	 * @param title
	 */
	public static void AlertFalseAccessMessage(Activity act, String name) {
		_act = act;
		
        AlertDialog.Builder builder=new AlertDialog.Builder(act);
        String message = "잘못된 접근입니다.";
        builder.setTitle(name).setMessage(message)
        	.setPositiveButton("确认", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							_act.finish();
						}})
			.show();
	}
	
	/**
	 * 네트워크 연결확인
	 * @param act
	 * @param intent
	 * @param title
	 */
	public static void AlertNetworkMessage(Activity act, Intent intent, String name) {
		_act = act;
		_intent = intent;
		
        AlertDialog.Builder builder=new AlertDialog.Builder(act);
        String message = "네트워크 연결이 원활하지 않습니다.";
        builder.setTitle(name).setMessage(message)
        	.setPositiveButton("취소", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
						}})
			.setNeutralButton("재시도", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							_act.finish();
							_act.startActivity(_intent);
						}})
			.show();
	}
	
	/**
	 * 로딩 알림
	 * @param cxt
	 * @param id
	 */
	public static Dialog AlertDialogForIng(Context cxt,int id)
	{
		ProgressDialog dialog = new ProgressDialog(cxt);
		
		switch (id) {
			case 0: {
				dialog.setMessage("로딩하는 중입니다...");
				dialog.setIndeterminate(true);
				dialog.setCancelable(true);
				return dialog;
			}
			
			case 1 : {
				
			}
			
			case 2 : {
				
			}
		}
		return null;
	}
}
