package cn.co.HunetLib.SQLite;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

import hunet.data.SQLite;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class SQLiteManager 
{
	private SQLite sQLite;
	
	
	public SQLiteManager(Context context)
	{	
		sQLite = new SQLite(context);
	}
	
	public String GetCurrentTime()
	{
		Date 		curDate = new Date();
		Timestamp 	curTime = new Timestamp(curDate.getTime());
		
		return curTime.toString();
	}
		
	private ArrayList<String[]> GetCursorListValue(Cursor cursor, int nValueLen)
	{
		ArrayList<String[]> tempList = new ArrayList<String[]>();
				
		while(cursor.moveToNext()) 
		{
			String[] arTempValue = new String[nValueLen];
			
			for(int nIndex = 0; nIndex < nValueLen; nIndex++)
			{
				arTempValue[nIndex] = cursor.getString(nIndex);				
			}
			
			tempList.add(arTempValue);
		}
		
		return tempList;
	}
	
	private String[] GetCursorValues(Cursor cursor, String[] arValues)
	{
		int nLength = arValues.length;
		
		while(cursor.moveToNext()) 
		{
			for(int nIndex = 0; nIndex < nLength; nIndex++)
				arValues[nIndex] = cursor.getString(nIndex);
		}
		
		return arValues;
	}
	
	public LoginModel GetLoginData()
	{
		SQLiteDatabase db 		= sQLite.getWritableDatabase();
		String[] 	arValues 	= {"", "", "", "", "", "", "", "",""};
		String 		strQuery 	= "SELECT id, pwd, auto_login, save_id, user_nm, mobile_nm, email_nm, userType_cd, loginYn"
								+ " FROM " + sQLite.eLOGIN_TABLE_NM;
		 
		Cursor cursor 	= db.rawQuery(strQuery, null);
		arValues 		= GetCursorValues(cursor, arValues);
		
		cursor.close();
		db.close();
		
		LoginModel model 	= new LoginModel();
		model.id 			= arValues[0];
		model.pwd 			= arValues[1];
		model.auto_login 	= arValues[2];
		model.save_id		= arValues[3];
		model.user_nm		= arValues[4];
		model.mobile_nm		= arValues[5];
		model.email_nm		= arValues[6];
		model.userType_cd	= arValues[7];
		model.loginYn		= arValues[8];
		
		return model;
	}
	
	public LoginModel GetLoginData(String id)
	{
		SQLiteDatabase db 		= sQLite.getWritableDatabase();
		String[] 	arValues 	= {"", "", "", "", "", "", "", "",""};
		String 		strQuery 	= "SELECT id, pwd, auto_login, save_id, user_nm, mobile_nm, email_nm, userType_cd, loginYn"
								+ " FROM " + sQLite.eLOGIN_TABLE_NM
								+ " WHERE id = '" + id + "'";
		 
		Cursor cursor 	= db.rawQuery(strQuery, null);
		arValues 		= GetCursorValues(cursor, arValues);
		
		cursor.close();
		db.close();
		
		LoginModel model 	= new LoginModel();
		model.id 			= arValues[0];
		model.pwd 			= arValues[1];
		model.auto_login 	= arValues[2];
		model.save_id		= arValues[3];
		model.user_nm		= arValues[4];
		model.mobile_nm		= arValues[5];
		model.email_nm		= arValues[6];
		model.userType_cd	= arValues[7];
		model.loginYn		= arValues[8];
		
		return model;
	}
	
	public void SetLoginData(LoginModel model) 
	{
		LoginModel tempModel 	= GetLoginData(model.id);
		SQLiteDatabase 	db 		= sQLite.getWritableDatabase();	
		
		if("".equals(tempModel.id))
		{
			ContentValues cv = new ContentValues();
			cv.put("id", model.id);
			cv.put("pwd", model.pwd);
			cv.put("auto_login", model.auto_login);
			cv.put("save_id", model.save_id);
			cv.put("user_nm", model.user_nm);
			cv.put("mobile_nm", model.mobile_nm);
			cv.put("email_nm", model.email_nm);
			cv.put("userType_cd", model.userType_cd);
			cv.put("loginYn", model.loginYn);
			
			db.insert(sQLite.eLOGIN_TABLE_NM, null, cv);
		}
		else
		{				
			String strQuery = "update " 	+ sQLite.eLOGIN_TABLE_NM
					+ " set pwd = '" 		+ model.pwd + "'"
					+ " , auto_login = '" 	+ model.auto_login + "'"
					+ " , save_id = '" 		+ model.save_id + "'"
					+ " , user_nm = '" 		+ model.user_nm + "'"
					+ " , mobile_nm = '" 	+ model.mobile_nm + "'"
					+ " , email_nm = '" 	+ model.email_nm + "'"
					+ " , userType_cd = '" 	+ model.userType_cd + "'"
					+ " , loginYn = '" + model.loginYn + "'"
					+ " where id = '" + model.id + "'";
			
			db.execSQL(strQuery);
		}
		
		db.close();		
	}
	
	public void DeleteLoginData()
	{		
		SQLiteDatabase db = sQLite.getWritableDatabase();
		String strQuery = "DELETE FROM " + sQLite.eLOGIN_TABLE_NM;
		
		db.execSQL(strQuery);
		db.close();
	}	
	
	public ScheduleModel GetScheduleData()
	{
		SQLiteDatabase db 		= sQLite.getWritableDatabase();
		String[] 	arValues 	= { "", "", "", "", "" };
		String 		strQuery 	= "SELECT id, use, week, hour, minute";
		strQuery				+= " FROM " + sQLite.eALARM_TABLE_NM + " WHERE id = '1'";
		
		Cursor cursor 	= db.rawQuery(strQuery, null);
		arValues 		= GetCursorValues(cursor, arValues);
		
		cursor.close();
		db.close();
		
		ScheduleModel model = new ScheduleModel();
		model.id 		= arValues[0];
		model.use 		= arValues[1];
		model.week 		= arValues[2];
		model.hour 		= arValues[3];
		model.minute 	= arValues[4];
		
		return model;
	}
	
	public void SetScheduleData(ScheduleModel model)
	{
		ScheduleModel 	tempModel 	= GetScheduleData();
		SQLiteDatabase 	db 		= sQLite.getWritableDatabase();	
		
		if("".equals(tempModel.id))
		{
			ContentValues cv = new ContentValues();
			
			cv.put("id", "1");
			cv.put("use", model.use);
			cv.put("week", model.week);
			cv.put("hour", model.hour);
			cv.put("minute", model.minute);
			
			db.insert(sQLite.eALARM_TABLE_NM, null, cv);
		}
		else
		{
			String strQuery = "update " + sQLite.eALARM_TABLE_NM
					+ " set use = '" + model.use + "'"
					+ " , week = '" + model.week + "'"
					+ " , hour = '" + model.hour + "'"
					+ " , minute = '" + model.minute + "'"
					+ " where id = '1' ;";
			
			db.execSQL(strQuery);
		}
		
		db.close();
	}
	
	public AccessModel GetAccessData()
	{		
		SQLiteDatabase db 		= sQLite.getWritableDatabase();
		String[] 	arValues 	= { "", "", "", "", "" };
		String 		strQuery 	= "SELECT id, access_text, access_mov, IFNULL(push_type, ''), IFNULL(external_memory, '') ";
		strQuery				+= " FROM " + sQLite.eACCESS_TABLE_NM + " WHERE id = '1'";
		
		Cursor cursor 	= db.rawQuery(strQuery, null);
		arValues 		= GetCursorValues(cursor, arValues);
		
		cursor.close();
		db.close();
		
		AccessModel model 		= new AccessModel();
		model.id 				= arValues[0];
		model.access_text 		= arValues[1];
		model.access_mov 		= arValues[2];
		model.push_type 		= arValues[3];
		model.external_memory 	= arValues[4];
		
		return model;
	}
	
	public void SetAccessData(AccessModel model)
	{
		AccessModel 	tempModel 	= GetAccessData();
		SQLiteDatabase 	db 			= sQLite.getWritableDatabase();	
		
		if("".equals(tempModel.id))
		{
			ContentValues cv = new ContentValues();
			
			cv.put("id", "1");
			cv.put("access_text", model.access_text);
			cv.put("access_mov", model.access_mov);
			cv.put("push_type", model.push_type);
			cv.put("external_memory", model.external_memory);
			
			db.insert(sQLite.eACCESS_TABLE_NM, null, cv);
		}
		else
		{
			String strQuery = "update " + sQLite.eACCESS_TABLE_NM
					+ " set access_text = '" + model.access_text + "'"
					+ " , access_mov = '" + model.access_mov + "'"
					+ " , push_type = '" + model.push_type + "'"
					+ " , external_memory = '" + model.external_memory + "'"
					+ " where id = '1' ;";
			
			db.execSQL(strQuery);
		}
		
		db.close();
	}
	
	public AppEventModel GetAppEvent()
	{
		SQLiteDatabase db 		= sQLite.getWritableDatabase();
		String[] 	arValues 	= {"", "", "", "", ""};
		String 		strQuery 	= "SELECT event_seq, url, reg_date, try_count, max_count "
								+ " FROM " + sQLite.eAPP_EVENT_TABLE_NM
								+ " ORDER BY event_seq asc LIMIT 1";
		 
		Cursor cursor 	= db.rawQuery(strQuery, null);
		arValues 		= GetCursorValues(cursor, arValues);
		
		cursor.close();
		db.close();
		
		AppEventModel model 	= new AppEventModel();
		model.event_seq 		= "".equals(arValues[0]) ? 0 : Integer.parseInt(arValues[0]);
		model.url 				= arValues[1];
		model.reg_date 			= arValues[2];
		model.try_count 		= "".equals(arValues[0]) ? 0 : Integer.parseInt(arValues[3]);
		model.max_count 		= "".equals(arValues[0]) ? 0 : Integer.parseInt(arValues[4]);
		
		return model; 
	}
	
	public void InsertAppEvent(AppEventModel model)
	{
		SQLiteDatabase 	db = sQLite.getWritableDatabase();
		ContentValues 	cv = new ContentValues();
		
		cv.put("url", model.url);
		cv.put("reg_date", GetCurrentTime());
		cv.put("try_count", model.try_count);
		cv.put("max_count", model.max_count);
		
		db.insert(sQLite.eAPP_EVENT_TABLE_NM, null, cv);		
		db.close();
	}
	
	public void UpdateAppEvent(AppEventModel model)
	{
		SQLiteDatabase db = sQLite.getWritableDatabase();
		String strQuery = "update " + sQLite.eAPP_EVENT_TABLE_NM
				+ " set try_count = " + model.try_count
				+ " where event_seq = " + model.event_seq + " ;";
		
		db.execSQL(strQuery);
		db.close();
	}
	
	public void DeleteAppEvent(AppEventModel model)
	{
		SQLiteDatabase db = sQLite.getWritableDatabase();
		String strQuery = "DELETE FROM " + sQLite.eAPP_EVENT_TABLE_NM + " WHERE event_seq = " + model.event_seq + " ";
		
		db.execSQL(strQuery);
		db.close();
	}
}
