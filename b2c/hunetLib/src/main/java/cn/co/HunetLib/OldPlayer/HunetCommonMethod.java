package cn.co.HunetLib.OldPlayer;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Calendar;
import android.text.format.Time;

/***
 * @author hyoungil
 * 공용 매소드 클래스
 * 작업자 : 장형일
 * 작업일 : 2011.05
 */
public class HunetCommonMethod {

	public HunetCommonMethod() {
		
	}
    
    /*
	 * 시간 관련 함수.
	 */
		
	/**
	 * 현재 날짜와 입력 문자열 시간 비교
	 * @param dateString 예)2010-10-10
	 * @return 0보다 클때 : 입력 문자열이 현재 날짜보다 나중일때, 0 : 입력 문자열이 현재 날짜와 같을 때, 0보다 작을 때 : 입력 문자열이 현재 날짜보다 앞 일때 
	 */
	public static int CompareTimeWithToday(String dateString)
	{
		Calendar date = Calendar.getInstance();
		Time newDateTime = new Time();
		Time todayTime = new Time();
		
		newDateTime.set(Integer.parseInt(dateString.substring(8, 10)), Integer.parseInt(dateString.substring(5, 7)), Integer.parseInt(dateString.substring(0, 4)));
		todayTime.set(date.get(Calendar.DAY_OF_MONTH), date.get(Calendar.MONTH)+1, date.get(Calendar.YEAR));
		
		return Time.compare(newDateTime, todayTime);
	}
	
	/***
	 * 숫자에 콤마 찍기
	 * @param str 
	 * @return 스트링형
	 */
	public static String FormatNumberWithComma(int number)
	{
		DecimalFormat df = new DecimalFormat("#,##0");
		DecimalFormatSymbols dfs = new DecimalFormatSymbols();
		dfs.setGroupingSeparator(',');
		df.setGroupingSize(3);
		df.setDecimalFormatSymbols(dfs);
		
		return df.format(number);
	}
	
	/***
	 * 동영상 Mark값을 시간으로 변경
	 * @param Mark
	 * @return 시,분,초 스트링
	 */
    public static String MarkToTimeString(int Mark)
    {
    	int intTime = Mark/1000;
		int time=0, min=0, sec=0;
		int inputSec = intTime;
		time = inputSec/3600;
		min = inputSec%3600/60;
		sec = inputSec%3600%60%60;
		
		String sTime = String.format("%s:%s:%s", UtilPlayer.fixDigit(time), UtilPlayer.fixDigit(min), UtilPlayer.fixDigit(sec));
		return sTime;
    }
    
    /***
     * 동영상 Mark값을 시간으로 변경
     * @param Mark 
     * @return 시,분 스트링
     */
    public static String MarkToTimeString2(int Mark)
    {
    	int intTime = Mark/1000;
		int time=0, min=0;
		int inputSec = intTime;
		time = inputSec/3600;
		min = inputSec%3600/60;
		
		String sTime = String.format("%s:%s", UtilPlayer.fixDigit(time), UtilPlayer.fixDigit(min));
		return sTime;
    }
}
