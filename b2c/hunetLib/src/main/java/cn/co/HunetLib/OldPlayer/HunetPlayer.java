package cn.co.HunetLib.OldPlayer;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import cn.co.HunetLib.Common.AppMain;
import cn.co.HunetLib.R;
import cn.co.HunetLib.Common.HunetWebChromeClient;
import android.app.*;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnVideoSizeChangedListener;
import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.TranslateAnimation;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.*;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.media.AudioManager;

/**
 * 강의관련 HTTP 통신. 플레이정보
 * 작업자 : 장형일
 * 작업일 : 2011.11
 * mlc 플레이어 인앱 수정. 2011.11.11 
 * HLearderShip 타입 동영상 레이아웃 추가(풀영상+목차) 2012.01.28
 * mlc 플레이어 호환성 작업. 2012.02.20
 */
public class HunetPlayer extends Activity {
	/** Called when the activity is first created. */
	
	private ProgressThread progressThread = null;
	private DisplayMetrics metrics = new DisplayMetrics();
	
	private FrameLayout totalFrame;               //플레이어 전체 프레임레이아웃
	private LinearLayout layoutVideo;           //ㄴ동영상부(영상+목차) 레이아웃
	private VideoViewPlayer video;                //ㄴ영상 레이아웃
	private LinearLayout layoutMain;              //PT 레이아웃
	private LinearLayout layoutHtmlWeb;           //ㄴPT 레이아웃 웹뷰
	private WebView wbHtml;                       //ㄴPT웹 레이아웃
	private LinearLayout vcontrol;                //컨트롤 레이아웃
	private ImageButton btnMediaPlay;             //ㄴ재생
	private ImageButton btnMediaBack;             //ㄴ뒤로
	private ImageButton btnMediaForward;          //ㄴ앞으로
	private ImageButton btnMediaFull;             //ㄴ영상확대
	private ImageButton btnPTFull;                //ㄴPT확대
	private ImageButton btnSound;                 //ㄴ사운드
	private ImageButton btnList;                  //ㄴ목차(선택) 보여주기
	
	private SeekBar seekBar = null;               //ㄴ슬라이드

	private TextView txtTimePosition = null;      //ㄴ슬라이드 시간
	private TextView txtTotalTime;                //ㄴ영상전체시간
    
	private LinearLayout layoutIndexBG;           //목차(선택) 레이아웃
	private LinearLayout layoutIndex;             //ㄴ목차
	private ImageButton btnListHidden;            //ㄴㄴ목차 감추기
	private ScrollView scrollListView;            //목차(뷰) 스크롤
	private LinearLayout layoutListView;          //목차(뷰) 레이아웃
	
	private int mPosition=0;                      //영상 위치 마크값
	private long lastTouchTime = -1;              //더블클릭 인지 변수
	private int touchXPos = 0;                    //터치시, X 좌표
	private int touchYPos = 0;                    //터치시, Y 좌표
	
	private LinearLayout layoutSound;             //사운드
	private LinearLayout volume_bar;              //볼륨바 레이아웃
    private LinearLayout volume_ctr;              //볼륨포인트 레이아웃
    private ImageView volume_point;               //볼륨포인트
    
    private LinearLayout layoutMenu;              //메뉴 레이아웃
	
	TranslateAnimation ani = null;

	Hunet_http_Contents_Cls content = new Hunet_http_Contents_Cls();
	
	private String course_cd  = "";
	private String chapter_no = "";
	private int frame_no = 0;
	private int take_course_seq = 0;
	private int progress_no = 0;
	private String user_id = "";
	private int course_type = 0;
	private String reviewEndDate = "";
	
	private String contents_url = "";             //영상 url 
	private int lastMarkNo = 0;                   //최종 진도 위치
	private boolean blPtMov = true;               //Pt있는 영상 
	private boolean blListMov = false;            //목차있는 영상
	private boolean blProgressRestrict = false;   //진도이동제한 여부
	private int studySec = 0;               //기존학습시간
	private int mobileStudySec = 0;               //모바일 기존학습시간
	
	public static Dialog loading;                 //영상 로딩 메세지용 변수
	
	private ArrayList<MarkData> viewList = null;
	
	int movWidth;
	int movHeight;
	
	int laterViewSec;    //다음 재생 위치, 앱 비활성화시 셋팅. 다시 활성화시 이어보기

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mlcplayer);
        
        SetLayout();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        SetZOrder();
        
        SetInit();      //초기화
        seekBar.setEnabled(!blProgressRestrict);
        SetVideo();     //동영상 셋팅
        SetList();      //목차 셋팅
        if(take_course_seq == 0) blPtMov = false;
        SetPT();        //PT 셋팅
        SetNotPtMov();  //PT없는 영상 설정
    }
    
    void SetZOrder()
    {
    	layoutMain.bringToFront();
    	vcontrol.bringToFront();
    	layoutSound.bringToFront();
    	layoutIndexBG.bringToFront();
    	layoutIndex.bringToFront();
    	layoutMenu.bringToFront();
    }
    
    protected void SetLayout() {
		
    	loading = HunetAlertMethod.AlertDialogForIng(this, 0);

		if(totalFrame == null)
			totalFrame = (FrameLayout) findViewById(R.id.totalFrame);
		if(video == null)
			video = (VideoViewPlayer) findViewById(R.id.VideoViewPlayer);
		if(vcontrol == null)
			vcontrol = (LinearLayout) findViewById(R.id.vcontrol);
		if(btnMediaPlay == null)
			btnMediaPlay = (ImageButton) findViewById(R.id.btnMediaPlay);
		if(btnMediaBack == null)
			btnMediaBack = (ImageButton) findViewById(R.id.btnMediaBack);
		if(btnMediaForward == null)
			btnMediaForward = (ImageButton) findViewById(R.id.btnMediaForward);
		if(btnMediaFull == null)
			btnMediaFull = (ImageButton) findViewById(R.id.btnMediaFull);
		if(btnPTFull == null)
			btnPTFull = (ImageButton) findViewById(R.id.btnPTFull);
		if(btnSound == null)
			btnSound = (ImageButton) findViewById(R.id.btnSound);
		if(txtTimePosition == null)
			txtTimePosition = (TextView) findViewById(R.id.txtTimePosition);
		if(txtTotalTime == null)
			txtTotalTime = (TextView) findViewById(R.id.txtTotalTime);
		if(layoutVideo == null)
			layoutVideo = (LinearLayout) findViewById(R.id.layoutVideo);
		if(layoutMain == null)
			layoutMain = (LinearLayout) findViewById(R.id.layoutMain);
		if(layoutHtmlWeb == null)
			layoutHtmlWeb = (LinearLayout) findViewById(R.id.layoutHtmlWeb);
		if(layoutIndexBG == null)
			layoutIndexBG = (LinearLayout) findViewById(R.id.layoutIndexBG);
		if(layoutIndex == null)
			layoutIndex = (LinearLayout) findViewById(R.id.layoutIndex);
		if(scrollListView == null)
			scrollListView = (ScrollView) findViewById(R.id.scrollListView);
		if(layoutListView == null)
			layoutListView = (LinearLayout) findViewById(R.id.layoutListView);
		if(btnList == null)
			btnList = (ImageButton) findViewById(R.id.btnList);
		if(btnListHidden == null)
			btnListHidden = (ImageButton) findViewById(R.id.btnListHidden);
		if(wbHtml == null)
			wbHtml = (WebView)findViewById(R.id.wvHtml);
		if(seekBar == null)
        	seekBar = (SeekBar) findViewById(R.id.sbBar);
		if(layoutSound == null)
			layoutSound = (LinearLayout) findViewById(R.id.layoutSound);
		if(volume_bar == null)
			volume_bar = (LinearLayout) findViewById(R.id.volume_bar);
		if(volume_ctr == null)
			volume_ctr = (LinearLayout) findViewById(R.id.volume_ctr);
		if(volume_point == null)
			volume_point = (ImageView) findViewById(R.id.volume_point);
		if(layoutMenu == null)
			layoutMenu = (LinearLayout) findViewById(R.id.layoutMenu);
	}
    
    @Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
		SetControl();   //컨트롤 셋팅
		
		if(laterViewSec > 0) //비활성화 후 다시 활성화 시 이어보기
		{
			video.seekTo(laterViewSec);
		}
		
        if(progressThread == null)
			progressThread = new ProgressThread(video, txtTimePosition, seekBar, vcontrol, wbHtml, layoutListView, layoutIndex, layoutSound, layoutMenu, viewList, course_cd, take_course_seq, frame_no, chapter_no, progress_no, user_id, lastMarkNo, studySec, mobileStudySec, blPtMov, blListMov, this);
		
		if(progressThread.isAlive() == false){
			progressThread.start();
		}
	}
    
    @Override
	protected void onPause() {
		// TODO Auto-generated method stub
    	super.onPause();
    	
		try {
			laterViewSec = video.getCurrentPosition();
			progressThread.stop();
		}
		catch (Exception e) {
			// TODO: handle exception
		}
	}

    protected void SetInit()
    {
    	laterViewSec = 0;
    	
	    try
	    {
	    	Intent intent = getIntent();
		    Bundle bundle = intent.getExtras();
		    
		    contents_url = bundle.getString("url");
		    
		    if(contents_url.equals(""))
		    {
			    course_cd = AppMain.getCourse_cd();
			    chapter_no = AppMain.getChapter_no();
			    frame_no = AppMain.getFrame_no();
			    take_course_seq = Integer.parseInt(AppMain.getTake_course_seq());
			    user_id = AppMain.getId();
			    
			    ProgressData progressInfo = content.SelectProgressInfo(course_cd, chapter_no, frame_no, take_course_seq);
			    
			    course_type = progressInfo.get_CourseData().getCourseType();
			    reviewEndDate = progressInfo.get_CourseData().getReviewEndDate();
			    
			    //진도 정보
			    progress_no = progressInfo.getProgressNo();
			    //영상 이어보기 변수 설정
			    lastMarkNo = progressInfo.getLastMarkNo();
			    //목차 있는 영상 변수 설정
			    blListMov = progressInfo.isBlListMov();
			    //진도이동제한
			    blProgressRestrict = progressInfo.isBlProgressRestrict();
			    //기존학습시간
			    studySec = progressInfo.getStudySec();
			    //모바일 기존학습시간
			    mobileStudySec = progressInfo.getMobileStudySec();
			    
			    AppMain.setCourse_type(course_type);
			    AppMain.setReviewEndDate(reviewEndDate);
			    AppMain.setProgress_no(progress_no);
			    
			    SetMovInfo();    //영상정보 설정
		    }
	    }
	    catch(Exception e)
	    {
	    	int compareTiem = -1;
	    	
	    	try
	    	{
	    		compareTiem = HunetCommonMethod.CompareTimeWithToday(AppMain.getReviewEndDate());
	    	}
	    	catch (Exception e1) {
				// TODO: handle exception
			}
	    	
	    	if(AppMain.getId().equals("") || compareTiem < 0)
	    	{
	    		HunetAlertMethod.AlertMessage("잘못된 접근입니다.", this, "재생");
	    		finish();
	    	}
	    	else
	    	{
	    		course_cd = AppMain.getCourse_cd();
			    chapter_no = AppMain.getChapter_no();
			    frame_no = AppMain.getFrame_no();
			    take_course_seq = Integer.parseInt(AppMain.getTake_course_seq());
			    progress_no = AppMain.getProgress_no();
			    user_id = AppMain.getId();
			    
			    course_type = AppMain.getCourse_type();
			    
			    SetMovInfo();    //영상정보 설정
	    	}
	    }
	    
	    wbHtml.setWebViewClient(new WebViewClient());
	    wbHtml.setWebChromeClient(new HunetWebChromeClient());
    }

    /***
     * 동영상 컨텐츠 정보, 진도 정보
     */
    protected void SetMovInfo()
    {
    	ContentsData info = new ContentsData();
    	
    	//컨텐츠 정보 조회.
	    if(course_type == 3)
	    {
	    	info = content.StudyContentView(course_cd, chapter_no, frame_no);
	    	contents_url = info.getContents_url();
	    }
	    else
	    {
	    	info = content.StudyFrameView(course_cd, chapter_no, frame_no);
	    	contents_url = info.getMobile_mov_url();
	    }
    }
    
    /***
     * 영상 부
     */
    protected void SetVideo()
    {
    	loading.show();
    
        String url = "";
        
//        if(!contents_url.contains("_definst_")) contents_url = contents_url.replace("mp4://", "_definst_/mp4:");
//        url = contents_url.replace("http", "rtsp").replace("/playlist.m3u8", "");
        url = contents_url;
        Uri uri = Uri.parse(url);
        
        video.setVideoURI(uri);
        video.requestFocus();
       
        //영상부 터치 시.
        video.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event)
			{
				// TODO Auto-generated method stub
				/*Switching movie to full sizing screen..*/
				if(event.getAction() == MotionEvent.ACTION_DOWN){
					long thisTime = System.currentTimeMillis();
					
					if(thisTime -lastTouchTime < 250){//double touch
						lastTouchTime = -1;
				
						if(blPtMov) SetVideoFull();
					}
					else
					{
						lastTouchTime = thisTime;
					}
				}
				
				return false;
			}
		});
        
        //영상 정보 셋팅
        video.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
			
			@Override
			public void onPrepared(MediaPlayer mp) {
				// TODO Auto-generated method stub
				
				Log.i("", "onPrepared");
				
				if(video.getDuration() == 0) //기기 특성상 0으로 인식 시.(예:젤리빈 OS 사용 기기)
				{
					mp.setOnVideoSizeChangedListener(new OnVideoSizeChangedListener() {

						@Override
						public void onVideoSizeChanged(MediaPlayer arg0, int arg1,
								int arg2) {
							// TODO Auto-generated method stub
	
							loading.dismiss();
							seekBar.setMax(arg0.getDuration());
							txtTotalTime.setText(HunetCommonMethod.MarkToTimeString(arg0.getDuration()));
						}
					});
				}
				else
				{
					loading.dismiss();
					seekBar.setMax(video.getDuration());
					txtTotalTime.setText(HunetCommonMethod.MarkToTimeString(video.getDuration()));
				}
				
				video.start();
			}
		});
        
        //영상 끝, 자동종료
        video.setOnCompletionListener(new OnCompletionListener() {

			@Override
			public void onCompletion(MediaPlayer mp) {
				// TODO Auto-generated method stub
				finish();
			}
        });
    }
    
    protected void SetList()
    {
    	//목차 조회.
    	viewList = content.MarkList(course_cd, chapter_no, frame_no, take_course_seq, this);
    	MarkData view = new MarkData();
    	
    	if(viewList.size() == 0)
    	{
    		blPtMov = false;     //Pt 없는 영상 판단
    	}
    	
    	if(layoutIndex.getChildCount() > 0)
    	{
    		layoutIndex.removeAllViews();
    		layoutListView.removeAllViews();
    	}
    	
    	int notPtCount = 0;      //PT주소 없는 목차 카운트
    	for(int i=0; i < viewList.size(); i++)
    	{
    		view = viewList.get(i);
    		
    		LinearLayout ll_Index = new LinearLayout(this);
    		final ImageView imgIndexView = new ImageView(this);      //목차 봤는지, 안 봤는지 구분 이미지
    		String markNm = "";                                      //목차명
    		TextView tvIndex = new TextView(this);                   //목차 텍스트뷰
    		
    		//구분 이미지 설정
    		imgIndexView.setLayoutParams(new Gallery.LayoutParams(27, 27));
    		if(view.getProgress_yn().equals("Y"))
    			imgIndexView.setBackgroundResource(R.drawable.icon_check_color);
    		else
    			imgIndexView.setBackgroundResource(R.drawable.icon_check_grey);
    		
    		//목차 텍스 트 설정
    		tvIndex.setLayoutParams(new Gallery.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
    		tvIndex.setTextSize(18);
    		if(!view.getMark_nm().equals(""))
    		{
    			markNm = "[" + HunetCommonMethod.MarkToTimeString2(view.getMark_value()) + "]";
    			tvIndex.setText(markNm + " " + view.getMark_nm());
    			tvIndex.setTextColor(Color.parseColor("#333333"));
    		}
    		tvIndex.setPadding(5, 0, 0, 0);
    		
    		//목차 선택 시, 영상 이동
    		final int markNo = view.getMark_no();
    		final int markValue = view.getMark_value();
    		tvIndex.setOnClickListener(
    				new OnClickListener()
    				{
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							layoutIndexBG.setVisibility(View.INVISIBLE);
							
							if(!blProgressRestrict) video.seekTo(markValue);  //영상 이동
							
							imgIndexView.setBackgroundResource(R.drawable.icon_check_color);
						}
    				}
    		);
    		
    		ll_Index.setLayoutParams(new Gallery.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
    		ll_Index.setOrientation(LinearLayout.HORIZONTAL);
    	
    		ll_Index.addView(imgIndexView);
    		ll_Index.addView(tvIndex);
    		
    		if( i == 0)
    			ll_Index.setPadding(0, 14, 0, 0);
    		else
    			ll_Index.setPadding(0, 32, 0, 0);
    		
    		layoutIndex.addView(ll_Index);
    		
    		if(view.getPpt_url().equals("notPt"))
    			notPtCount = notPtCount + 1;
    		
    		//목차(뷰) 리스트 추가
    		TextView tvIndex2 = new TextView(this);
    		tvIndex2.setLayoutParams(new Gallery.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
    		tvIndex2.setTextSize(10);
    		String indexNm;
    		if(!view.getMark_nm().equals(""))
    		{
    			SpannableStringBuilder str = new SpannableStringBuilder();
    			SpannableString tvSpan;
    			
    			indexNm = markNm + " " + view.getMark_nm();
    			if(indexNm.length() > 30)
    				indexNm = indexNm.substring(0, 29) + "...";

    			tvIndex2.setText(indexNm);
    			
    			if(markNo == lastMarkNo)
    				tvIndex2.setTextColor(Color.parseColor("#ff0000"));
    			else
    				tvIndex2.setTextColor(Color.parseColor("#000000"));
    			
    			tvSpan = new SpannableString(tvIndex2.getText());
    			tvSpan.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), 0, tvSpan.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
    			str.append(tvSpan);
    			tvIndex2.setText(str);
    			str.clear();
    		}
    		layoutListView.addView(tvIndex2);
    		
    		//영상 이어보기
    		if(markNo == lastMarkNo)
    		{
    			if(i == viewList.size() - 1) video.seekTo(0);  //처음으로
    			else video.seekTo(view.getMark_value());  //영상 이동
    		}
    	}
    	if(notPtCount == viewList.size())
    		blPtMov = false;     //Pt 없는 영상 판단
    	
        layoutIndexBG.setVisibility(View.INVISIBLE);
        
        //목차 보여주기, 감추기.
        btnList.setOnClickListener(
        		new OnClickListener()
        		{
					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
					
						vcontrol.setVisibility(View.INVISIBLE);
						layoutSound.setVisibility(View.INVISIBLE);
						layoutMenu.setVisibility(View.INVISIBLE);
						
						if(blPtMov) SetPTFull();
						ani = new TranslateAnimation(-400,0,0,0);
				        ani.setDuration(500);
				        layoutIndexBG.setAnimation(ani);
				        layoutIndexBG.setVisibility(View.VISIBLE);
					}
        		}
        );
        
        //목차 보여주기, 감추기.
        btnListHidden.setOnClickListener(
        		new OnClickListener()
        		{
					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						if(blPtMov) SetPTFull();
				        layoutIndexBG.setVisibility(View.INVISIBLE);
					}
        		}
        );
        
        //목차(뷰)에서 소리 조절
        layoutListView.setOnTouchListener(new OnTouchListener() {
        	
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				
				if(event.getAction() == MotionEvent.ACTION_DOWN)
				{
					touchXPos = (int)event.getX();
					touchYPos = (int)event.getY();
					
					long thisTime = System.currentTimeMillis();
					
					if(thisTime -lastTouchTime < 250){//double touch
						lastTouchTime = -1;
						
						SetPTFull();
						ani = new TranslateAnimation(-400,0,0,0);
				        ani.setDuration(500);
				        layoutIndexBG.setAnimation(ani);
				        layoutIndexBG.setVisibility(View.VISIBLE);
					}
					else
					{
						lastTouchTime = thisTime;
						
						if(layoutListView.getChildCount() <= 11)
							SetCTLVisible();
					}
				}
				else if(event.getAction() == MotionEvent.ACTION_MOVE)
				{
					int moveXPos = (int)event.getX();
					int moveYPos = (int)event.getY();
					
					if(touchYPos - moveYPos < 2 || touchYPos - moveYPos > -2)
					{
						int currentVolum = getVolume();
						
						if(moveXPos - touchXPos > 40 && currentVolum < 15)
						{
							currentVolum = currentVolum + 1;
							SetVolume(currentVolum);
						}
						else if(moveXPos - touchXPos < -40 && currentVolum > 0)
						{
							currentVolum = currentVolum - 1;
							SetVolume(currentVolum);
						}
						
						touchXPos = moveXPos;
						touchYPos = moveYPos;
					}
				}
				
				return true;
			}
		});
    }
    
    /***
     * 컨트롤 부
     */
    protected void SetControl()
    {
    	/*If selected all frame then controls will be responded*/
        totalFrame.setOnTouchListener(new OnTouchListener() {
        	
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				
				if(event.getAction() == MotionEvent.ACTION_DOWN)
				{
					SetCTLVisible();
					touchXPos = (int)event.getX();
					touchYPos = (int)event.getY();
				}
				else if(event.getAction() == MotionEvent.ACTION_MOVE)
				{
					int moveXPos = (int)event.getX();
					int moveYPos = (int)event.getY();
					
					if(touchYPos - moveYPos < 2 || touchYPos - moveYPos > -2)
					{
						int currentVolum = getVolume();
						
						if(moveXPos - touchXPos > 40 && currentVolum < 15)
						{
							currentVolum = currentVolum + 1;
							SetVolume(currentVolum);
						}
						else if(moveXPos - touchXPos < -40 && currentVolum > 0)
						{
							currentVolum = currentVolum - 1;
							SetVolume(currentVolum);
						}
						
						touchXPos = moveXPos;
						touchYPos = moveYPos;
					}
				}
				
				return true;
			}
		});
        
        seekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
				if(!blProgressRestrict)
				{
					video.seekTo(seekBar.getProgress());
					video.start();
				}
			}
			
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
				video.pause();
			}
			
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				// TODO Auto-generated method stub
			}
		});
        
        /*when pressed seekbar */
        /*seekBar.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				if(video.isEnabled()){
					video.seekTo(seekBar.getProgress());
				}
				return false;
			}
		});*/
        
        txtTimePosition.setText("00:00:00");
        
        /*play button*/
        btnMediaPlay.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_pause));
        btnMediaPlay.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
 				if(video.isPlaying())
				{
					video.pause();
					btnMediaPlay.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_play));
				}
 				else
 				{
					video.start();
					btnMediaPlay.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_pause));
				}
			}
		});
        btnMediaPlay.setOnTouchListener(new OnTouchListener() {
        	
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				
				if(video.isPlaying())
				{
					btnMediaPlay.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_pause_on));
				}
 				else
 				{
					btnMediaPlay.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_play_on));
				}
					
				return false;
			}
		});
        
        /*back*/
        btnMediaBack.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_rewind));
    	btnMediaBack.setOnClickListener(new OnClickListener() {
    		
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
								
 				if(video.isPlaying()){
 				      mPosition = video.getCurrentPosition();
 				      video.seekTo(mPosition-10000);
 				}
 				else {
 				      mPosition = video.getCurrentPosition();
 				      video.seekTo(mPosition-10000);
 				}
			}
		});
    	btnMediaBack.setOnTouchListener(new OnTouchListener() {
        	
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				
				if(event.getAction() == MotionEvent.ACTION_DOWN)
					btnMediaBack.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_rewind_on));
				else
					btnMediaBack.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_rewind));
					
				return false;
			}
		});
    	
    	/*Forward*/
    	btnMediaForward.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_forward));
    	btnMediaForward.setOnClickListener(new OnClickListener() {
    		
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
								
				if(!blProgressRestrict)
				{
	 				if(video.isPlaying()){
	 				     mPosition = video.getCurrentPosition();
	 				     video.seekTo(mPosition+10000);
	 				}
	 				else {
	 				     mPosition = video.getCurrentPosition();
	 				     video.seekTo(mPosition+10000);
	 				}
				}
			}
		});
    	btnMediaForward.setOnTouchListener(new OnTouchListener() {
        	
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				
				if(event.getAction() == MotionEvent.ACTION_DOWN)
					btnMediaForward.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_forward_on));
				else
					btnMediaForward.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_forward));
					
				return false;
			}
		});
    	
    	/*MediaFull*/
    	btnMediaFull.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_movie));
    	btnMediaFull.setOnClickListener(new OnClickListener() {
    		
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(layoutHtmlWeb.getLayoutParams().width == getDiviceDisplayWidth())
					SetPTFull();
				
				SetVideoFull();
			}
		});
    	btnMediaFull.setOnTouchListener(new OnTouchListener() {
        	
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				
				if(layoutHtmlWeb.getLayoutParams().width == getDiviceDisplayWidth())
					btnMediaFull.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_movie_s_on));
				else
					btnMediaFull.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_movie_on));
					
				return false;
			}
		});
    	
    	/*PTFull*/
    	btnPTFull.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_ppt));
    	btnPTFull.setOnClickListener(new OnClickListener() {
    		
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(video.getWidth() == getDiviceDisplayWidth())
					SetVideoFull();
				
				SetPTFull();
			}
		});
    	btnPTFull.setOnTouchListener(new OnTouchListener() {
        	
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				
				if(video.getWidth() == getDiviceDisplayWidth())
					btnPTFull.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_ppt_s_on));
				else
					btnPTFull.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_ppt_on));
					
				return false;
			}
		});
    	
    	btnSound.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_sound));
    	btnSound.setOnClickListener(new OnClickListener() {
    		
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(layoutSound.getVisibility() == View.INVISIBLE)
				{
					if(vcontrol.getVisibility() == View.INVISIBLE)
						vcontrol.setVisibility(View.VISIBLE);
					
					layoutSound.setVisibility(View.VISIBLE);
					SetVolumeMov(); //볼륨 셋팅
				}
				else
					layoutSound.setVisibility(View.INVISIBLE);
			}
		});
    	btnSound.setOnTouchListener(new OnTouchListener() {
        	
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				
				if(event.getAction() == MotionEvent.ACTION_DOWN)
					btnSound.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_sound_on));
				else
					btnSound.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_sound));
					
				return false;
			}
		});
    	
    	//해상도 720 이상 일 시(갤럭시 탭 10.1), 소리조절 버튼 안 보여준다. 레이아웃 공용으로 조건 잡기 어려움으로 인해..
    	if(getDiviceDisplayHeight() > 720) btnSound.setVisibility(View.INVISIBLE);
    }
    
    /***
     * PT 부
     */
    protected void SetPT()
    {
    	wbHtml.getSettings().setJavaScriptEnabled(true);
    	
    	if(lastMarkNo == 0 && viewList.size() > 0)
    	{
    		String strImg = "";
			try {
				strImg = URLEncoder.encode(viewList.get(0).getPpt_url(), "EUC_KR");
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		wbHtml.loadUrl("http://study.hunet.co.kr/mobile/mobilePtGate.aspx?imgPtUrl=" + strImg);
    	}
    	else if(lastMarkNo > 0)
    	{
	    	for(int i=0; i < viewList.size(); i++)
	    	{
	    		MarkData view = viewList.get(i);
	    		
	    		if(lastMarkNo == view.getMark_no())
	    		{
	    			String strImg = "";
					try {
						strImg = URLEncoder.encode(view.getPpt_url(), "EUC_KR");
					} catch (UnsupportedEncodingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	    			wbHtml.loadUrl("http://study.hunet.co.kr/mobile/mobilePtGate.aspx?imgPtUrl=" + strImg);
	    			break;
	    		}
	    	}
    	}
        
        wbHtml.setOnTouchListener(new OnTouchListener() {
        	
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				
				if(event.getAction() == MotionEvent.ACTION_DOWN){
					touchXPos = (int)event.getX();
					touchYPos = (int)event.getY();
					
					long thisTime = System.currentTimeMillis();
					
					if(thisTime -lastTouchTime < 250){//double touch
						lastTouchTime = -1;
						
						SetPTFull();
					}
					else
					{
						lastTouchTime = thisTime;
						
						SetCTLVisible(); //totalFrame 클릭이벤트로, PT 부분이 적용이 안되어, 추가
					}
				}
				else if(event.getAction() == MotionEvent.ACTION_MOVE)
				{
					int moveXPos = (int)event.getX();
					int moveYPos = (int)event.getY();
					
					if(touchYPos - moveYPos < 2 || touchYPos - moveYPos > -2)
					{
						int currentVolum = getVolume();
						
						if(moveXPos - touchXPos > 40 && currentVolum < 15)
						{
							currentVolum = currentVolum + 1;
							SetVolume(currentVolum);
						}
						else if(moveXPos - touchXPos < -40 && currentVolum > 0)
						{
							currentVolum = currentVolum - 1;
							SetVolume(currentVolum);
						}
						
						touchXPos = moveXPos;
						touchYPos = moveYPos;
					}
				}
				
				lastTouchTime = System.currentTimeMillis();

				return false;
			}
		});
    }
    
    /***
     * 재생 컨트롤 보여주기, 안보여주기
     */
    protected void SetCTLVisible()
    {
    	if(vcontrol.getVisibility() == View.INVISIBLE){
    		ani = new TranslateAnimation(0,0,100,0);
            ani.setDuration(500);
            vcontrol.setAnimation(ani);
			vcontrol.setVisibility(View.VISIBLE);
			
			if(blPtMov || blListMov) layoutMenu.setVisibility(View.VISIBLE);
		}
		else{
			ani = new TranslateAnimation(0,0,0,100);
	        ani.setDuration(500);
	        vcontrol.setAnimation(ani);
			vcontrol.setVisibility(View.INVISIBLE);
			layoutSound.setVisibility(View.INVISIBLE);
			
			layoutMenu.setVisibility(View.INVISIBLE);
		}
    }
    
    /***
     * 영상 확대
     */
    protected void SetVideoFull()
    {
    	if(video.getWidth() == getDiviceDisplayWidth())
		{
			layoutVideo.setLayoutParams(new LinearLayout.LayoutParams(0, LayoutParams.FILL_PARENT, 205));
    		layoutHtmlWeb.setLayoutParams(new LinearLayout.LayoutParams(0, LayoutParams.FILL_PARENT, 329));
    		video.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
    		scrollListView.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
    		
    		video.setVideoAspect(movWidth, movHeight);
			
    		layoutHtmlWeb.setVisibility(View.VISIBLE);
			wbHtml.setVisibility(View.VISIBLE);
			
			btnMediaFull.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_movie));
		}
		else
		{
			layoutHtmlWeb.setLayoutParams(new LinearLayout.LayoutParams(0, 0));
			layoutVideo.setLayoutParams(new LinearLayout.LayoutParams(getDiviceDisplayWidth(), getDiviceDisplayHeight()));
			video.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
    		scrollListView.setLayoutParams(new LinearLayout.LayoutParams(0, 0));
			
    		movWidth = video.getWidth();
    		movHeight = video.getHeight();
			video.setVideoAspect(getDiviceDisplayWidth(), getDiviceDisplayHeight());
			
			layoutHtmlWeb.setVisibility(View.INVISIBLE);
			wbHtml.setVisibility(View.INVISIBLE);
			
			btnMediaFull.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_movie_s));
		}
    }
    
    /***
     * PT 확대
     */
    protected void SetPTFull()
    {
    	if(layoutHtmlWeb.getLayoutParams().width == getDiviceDisplayWidth())
		{
    		layoutVideo.setLayoutParams(new LinearLayout.LayoutParams(0, LayoutParams.FILL_PARENT, 205));
    		layoutHtmlWeb.setLayoutParams(new LinearLayout.LayoutParams(0, LayoutParams.FILL_PARENT, 329));
			
			btnPTFull.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_ppt));
		}
		else
		{
			layoutVideo.setLayoutParams(new LinearLayout.LayoutParams(0, 0));
    		layoutHtmlWeb.setLayoutParams(new LinearLayout.LayoutParams(getDiviceDisplayWidth(), getDiviceDisplayHeight()));
			
			btnPTFull.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_ppt_s));
		}
    	
    	wbHtml.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
    }
    
    /***
     * PT 없는 영상 일 시.
     */
    protected void SetNotPtMov()
    {
    	if(!blPtMov)
    	{
	    	SetVideoFull();
	    	btnMediaFull.setVisibility(View.INVISIBLE);
	    	btnPTFull.setVisibility(View.INVISIBLE);
	    	video.setOnTouchListener(null);
	    	SetCTLVisible();
    	}
    }
    
    /***
	 * 동영상 볼륨(미디어 볼륨 조절)
	 */
	private void SetVolumeMov()
	{		
		//현재볼륨, 볼륨 포인터 위치 설정
		volume_ctr.setPadding(0, 0, 0, getVolumePointY());
		setVolumeLayout(getVolume());
		
		//볼륨 포인터 이동 시
		volume_point.setOnTouchListener(
				new OnTouchListener() {

					@Override
					public boolean onTouch(View v, MotionEvent event) {
						// TODO Auto-generated method stub

						int keyAction = event.getAction();
						int y = (int)event.getY();
						switch (keyAction){
							case MotionEvent.ACTION_MOVE:
								
								int sel_volume = 0;  //설정 볼륨;
								
								//위치 = 레이아웃 paddingLeft
								int max = volume_bar.getHeight() - (volume_bar.getHeight() / 16);                               //맥스 범위
								int point_y = volume_ctr.getPaddingBottom(); //현재 위치
								int move_y = 0;                              //이동할 위치
								
								if(point_y > max) move_y = point_y;
								else move_y = point_y - y;
								
								if(move_y < 0) move_y = 0;
								
								if(move_y > max) move_y = max;
								
								sel_volume = getVolume(move_y);
								
								volume_ctr.setPadding(0, 0, 0, move_y);
								setVolumeLayout(sel_volume);
								SetVolume(sel_volume);
								
								break;
						}

						return true;
					}
				}
		);
	}
	
	protected void SetVolume(int currentVolum)
    {
    	((AudioManager)getSystemService(AUDIO_SERVICE)).
		setStreamVolume(AudioManager.STREAM_MUSIC, currentVolum, AudioManager.FLAG_SHOW_UI);
    }
    
    protected int getVolume()
    {
    	return ((AudioManager)getSystemService(AUDIO_SERVICE)).getStreamVolume(AudioManager.STREAM_MUSIC);
    }
	
	/***
	 * 볼륨 포인터 위치 구하기
	 * @return
	 */
	private int getVolumePointY()
	{
		int rtnResult = 0;
		int current_vol = getVolume();
		
		if(current_vol == 15)
			rtnResult = volume_bar.getHeight() - (volume_bar.getHeight() / 16);
		else
			rtnResult = (volume_bar.getHeight() / 16) * current_vol;
		
		return rtnResult;
	}
	
	/***
	 * 포인터 위치에 따른 볼륨 구하기
	 * @param move_x
	 * @return
	 */
	private int getVolume(int move_x)
	{
		int rtnResult = 0;
		
		rtnResult = Math.round(move_x / (volume_bar.getHeight() / 16));
		
		return rtnResult;
	}
	
	/***
	 * 볼륨 포인터 이동시, 레이아웃 셋팅
	 */
	private void setVolumeLayout(int volume)
	{
		if(volume_bar.getChildCount() > 0)
			volume_bar.removeAllViews();

		if(volume > 14)
		{
			ImageView ivTop = new ImageView(this);
			
			ivTop.setLayoutParams(new Gallery.LayoutParams(LayoutParams.WRAP_CONTENT, 4));
			ivTop.setBackgroundResource(R.drawable.green_top);
			volume_bar.addView(ivTop);
		}
		
		for(int i=1; i<=volume; i++)
		{
			ImageView ivBar = new ImageView(this);
			
			if(i == 15)
				ivBar.setLayoutParams(new Gallery.LayoutParams(LayoutParams.WRAP_CONTENT, 7));
			else
				ivBar.setLayoutParams(new Gallery.LayoutParams(LayoutParams.WRAP_CONTENT, ((volume_bar.getHeight() - (volume_bar.getHeight() / 16) - 11) / 13)));
			ivBar.setBackgroundResource(R.drawable.green_bar);
			volume_bar.addView(ivBar);
		}
		
		if(volume > 0)
		{
			ImageView ivBottom = new ImageView(this);
			
			ivBottom.setLayoutParams(new Gallery.LayoutParams(LayoutParams.WRAP_CONTENT, 4));
			ivBottom.setBackgroundResource(R.drawable.green_bottom);
			volume_bar.addView(ivBottom);
		}
	}
	
	//가로
	public int getDiviceDisplayWidth()
	{
		Display d = ((WindowManager)this.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();		
		return d.getWidth();
	}
	//세로
	public int getDiviceDisplayHeight()
	{
		Display d = ((WindowManager)this.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
		
		return d.getHeight();
	}
    
    /***
	 * 키 선택 이벤트
	 */
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		switch(keyCode)
		{
			case KeyEvent.KEYCODE_BACK:
				new AlertDialog.Builder(HunetPlayer.this)
				   .setTitle("플레이어 종료")
				   .setMessage("플레이어을 종료 하시겠습니까?")
				.setPositiveButton("종료", new DialogInterface.OnClickListener() {
						 						@Override
												public void onClick(DialogInterface dialog, int which) {
						 							finish();
						 						}
			          						})
			    .setNegativeButton("취소", new DialogInterface.OnClickListener() {
			          							@Override
												public void onClick(DialogInterface dialog, int which) {
			          							}
			    })
			    .show();
				return true;
			default:
				return false;
		}
	}

	@Override
	protected void onDestroy() 
	{
		if(progressThread != null)
			progressThread.Finish();
		
		super.onDestroy();
		
		AppMain.setCourse_cd("");
	    AppMain.setChapter_no("");
	    AppMain.setFrame_no(0);
	    AppMain.setTake_course_seq("0");
	    AppMain.setProgress_no(0);
	    AppMain.setCourse_type(0);
	    AppMain.setReviewEndDate("");
	}

}
