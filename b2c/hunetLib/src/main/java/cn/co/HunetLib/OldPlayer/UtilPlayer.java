package cn.co.HunetLib.OldPlayer;

import android.app.Application;
import android.app.Dialog;

/**
 * 강의관련 HTTP 통신. 플레이정보
 * 작업자 : 장형일
 * 작업일 : 2011.11
 * mlc 플레이어 인앱 수정. 2011.11.04
 */
public class UtilPlayer extends Application{

	public static Dialog loading;
	
	public static String fixDigit(int getInt){
		String _str = "";
			if(getInt < 10)
				_str = "0" + getInt;
			else
				_str = String.valueOf(getInt);
				
		return _str;
	}
}
