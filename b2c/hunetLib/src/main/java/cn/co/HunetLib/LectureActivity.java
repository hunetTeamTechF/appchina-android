package cn.co.HunetLib;

import java.net.URLDecoder;

import cn.co.HunetLib.Base.TabWebviewActivity;
import cn.co.HunetLib.Company.Company;
import android.content.Intent;
import android.os.Bundle;

public class LectureActivity extends TabWebviewActivity
{	
	private int m_SubSelectId;
	private String m_NextParam = "";
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
	    super.onCreate(savedInstanceState);
	    	    
	    setSelectTab(Company.eTAB_LECTURE);
	    InitLecture(this.getIntent());
	}
	
	@Override
	public void onNewIntent(Intent intent)
	{
		super.onNewIntent(intent);
		
		InitLecture(intent);
	}
	
	private void InitLecture(Intent intent)
	{
		Bundle bundle = intent.getExtras();
		m_SubSelectId = Integer.parseInt(getBundleValue(bundle, "TAB_ID", "0"));
		m_NextParam = URLDecoder.decode(getBundleValue(bundle, "NEXT_PARAM", ""));
		
		if(m_SubSelectId > 0)
			setFirstResume(true);
	}
	
	@Override
	protected void onFirstResume()
	{
		String url = getCompany().getUrl(getCompany().getTabUrlType(m_SubSelectId > 0 ? m_SubSelectId : m_TabControl.getSelectTab()));
		
		if(m_NextParam != "")
			url += "&" +  m_NextParam;
		
		m_SubSelectId = 0;
		
		if("".equals(url))
			return;
		
		setLoadUrl(url);
	}
}
