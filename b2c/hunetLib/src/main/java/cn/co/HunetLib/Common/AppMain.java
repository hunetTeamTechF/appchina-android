package cn.co.HunetLib.Common;

import android.app.Activity;
import android.content.Context;
import android.os.Environment;
import android.view.inputmethod.InputMethodManager;
import cn.co.HunetLib.Company.Company;

public class AppMain 
{
	public static Company CompanyInfo;
	
	public final static String eACTION_BOOT_COMPLETED 	= "android.intent.action.BOOT_COMPLETED";
	public final static String eACTION_ALARM_SETTING 	= "kr.co.HunetLib.action.ALARAM_SETTING";
	public final static String eACTION_NETWORK_CHANGE 	= "android.net.conn.CONNECTIVITY_CHANGE";
	
	public final static int eNOTIFICATION_ALARM			= 1000;	
			
	public static String GetExternalDir()
	{
		return Environment.getExternalStorageDirectory() + "/hunet_mba/";
	}
	
	public static void setSoftKeyboardVisible(Activity activity, boolean show)
	{
		InputMethodManager inputManager = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE); 
		
		if(show)
			inputManager.showSoftInput(activity.getCurrentFocus(), InputMethodManager.SHOW_FORCED);
		else
			inputManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
	}
	
	
	/*
	 * kr.hunet.HunetLib.OldPlayer 삭제시 지울 내용들
	 * res/drawable-ldpi 이미지 폴더도 삭제 할것
	 * */
	
	public static String getId()
	{
		return CompanyInfo.LoginModel.id;
	}
	public static String getPw() { return CompanyInfo.LoginModel.pwd; }
	
	public static int getCompany_seq() { return CompanyInfo.getSeq(); }
	
	private static String NetworkChangeYn = "";
	public static void setNetworkChangeYn(String yn){ NetworkChangeYn = yn; }
	public static String getNetworkChangeYn() { return NetworkChangeYn; }
	
	private static String Course_cd = "";
	public static String getCourse_cd() { return Course_cd; }
	public static void setCourse_cd(String cd){ Course_cd = cd; }

	private static String Chapter_no = "";
	public static String getChapter_no() { return Chapter_no; }
	public static void setChapter_no(String no){ Chapter_no = no; }
	
	private static int Frame_no = 0;
	public static int getFrame_no() { return Frame_no; }
	public static void setFrame_no(int no){ Frame_no = no; }
	
	private static String Take_course_seq = "";
	public static String getTake_course_seq() { return Take_course_seq; }
	public static void setTake_course_seq(String seq) { Take_course_seq = seq; }
	
	private static int Course_type = 0;
	public static void setCourse_type(int type) { Course_type = type; }
	public static int getCourse_type() { return Course_type; }
	
	private static String ReviewEndDate = "";
	public static void setReviewEndDate(String date) { ReviewEndDate = date; }
	public static String getReviewEndDate() { return ReviewEndDate; }

	private static int Progress_no = 0;
	public static void setProgress_no(int no) { Progress_no = no; }
	public static int getProgress_no() { return Progress_no; }
	
	/*
	 * kr.hunet.HunetLib.OldPlayer 삭제시 지울 내용들
	 * */
}
