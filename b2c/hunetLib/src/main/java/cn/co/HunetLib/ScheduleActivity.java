package cn.co.HunetLib;

import cn.co.HunetLib.Common.HunetAlarmManager;
import cn.co.HunetLib.SQLite.ScheduleModel;
import hunet.library.Utilities;
import cn.co.HunetLib.Base.TabActivity;
import cn.co.HunetLib.Company.Company;
import cn.co.HunetLib.Interface.ITabControlEventListener;
import cn.co.HunetLib.R;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TimePicker;
import android.widget.Toast;

public class ScheduleActivity extends TabActivity implements ITabControlEventListener {

	private HunetAlarmManager m_AlarmManager				= null;
	private LinearLayout 		m_main_btnTopPriv			= null;
	private CheckBox			m_schedule_cb_use			= null;
	private ImageButton			m_schedule_btn_save			= null;
	private TimePicker			m_tp_schedule_time 			= null;
	private Button[] 			m_arSchedule_btns 			= new Button[7];
	
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.schedule_activity);
	    
	    Utilities.changeTitlebarColor(this, R.id.schedule_alarm_fl, R.id.header_text_color);

	    m_AlarmManager				= new HunetAlarmManager(this);
	    m_main_btnTopPriv 			= (LinearLayout)findViewById(R.id.main_btnTopPriv);
	    m_schedule_cb_use			= (CheckBox)findViewById(R.id.schedule_cb_use);
	    m_schedule_btn_save 		= (ImageButton)findViewById(R.id.schedule_btn_save);
	    m_tp_schedule_time 			= (TimePicker)findViewById(R.id.more_tp_schedule_time);
	    m_arSchedule_btns[0] 		= (Button)findViewById(R.id.schedule_btn_sun);
	    m_arSchedule_btns[1] 		= (Button)findViewById(R.id.schedule_btn_mon);
	    m_arSchedule_btns[2] 		= (Button)findViewById(R.id.schedule_btn_tue);
	    m_arSchedule_btns[3] 		= (Button)findViewById(R.id.schedule_btn_wed);
	    m_arSchedule_btns[4] 		= (Button)findViewById(R.id.schedule_btn_thu);
	    m_arSchedule_btns[5] 		= (Button)findViewById(R.id.schedule_btn_fri);
	    m_arSchedule_btns[6] 		= (Button)findViewById(R.id.schedule_btn_sat);
	    	    
	    m_main_btnTopPriv.setOnClickListener(new OnClickListener() { @Override
		public void onClick(View v) 
		{
	    	finish();
		}});
	    
	    m_schedule_btn_save.setOnClickListener(new OnClickListener() { @Override
		public void onClick(View v) 
		{
	    	SetSaveConfigInfo();
		}});
	    
	    for(int nIndex = 0; nIndex < 7; nIndex++)
	    {
	    	m_arSchedule_btns[nIndex].setOnClickListener(new OnClickListener() { @Override
			public void onClick(View v)
		    {
		    	OnClickCheckButton(v);
		    }});
	    }
	    
	    CreateTabControl((LinearLayout)findViewById(R.id.main_tabMenu));
		setSelectTab(Company.eTAB_SCHEDULE);
	}

	private void SetSaveConfigInfo()
	{
		String 	strWeek 	= "";
		String	strUse		= m_schedule_cb_use.isChecked() ? "1" : "0";
		String 	strHour 	= m_tp_schedule_time.getCurrentHour().toString();
		String 	strMinute 	= m_tp_schedule_time.getCurrentMinute().toString();
		
		for(int nIndex = 0; nIndex < 7; nIndex++)
	    {
			if(IsChecked(m_arSchedule_btns[nIndex]))
				strWeek += "&1";
			else
				strWeek += "&0";
	    }
		
		strWeek = strWeek.substring(1, strWeek.length());
		ScheduleModel model = new ScheduleModel();
		model.use 		= strUse;
		model.week 		= strWeek;
		model.hour 		= strHour;
		model.minute 	= strMinute;
		
		getSQLiteManager().SetScheduleData(model);
		
		m_AlarmManager.RunAlarm();
		
		if("1".equals(strUse) && strWeek.indexOf("1") > -1)
		{
			Toast.makeText(this, "학습 알림 기능을 시작합니다.", Toast.LENGTH_LONG).show();
			Vibrator vibrator = (Vibrator)getSystemService(VIBRATOR_SERVICE);
			vibrator.vibrate(500);
		}
		
		finish();
	}
	
	private boolean IsChecked(Button btnView)
	{
		return btnView.getCurrentTextColor() == Color.WHITE;
	}
	
	private void OnClickCheckButton(View view)
	{
		Button btnTemp = (Button)view;
		SetCheckButton(btnTemp, !(btnTemp.getCurrentTextColor() == Color.WHITE));
	}
	
	private void SetCheckButton(Button btnView, boolean bIsCheck)
	{		
		if(bIsCheck)
		{
			btnView.setBackgroundResource(R.drawable.btn_week_on);
			btnView.setTextColor(Color.WHITE);
		}
		else
		{
			btnView.setBackgroundResource(R.drawable.btn_week);
			btnView.setTextColor(Color.BLACK);
		}
	}
	
	@Override
	protected void onFirstResume() 
	{		
	}
	
	@Override
	protected void onResume()
	{
		super.onResume();
				
		ScheduleModel model = getSQLiteManager().GetScheduleData();
		
		if("".equals(model.use))
			 return;
		
		String[] arWeeks = model.week.split("&");
				
		m_schedule_cb_use.setChecked("1".equals(model.use));		
		
		for(int nIndex = 0; nIndex < 7; nIndex++)
			SetCheckButton(m_arSchedule_btns[nIndex], "1".equals(arWeeks[nIndex]));
		
		m_tp_schedule_time.setCurrentHour(Integer.parseInt(model.hour));
		m_tp_schedule_time.setCurrentMinute(Integer.parseInt(model.minute));
	}
}
