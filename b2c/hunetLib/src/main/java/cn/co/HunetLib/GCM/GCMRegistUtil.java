package cn.co.HunetLib.GCM;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

/**
 * Created by zeple on 2015-07-06.
 */
public class GCMRegistUtil {

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String TAG = "GCMRegistUtil";

    public static String GCM_BROADCAST_RECEIVER = "GCMRegistrationResult";
    // 지원하지 않는 기기
    public static final String REGISTRATION_NOT_AVAILABLE = "registrationNotAvailable";
    // GCM 등록 완료
    public static final String REGISTRATION_COMPLETE = "registrationComplete";

    public static void registGcm(Context context) {
        if (checkPlayServices(context)) {
            context.startService(new Intent(context, GCMRegistrationIntentService.class));
        } else {
            // 구글 플레이서비스를 지원하지 않는 디바이스인 경우 (푸시 사용불가)
            LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent(GCMRegistUtil.REGISTRATION_NOT_AVAILABLE));
        }
    }

    private static boolean checkPlayServices(Context context) {
        return GooglePlayServicesUtil.isGooglePlayServicesAvailable(context) == ConnectionResult.SUCCESS;
    }
}
