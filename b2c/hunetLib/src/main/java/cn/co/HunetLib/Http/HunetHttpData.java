package cn.co.HunetLib.Http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class HunetHttpData 
{
	public 	String 			m_strHttpType	= "POST";
	public 	int				m_nId			= -1;
	public 	String 			m_strUrl		= "";
	public 	String 			m_strParam		= "";
	public 	String 			m_strData		= "";
	public 	InputStream 	m_InputStream	= null;
	public 	HttpClient 		m_HttpClient	= null;
	public 	HttpResponse 	m_HttpResponse	= null;
	public 	Object			m_objTag		= null;
	private JSONObject 		m_JSONObject	= null;
	
	public void SetDataInfo(HttpClient httpClient, HttpResponse httpResponse, InputStream inputStream)
	{
		m_HttpClient	= httpClient;
		m_HttpResponse 	= httpResponse;
		m_InputStream 	= inputStream;

		SetData();
		//Log.i("hunet", "StatusCode : " + StatusCode() + ", Url : " + GetUrl() + "\n\nData : " + GetData());
	}
	
	public String GetUrl()
	{
		if("".equals(m_strParam))
			return m_strUrl;
		
		return m_strUrl + "?" + m_strParam;
	}
	
	public int StatusCode()
	{
		if(m_HttpResponse == null)
			return 0;
		
		return m_HttpResponse.getStatusLine().getStatusCode();
	}
	
	public void SetData()
	{
		if(m_InputStream == null)
			return;
		
		BufferedReader 	br 			= null;
		StringBuilder 	strBuilder 	= new StringBuilder();
		br = new BufferedReader(new InputStreamReader(m_InputStream));
	    
	    try 
	    {
	    	String strData;
	    	
			while ((strData = br.readLine()) != null)
			{
				strBuilder.append(strData);
			}			
			
		} catch (IOException e) { e.printStackTrace(); } 
	    
	    m_strData = strBuilder.toString();
	}
	
	public String GetData()
	{
		return m_strData;
	}
	
	public String GetJsonValue(String strKey)
	{
		if(m_JSONObject == null)
			m_JSONObject = ConvertJsonObject();
		
		String strValue = "";
		
		if(m_JSONObject == null)
			return "";
		
		try {
			strValue = m_JSONObject.getString(strKey);
		} catch (JSONException e) {
			strValue = "";
			e.printStackTrace();
		}
		
		return strValue;
	}
	
	public String GetJsonValue(String strKey, String strDefault)
	{
		if(m_JSONObject == null)
			m_JSONObject = ConvertJsonObject();
		
		String strValue = "";
		
		if(m_JSONObject == null)
			return strDefault;
		
		try 
		{
			strValue = m_JSONObject.getString(strKey);
		} 
		catch (JSONException e) 
		{ strValue = strDefault; }
		
		return strValue;
	}
	
	public JSONArray GetJsonArray(String strKey)
	{
		if(m_JSONObject == null)
			m_JSONObject = ConvertJsonObject();
		
		if(m_JSONObject == null)
			return null;
		
		JSONArray jsonArray = null;
		
		try 
		{
			jsonArray = m_JSONObject.getJSONArray(strKey);
		} 
		catch (JSONException e) 
		{ jsonArray = null; }
		
		return jsonArray;
	}
	
	public String GetJsonValue(JSONObject json, String strKey, String strDefault)
	{
		if(json == null)
			return strDefault;
		
		String strValue = "";
		
		try 
		{
			strValue = json.getString(strKey);
		} 
		catch (JSONException e) 
		{ strValue = strDefault; }
		
		return strValue;
	}
	
	private JSONObject ConvertJsonObject()
	{
		JSONObject JO = null;
		
		if("".equals(m_strData))
			return null;
			    
	    try 
	    {			
			JO = new JSONObject(m_strData);
		} catch (JSONException e) { e.printStackTrace(); }
				
		return JO;
	}
}
