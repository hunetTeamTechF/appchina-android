package cn.co.HunetLib.GCM;

import android.annotation.SuppressLint;
import android.app.IntentService;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import java.io.IOException;

/**
 * Created by zeple on 2015-07-06.
 */
public class GCMRegistrationIntentService extends IntentService {

    private static final String TAG = "GCMRegistrationIntentService";
    private static final String sender_id = "744994322412";


    public GCMRegistrationIntentService() {
        super(TAG);
    }

    /**
     * GCM을 위한 Instance ID의 토큰을 생성하여 가져온다.
     *
     * @param intent
     */
    @SuppressLint("LongLogTag")
    @Override
    protected void onHandleIntent(Intent intent) {

        // GCM을 위한 Instance ID를 가져온다.
        InstanceID instanceID = InstanceID.getInstance(this);
        String token = null;
        try {
            synchronized (TAG) {
                // Instance ID에 해당하는 토큰을 생성하여 가져온다.
                token = instanceID.getToken(sender_id, GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                Log.i(TAG, "GCM Registration Token: " + token);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        Intent broadcastIntent = new Intent(this, GCMRegistrationReceiver.class);

        if (!TextUtils.isEmpty(token)) {
            broadcastIntent.setAction(GCMRegistUtil.GCM_BROADCAST_RECEIVER);
            broadcastIntent.putExtra("type", GCMRegistUtil.REGISTRATION_COMPLETE);
            broadcastIntent.putExtra("token", token);
            sendBroadcast(broadcastIntent);
        } else {
            broadcastIntent.setAction(GCMRegistUtil.GCM_BROADCAST_RECEIVER);
            broadcastIntent.putExtra("type", GCMRegistUtil.REGISTRATION_NOT_AVAILABLE);
            sendBroadcast(broadcastIntent);
        }
    }
}
