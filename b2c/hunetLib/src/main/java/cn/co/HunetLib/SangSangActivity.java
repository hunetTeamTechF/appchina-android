package cn.co.HunetLib;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.webkit.WebView;
import cn.co.HunetLib.Base.TabWebviewActivity;
import cn.co.HunetLib.Company.Company;
import cn.co.HunetLib.Interface.IWebViewEventListener;

public class SangSangActivity extends TabWebviewActivity implements IWebViewEventListener
{		
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
	    super.onCreate(savedInstanceState);
	    setSelectTab(Company.eTAB_SANGSANG);
	    this.setWebViewEventListener(this);
	}
	
	@Override
	public void onClickTab(int selectTab, int beforeSelectTab) 
	{		
		switch(selectTab)
		{
		case Company.eTAB_SANGSANG_HOME: 
			finish(); 
			break;
		case Company.eTAB_SANGSANG_BACK:
			if(m_WebView.canGoBack())
				m_WebView.goBack(); 
			break;
		case Company.eTAB_SANGSANG_FORWARD:
			if(m_WebView.canGoForward())
				m_WebView.goForward();
			break;
		case Company.eTAB_SANGSANG_REFRESH:
			m_WebView.reload();
			break;
		}
	}

	@Override
	public void OnPageFinished(WebView view, String url) {
		m_TabControl.OnChangeSangSangTab(view.canGoBack(), view.canGoForward());
	}

	@Override
	public void OnPageStarted(WebView view, String url, Bitmap favicon) {

		
	}

	@Override
	public boolean OnShouldOverrideUrlLoading(WebView view, String url) {

		return false;
	}

	@Override
	public void OnReceivedError(WebView view, int errorCode,
			String description, String failingUrl) {

		
	}
}
