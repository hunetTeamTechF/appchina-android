package cn.co.HunetLib;

import cn.co.HunetLib.Common.HunetProtocal;
import cn.co.HunetLib.Base.BaseActivity;
import cn.co.HunetLib.Company.Company;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

public class SSOWebToAppActivity extends BaseActivity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		String browserSchema 	= "";
		String browserHost 		= "";
		
	    try
	    {
	    	String cSeq 	= "";
	    	String userId 	= "";
	    	String processCd = "";
	    	String processYear = "";
	    	String processTerm = "";

	    	Uri uri			= getIntent().getData();
	    	browserSchema 	= getIntent().getData().getScheme();
	    	browserHost 	= getIntent().getData().getHost();
	    	
	    	HunetProtocal protocal = new HunetProtocal();
	    	protocal.TryParse(browserHost);	    	
	    	
	    	if("".equals(protocal.GetValue("cSeq")))
	    	{
	    		if (browserHost.split(",").length >= 2) {
	    			cSeq 	= browserHost.split(",")[0];
	    			userId 	= browserHost.split(",")[1];
	    		}
	    	}
	    	else
	    	{
	    		cSeq 	= protocal.GetValue("cSeq");
	    		userId 	= protocal.GetValue("userId");
	    	}

	    	if ("".equals(cSeq) && "".equals(userId)) {
	    		cSeq = uri.getQueryParameter("cSeq") == null ? "" : uri.getQueryParameter("cSeq");
	    		userId = uri.getQueryParameter("userId") == null ? "" : uri.getQueryParameter("userId");
	    		processCd = uri.getQueryParameter("processCd") == null ? "" : uri.getQueryParameter("processCd");
	    		processYear = uri.getQueryParameter("processYear") == null ? "" : uri.getQueryParameter("processYear");
	    		processTerm = uri.getQueryParameter("processTerm") == null ? "" : uri.getQueryParameter("processTerm");
	    	}

			Intent intent = new Intent(this, getCompany().getNextActivity(Company.eNEXT_ACTIVITY_INTRO));
	    	intent.putExtra("sso", true);
			intent.putExtra("cSeq", cSeq);
			intent.putExtra("userId", userId);
			intent.putExtra("processCd", processCd);
			intent.putExtra("processYear", processYear);
			intent.putExtra("processTerm", processTerm);
	        startActivity(intent);
	        finish();
	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    }
	}

	@Override
	protected void onFirstResume() {
		
	}
}
