package cn.co.HunetLib;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import cn.co.HunetLib.Base.BaseActivity;
import cn.co.HunetLib.Company.Company;

/**
 * Created by zeple on 2015-09-08.
 */
public class SSOGateActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getIntent(getIntent());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        getIntent(intent);
    }

    private void getIntent(Intent intent) {
        Uri uri = intent.getData();
        String path = uri.getPath();

        if (path.equalsIgnoreCase("/classroom")) {
            String process_cd = uri.getQueryParameter("process_cd");
            String process_year = uri.getQueryParameter("process_year");
            String process_term = uri.getQueryParameter("process_term");
            String company_seq = uri.getQueryParameter("company_seq");
            String user_id = uri.getQueryParameter("user_id");

            Intent nextIntent = new Intent(this, getCompany().getNextActivity(Company.eNEXT_ACTIVITY_INTRO));
            nextIntent.putExtra("sso", true);
            nextIntent.putExtra("cSeq", company_seq);
            nextIntent.putExtra("userId", user_id);
            nextIntent.putExtra("processCd", process_cd);
            nextIntent.putExtra("processYear", process_year);
            nextIntent.putExtra("processTerm", process_term);
            nextIntent.putExtra("action", "classroom");
            startActivity(nextIntent);
            finish();
        }
    }

    @Override
    protected void onFirstResume() {

    }
}
