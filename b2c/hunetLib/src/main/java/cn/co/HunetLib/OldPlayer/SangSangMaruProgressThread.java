package cn.co.HunetLib.OldPlayer;

import org.json.JSONObject;

import cn.co.HunetLib.Common.AppMain;

import android.app.Activity;
import android.os.Handler;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

/**
 * 강의관련 HTTP 통신. 플레이정보
 * 작업자 : 장형일
 * 작업일 : 2011.11
 * mlc 상상마루 플레이어 인앱 수정. 2011.11.04 
 */
public class SangSangMaruProgressThread extends Thread{
	
	private VideoView videoView;
	private TextView txtTimePosition;
	private SeekBar seekBar;
	private LinearLayout vcontrol;
	private LinearLayout layoutSound;
	
	private int contractNo;
	private int contentsSeq;
	private String goodsId;
	private int viewSec;
	private int viewSecMobile;
	private int addSec;
	private int preAddSec;
	
	private int seekBarVisibleMark = 0;
	private int preWhat;
	private int skipWhat;
	private boolean bFinish;
	
	TranslateAnimation ani = null;
	Hunet_http_Data_Cls data = new Hunet_http_Data_Cls();
	
	String toastYn;
	
	Activity act; //데이터 확인용 추가 변수.
	
	private int WaitCnt;
	
	Handler mHandler = new Handler(){
		@Override
		public void handleMessage(android.os.Message msg){
			//네트워크 변경 여부 확인. 변경 되었으면. 종료
			if(AppMain.getNetworkChangeYn().equals("Y"))
			{
				try
				{
					if(toastYn.equals("N"))
					{
						toastYn = "Y";
						Toast.makeText(act.getBaseContext(), "네트워크가 전환되어 재생을 종료합니다.", Toast.LENGTH_SHORT).show();
					}
				} catch (NullPointerException e) {
					toastYn = "N";
				}
				
				act.finish();
			}
			else
			{
				CtlSeekBar(msg.what);  //프로그레스바 컨트롤
				CtlProgress(msg.what);               //진도
			}
		};
	};

	public SangSangMaruProgressThread(VideoView videoView, TextView txtTimePosition, SeekBar seekBar, LinearLayout vcontrol
			, LinearLayout layoutSound, int contractNo,  int contentsSeq, String goodsId, int viewSec, int viewSecMobile, Activity act){
		
		this.videoView = videoView;
		this.txtTimePosition = txtTimePosition;
		this.seekBar = seekBar;
		this.vcontrol = vcontrol;
		this.layoutSound = layoutSound;
		
		this.contractNo = contractNo;
		this.contentsSeq = contentsSeq;
		this.goodsId = goodsId;
		this.viewSec = viewSec;
		this.viewSecMobile = viewSecMobile;
		this.addSec = 0;
		this.preAddSec = 0;
		this.preWhat = 0;
		this.skipWhat = 0;
		
		this.act = act;
		
		AppMain.setNetworkChangeYn("N");
		toastYn = "N";
		
		this.WaitCnt = 10;
	}
	
	public void Finish()
	{
		bFinish = true;
	}
	
	@Override
	public void run(){
		
		while(true) 
		{
			if(bFinish)
				break;
			
			if(HunetNetworkMethod.connectAllConfirm(act.getBaseContext()) == false)
			{
				if(WaitCnt > 0)
				{
					videoView.pause();
					WaitCnt = WaitCnt - 1;
				}
				else
				{
					Toast.makeText(act.getBaseContext(), "네트워크가 불안정하여 재생을 종료합니다.", Toast.LENGTH_SHORT).show();
					act.finish();
				}
			}
			else
			{
				if(!videoView.isPlaying() && WaitCnt < 10)
				{
					videoView.start();
				}
				if(WaitCnt < 10) WaitCnt = 10;
				
				mHandler.sendEmptyMessage(videoView.getCurrentPosition());
			}
			
			try
			{
				Thread.sleep(1000);
			}
			catch(InterruptedException e){
				e.printStackTrace();
			}
		}
	}
	
	/***
	 * SeekBar 컨트롤
	 * @param what
	 */
	protected void CtlSeekBar(int what)
	{
		txtTimePosition.setText(HunetCommonMethod.MarkToTimeString(what));
		
		seekBar.setProgress(what);
		
		if(vcontrol.getVisibility() == View.VISIBLE && seekBarVisibleMark == 0)
		{
			seekBarVisibleMark = what;
		}
		else if(vcontrol.getVisibility() == View.VISIBLE && what - seekBarVisibleMark >= 5000 && what - seekBarVisibleMark < 6000)
		{
			ani = new TranslateAnimation(0,0,0,100);
	        ani.setDuration(500);
	        vcontrol.setAnimation(ani);
			vcontrol.setVisibility(View.INVISIBLE);
			layoutSound.setVisibility(View.INVISIBLE);
			
			seekBarVisibleMark = 0;
		}
		else if(vcontrol.getVisibility() == View.VISIBLE && what - seekBarVisibleMark > 10000)
		{
			seekBarVisibleMark = 0;
		}
	}
	
	/***
	 * 진도 저장
	 * @param what
	 */
	protected void CtlProgress(int what)
	{		
		if(what - preWhat >= 1000)
		{
			if(what - preWhat >= 10000)
			{
				skipWhat += (what - preWhat);
			}
			
			addSec = (what - skipWhat) / 1000;
			
			if(((what / 1000) % 20) == 0)
			{
				update(contractNo, contentsSeq, goodsId, (viewSec + addSec + preAddSec), (viewSecMobile + addSec + preAddSec), (what/1000), "22");
			}
	
			preWhat = what;
		}
		else if(what - preWhat < 0)
		{
			preWhat = what;
			preAddSec += addSec;
			skipWhat = what;
		}
	}
	
	/***
	 * 업데이트
	 * @param cno
	 * @param seq
	 * @param gid
	 * @param sec
	 * @param lastSec
	 */
	protected JSONObject update(int cno, int seq, String gid, int sec, int mobileSec, int lastSec, String type)
	{
		return data.UpdateProgressInfo(cno, seq, gid, sec, mobileSec, lastSec, type);
	}
	
	public void lastUpdate()
	{
		if(addSec + preAddSec > 0)
		{
			update(contractNo, contentsSeq, goodsId, (viewSec + addSec + preAddSec), (viewSecMobile + addSec + preAddSec), (preWhat/1000), "22");
		}
	}
}
