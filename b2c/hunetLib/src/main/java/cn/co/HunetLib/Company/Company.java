package cn.co.HunetLib.Company;

import cn.co.HunetLib.ClassroomActivity;
import cn.co.HunetLib.Common.UrlAction;
import cn.co.HunetLib.ConfigActivity;
import cn.co.HunetLib.Control.TabControl;
import cn.co.HunetLib.CustomActivity1;
import cn.co.HunetLib.CustomActivity2;
import cn.co.HunetLib.DevOptionActivity;
import cn.co.HunetLib.LectureActivity;
import cn.co.HunetLib.SangSangActivity;
import cn.co.HunetLib.SangSangSelfStudyActivity;
import cn.co.HunetLib.ScheduleActivity;
import cn.co.HunetLib.SelfStudyActivity;
import cn.co.HunetLib.Stw.PlayerStw;
import cn.co.HunetLib.Stw.SangSangStw;
import cn.co.HunetLib.UserTabWebviewActivity;
import cn.co.HunetLib.VersionInfoActivity;
import hunet.domain.DomainAddress;
import hunet.drm.download.DownloadCourseActivity;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import cn.co.HunetLib.CustomActivity3;
import cn.co.HunetLib.HomeActivity;
import cn.co.HunetLib.LoginActivity;
import cn.co.HunetLib.MoreActivity;
import cn.co.HunetLib.MyStudyWebtoonActivity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.webkit.WebView;

public abstract class Company
{
	public cn.co.HunetLib.SQLite.LoginModel LoginModel;

	protected List<ViewModel> ViewModelList 		= new ArrayList<ViewModel>();
	protected boolean 	ShowRuleId					= false;  	// 로그인 화면에서 공통 아이디를 보여줄지 여부
	protected String 	RuleId 						= "";		// 공통 아이디
	protected int 		Seq 						= 0;		// 기업 코드
	protected int 		LoginSeq					= 0;		// 로그인 기업 코드
	protected String 	AppName						= "";		// 푸쉬 메시지에서 제목으로 사용 - GCMLibIntentService
	protected boolean 	GCMSendManageEnabled 		= false; 	// 푸쉬 메시지를 받을지 or 안받을지 선택 할 수 있는 기능 - ConfigActivity
	protected boolean 	ShowButtonLoginDrm			= true;		// 로그인 화면에서 DRM 버튼을 보여줄지 여부
	protected boolean   EnabledSangSangOverlapStudy	= true;		// 상상마루 동시접속학습 가능여부

	protected boolean 	DrmUse 				= false;		// DRM 을 사용하는 기업인지
	protected String 	HReminderUseType 	= "";
	protected String 	HStoryUseType 		= "";
	protected String 	EduType				= "";

	public float LoginTop 			= 0;
	public float LoginBottom 		= 0;
	public float LoginDrmTop 		= 0;
	public float LoginDrmBottom 	= 0;

	public boolean ShowDevOption 	= false;

	protected Class<?> HomeActivity;
	protected Class<?> LoginActivity;
	protected Class<?> LectureActivity;
	protected Class<?> ClassroomActivity;
	protected Class<?> MoreActivity;

	protected String eURL_DEFAULT_HOST				= "";
	protected String eURL_LOGIN_HOST 				= "https://ssl.hunet.co.kr";
//	protected String eURL_MLC_HOST					= "http://mlc.hunet.co.kr"; --> hunetplayer_new 로 옮김 (DomainAddress클래스)

	/* enum list */
	public final static int eURL_LOGIN 					= 1; // 로그인
	public final static int eURL_LOGIN_MOBILE			= 2; // 모바일 로그인

	public final static int eURL_HOME					= 10; // 메인
	public final static int eURL_CURRICULUM 			= 11; // 과정리스트
	public final static int eURL_SELFSTUDY 				= 12; // 자율학습
	public final static int eURL_LECTURE 				= 13; // 교육과정
	public final static int eURL_LECTURE_INMUN 			= 14; // 교육과정
	public final static int eURL_CLASSROOM 				= 15; // 나의 강의실
	public final static int eURL_PROCESS				= 16;
	public final static int eURL_SANGSANG				= 17;
	public final static int eURL_SANGSANG_SELFSTUDY		= 18;
	public final static int eURL_HREMINDER				= 19;
	public final static int eURL_HSTORY					= 20;
	public final static int eURL_PASSWORD				= 21;
	public final static int eURL_NOTIFY_EVENT			= 22;


	public final static int eURL_MORE_NOTICE			= 30; // 공지사항
	public final static int eURL_MORE_QNA				= 31; // 1:1 QnA
	public final static int eURL_MORE_MEMBERINFO		= 32; // 회원 정보 관리
	public final static int eURL_MORE_FAQ				= 34;

	public final static int eURL_APP_VERSION			= 40;
	public final static int eURL_H_LEADERSHIP_CLASSROOM	= 41;

	public final static int eURL_CUSTOM_1				= 50;
	public final static int eURL_CUSTOM_2				= 51;
	public final static int eURL_CUSTOM_3				= 52;
	public final static int eURL_CUSTOM_4				= 53;
	public final static int eURL_CUSTOM_5				= 54;

	public final static int eNEXT_ACTIVITY_INTRO		= 100;
	public final static int eNEXT_ACTIVITY_LOGIN		= 101;
	public final static int eNEXT_ACTIVITY_NOTIFY_ALARM	= 102;

	public final static int eACTIVITY_HOME				= 140;
	public final static int eACTIVITY_CLASSROOM			= 141;
	public final static int eACTIVITY_MORE				= 150;
	public final static int eACTIVITY_LOGIN				= 151;
	public final static int eACTIVITY_SCHEDULE			= 152;
	public final static int eACTIVITY_CONFIG			= 153;
	public final static int eACTIVITY_VERSION			= 154;
	public final static int eACTIVITY_FAQ				= 155;
	public final static int eACTIVITY_QNA				= 156;
	public final static int eACTIVITY_NOTICE			= 157;
	public final static int eACTIVITY_MEMBERINFO		= 158;
	public final static int eACTIVITY_DOWNLOAD_CENTER	= 159;
	public final static int eACTIVITY_LECTURE			= 160;
	public final static int eACTIVITY_LECTURE_INMUN		= 161;
	public final static int eACTIVITY_WEBTOON			= 162;
	public final static int eACTIVITY_HREMINDER			= 163;
	public final static int eACTIVITY_HSTORY			= 164;
	public final static int eACTIVITY_SANGSANG_SELFSTUDY = 165;
	public final static int eACTIVITY_SELFSTUDY			= 166;
	public final static int eACTIVITY_PASSWORD			= 167;
	public final static int eACTIVITY_DEVOPTION			= 168;
	public final static int eACTIVITY_MODAL				= 169;

	public final static int eACTIVITY_CUSTOM_1			= 190;
	public final static int eACTIVITY_CUSTOM_2			= 191;
	public final static int eACTIVITY_CUSTOM_3			= 192;
	public final static int eACTIVITY_CUSTOM_4			= 193;
	public final static int eACTIVITY_CUSTOM_5			= 194;

	public final static int eRESOURCE_BACKGROUND_INTRO 			= 200;
	public final static int eRESOURCE_BACKGROUND_LOGIN 			= 201;
	public final static int eRESOURCE_NOTIFICATION 				= 202;
	public final static int eRESOURCE_BUTTON_LOGIN				= 203;

	public final static int eTAB_HOME				= 401;

	public final static int eTAB_LECTURE			= 402;
	public final static int eTAB_LECTURE_INMUN		= 403;
	public final static int eTAB_SELFSTUDY			= 404;
	public final static int eTAB_CLASSROOM			= 405;
	public final static int eTAB_PROCESS			= 406;
	public final static int eTAB_SANGSANG			= 407;
	public final static int eTAB_SANGSANG_SELFSTUDY	= 408;

	// 리더십 탭
	public final static int eTAB_H_LEADERSHIP_CLASSROOM	= 409;
	public final static int eTAB_H_LEADERSHIP_LECTURE 	= 410;
	public final static int eTAB_H_LEADERSHIP_MENTOR 	= 411;
	public final static int eTAB_H_LEADERSHIP_DOWN 		= 412;


	public final static int eTAB_MORE				= 420;

	/*
	 * MORE TAB SUB ACTIVITY LIST
	 */
	public final static int eTAB_SCHEDULE			= 421;
	public final static int eTAB_CONFIG				= 422;
	public final static int eTAB_VERSION			= 423;
	public final static int eTAB_FAQ				= 424;
	public final static int eTAB_QNA				= 425;
	public final static int eTAB_NOTICE				= 426;
	public final static int eTAB_MEMBERINFO			= 427;
	public final static int eTAB_DOWNLOAD_CENTER	= 428;
	public final static int eTAB_HREMINDER			= 429;
	public final static int eTAB_HSTORY				= 430;

	public final static int eTAB_SANGSANG_HOME		= 430;
	public final static int eTAB_SANGSANG_BACK		= 431;
	public final static int eTAB_SANGSANG_FORWARD	= 432;
	public final static int eTAB_SANGSANG_REFRESH	= 433;

	public final static int eTAB_PASSWORD			= 434;
	public final static int eTAB_DEVOPTION			= 435;

	public final static int eTAB_CUSTOM_1			= 500;
	public final static int eTAB_CUSTOM_2			= 501;
	public final static int eTAB_CUSTOM_3			= 502;
	public final static int eTAB_CUSTOM_4			= 503;
	public final static int eTAB_CUSTOM_5			= 504;

	/**
	 * 알수 없음
	 */
	public final static int eIMAGINE_CONTRACT_NONE 					= 500;
	/**
	 * 연수원 전용
	 */
	public final static int eIMAGINE_CONTRACT_ONLYLEARNING 			= 501;
	/**
	 * 상상마루 전용
	 */
	public final static int eIMAGINE_CONTRACT_ONLYIMAGINE 			= 502;
	/**
	 * 연수원, 상상마루 전용
	 */
	public final static int eIMAGINE_CONTRACT_LEARNINGANDIMAGINE 	= 503;

	public final static int eACTION_LOGIN_HOME_NEXTURL	= 200; // 로그인 => 홈 => 다음 주소로 이동

	// 액티비티 결과 코드
	public final static int eACTIVITY_RESULT_GALLERY_IMG = 600; // 갤러리 업로드 완료 코드

	public Company()
	{
		Seq = 0;

		CreateViewModel();
	}

	protected void CreateViewModel()
	{
		setViewModel(eTAB_NOTICE, "学员公告");
		//setViewModel(eTAB_FAQ, "자주묻는 질문");
		setViewModel(eTAB_QNA, "1:1 咨询");
		//setViewModel(eTAB_MEMBERINFO, "회원정보 관리");
	//	setViewModel(eTAB_PASSWORD, "修改密码");
		//setViewModel(eTAB_SCHEDULE, "스케줄 알리미");
		setViewModel(eTAB_CONFIG, "环境设置");
		setViewModel(eTAB_VERSION, "版本信息");
		setViewModel(eTAB_DOWNLOAD_CENTER, "下载中心");
	}

	public void setViewModel(int tabId, String title)
	{
		ViewModel model = new ViewModel();

		model.TabId 		= tabId;
		model.Title 		= title;
		model.DisplayOrder 	= ViewModelList.size() + 1;

		ViewModelList.add(model);
	}

	public ViewModel getViewModel(int tabId)
	{
		ViewModel model = null;

		for(ViewModel temp : ViewModelList)
		{
			if(temp.TabId == tabId)
			{
				model = temp;
				break;
			}
		}

		return model;
	}

	public TabControl getTabControl()
	{
		// Background, Icon Color, Text Color, Selected Icon Color, Selected Text Color
		return new TabControl("#ffadadad", "#ffefefef", "#ffefefef", "#ff305592", "#ff305592");
	}

	public UrlAction getUrlAction()
	{
		return new UrlAction();
	}

	public PlayerStw getPlayerStw(Activity activity, WebView webView, int companySeq, String loginId) {
		return new PlayerStw(activity, webView, companySeq, loginId);
	}

	public SangSangStw getSangSangStw(Activity activity, WebView webView, int companySeq, String loginId) {
		return new SangSangStw(activity, webView, companySeq, loginId);
	}

	public String getGalleryUploadUrl()
	{
		return eURL_DEFAULT_HOST + "/Common/MobileUpload?path=Community";
	}

	public String getDefaultHostUrl()
	{
		return eURL_DEFAULT_HOST;
	}

	public Class<?> getNextActivity(int type)
	{
		Class<?> tempClass = null;

		switch(type)
		{
		case eNEXT_ACTIVITY_INTRO: case eNEXT_ACTIVITY_NOTIFY_ALARM:
			tempClass = LoginActivity.class; 
			break;
		case eNEXT_ACTIVITY_LOGIN:
			tempClass = HomeActivity.class;
			break;
		case eACTIVITY_HOME: 				tempClass = HomeActivity.class; break;
		case eACTIVITY_CLASSROOM:			tempClass = cn.co.HunetLib.ClassroomActivity.class; break;
		case eACTIVITY_LECTURE:
		case eACTIVITY_LECTURE_INMUN:		tempClass = cn.co.HunetLib.LectureActivity.class; break;
		case eACTIVITY_DOWNLOAD_CENTER:	 	tempClass = DownloadCourseActivity.class; break;
		case eACTIVITY_MORE:				tempClass = MoreActivity.class; break;
		case eACTIVITY_LOGIN:				tempClass = LoginActivity.class; break;
		case eACTIVITY_SCHEDULE:			tempClass = ScheduleActivity.class; break;
		case eACTIVITY_CONFIG:				tempClass = ConfigActivity.class; break;
		case eACTIVITY_VERSION:				tempClass = VersionInfoActivity.class; break;
		case eACTIVITY_WEBTOON:				tempClass = MyStudyWebtoonActivity.class; break;
		case eACTIVITY_SANGSANG_SELFSTUDY: 	tempClass = SangSangSelfStudyActivity.class; break;
		case eACTIVITY_SELFSTUDY:			tempClass = SelfStudyActivity.class; break;
		case eACTIVITY_DEVOPTION:			tempClass = DevOptionActivity.class; break;
		case eACTIVITY_QNA:
		case eACTIVITY_FAQ:
		case eACTIVITY_NOTICE:
		case eACTIVITY_MEMBERINFO: 
		case eACTIVITY_HSTORY:
		case eACTIVITY_HREMINDER:
		case eACTIVITY_PASSWORD:
		case eACTIVITY_MODAL:
			tempClass = UserTabWebviewActivity.class;
			break;
		}

		return tempClass;
	}

	public void onChangeTab(Activity activity, int selectTab, int beforeSelectTab)
	{
		onChangeTab(activity, selectTab, beforeSelectTab, "", "");
	}

	public void onChangeTab(Activity activity, int selectTab, int beforeSelectTab, String nextUrl, String indexDetail)
	{
		if(selectTab == beforeSelectTab)
			return;

		Intent intent = null;

		switch(selectTab)
		{
		case Company.eTAB_HOME: 				intent = new Intent(activity, getNextActivity(eACTIVITY_HOME)); break;
		case Company.eTAB_LECTURE: 				
		case Company.eTAB_LECTURE_INMUN:		intent = new Intent(activity, LectureActivity.class); break;
		case Company.eTAB_SELFSTUDY: 			intent = new Intent(activity, SelfStudyActivity.class); break;
		case Company.eTAB_CLASSROOM: 			intent = new Intent(activity, ClassroomActivity.class); break;
		case Company.eTAB_SANGSANG:				intent = new Intent(activity, SangSangActivity.class); break;
		case Company.eTAB_SANGSANG_SELFSTUDY: 	intent = new Intent(activity, SangSangSelfStudyActivity.class); break;
		case Company.eTAB_MORE: 				intent = new Intent(activity, getNextActivity(eACTIVITY_MORE)); break;
		case Company.eTAB_DOWNLOAD_CENTER: 		intent = new Intent(activity, DownloadCourseActivity.class); break;
		case Company.eTAB_CUSTOM_1:				intent = new Intent(activity, CustomActivity1.class); break;
		case Company.eTAB_CUSTOM_2:				intent = new Intent(activity, CustomActivity2.class); break;
		case Company.eTAB_CUSTOM_3:             intent = new Intent(activity, CustomActivity3.class); break;
		}

		if(intent != null)
		{
			if("".equals(nextUrl) == false)
				intent.putExtra("NEXT_URL", nextUrl);

			if("".equals(indexDetail) == false)
				intent.putExtra("INDEX_DETAIL", indexDetail);

			intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
			activity.startActivity(intent);
		}

		switch(beforeSelectTab)
		{
		case Company.eTAB_CONFIG:
		case Company.eTAB_SCHEDULE:
		case Company.eTAB_VERSION:
		case Company.eTAB_DEVOPTION:
			activity.finish();
			break;
		}
	}

	public int getTabUrlType(int type)
	{
		int urlType = 0;
		switch(type)
		{
		case eTAB_HOME: 				urlType = eURL_HOME; break;
		case eTAB_LECTURE: 				urlType = eURL_LECTURE; break;
		case eTAB_LECTURE_INMUN: 		urlType = eURL_LECTURE_INMUN; break;
		case eTAB_SELFSTUDY: 			urlType = eURL_SELFSTUDY; break;
		case eTAB_CLASSROOM: 			urlType = eURL_CLASSROOM; break;
		case eTAB_FAQ: 					urlType = eURL_MORE_FAQ; break;
		case eTAB_QNA: 					urlType = eURL_MORE_QNA; break;
		case eTAB_NOTICE: 				urlType = eURL_MORE_NOTICE; break;
		case eTAB_MEMBERINFO: 			urlType = eURL_MORE_MEMBERINFO; break;
		case eTAB_SANGSANG: 			urlType = eURL_SANGSANG; break;
		case eTAB_SANGSANG_SELFSTUDY: 	urlType = eURL_SANGSANG_SELFSTUDY; break;
		case eTAB_HREMINDER: 			urlType = eURL_HREMINDER; break;
		case eTAB_HSTORY:				urlType = eURL_HSTORY; break;
		case eTAB_PASSWORD:				urlType = eURL_PASSWORD; break;
		case eTAB_H_LEADERSHIP_CLASSROOM:	urlType = eURL_H_LEADERSHIP_CLASSROOM; break;
			case eTAB_CUSTOM_2:	urlType = eURL_CUSTOM_2; break;
			case eTAB_CUSTOM_3:	urlType = eURL_CUSTOM_3; break;

		}

		return urlType;
	}

	public String getUrl(int type)
	{
		String url = "";

		switch(type)
		{
		case eURL_LOGIN: 				url = String.format("%s/app/JLog.aspx", DomainAddress.getUrlMLC()); break;
		case eURL_LOGIN_MOBILE:			url = String.format("%s/ECLS/SSO/MLogin.aspx?cSeq=%d&appNm=%s&userId=%s&passWd=%s", eURL_LOGIN_HOST, Seq, AppName, URLEncoder.encode(LoginModel.id), URLEncoder.encode(LoginModel.pwd)); break;
		case eURL_HOME: 				url = String.format("%s/app/JLog.aspx?type=4&uid=%s&pw=%s", DomainAddress.getUrlMLC(), URLEncoder.encode(LoginModel.id), URLEncoder.encode(LoginModel.pwd)); break;

		case eURL_LECTURE:				url = String.format("%s/app/JLog?type=5&uid=%s&pw=%s", DomainAddress.getUrlMLC(), URLEncoder.encode(LoginModel.id), URLEncoder.encode(LoginModel.pwd)); break;
		case eURL_LECTURE_INMUN:		url = String.format("%s/app/JLog?type=7&uid=%s&pw=%s", DomainAddress.getUrlMLC(), URLEncoder.encode(LoginModel.id), URLEncoder.encode(LoginModel.pwd)); break;
		case eURL_SELFSTUDY:			url = String.format("%s/Imagine", eURL_DEFAULT_HOST); break;
		//case eURL_CLASSROOM:			url = String.format("%s/app/JLog.aspx?type=8&uid=%s&pw=%s", DomainAddress.getUrlMLC(), URLEncoder.encode(LoginModel.id), URLEncoder.encode(LoginModel.pwd)); break;
		case eURL_SANGSANG:				url = String.format("%s/JLog?type=17&uid=%s&pw=%s", DomainAddress.getUrlMLC(), URLEncoder.encode(LoginModel.id), URLEncoder.encode(LoginModel.pwd)); break;
		case eURL_SANGSANG_SELFSTUDY:	url = String.format("%s/JLog?type=35&uid=%s&pw=%s", DomainAddress.getUrlMLC(), URLEncoder.encode(LoginModel.id), URLEncoder.encode(LoginModel.pwd)); break;

		case eURL_MORE_NOTICE: 			url = String.format("%s/app/JLog.aspx?type=13&uid=%s&pw=%s", DomainAddress.getUrlMLC(), URLEncoder.encode(LoginModel.id), URLEncoder.encode(LoginModel.pwd)); break;
		case eURL_MORE_FAQ:				url = String.format("%s/app/JLog?type=14&uid=%s&pw=%s", DomainAddress.getUrlMLC(), URLEncoder.encode(LoginModel.id), URLEncoder.encode(LoginModel.pwd)); break;
		case eURL_MORE_QNA: 			url = String.format("%s/app/JLog.aspx?type=15&uid=%s&pw=%s", DomainAddress.getUrlMLC(), URLEncoder.encode(LoginModel.id), URLEncoder.encode(LoginModel.pwd)); break;
		case eURL_MORE_MEMBERINFO: 		url = String.format("%s/JLog?type=16&uid=%s&pw=%s", DomainAddress.getUrlMLC(), URLEncoder.encode(LoginModel.id), URLEncoder.encode(LoginModel.pwd)); break;
		case eURL_APP_VERSION:			url = String.format("%s/app/JLog.aspx?type=28&device=android", DomainAddress.getUrlMLC()) + "&app=%s"; break;
		case eURL_HREMINDER:			url = String.format("http://study.hunet.co.kr/Study/HTools/Gate.aspx?userId=%s&typeCd=0&isMobile=Y", URLEncoder.encode(LoginModel.id)); break;
		case eURL_HSTORY:				url = String.format("%s/JLog?type=37&uid=%s&pw=%s", DomainAddress.getUrlMLC(), URLEncoder.encode(LoginModel.id), URLEncoder.encode(LoginModel.pwd)); break;
		case eURL_PASSWORD:				url = String.format("%s/JLog?type=40&uid=%s&pw=%s", DomainAddress.getUrlMLC(), URLEncoder.encode(LoginModel.id), URLEncoder.encode(LoginModel.pwd)); break;
		case eURL_NOTIFY_EVENT:			url = String.format("%s/JLog?type=1004", DomainAddress.getUrlMLC()); break;
		case eURL_H_LEADERSHIP_CLASSROOM:	url = String.format("http://h-leadership.hunet.co.kr/App/MyPage/%s", URLEncoder.encode(LoginModel.id)); break;
			case eURL_CUSTOM_2:         url = String.format("%s/app/JLog.aspx?type=13&uid=%s&pw=%s", DomainAddress.getUrlMLC(), URLEncoder.encode(LoginModel.id), URLEncoder.encode(LoginModel.pwd)); break;
			case eURL_CUSTOM_3:         url = String.format("%s/app/JLog.aspx?type=15&uid=%s&pw=%s", DomainAddress.getUrlMLC(), URLEncoder.encode(LoginModel.id), URLEncoder.encode(LoginModel.pwd)); break;

		}

		return url;
	}

	public String getUrl(int type, String param)
	{
		String url = getUrl(type);

		if(url.indexOf("?") > -1)
			url = String.format("%s&%s", url, param);
		else
			url = String.format("%s?%s", url, param);

		return url;
	}

	public void setStartActivity(Activity activity, int type, String putData, boolean finish, int flags)
	{
		Intent 		intent 		= null;
		Class<?> 	tempClass 	= getNextActivity(type);

		if(tempClass == null)
			return;

		String[] arQueryString = putData.split("\\&");
		intent = new Intent(activity, tempClass);

		for(int index = 0; index < arQueryString.length; index++)
		{
			String[] arValue = arQueryString[index].split("=");

			if(arValue.length != 2)
				continue;

			intent.putExtra(arValue[0], arValue[1]);
		}

		if(flags > 0)
			intent.addFlags(flags);

		activity.startActivity(intent);		

		if(finish == false)
			return;

		activity.finish();
	}

	public boolean getShowRuleId() { return ShowRuleId; }
	protected void setShowRuleId(boolean _showRuleId) { ShowRuleId = _showRuleId; }

	public String getRuleId() { return RuleId; }
	protected void setRuleId(String _ruleId) { RuleId = _ruleId; }

	public String getAppName() { return AppName; }
	protected void setAppName(String _appName) { AppName = _appName; }

	public int getSeq()
	{
		if(LoginSeq != 0)
			return LoginSeq;

		return Seq; 
	}

	protected void setSeq(int _seq) { Seq = _seq; }

	public int getLoginSeq() { return Seq; }
	public void setLoginSeq(int _seq) { LoginSeq = _seq; }

	public boolean getDrmUse() { return DrmUse; }
	public void setDrmUse(boolean _drmUse) { DrmUse = _drmUse; }

	public String getHReminderUseType() { return HReminderUseType; }
	public void setHReminderUseType(String _HReminderUseType) { HReminderUseType = _HReminderUseType; }

	public String getHStoryUseType() { return HStoryUseType; }
	public void setHStoryUseType(String _HStoryUseType) { HStoryUseType = _HStoryUseType; }

	public boolean getShowButtonLoginDrm() { return ShowButtonLoginDrm; }
	protected void setShowButtonLoginDrm(boolean _showButtonLoginDrm) { ShowButtonLoginDrm = _showButtonLoginDrm; }

	public boolean getEnabledSangSangOverlapStudy() { return EnabledSangSangOverlapStudy; }
	public void setEnabledSangSangOverlapStudy(boolean value) { EnabledSangSangOverlapStudy = value; }

	public void setDefaultHost(String _defaultHost)
	{
		eURL_DEFAULT_HOST = _defaultHost;

		if(DomainAddress.IS_STAGING)
		{
			if(eURL_DEFAULT_HOST.toLowerCase().indexOf("http://test.") == -1)
				eURL_DEFAULT_HOST = eURL_DEFAULT_HOST.replaceFirst("(?i)http://", "http://test.");
		}
		else
		{
			if(eURL_DEFAULT_HOST.toLowerCase().indexOf("http://test.") == 0)
				eURL_DEFAULT_HOST = eURL_DEFAULT_HOST.replaceFirst("(?i)http://test.", "http://");
		}
	}

	public void setEduType(String _EduType) { EduType = _EduType; }
	public int getEduType()
	{
		if("ONLYIMAGINE".equals(EduType) || "ONLYSANGSANG".equals(EduType))
			return eIMAGINE_CONTRACT_ONLYIMAGINE;
		else if("LEARNINGANDIMAGINE".equals(EduType))
			return eIMAGINE_CONTRACT_LEARNINGANDIMAGINE;
		else if("ONLYLEARNING".equals(EduType))
			return eIMAGINE_CONTRACT_ONLYLEARNING;

		return eIMAGINE_CONTRACT_NONE;
	}

	public boolean getGCMSendManageEnabled() { return GCMSendManageEnabled; }

//	public void setTestHost(boolean isTest)
//	{
//		if(isTest)
//		{
//			if(eURL_MLC_HOST.toLowerCase().indexOf("http://test.") == -1)
//				eURL_MLC_HOST = eURL_MLC_HOST.replaceFirst("(?i)http://", "http://test.");
//
//			if(eURL_LOGIN_HOST.toLowerCase().indexOf("http://test.") == -1)
//				eURL_LOGIN_HOST = eURL_LOGIN_HOST.replaceFirst("(?i)http://", "http://test.");
//
//			if(eURL_DEFAULT_HOST.toLowerCase().indexOf("http://test.") == -1)
//				eURL_DEFAULT_HOST = eURL_DEFAULT_HOST.replaceFirst("(?i)http://", "http://test.");
//		}
//		else
//		{
//			if(eURL_MLC_HOST.toLowerCase().indexOf("http://test.") == 0)
//				eURL_MLC_HOST = eURL_MLC_HOST.replaceFirst("(?i)http://test.", "http://");
//
//			if(eURL_LOGIN_HOST.toLowerCase().indexOf("http://test.") == 0)
//				eURL_LOGIN_HOST = eURL_LOGIN_HOST.replaceFirst("(?i)http://test.", "http://");
//
//			if(eURL_DEFAULT_HOST.toLowerCase().indexOf("http://test.") == 0)
//				eURL_DEFAULT_HOST = eURL_DEFAULT_HOST.replaceFirst("(?i)http://test.", "http://");
//		}
//	}
//
//	public boolean isTestHost()
//	{		
//		return eURL_MLC_HOST.toLowerCase().indexOf("http://test.") > -1;
//	}

	public String getPackageName(Activity activity)
	{
		return activity.getPackageName();
	}
}
