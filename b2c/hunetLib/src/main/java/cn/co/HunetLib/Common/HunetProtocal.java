package cn.co.HunetLib.Common;

import java.util.ArrayList;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

public class HunetProtocal
{
	private List<NameValuePair> m_QueryStrList; 
	
	public HunetProtocal()
	{
		m_QueryStrList = new ArrayList<NameValuePair>();
	}
	
	public List<NameValuePair> QueryStrList()
	{
		return m_QueryStrList;
	}
	
	public boolean TryParse(String url)
	{
		m_QueryStrList.clear();
		
		try
		{
			int nFind = url.indexOf("://");
			
			if(nFind > 0)
				url = url.substring(nFind + 3);
		
			String[] arParam = url.split("\\&");
			
			for(int index = 0; index < arParam.length; index++)
			{
				String[] arValue = arParam[index].split("=");
				
				if(arValue.length != 2)
					continue;
				
				m_QueryStrList.add(new BasicNameValuePair(arValue[0].replace("?", ""), arValue[1]));
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}		
		
		return true;
	}
	
	public String GetValue(String key)
	{
		int count = m_QueryStrList.size();
		
		for(int index = 0; index < count; index++)
		{
			NameValuePair item = m_QueryStrList.get(index);
			
			if(key.equalsIgnoreCase(item.getName()))
				return item.getValue();
		}
				
		return "";
	}
}
