package cn.co.HunetLib.OldPlayer;

import cn.co.HunetLib.Common.AppMain;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;


/**
 * @author hyoungil
 * 네트워크 관련 매소드 클래스
 * 작업자 : 장형일
 * 작업일 : 2011.08
 */
public class HunetNetworkMethod {
	
	static Hunet_Sql_CRUD_HUNET_MLC_ACCESS_Cls AccessCls;
	
	/*
	 * 생성자
	 */
	public HunetNetworkMethod() {
		
	}
	
	/**
	 * 네트워크 연결 확인
	 * @param context
	 * @return
	 */
	public static boolean connectConfirm(Context context)
	{
		String rtnAccess;
		if(AppMain.getId().equals(""))
		{
			rtnAccess = "1";
		}
		else
		{
			AccessCls = new Hunet_Sql_CRUD_HUNET_MLC_ACCESS_Cls(context, AppMain.getId());
			
			rtnAccess = AccessCls.Select_Access_Data()[1];
		}
		
		if(rtnAccess.equals("0"))
			return connectWifiConfirm(context);
		else
			return connectAllConfirm(context);
	}
	
	/***
	 * WIFI 3G 연결 모두 확인
	 * @param context
	 * @return
	 */
	public static boolean connectAllConfirm(Context context)
	{
		try
		{
			if(connect3GConfirm(context) || connectWifiConfirm(context) || connectWimaxConfirm(context))
				return true;
			else
				return false;
		}
		catch (Exception e) {
			// TODO: handle exception
			try
			{
				if(connectWifiConfirm(context))
					return true;
				else
					return false;
			}
			catch (Exception e2) {
				// TODO: handle exception
				return false;
			}
		}
	}
	
	/***
	 * 3G망 연결 확인
	 * @param context
	 * @return
	 */
	public static boolean connect3GConfirm(Context context)
	{
		ConnectivityManager cm =  (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		
		try
		{
			NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
			
			if(mobile.isConnected())
				return true;
			else
				return false;
		}
		catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	}
	
	/**
	 * WIFI 연결 확인
	 * @param context
	 * @return
	 */
	public static boolean connectWifiConfirm(Context context)
	{
		ConnectivityManager cm =  (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		
		try
		{
			NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		
			if(wifi.isConnected())
				return true;
			else
				return false;
		}
		catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	}
	
	/**
	 * 4G 연결 확인
	 * @param context
	 * @return
	 */
	public static boolean connectWimaxConfirm(Context context)
	{
		ConnectivityManager cm =  (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

		try
		{
			NetworkInfo wimax = cm.getNetworkInfo(ConnectivityManager.TYPE_WIMAX);
			
			if(wimax.isConnected()) return true;
			else return false;
		}
		catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	}
}
