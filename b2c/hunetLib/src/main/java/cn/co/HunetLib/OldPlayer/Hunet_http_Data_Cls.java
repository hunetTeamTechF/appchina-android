package cn.co.HunetLib.OldPlayer;

import hunet.domain.DomainAddress;

import java.io.InputStream;

import cn.co.HunetLib.Common.AppMain;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * 강의관련 HTTP 통신.
 * 작업자 : 장형일
 * 작업일 : 2011.06
 * 일반 데이터 및 상상마루 플레이어용
 */
public class Hunet_http_Data_Cls extends Hunet_Http_Post_Cls {
	
	/*
	 * 기본 생성자.
	 */
	public Hunet_http_Data_Cls() {
		
	}
	

	public JSONObject LoginAct(String userId, String userPw)
	{
		JSONObject Jobj = null;
		
		setUrl(DomainAddress.getUrlMLC() + "/JLog");
		setType("1");
		setParam("uid=" + userId + "&pw=" + userPw);
		InputStream instream = hunetHttpPostData();
		
		Jobj = HunetJsonMethod.convertJsonObject(instream);
		
		return Jobj;
	}
	
	public String LastVersionInfo()
	{
		String lastVersion = "";
		JSONObject jobj = null;
		
		setUrl(DomainAddress.getUrlMLC() + "/JLog");
		setType("28");
		setParam("device=android&app=mlc");
		InputStream instream = hunetHttpPostData();
		
		//Json데이터 변환
		jobj = HunetJsonMethod.convertJsonObject(instream);
		
		//ContentsData 데이터 변환
		try {
			lastVersion = jobj.getString("Version");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return lastVersion;
	}
	
	/*public String LastVersionInfo()
	{
		String lastVersion = "";
		JSONObject jobj = null;
		
		setUrl(DomainAddress.getUrlMLC() + "/App/JLog.aspx");
		setType("18");
		InputStream instream = hunetHttpPostData();
		
		//Json데이터 변환
		jobj = HunetJsonMethod.convertJsonObject(instream);
		
		//ContentsData 데이터 변환
		try {
			lastVersion = jobj.getString("Version");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return lastVersion;
	}*/
	
	public String NoticeNew()
	{
		String newSuccess = "";
		JSONObject jobj = null;
		
		setUrl(DomainAddress.getUrlMLC() + "/JLog");
		setType("2");
		setParam("uid=" + AppMain.getId() + "&pw=" + AppMain.getPw() + "&company_seq=" + AppMain.getCompany_seq());
		InputStream instream = hunetHttpPostData();
		
		//Json데이터 변환
		jobj = HunetJsonMethod.convertJsonObject(instream);
		
		//ContentsData 데이터 변환
		try {
			newSuccess = jobj.getString("IsSuccess");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return newSuccess;
	}
	
	/**
     * 상상마루 영상정보 조회
     * @param contractNo 계약번호
     * @param contentsSeq 컨텐츠번호
     * @param goodsId 상품번호
     * @return
     */
	public JSONObject SelectContentsInfo(int contractNo, int contentsSeq, String goodsId)
	{
		JSONObject jobj = null;
		
		setUrl(DomainAddress.getUrlMLC() + "/JLog");
		setType("21");
		setParam("company_seq=" + AppMain.getCompany_seq() + "&uid=" + AppMain.getId() + "&contract_no=" + String.valueOf(contractNo)
				+ "&contents_seq=" + String.valueOf(contentsSeq) + "&goods_id=" + goodsId);
		InputStream instream = hunetHttpPostData();
		
		//Json데이터 변환
		jobj = HunetJsonMethod.convertJsonObject(instream);
		
		return jobj;
	}
	
	/**
	 * 상상마루 영상 진도 저장. 20초 단위 저장
	 * @param contractNo 계약번호
	 * @param contentsSeq 컨텐츠번호
	 * @param goodsId 상품번호
	 * @param viewSec 시청시간
	 * @param lastViewSec 시청위치
	 * @return
	 */
	public JSONObject UpdateProgressInfo(int contractNo, int contentsSeq, String goodsId, int viewSec, int viewSecMobile, int lastViewSec, String type)
	{
		JSONObject jobj = null;
		
		setUrl(DomainAddress.getUrlMLC() + "/JLog");
		setType(type);
		setParam("company_seq=" + AppMain.getCompany_seq() + "&uid=" + AppMain.getId() + "&contract_no=" + String.valueOf(contractNo) 
				+ "&contents_seq=" + String.valueOf(contentsSeq) + "&goods_id=" + goodsId + "&view_sec=" + String.valueOf(viewSec) + "&view_sec_mobile=" + String.valueOf(viewSecMobile)
				+ "&last_view_sec=" + String.valueOf(lastViewSec));
		InputStream instream = hunetHttpPostData();
		
		//Json데이터 변환
		jobj = HunetJsonMethod.convertJsonObject(instream);
		
		return jobj;
	}
	
	public JSONObject LoginAppToAppGetUserInfo(int companySeq, String Id)
	{
		JSONObject Jobj = null;
		
		setUrl(DomainAddress.getUrlMLC() + "/JLog");
		setType("24");
		setParam("etc1=" + String.valueOf(companySeq) + "&etc3=" + Id);
		InputStream instream = hunetHttpPostData();
		
		Jobj = HunetJsonMethod.convertJsonObject(instream);
		
		return Jobj;
	}
}
