package cn.co.HunetLib.Common;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

import cn.co.HunetLib.SQLite.SQLiteManager;
import cn.co.HunetLib.SQLite.ScheduleModel;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

public class HunetAlarmManager 
{
	private Context 			m_Context 		= null;
	private GregorianCalendar 	m_Calendar 		= null;
	private AlarmManager		m_AlarmManager 	= null;
	private SQLiteManager m_SqlManager	= null;
	
	public final int eALARM_CODE_MON = 3651;
	public final int eALARM_CODE_TUE = 3652;
	public final int eALARM_CODE_WED = 3653;
	public final int eALARM_CODE_THU = 3654;
	public final int eALARM_CODE_FRI = 3655;
	public final int eALARM_CODE_SAT = 3656;
	public final int eALARM_CODE_SUN = 3657;
	
	public HunetAlarmManager(Context context)
	{
		 m_Context 		= context;
		 m_Calendar 	= new GregorianCalendar();
		 m_AlarmManager = (AlarmManager)m_Context.getSystemService(Context.ALARM_SERVICE);
		 m_SqlManager	= new SQLiteManager(context);
	}
	
	public void RunAlarm()
	{
		ScheduleModel model = m_SqlManager.GetScheduleData();
		
		if("".equals(model.use))
			return;
		
		if("0".equals(model.use))
		{
			for(int nCode = eALARM_CODE_MON; nCode <= eALARM_CODE_SUN; nCode++)
				StopAlram(nCode);
			return;
		}
		
		String[] 	arWeekValues 	= model.week.split("&");
		int 		nHour 			= Integer.parseInt(model.hour);
		int 		nMinute 		= Integer.parseInt(model.minute);

		for(int nIndex = 0, nCode = eALARM_CODE_MON; nCode <= eALARM_CODE_SUN; nCode++, nIndex++)
		{
			if("1".equals(arWeekValues[nIndex]))
				StartAlarm(nCode, nIndex + 1, nHour, nMinute);
			else
				StopAlram(nCode);
		}
	}
	
	public void StartAlarm(int requestCode, int dayOfWeek, int hour, int minute)
	{
		Calendar 	todayCalendar 	= Calendar.getInstance(Locale.KOREA);
		Calendar 	tempCalendar	= Calendar.getInstance(Locale.KOREA);
		int 		nTodayOfWeek 	= m_Calendar.get(Calendar.DAY_OF_WEEK); // 일 월 화 수 목 금 토 일 (1, 2, 3, 4, 5, 6, 7)
		int 		nNextGabTime	= 604800 * 1000; //604800 : 60 * 60 * 24 * 7 : 1주일을 초로 했을때 값.
			
		tempCalendar.add(Calendar.DATE, (dayOfWeek - nTodayOfWeek));
		tempCalendar.set(Calendar.HOUR_OF_DAY, hour);
		tempCalendar.set(Calendar.MINUTE, minute);
		
		if(todayCalendar.after(tempCalendar)) 		tempCalendar.add(Calendar.DATE, 7); // 현재 시간이 셋팅한 시간보다 클 경우	
		else if(todayCalendar.before(tempCalendar)) {} 									// 현재 시간이 셋팅한 시간보다 작을 경우
		else 										tempCalendar.add(Calendar.DATE, 7); // 현재 시간이 셋팅한 시간이 같을 경우
		
		tempCalendar.add(Calendar.SECOND, 0);

		Intent intent = new Intent(m_Context, HunetReceiver.class);
		intent.setAction(AppMain.eACTION_ALARM_SETTING);
		PendingIntent pIntent = PendingIntent.getBroadcast(m_Context, requestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);

		m_AlarmManager.setRepeating(AlarmManager.RTC_WAKEUP, tempCalendar.getTimeInMillis(), nNextGabTime, pIntent);
	}
	
	public void StopAlram(int requestCode)
	{
		PendingIntent sender = PendingIntent.getService(m_Context, requestCode, new Intent(m_Context, HunetReceiver.class), 0);
		m_AlarmManager.cancel(sender);
	}
}
