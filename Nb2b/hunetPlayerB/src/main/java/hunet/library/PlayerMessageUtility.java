package hunet.library;

import hunet.data.SQLiteManager;
import hunet.drm.models.AccessModel;
import hunet.library.hunetplayer.R;
import hunet.net.Network;
import android.content.Context;

public class PlayerMessageUtility {

	/**
	 * 와이파이 연결 상태
	 */
	public static final	int NETWORK_WIFI = 0;
	/**
	 * 접속알림 ON / 강의시청 OFF
	 */
	public static final int NETWORK_ALARM_OFF_MOVIE_OFF = 1;
	/**
	 * 접속알림 OFF / 강의시청 ON
	 */
	public static final int NETWORK_ALARM_OFF_MOVIE_ON = 2;
	/**
	 * 접속알림 ON / 강의시청 OFF
	 */
	public static final int NETWORK_ALARM_ON_MOVIE_OFF = 3;
	/**
	 * 접속알림 ON / 강의시청 ON
	 */
	public static final int NETWORK_ALARM_ON_MOVIE_ON = 4;
	/**
	 * 오프라인 상태
	 */
	public static final int NETWORK_OFFLINE = -1;

	/**
	 * 현재 네트워크 설정상태를 가져옵니다.
	 * @param context
	 * @return 연결 상태
	 */
	public static int getNetworkSettings(Context context) {
		SQLiteManager sqlManager = new SQLiteManager(context);
		AccessModel accessModel = sqlManager.GetAccessData();

		if (Network.connectWifiConfirm(context)) {
			return NETWORK_WIFI;
		} else if (Network.connect3GConfirm(context) || Network.connectWimaxConfirm(context)) {
			// 00 (접속알림 off / 강의시청 off)
			if (!"1".equals(accessModel.access_text) && !"1".equals(accessModel.access_mov))
				return NETWORK_ALARM_OFF_MOVIE_OFF;
			
			// 01 (접속알림 off / 강의시청 on)
			if (!"1".equals(accessModel.access_text) && "1".equals(accessModel.access_mov))
				return NETWORK_ALARM_OFF_MOVIE_ON;
			
			// 10 (접속알림 on / 강의시청 off)
			if ("1".equals(accessModel.access_text) && !"1".equals(accessModel.access_mov))
				return NETWORK_ALARM_ON_MOVIE_OFF;
			
			// 11 (접속알림 on / 강의시청 on)
			if ("1".equals(accessModel.access_text) && "1".equals(accessModel.access_mov))
				return NETWORK_ALARM_ON_MOVIE_ON;
		}

		return NETWORK_OFFLINE;
	}
	
	public static void setOnMovieStatus(Context context) {
		SQLiteManager sqlManager = new SQLiteManager(context);
		AccessModel accessModel = sqlManager.GetAccessData();
		accessModel.access_mov = "1";
		sqlManager.SetAccessData(accessModel);
	}
	
	public static String getNetworkChangeMessage(Context context, int networkSettings) {
		String retVal = "";
		
		switch (networkSettings) {
		case NETWORK_WIFI:
			retVal = context.getResources().getString(R.string.play_msg_network_change);
			break;
		case NETWORK_OFFLINE:
			retVal = context.getResources().getString(R.string.play_msg_network_change);
			break;
		case NETWORK_ALARM_OFF_MOVIE_OFF:
			retVal = context.getResources().getString(R.string.play_msg_network_change);
			break;
		case NETWORK_ALARM_OFF_MOVIE_ON:
			retVal = context.getResources().getString(R.string.play_msg_network_change);
			break;
		case NETWORK_ALARM_ON_MOVIE_OFF:
			retVal = context.getResources().getString(R.string.play_msg_network_change);
			break;
		case NETWORK_ALARM_ON_MOVIE_ON:
			retVal = context.getResources().getString(R.string.play_msg_network_change_alarm_on_movie_on);
			break;
		}
		
		return retVal;
	}
	
	/**
	 * 네트워크 설정 상태에 따른 바로학습 관련 메세지를 가져옵니다.
	 * @param context
	 * @param networkSettings
	 * @return
	 */
	public static String getPlayMessage(Context context, int networkSettings) {
		String retVal = "";
		
		switch (networkSettings) {
		case NETWORK_WIFI:
			retVal = context.getResources().getString(R.string.play_msg_network_wifi);
			break;
		case NETWORK_OFFLINE:
			retVal = context.getResources().getString(R.string.play_msg_network_offline);
			break;
		case NETWORK_ALARM_OFF_MOVIE_OFF:
			retVal = context.getResources().getString(R.string.play_msg_network_alarm_off_movie_off);
			break;
		case NETWORK_ALARM_OFF_MOVIE_ON:
			retVal = context.getResources().getString(R.string.play_msg_network_alarm_off_movie_on);
			break;
		case NETWORK_ALARM_ON_MOVIE_OFF:
			retVal = context.getResources().getString(R.string.play_msg_network_alarm_on_movie_off);
			break;
		case NETWORK_ALARM_ON_MOVIE_ON:
			retVal = context.getResources().getString(R.string.play_msg_network_alarm_on_movie_on);
			break;
		}
		
		return retVal;
	}
	
	/**
	 * 네트워크 설정 상태에 따른 다운로드 관련 메세지를 가져옵니다.
	 * @param context
	 * @param networkSettings
	 * @return
	 */
	public static String getDownloadMessage(Context context, int networkSettings) {
		String retVal = "";
		
		switch (networkSettings) {
		case NETWORK_WIFI:
			retVal = context.getResources().getString(R.string.download_msg_network_wifi);
			break;
		case NETWORK_OFFLINE:
			retVal = context.getResources().getString(R.string.download_msg_network_offline);
			break;
		case NETWORK_ALARM_OFF_MOVIE_OFF:
			retVal = context.getResources().getString(R.string.download_msg_network_alarm_off_movie_off);
			break;
		case NETWORK_ALARM_OFF_MOVIE_ON:
			retVal = context.getResources().getString(R.string.download_msg_network_alarm_off_movie_on);
			break;
		case NETWORK_ALARM_ON_MOVIE_OFF:
			retVal = context.getResources().getString(R.string.download_msg_network_alarm_on_movie_off);
			break;
		case NETWORK_ALARM_ON_MOVIE_ON:
			retVal = context.getResources().getString(R.string.download_msg_network_alarm_on_movie_on);
			break;
		}
		
		return retVal;
	}
}
