package hunet.core;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class HunetJsonParse 
{
	private JSONObject m_JsonObj = null;
		
	public void SetData(String strData)
	{	
		try {
			m_JsonObj = new JSONObject(strData);
		} catch (JSONException e) { e.printStackTrace(); }
	}
	
	public String GetJsonValue(String strKey)
	{
		if(m_JsonObj == null)
			return "";
		
		String strValue = "";
				
		try 
		{
			strValue = m_JsonObj.getString(strKey);
		} 
		catch (JSONException e) {  }
		
		return strValue;
	}
	
	public String GetJsonValue(String strKey, String strDefault)
	{
		if(m_JsonObj == null)
			return strDefault;
		
		String strValue = "";
				
		try 
		{
			strValue = m_JsonObj.getString(strKey);
		} 
		catch (JSONException e) { e.printStackTrace(); }
		
		if("".equals(strValue))
			strValue = strDefault;
		
		return strValue;
	}
	
	public String GetJsonValue(JSONObject json, String strKey, String strDefault)
	{
		if(json == null)
			return strDefault;
		
		String strValue = "";
		
		try 
		{
			strValue = json.getString(strKey);
		} 
		catch (JSONException e) { e.printStackTrace(); }
		
		if("".equals(strValue))
			strValue = strDefault;
		
		return strValue;
	}
	
	public JSONArray GetJsonArray(String strKey)
	{
		if(m_JsonObj == null)
			return null;
		
		JSONArray jsonArray = null;
		
		try 
		{
			jsonArray = m_JsonObj.getJSONArray(strKey);
		} 
		catch (JSONException e) { return null; }
		
		return jsonArray;
	}
}
