package hunet.player.model;

import hunet.domain.DomainAddress;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

public class PlayerWHModel extends PlayerModelBase {
	private String contentsUrl = "";
	private String progressRestrictionYn = "";
	
	private String courseCd = "";
	private String takeCourseSeq  = "0";
	private String progressNo   = "0";
	private String currentFrame   = "0";
	private int lastViewSec = 0;

	private String userId = "0";
	private Boolean bHtmlLanguageType = false;

	public PlayerWHModel(Activity activity) {
		super(activity);
		Bundle bundle = activity.getIntent().getExtras();
		m_arMarkList = new ArrayList<MarkDataModel>();
		m_CurMarkData = new MarkDataModel();

		if (bundle == null)
			return;

		contentsUrl = bundle.getString("url");
		progressRestrictionYn = bundle.getString("progressRestrictionYN");

		if (bundle.getString("indexNo") != null
				&& bundle.getString("currentFrame") != null
				&& bundle.getString("totalFrame") != null) {
			super.title = String.format("%s차시 (%s/%s)",
					bundle.getString("indexNo"),
					bundle.getString("currentFrame"),
					bundle.getString("totalFrame"));
		}
		
		bHtmlLanguageType = bundle.getBoolean("htmlLanguageType", false);
		
		if(bHtmlLanguageType)
			return;
		
		courseCd = bundle.getString("courseCd");
		takeCourseSeq = bundle.getString("takeCourseSeq");
		progressNo = bundle.getString("progressNo");
		currentFrame = bundle.getString("currentFrame");
		lastViewSec = Integer.parseInt(bundle.getString("lastViewSec"));
		
		userId = bundle.getString("userId");

	}

	@Override
	public boolean CheckProgressRestriction() {
		return "N".equals(progressRestrictionYn);
//		return true;
	}

	@Override
	public boolean CheckSendProgress(int lastViewSec, int studySec) {
		boolean result = false;
		this.lastViewSec = lastViewSec;
		
		if(bHtmlLanguageType)
			return true;
		
		if (studySec > 0 && studySec % 60 == 0)
			result = true;
		return result;
	}

	@Override
	public void SaveProgress(int lastViewSec, int studySec) {
		if(bHtmlLanguageType)
			return;
		
		String strUrl = GetProgressUrl(lastViewSec, studySec);
		Log.d("Htmlcource WHMOdle", strUrl);
		m_SqlManager.InsertStudyProgress(strUrl, userId);
		super.m_ParentActivity.startService(m_StudyProgressService);
	}

	@Override
	public int GetLastViewSec() {
		return lastViewSec;
	}

	@Override
	public String GetPlayerUrl() {
		return contentsUrl;
	}

	private String GetProgressUrl(int lastViewSec, int studySec) {
		if(bHtmlLanguageType)
			return "";
		String strUrl = String
				.format(DomainAddress.getUrlApps() + "/Hunet_Player/Android.aspx?action=ProgressUpdateByHtmlModule&courseCd=%s&takeCourseSeq=%s&progressNo=%s&frameNo=%s&lastViewSec=%d",
						courseCd,
						takeCourseSeq,
						progressNo,
						currentFrame,
						lastViewSec);
		return strUrl;
	}

	@Override
	public boolean HasPpt() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean UseSpeeedControl() {
		return true;
	}
}
