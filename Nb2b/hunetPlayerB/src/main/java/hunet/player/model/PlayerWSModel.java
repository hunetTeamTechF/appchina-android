package hunet.player.model;

import hunet.core.JsonParse;
import hunet.domain.DomainAddress;
import hunet.net.HttpData;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

public class PlayerWSModel extends PlayerModelBase {
	private String userId = "";
	private String progressRestrictionYn = "";
	private boolean hasPpt = false;
	private String companySeq = "";
	private String contentsUrl = "";
	private int contractNo; // 계약번호
	private int contentsSeq; // 컨텐츠 번호
	private String goodsId; // 상품번호
	
	private boolean useSpeedBar = false;

	private int lastViewSec = 0;
	private int viewSec = 0;
	private int viewSecMobile = 0;

	private HttpData data;

	public PlayerWSModel(Activity activity) {
		super(activity);
		Bundle bundle = activity.getIntent().getExtras();
		// 상상마루 필수 파라미터
		if (bundle != null && bundle.containsKey("companySeq")) {
			companySeq = bundle.getString("companySeq");
			useSpeedBar = "Y".equalsIgnoreCase(m_SqlManager.GetAppSettings(Integer.parseInt(companySeq)).enable_sangsang_player_speedbar);
		}
        
		if (bundle != null && bundle.containsKey("jsonDataLoaded")
				&& bundle.getString("jsonDataLoaded") != "")
			ParseInfo(bundle.getString("jsonDataLoaded"));
		else {
			if (bundle != null && bundle.containsKey("contractNo"))
				contractNo = bundle.getInt("contractNo");
			if (bundle != null && bundle.containsKey("contentsSeq"))
				contentsSeq = bundle.getInt("contentsSeq");
			if (bundle != null && bundle.containsKey("goodsId"))
				goodsId = bundle.getString("goodsId");
			if (bundle != null && bundle.containsKey("userId"))
				userId = bundle.getString("userId");
			
			data = new HttpData();
			// data.EncodingString = "EUC-KR";
			data.url = DomainAddress.getUrlMLC() + "/App/JLog.aspx";
			data.param = String
					.format("type=21&company_seq=%s&uid=%s&contract_no=%s&contents_seq=%s&goods_id=%s",
							companySeq, userId, contractNo, contentsSeq,
							goodsId);
			hunet.net.HttpRequester.GetPostData(data);
			contentsUrl = data
					.GetJsonValue("url")
					.replace(
							"http://hunet2.hscdn.com/hunetvod/_definst_/mp4:Mvod/",
							"http://m.hunet.hscdn.com/hunet/Mvod/")
					.replace("/playlist.m3u8", "");
			lastViewSec = Integer.parseInt(data.GetJsonValue("last_view_sec"));
			viewSec = Integer.parseInt(data.GetJsonValue("view_sec"));
			viewSecMobile = Integer.parseInt(data
					.GetJsonValue("view_sec_mobile"));
			progressRestrictionYn = data.GetJsonValue("pass_yn");
			super.title = data.GetJsonValue("contents_nm");
		}
	}

	public void ParseInfo(String input) {
		JsonParse jsonParse = new JsonParse();
		jsonParse.SetData(input);		
		contractNo = Integer.parseInt(jsonParse.GetJsonValue("contract_no"));		
		contentsSeq =Integer.parseInt(jsonParse.GetJsonValue("contents_seq"));		
		goodsId = jsonParse.GetJsonValue("goods_id");	
		userId = jsonParse.GetJsonValue("user_id");		
		contentsUrl = jsonParse
				.GetJsonValue("url")
				.replace(
						"http://hunet2.hscdn.com/hunetvod/_definst_/mp4:Mvod/",
						"http://m.hunet.hscdn.com/hunet/Mvod/")
				.replace("/playlist.m3u8", "");
		lastViewSec = Integer.parseInt(jsonParse.GetJsonValue("last_view_sec"));
		viewSec = Integer.parseInt(jsonParse.GetJsonValue("view_sec"));
		viewSecMobile = Integer.parseInt(jsonParse
				.GetJsonValue("view_sec_mobile"));
		progressRestrictionYn = jsonParse.GetJsonValue("pass_yn");
		super.title = jsonParse.GetJsonValue("contents_nm");
	}

	@Override
	public boolean CheckProgressRestriction() {
		return "Y".equals(progressRestrictionYn);
	}

	@Override
	public boolean CheckSendProgress(int lastViewSec, int studySec) {
		boolean result = false;
		this.lastViewSec = lastViewSec;
		if (studySec > 0 && studySec % 60 == 0)
			result = true;
		return result;
	}

	@Override
	public void SaveProgress(int lastViewSec, int studySec) {
		String strUrl = GetProgressUrl(lastViewSec, studySec);
		Log.d("PlayerStudyDataWeb", strUrl);
		m_SqlManager.InsertStudyProgress(strUrl, userId);
		super.m_ParentActivity.startService(m_StudyProgressService);
	}

	@Override
	public int GetLastViewSec() {
		return lastViewSec;
	}

	@Override
	public String GetPlayerUrl() {
		return contentsUrl;
	}

	private String GetProgressUrl(int lastViewSec, int studySec) {
		String strUrl = String
				.format(DomainAddress.getUrlMLC() + "/App/JLog.aspx?type=22&company_seq=%s&uid=%s&contract_no=%s&contents_seq=%s&goods_id=%s&view_sec=%d&view_sec_mobile=%d&last_view_sec=%d",
						companySeq, userId, contractNo, contentsSeq, goodsId,
						viewSec + studySec, viewSecMobile + studySec,
						lastViewSec);

		return strUrl;
	}

	@Override
	public boolean HasPpt() {
		// TODO Auto-generated method stub
		return hasPpt;
	}

	@Override
	public boolean UseSpeeedControl() {
		return useSpeedBar;
	}
}
