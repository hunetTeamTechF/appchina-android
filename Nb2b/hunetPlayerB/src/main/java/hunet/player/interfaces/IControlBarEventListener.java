package hunet.player.interfaces;

import hunet.player.controls.ControlBar;

public interface IControlBarEventListener 
{
	public void 	OnChangeVolume(ControlBar controlBar, int nValue);
	public boolean 	OnClickPlay(ControlBar controlBar);
	public boolean 	OnClickPause(ControlBar controlBar);
	public boolean 	OnClickBack(ControlBar controlBar);
	public boolean 	OnClickForward(ControlBar controlBar);
	public boolean 	OnClickSound(ControlBar controlBar);
	public boolean 	OnClickSpeedUp(ControlBar controlBar);
	public boolean 	OnClickSpeedDown(ControlBar controlBar);
	public void 	OnStartTrackingTouch(ControlBar controlBar, int progress);
	public void 	OnStopTrackingTouch(ControlBar controlBar, int progress);
	public void 	OnProgressChanged(ControlBar controlBar, int progress);
	public boolean 	OnClickIndexFull(ControlBar controlBar, boolean bIsFull);
	public boolean 	OnClickPptFull(ControlBar controlBar, boolean bIsFull);
	public void 	OnClickComplete(ControlBar controlBar);
	public void 	OnClickScreenFit(boolean fit);
	public void 	OnClickRepeat(String status);
}
