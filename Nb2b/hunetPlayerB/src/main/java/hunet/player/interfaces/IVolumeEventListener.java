package hunet.player.interfaces;

import hunet.player.controls.VolumeBar;
import android.view.MotionEvent;
import android.view.View;

public interface IVolumeEventListener 
{
	public void OnChangeVolume(VolumeBar v, int nValue);
	public boolean onTouch(View view, MotionEvent event);
}
