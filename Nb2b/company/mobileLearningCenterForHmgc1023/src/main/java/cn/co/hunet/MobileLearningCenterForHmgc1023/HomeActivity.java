package cn.co.hunet.MobileLearningCenterForHmgc1023;

import android.os.Bundle;
import android.webkit.WebView;

import cn.co.HunetLib.Company.Company;
import cn.co.hunet.MobileLearningCenterForHmgc1023.TabClassLoader.CompanySeq;


public class HomeActivity extends cn.co.HunetLib.HomeActivity {
    private WebView wvHtml;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        int company_seq = getCompany().getSeq();
        //Fabric.with(this, new Crashlytics());
        if (company_seq == CompanySeq.SLS_PROWAY) {
            setSelectTab(Company.eTAB_CLASSROOM);
        }
    }


}
