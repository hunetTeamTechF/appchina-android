package cn.co.hunet.MobileLearningCenterForLLemon;

import android.content.res.Resources;
import android.provider.Settings;

import cn.co.HunetLib.Company.Company;
import cn.co.HunetLib.Control.TabControl;


public class MainCompany extends Company 
{
	public MainCompany()
	{
		super();
		AppName						= "L-LEMON";
		Seq 						= 1002;
		DrmUse 						= true;
		GCMSendManageEnabled 		= true;
		EnabledSangSangOverlapStudy = true;

		LoginTop 			= 290;
		LoginBottom 		= 120;
		LoginDrmTop 		= 0;
		LoginDrmBottom 		= 30;
		RuleId				= "lgcare-";
		eURL_DEFAULT_HOST = "";
	}

	@Override
	protected void CreateViewModel()
	{

		setViewModel(eTAB_NOTICE,  "学员公告");
		setViewModel(eTAB_QNA, "1:1 咨询");
		//setViewModel(eTAB_MEMBERINFO, "회원정보 관리");
		//setViewModel(eTAB_PASSWORD, "修改密码");
		//setViewModel(eTAB_SCHEDULE, "스케줄 알리미");
		setViewModel(eTAB_CONFIG, "环境设置");
		setViewModel(eTAB_VERSION, "版本信息");
		setViewModel(eTAB_DOWNLOAD_CENTER, "下载中心");
	}

	@Override
	public Class<?> getNextActivity(int type)
	{
		Class<?> tempClass = null;

		switch(type)
		{
		case eNEXT_ACTIVITY_LOGIN:
		case eACTIVITY_HOME:
			tempClass = HomeActivity.class;
			break;

		default:
			tempClass = super.getNextActivity(type);
			break;
		}

		return tempClass;
	}

	public TabControl getTabControl()
	{
		int company_seq = getSeq();

		try {
			Class classObj = Class.forName(String.format("kr.co.hunet.MobileLearningCenterForBusiness.TabClassLoader.TabControl_%d", company_seq));
			return (TabControl) classObj.newInstance();
		} catch (Exception e) {
			return super.getTabControl();
		}
	}
}
