package cn.co.hunet.MobileLearningCenterForKia;

import cn.co.HunetLib.Company.Company;
import cn.co.hunet.MobileLearningCenterForKia.TabClassLoader.CompanySeq;

import android.os.Bundle;


public class HomeActivity extends cn.co.HunetLib.HomeActivity
{
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
	    super.onCreate(savedInstanceState);

	    int company_seq = getCompany().getSeq();
	    //Fabric.with(this, new Crashlytics());
		if(company_seq == CompanySeq.SLS_PROWAY){
			setSelectTab(Company.eTAB_CLASSROOM);
		}

	}
}
