package cn.co.hunet.MobileLearningCenterForFurterer;

import cn.co.HunetLib.Company.Company;
import cn.co.HunetLib.Control.TabControl;
import hunet.domain.DomainAddress;

public class MainCompany extends Company {
    public MainCompany() {
        super();
        AppName = "Furterer";
        Seq = 1033;
        DrmUse = true;
        GCMSendManageEnabled = true;
        EnabledSangSangOverlapStudy = true;

        LoginTop = 190;
        LoginBottom = 220;
        LoginDrmTop = 0;
        LoginDrmBottom = 30;
        RuleId = "rf-";
        eURL_DEFAULT_HOST = "";
    }

    @Override
    protected void CreateViewModel() {
        setViewModel(eTAB_NOTICE, "学员公告");
        setViewModel(eTAB_QNA, "在线答疑");
        setViewModel(eTAB_MEMBERINFO, "个人信息");
        //setViewModel(eTAB_PASSWORD, "修改密码");
        //setViewModel(eTAB_SCHEDULE, "스케줄 알리미");
        setViewModel(eTAB_CONFIG, "环境设置");
        setViewModel(eTAB_VERSION, "版本信息");
        setViewModel(eTAB_DOWNLOAD_CENTER, "下载中心");
    }

    @Override
    public Class<?> getNextActivity(int type) {
        Class<?> tempClass = null;

        switch (type) {
            case eNEXT_ACTIVITY_INTRO:
            case eNEXT_ACTIVITY_NOTIFY_ALARM:
            case eACTIVITY_LOGIN:
                tempClass = LoginActivity.class;
                break;
            case eNEXT_ACTIVITY_LOGIN:
                tempClass = HomeActivity.class;
                break;
            case eACTIVITY_HOME:
                tempClass = HomeActivity.class;
                break;

            default:
                tempClass = super.getNextActivity(type);
                break;
        }

        return tempClass;
    }

    public TabControl getTabControl() {
        int company_seq = getSeq();

        try {
            Class classObj = Class.forName(String.format("cn.co.hunet.MobileLearningCenterForFurterer.TabClassLoader.TabControl_c", company_seq));
            return (TabControl) classObj.newInstance();
        } catch (Exception e) {
            return super.getTabControl();
        }
    }

    @Override
    public String getUrl(int type) {
        String url = "";
        switch (type) {
            case eURL_LOGIN:
                url = String.format("%s/JLog", DomainAddress.getUrlMLC());
                break;
            default:
                url = super.getUrl(type);
                break;
        }
        return url;
    }
}
