package cn.co.hunet.MobileLearningCenterForKcc;

/**
 * Created by albert on 2016. 8. 5..
 */



import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.view.View.OnFocusChangeListener;
import android.widget.Toast;

import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

import cn.co.HunetLib.Common.AppMain;
import cn.co.HunetLib.Common.Define;
import cn.co.HunetLib.Common.DeviceUtility;
import cn.co.HunetLib.Common.DownLoadManager;
import cn.co.HunetLib.Common.HunetNetwork;
import cn.co.HunetLib.Common.UpdataInfo;
import cn.co.HunetLib.Company.Company;
import cn.co.HunetLib.GCM.GCMRegistUtil;
import cn.co.HunetLib.Http.HunetHttpData;
import cn.co.HunetLib.SQLite.AccessModel;
import cn.co.HunetLib.SQLite.AppEventModel;
import cn.co.HunetLib.SQLite.LoginModel;
import cn.co.HunetLib.SQLite.SQLiteManager;
import cn.co.HunetLib.Service.HttpSenderService;
import cn.co.hunet.MobileLearningCenterForKcc.R;
import hunet.domain.DomainAddress;
import hunet.drm.download.DownloadCourseActivity;
import hunet.drm.models.AppSettingsModel;
import hunet.drm.models.DownloadCenterSettingsModel;
import hunet.library.Utilities;

import com.baidu.android.pushservice.PushConstants;
import com.baidu.android.pushservice.PushManager;


public class LoginActivity extends cn.co.HunetLib.LoginActivity {

    private ViewGroup m_LoginBackground;
    private LinearLayout m_LoginTop;
    private LinearLayout m_LoginBottom;
    private LinearLayout m_LoginDrmTop;
    private LinearLayout m_LoginDrmBottom;
    private EditText m_LoginUserId;
    private EditText m_LoginUserPwd;
    private ImageButton m_LoginRun;
    protected ImageButton m_LoginDrmOffline;
    private TextView m_AutoLogin;
    private TextView m_SaveId;
    private TextView GetBackPwd;
    private SQLiteManager m_SQLiteManager;
    private hunet.data.SQLiteManager manager;
    private LoginModel m_LoginModel;
    private boolean m_IsKillApp = false;
    private String m_NextUrl = "";
    private boolean m_SSo = false;

    private int m_TabIndex = 0;
    private String m_IndexDetail = "";

    int ClickCount = 0;

    private final int eLOGIN_INFO = 1;
    private final int eVERSION_INFO = 2;

    /*앱버전업데이트 관련*/
    private final String TAGU = this.getClass().getName();
    private final int UPDATA_NONEED = 0;
    private final int UPDATA_CLIENT = 1;
    private final int GET_UNDATAINFO_ERROR = 2;
    private final int DOWN_ERROR = 4;
    public UpdataInfo info;

    public String DownUrl;
    public String PushApiKey = "b0IZDOcrDFsYSwA6IEnCyF0Z";

    private static final String TAG = LoginActivity.class.getSimpleName();


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(cn.co.hunet.MobileLearningCenterForKcc.R.layout.login_activity);


        m_SQLiteManager = getSQLiteManager();
        manager = new hunet.data.SQLiteManager(this);

        registRunApplicationFlag();

        if (getResourceId(this, "@id/loginTop") > 0)
            m_LoginTop = (LinearLayout) findViewById(getResourceId(this, "@id/loginTop"));
        if (getResourceId(this, "@id/loginBottom") > 0)
            m_LoginBottom = (LinearLayout) findViewById(getResourceId(this, "@id/loginBottom"));
        if (getResourceId(this, "@id/loginDrmTop") > 0)
            m_LoginDrmTop = (LinearLayout) findViewById(getResourceId(this, "@id/loginDrmTop"));
        if (getResourceId(this, "@id/loginDrmBottom") > 0)
            m_LoginDrmBottom = (LinearLayout) findViewById(getResourceId(this, "@id/loginDrmBottom"));
        if (getResourceId(this, "@id/autoLogin") > 0) {
            View v = findViewById(getResourceId(this, "@id/autoLogin"));

            if (v instanceof CheckBox)
                new AlertDialog.Builder(this).setMessage("CheckBox는 API간 UI 호환성 문제로 TextView의 DrawableLeft 속성으로 대체해 주시기 바랍니다.").setPositiveButton("확인", null).show();

            m_AutoLogin = (TextView) v;
        }
        if (getResourceId(this, "@id/saveId") > 0) {
            View v = findViewById(getResourceId(this, "@id/saveId"));

            if (v instanceof CheckBox)
                new AlertDialog.Builder(this).setMessage("CheckBox는 API간 UI 호환성 문제로 TextView의 DrawableLeft 속성으로 대체해 주시기 바랍니다.").setPositiveButton("확인", null).show();

            m_SaveId = (TextView) v;
        }

        m_LoginUserId = (EditText) findViewById(R.id.loginId);
        m_LoginUserPwd = (EditText) findViewById(R.id.loginPwd);

        m_LoginBackground = (ViewGroup) findViewById(R.id.loginBackground);
        m_LoginRun = (ImageButton) findViewById(R.id.loginRun);
        m_LoginDrmOffline = (ImageButton) findViewById(R.id.loginDrmOffline);

        m_LoginBackground.setOnClickListener(this);
        m_LoginRun.setOnClickListener(this);
        m_LoginDrmOffline.setOnClickListener(this);
        //m_LoginDrmOffline.setLayoutParams();

        m_LoginUserId.setOnFocusChangeListener(myEditTextFocus);
        m_LoginUserPwd.setOnFocusChangeListener(myEditTextFocus);

        //SetLoginLayout();
        GetBackPwd = (TextView) findViewById(R.id.findPwd);
        GetBackPwd.setMovementMethod(LinkMovementMethod.getInstance());

        InitIntent(this.getIntent());
    }



    protected OnFocusChangeListener myEditTextFocus =  new OnFocusChangeListener() {
        public void onFocusChange(View view, boolean gainFocus) {
            //onFocus
//            if (gainFocus) {
//                //set the text
//                ((EditText) view).setBackgroundColor(Color.parseColor("#FFFFFF"));
//            }
//            //onBlur
//            else {
//                //clear the text
//                ((EditText) view).setBackgroundColor(Color.parseColor("#FFFFFF"));
//            }
        };
    };



    protected boolean isSSO() {
        return m_SSo;
    }

    protected void SetLoginLayout() {
        if (m_LoginTop != null) {
            LayoutParams topParam = (LayoutParams) m_LoginTop.getLayoutParams();
            topParam.weight = getCompany().LoginTop;
            m_LoginTop.setLayoutParams(topParam);
        }

        if (m_LoginBottom != null) {
            LayoutParams bottomParam = (LayoutParams) m_LoginBottom.getLayoutParams();
            bottomParam.weight = getCompany().LoginBottom;
            m_LoginBottom.setLayoutParams(bottomParam);
        }

        if (m_LoginDrmTop != null) {
            LayoutParams drmTopParam = (LayoutParams) m_LoginDrmTop.getLayoutParams();
            drmTopParam.weight = getCompany().LoginDrmTop;
            m_LoginDrmTop.setLayoutParams(drmTopParam);
        }

        if (m_LoginDrmBottom != null) {
            LayoutParams drmBottomParam = (LayoutParams) m_LoginDrmBottom.getLayoutParams();
            drmBottomParam.weight = getCompany().LoginDrmBottom;
            m_LoginDrmBottom.setLayoutParams(drmBottomParam);
        }
    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        InitIntent(intent);
    }

    private void InitIntent(Intent intent) {
        setFirstResume(true);
        Bundle bundle = intent.getExtras();
        m_IsKillApp = this.getBundleValue(bundle, "KILL_APP", false);
        m_NextUrl = this.getBundleValue(bundle, "NEXT_URL", "");
        m_SSo = this.getBundleValue(bundle, "sso", false);
        Define notify = this.getBundleValue(bundle, "DEFINE_NOTIFY", Define.NONE);
        m_TabIndex = this.getBundleValue(bundle, "TAB_INDEX", 0);
        m_IndexDetail = this.getBundleValue(bundle, "INDEX_DETAIL", "");
        m_LoginModel = m_SQLiteManager.GetLoginData();

        //SSO인지 체크
        if (m_SSo)
            sso_login(intent);

        // 자동로그인 체크박스 상태 복원
        if (m_AutoLogin != null) {
            m_AutoLogin.setSelected(!"0".equals(m_LoginModel.auto_login));
            m_AutoLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    v.setSelected(!v.isSelected());
                    // 자동로그인이 체크될 경우 아이디 저장이 체크되어야 함.
                    if (v.isSelected()) {
                        if (m_SaveId != null)
                            m_SaveId.setSelected(true);
                    }
                }
            });
        }

        // 아이디 저장 체크박스 상태 복원
        if (m_SaveId != null) {
            m_SaveId.setSelected(!"0".equals(m_LoginModel.save_id));
            m_SaveId.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    v.setSelected(!v.isSelected());
                    // 개인정보 저장을 취소한 경우 자동로그인도 취소되어야 함.
                    if (!v.isSelected()) {
                        if (m_AutoLogin != null)
                            m_AutoLogin.setSelected(false);
                    }
                }
            });
        }

        // 아이디 저장 선택을 하지 않은 경우 자동로그인은 해제 (로그인 정보 삭제)
        if ("0".equals(m_LoginModel.save_id)) {
            m_LoginModel.id = "";
            m_LoginModel.pwd = "";
            m_LoginModel.auto_login = "0";
            m_SQLiteManager.SetLoginData(m_LoginModel);
        }

        switch (notify) {
            case NOTIFY_GCM_NOTI_REGIST: {
                String userSeq = this.getBundleValue(bundle, "USER_SEQ", "");

                if ("".equals(userSeq) == false) {
                    AppEventModel model = new AppEventModel();
                    model.max_count = 10;
                    model.url = getCompany().getUrl(Company.eURL_NOTIFY_EVENT) + "&eventType=click&userSeq=" + userSeq;
                    m_SQLiteManager.InsertAppEvent(model);
                }
            }
            break;
        }

        Intent httpSenderService = new Intent(this, HttpSenderService.class);
        this.startService(httpSenderService);
    }

    @Override
    protected void onFirstResume() {
        if (m_IsKillApp) {
            finish();
            return;
        }

        if (getCompany().getDrmUse() && getCompany().getShowButtonLoginDrm()) {
//			File file = new File(AppMain.GetExternalDir());

//			if(file.exists()) {
//				m_LoginDrmOffline.setVisibility(View.VISIBLE);
//			} else {
//				m_LoginDrmOffline.setVisibility(View.INVISIBLE);
//			}

//            if (isFileExists(getApplicationContext())) {
//                m_LoginDrmOffline.setVisibility(View.VISIBLE);
//            } else {
//                m_LoginDrmOffline.setVisibility(View.INVISIBLE);
//            }
            m_LoginDrmOffline.setVisibility(View.INVISIBLE);

            if (getResourceId(this, "@id/loginDrmOfflineLayout") > 0)
                ((LinearLayout) findViewById(getResourceId(this, "@id/loginDrmOfflineLayout"))).setVisibility(m_LoginDrmOffline.getVisibility());
        }

        m_LoginModel = m_SQLiteManager.GetLoginData();

        String loginId = m_LoginModel.id;
        String ruleId = getCompany().getRuleId();

        if (!TextUtils.isEmpty(ruleId)) {
            if (loginId.toLowerCase().indexOf(ruleId.toLowerCase()) == 0)
                loginId = loginId.replaceFirst("(?i)" + ruleId, "");

            if (getCompany().getShowRuleId())
                loginId = ruleId + loginId;
        }

        m_LoginUserId.setText(loginId);

        if ("1".equals(m_LoginModel.auto_login) && "".equals(m_LoginModel.pwd) == false && m_SSo == false) {
            m_LoginUserPwd.setText(m_LoginModel.pwd);
            DoLogin();
        } else if ("".equals(m_LoginModel.id))
            m_LoginUserId.requestFocus();
        else
            m_LoginUserPwd.requestFocus();
    }

    @Override
    protected void onResume() {
        super.onResume();
        ClickCount = 0;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    protected boolean CheckLogin(String strUserId, String strUserPwd, String isAutoLogin) {
        // Step 1. 아이디 검사
        if (strUserId.length() < 1) {
            ShowToast(getString(R.string.label_text_login_check_msg));
            m_LoginUserId.requestFocus();
            return false;
        }

        // Step 2. 비밀번호 검사
        if (strUserPwd.length() < 1) {
            ShowToast(getString(R.string.label_text_pwd_check_msg));
            m_LoginUserPwd.requestFocus();
            return false;
        }

        return true;
    }

    @Override
    public void onClick(View view) {
        if (m_LoginBackground == view)
            AppMain.setSoftKeyboardVisible(this, false);

        if (m_LoginRun == view) {
            AppMain.setSoftKeyboardVisible(this, false);
            DoLogin();
        } else if (m_LoginDrmOffline == view) {
            Intent intent = new Intent(this, DownloadCourseActivity.class);
            startActivity(intent);
        }
    }

    protected void DoLogin() {
        String ruleId = AppMain.CompanyInfo.getRuleId();
        m_LoginModel.id = m_LoginUserId.getText().toString().trim();
        m_LoginModel.pwd = m_LoginUserPwd.getText().toString().trim();

        // Step 1. 아이디, 비번 검사
        if (CheckLogin(m_LoginModel.id, m_LoginModel.pwd, m_LoginModel.auto_login) == false)
            return;

        if (HunetNetwork.GetCurrentUseNetwork(this) == 0) {
            ShowToast(this.getString(R.string.app_msg_none_network));
            return;
        }

        if ("".equals(ruleId) == false) {
            if (m_LoginModel.id.toLowerCase().indexOf(ruleId.toLowerCase()) == 0)
                m_LoginModel.id = m_LoginModel.id.replaceFirst("(?i)" + ruleId, "");

            m_LoginModel.id = ruleId + m_LoginModel.id;
        }

        ShowDialog(this, "", false);

        String param = String.format("type=1&uid=%s&pw=%s&cseq=%d", URLEncoder.encode(m_LoginModel.id), URLEncoder.encode(m_LoginModel.pwd), getCompany().getLoginSeq());
        AsyncExecute(eLOGIN_INFO, getCompany().getUrl(Company.eURL_LOGIN, param), null);
    }

    @Override
    public void AsyncRequestDataResult(HunetHttpData result) {
        super.AsyncRequestDataResult(result);

        switch (result.m_nId) {
            case eLOGIN_INFO:
                // Step 1. 로그인 성공했는지 검사
                if ("YES".equals(result.GetJsonValue("IsSuccess")) == false) {
                    HideDialog();
                    ShowToast("请输入正确的用户信息！");
                    return;
                }

                // Step 2. 로그인 정보 DB 저장
                AccessModel accessModel = m_SQLiteManager.GetAccessData();

                // 동영상 강의 시청 (설정 값이 없을 경우 기본 값 셋팅)
                if ("".equals(accessModel.access_mov) && "Y".equalsIgnoreCase(getString(R.string.label_text_config_3g_mov_default_value)))
                    accessModel.access_mov = "1";
                // 3G/4G 알림 팝업 허용 (설정 값이 없을 경우 기본 값 셋팅)
                if ("".equals(accessModel.access_text) && "Y".equalsIgnoreCase(getString(R.string.label_text_config_3g_textdata_default_value)))
                    accessModel.access_text = "1";
                // 신규 소식, 결재 등 PUSH 알림 수신 (설정 값이 없을 경우 기본 값 셋팅)
                if ("".equals(accessModel.push_type) && "Y".equalsIgnoreCase(getString(R.string.label_text_config_gcm_default_value)))
                    accessModel.push_type = "1";
                // 이동식 메모리 설정
                if ("".equals(accessModel.external_memory))
                    accessModel.external_memory = "0";

                //			accessModel.access_mov 	= "".equals(accessModel.access_mov) ? "0" : accessModel.access_mov;
                //			accessModel.access_text = "".equals(accessModel.access_text) ? "1" : accessModel.access_text;
                //			accessModel.push_type 	= "".equals(accessModel.push_type) ? "1" : accessModel.push_type;
                m_SQLiteManager.SetAccessData(accessModel);

                m_LoginModel.id = result.GetJsonValue("UserId", "");
                m_LoginModel.pwd = result.GetJsonValue("Password", "");

                m_LoginModel.id = m_LoginModel.id.toString().trim();

                // 자동로그인 여부
                if (m_AutoLogin == null)
                    m_LoginModel.auto_login = "".equals(m_LoginModel.auto_login) ? "1" : m_LoginModel.auto_login;
                else
                    m_LoginModel.auto_login = m_AutoLogin.isSelected() ? "1" : "0";

                // 아이디 저장 여부
                if (m_SaveId == null)
                    m_LoginModel.save_id = "".equals(m_LoginModel.save_id) ? "1" : m_LoginModel.save_id;
                else
                    m_LoginModel.save_id = m_SaveId.isSelected() ? "1" : "0";

                m_SQLiteManager.DeleteLoginData();
                m_LoginModel.loginYn = "Y";
                m_SQLiteManager.SetLoginData(m_LoginModel);
                getCompany().LoginModel = m_LoginModel;

                // Step 3. 로그인 정보 저장
                getCompany().setDrmUse("Y".equals(result.GetJsonValue("DrmUseType").toUpperCase()));
                getCompany().setHReminderUseType(result.GetJsonValue("HReminderUseType").toUpperCase());
                getCompany().setHStoryUseType(result.GetJsonValue("HStoryUseType").toUpperCase());
                getCompany().setEduType(result.GetJsonValue("EduType").toUpperCase());
                getCompany().setDefaultHost(result.GetJsonValue("Url"));
                getCompany().setLoginSeq(Integer.parseInt(result.GetJsonValue("CompanySeq", "0")));

                if ("1".equals(getCompany().getHStoryUseType()))
                    getCompany().setViewModel(Company.eTAB_HSTORY, "H-스토리");

                if ("1".equals(getCompany().getHReminderUseType()))
                    getCompany().setViewModel(Company.eTAB_HREMINDER, "H-리마인더");

                // Step 4. 로그인 쿠키 굽기
                List<Cookie> cookies = ((DefaultHttpClient) result.m_HttpClient).getCookieStore().getCookies();

                if (cookies.isEmpty() == false) {
                    CookieSyncManager.createInstance(this);
                    CookieManager cookieManager = CookieManager.getInstance();
                    cookieManager.removeSessionCookie();

                    try {
                        Thread.sleep(500);
                    } catch (Exception e) {
                    }

                    for (int nIndex = 0; nIndex < cookies.size(); nIndex++) {
                        Cookie cookie = cookies.get(nIndex);
                        String strCookie = "";

                        strCookie = cookie.getName() + "=" + cookie.getValue();
                        strCookie += "; path=" + cookie.getPath() + "; domain=" + cookie.getDomain();

                        cookieManager.setCookie("xiunaichina.com", strCookie);
                    }

                    CookieSyncManager.getInstance().startSync();
                }

                DownloadCenterSettingsModel model = new DownloadCenterSettingsModel();
                model.company_seq = Integer.parseInt(result.GetJsonValue("CompanySeq"));
                model.only_imagine_yn = result.GetJsonValue("OnlyImagineYn");
                model.tab1_text = result.GetJsonValue("ProcessMenuAlias");
                model.tab2_text = result.GetJsonValue("ImagineMenuAlias");
                manager.SetDownloadCenterSettings(model);

                AppSettingsModel appSetting = new AppSettingsModel();
                appSetting.company_seq = Integer.parseInt(result.GetJsonValue("CompanySeq", "0"));
                appSetting.enable_sangsang_player_speedbar = result.GetJsonValue("EnableSSPlayerSpeedBar", "N");
                appSetting.enable_sangsang_player_overlap_study = getCompany().getEnabledSangSangOverlapStudy() ? "Y" : "N";
                manager.SetAppSettings(appSetting);

                GCMRegistUtil.registGcm(this);
                PushManager.startWork(getApplicationContext(),PushConstants.LOGIN_TYPE_API_KEY,PushApiKey);
                String url = String.format(getCompany().getUrl(Company.eURL_APP_VERSION), getCompany().getPackageName(this).replace("cn.co.hunet.MobileLearningCenterFor", ""));
                String ClientId = cn.co.HunetLib.BaiduUtils.getMetaValue(LoginActivity.this, PushApiKey);


                // Set Tags
                String companySeq = result.GetJsonValue("CompanySeq");
                String WorkAreaCode = result.GetJsonValue("WorkAreaCode");
                String TagStaring = "tag"+companySeq+WorkAreaCode;
                List<String> tags = cn.co.HunetLib.BaiduUtils.getTagsList(TagStaring);
                PushManager.setTags(getApplicationContext(), tags);


                AsyncExecute(eVERSION_INFO, url, null);
                break;
            case eVERSION_INFO:
                HideDialog();
                DownUrl = result.GetJsonValue("DownloadUrl","");
                String updateVersion = result.GetJsonValue("Version", "0.0.0");

                if (CheckUpdateVersion(updateVersion)) {
                    versionUpgradeMsgAlert(updateVersion);
                    return;
                }

                NextActivity();
                break;
        }
    }

    protected void versionUpgradeMsgAlert(String updateVersion) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("有新版本").setMessage(this.getString(R.string.app_msg_confirm_update))
                .setPositiveButton("更新", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
//                        String url = "market://details?id=" + getPackageName();
//                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
//                        startActivity(intent);
//                        finish();
                        downLoadApk();
                    }
                })
                .setNeutralButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        NextActivity();
                    }
                })
                .show();
    }

    protected void NextActivity() {
        Bundle bundle = getIntent().getExtras();
        String companySeq = this.getBundleValue(bundle, "cSeq", "");
        String Id = this.getBundleValue(bundle, "userId", "");
        String processCd = this.getBundleValue(bundle, "processCd", "");
        String processYear = this.getBundleValue(bundle, "processYear", "");
        String processTerm = this.getBundleValue(bundle, "processTerm", "");
        String action = this.getBundleValue(bundle, "action", "");

        // 녹십자 App to App
        if (isSSO() && ("13830".equals(companySeq) || action.equalsIgnoreCase("classroom")) && !"".equals(processCd) && !"".equals(processYear)) {
            NextActivity_MyClassroom(processCd, processYear, processTerm, companySeq);
        } else {
            Intent intent = new Intent(this, getCompany().getNextActivity(getCompany().eNEXT_ACTIVITY_LOGIN));

            if ("".equals(m_NextUrl) == false)
                intent.putExtra("NEXT_URL", m_NextUrl);

            // Code begin - SSO 기업들 차후 삭제할 코드
            if (getCompany().getSeq() == 9242 || getCompany().getSeq() == 1075 || getCompany().getSeq() == 214)
                m_TabIndex = 2;
            // Code end

            intent.putExtra("TAB_INDEX", m_TabIndex);
            intent.putExtra("INDEX_DETAIL", m_IndexDetail);
            intent.putExtra("IS_SSO", isSSO());

            m_NextUrl = m_IndexDetail = "";
            m_TabIndex = 0;

            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            startActivity(intent);
        }
    }

    /**
     * 해당 기업의 해당 과정 학습방으로 이동 합니다.
     *
     * @param processCd   과정코드
     * @param processYear 년도
     * @param processTerm 기수
     * @param companySeq  기업코드
     */
    private void NextActivity_MyClassroom(String processCd, String processYear, String processTerm, String companySeq) {
        // 학습방으로 이동
        Intent intent = new Intent(this, getCompany().getNextActivity(getCompany().eACTIVITY_CLASSROOM));

        intent.putExtra("type", "userCourseRoom");
        intent.putExtra("processCd", processCd);
        intent.putExtra("processYear", processYear);
        intent.putExtra("processTerm", processTerm);
        intent.putExtra("cSeq", companySeq);
        intent.putExtra("userId", m_LoginModel.id);

        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }

    private void registRunApplicationFlag() {
        DeviceUtility deviceUtility = new DeviceUtility(this);
        String packageNm = "";
        String modelName = "";
        String androidID = "";

        try {
            packageNm = URLEncoder.encode(getPackageName(), "utf-8");
            modelName = URLEncoder.encode(deviceUtility.getModel(), "utf-8");
            androidID = URLEncoder.encode(deviceUtility.getAndroidId(), "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        AppEventModel model = new AppEventModel();
        model.url = String.format(DomainAddress.getUrlMLC() + "/App/JLog?type=1002&userId=%s&deviceModel=%s&androidId=%s&gcmId=%s&packageNm=%s&companySeq=%d"
                , "", modelName, androidID, "", packageNm, getCompany().getSeq());

        m_SQLiteManager.InsertAppEvent(model);

        startService(new Intent(this, HttpSenderService.class));
    }



    protected void sso_login(Intent intent) {
        Bundle bundle = intent.getExtras();

        String companySeq = this.getBundleValue(bundle, "cSeq", "");
        String Id = this.getBundleValue(bundle, "userId", "");

        String param = String.format("type=24&etc3=%s&etc1=%s", URLEncoder.encode(Id), companySeq);
        ShowDialog(this, "", false);
        AsyncExecute(eLOGIN_INFO, getCompany().getUrl(Company.eURL_LOGIN, param), null);
    }

    protected boolean isFileExists(Context context) {
        boolean bExist = false;
        File file = new File(AppMain.GetExternalDir()); // hunet_mlc
        bExist = isFileExist(file);

        if (bExist)
            return true;

        if (Utilities.getExternalMemoryCheck(context)) {
            file = Utilities.getExternalMemoryFileHandle(context);
            bExist = isFileExist(file);
            if (bExist)
                return true;
        }

        file = Utilities.getInternalMemoryFileHandle(context);
        bExist = isFileExist(file);

        return bExist;
    }

    protected boolean isFileExist(File file) {
        boolean bExist = false;

        if (file == null || (file.exists() == false))
            return bExist;

        File[] fileList = file.listFiles();

        if (fileList.length <= 0) {
            if (file.exists())
                file.delete();
            return false;
        }

        String message = null;

        for (int i = 0; i < fileList.length; i++) {

            if (fileList[i].isDirectory()) {
                message = "[디렉토리] ";
                message += fileList[i].getName();
                Log.d(TAG, message);

                try {
                    if (!subDirList(fileList[i].getCanonicalPath().toString())) {
                        // 비어있는 폴더 삭제
                        if (fileList[i].exists())
                            fileList[i].delete();
                        Log.d(TAG, "폴더 삭제");
                    } else {
                        bExist = true;
                        break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                message = "[파일] ";
                message += fileList[i].getName();
                bExist = true;
                Log.d(TAG, message);
                break;
            }
        }
        return bExist;
    }

    private static boolean subDirList(String source) {
        File dir = new File(source);
        File[] fileList = dir.listFiles();

        if (fileList.length <= 0) {
            return false;
        }

        try {
            for (int i = 0; i < fileList.length; i++) {
                File file = fileList[i];
                if (file.isFile()) {
                    // 파일이 있다면 파일 이름 출력
                    Log.d(TAG, "[파일 이름] = " + file.getName());
                    return true;
                } else if (file.isDirectory()) {
                    // 서브디렉토리가 존재하면 재귀적 방법으로 다시 탐색
                    if (!subDirList(file.getCanonicalPath().toString())) {
                        if (file.exists())
                            file.delete();
                        return false;
                    } else {
                        return true;
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }


    /* 앱접속시 최초 버전체크 및 다운로드 설치 */


    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
            switch (msg.what) {
                case UPDATA_NONEED:
                    Toast.makeText(getApplicationContext(), "不需要更新",
                            Toast.LENGTH_SHORT).show();
                case UPDATA_CLIENT:
                    //对话框通知用户升级程序
                    showUpdataDialog();
                    break;
                case GET_UNDATAINFO_ERROR:
                    //服务器超时
                    Toast.makeText(getApplicationContext(), "获取服务器更新信息失败", 1).show();
                    break;
                case DOWN_ERROR:
                    //下载apk失败
                    Toast.makeText(getApplicationContext(), "下载新版本失败", 1).show();
                    break;
            }
        }
    };


    /*
     *
     * 弹出对话框通知用户更新程序
     *
     * 弹出对话框的步骤：
     *  1.创建alertDialog的builder.
     *  2.要给builder设置属性, 对话框的内容,样式,按钮
     *  3.通过builder 创建一个对话框
     *  4.对话框show()出来
     */
    protected void showUpdataDialog() {
        AlertDialog.Builder builer = new AlertDialog.Builder(this);
        builer.setTitle("版本升级");
        builer.setMessage(info.getDescription());
        //当点确定按钮时从服务器上下载 新的apk 然后安装   װ
        builer.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Log.i(TAGU, "下载apk,更新");
                downLoadApk();
            }
        });
        builer.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                //do sth
            }
        });
        AlertDialog dialog = builer.create();
        dialog.show();
    }

    /*
     * 从服务器中下载APK
     */
    protected void downLoadApk() {

        final ProgressDialog pd;    //进度条对话框
        pd = new  ProgressDialog(this);
        pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        pd.setMessage("正在下载更新");
        pd.show();

        new Thread(){
            @Override
            public void run() {
                try {
                    File file = DownLoadManager.getFileFromServer(DownUrl, pd);
                    sleep(3000);
                    installApk(file);
                    pd.dismiss(); //结束掉进度条对话框
                } catch (Exception e) {
                    Message msg = new Message();
                    msg.what = DOWN_ERROR;
                    handler.sendMessage(msg);
                    e.printStackTrace();
                }
            }}.start();
    }

    //安装apk
    protected void installApk(File file) {
        Intent intent = new Intent();
        //执行动作
        intent.setAction(Intent.ACTION_VIEW);
        //执行的数据类型
        intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
        startActivity(intent);
    }



}
