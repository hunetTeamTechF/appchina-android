package cn.co.hunet.MobileLearningCenterForLLemon;

import cn.co.HunetLib.Company.Company;
import cn.co.hunet.MobileLearningCenterForLLemon.TabClassLoader.CompanySeq;
import android.os.Bundle;
//import com.crashlytics.android.Crashlytics;
//import io.fabric.sdk.android.Fabric;


public class HomeActivity extends cn.co.HunetLib.HomeActivity
{
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
	    super.onCreate(savedInstanceState);

	    int company_seq = getCompany().getSeq();
	    //Fabric.with(this, new Crashlytics());
		if(company_seq == CompanySeq.SLS_PROWAY){
			setSelectTab(Company.eTAB_CLASSROOM);
		}

	}
}
