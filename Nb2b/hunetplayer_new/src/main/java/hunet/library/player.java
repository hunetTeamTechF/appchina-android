package hunet.library;


import hunet.data.SQLiteManager;
import hunet.domain.DomainAddress;
import hunet.library.hunetplayer.R;
import hunet.net.AsyncHttpRequestData;
import hunet.net.HttpData;
import hunet.net.IAsyncHttpRequestData;
import hunet.net.Network;
import hunet.player.stwplayer.HunetStwPlayerActivity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

public class player implements IAsyncHttpRequestData {	
	private class CommonData
	{
		public String courseCd		= "";
		public String takeCourseSeq	= "";
		public String chapterNo		= "";
		public String frameNo		= "";
		public String view_index	= "";

		// player
		public String defaultInfo 	= "";
		public String progressInfo	= "";
		public String markList 		= "";
		public String playUrl 		= "";
		public String title 		= "";
	}

	String actionNm = "";
	private final int step1ProgressInfo 	= 1; // 진도 정보 가져오기
	//private final int step2ProgressInfoUrlInfo = 2; // 플레이 url 가져 오기
	private final int step3MarkList		= 3; // 마크 정보 가져 오기
	private final int step1SangSangInfo = 4;
	protected Context context;
	protected String userId;
	CommonData commonData;
	String companySeq = "0";
	private ProgressDialog loadingDialog = null;
	private AsyncHttpRequestData asyncReqData;
	private SQLiteManager sqlManager;
	public player(Context context, String userId) {
		// TODO Auto-generated constructor stub
		this.context = context;
		this.userId = userId;
		commonData = new CommonData();
		asyncReqData = new AsyncHttpRequestData();
		asyncReqData.SetEventListener(this);
		sqlManager = new SQLiteManager(context.getApplicationContext());		
	}	
	private void SangSangStep1(String url){

		String[] strTemp;
		strTemp = url.replace("sangsangdrmplayer://", "").split("/");	

		String strReqUrl 	= DomainAddress.getUrlMLC() + "/App/JLog.aspx";
		String strReqParam 	= String.format("type=21&company_seq=%s&uid=%s&contract_no=%s&contents_seq=%s&goods_id=%s"
				,companySeq,userId,strTemp[0],strTemp[1],strTemp[2]); 

		ShowDialog(context, "동영상 재생을 위한 정보\n구성중입니다.\n\n잠시만 기다려주세요.", true);
		asyncReqData.Request(step1SangSangInfo, strReqUrl, strReqParam, null);	


	}
	public String CheckAction()
	{
		String tempActionNm = actionNm;
		actionNm = "";
		return tempActionNm;		
	}

//	protected int confirmMovNetwork() {
//		int rtnResult; // 0 : wifi 연결, 1: 3G연결허용, 2: 3G연결불가, 3: 네트워크 연결 안됨.
//		String[] rtnAccess;
//
//		rtnAccess = sqlManager.Select_Access_Data();
//
//		if (Network.connectWifiConfirm(context)) {
//			rtnResult = 0;
//		} else if (Network.connect3GConfirm(context)
//				|| Network.connectWimaxConfirm(context)) {
//			if (!rtnAccess[2].equals("1"))
//				rtnResult = 3;
//			else {
//				if (rtnAccess[1].equals("1"))
//					rtnResult = 2;
//				else
//					rtnResult = 1;
//			}
//		} else
//			rtnResult = 4;
//
//		return rtnResult;
//	}

	public void ConfirmSangSangDrmPlayer(final String url,String companySeq)
	{		
		this.companySeq = companySeq;
		
		int network = PlayerMessageUtility.getNetworkSettings(this.context);
		String message = PlayerMessageUtility.getPlayMessage(this.context, network);

		switch (network) {
		case PlayerMessageUtility.NETWORK_WIFI:
			SangSangStep1(url);
			actionNm= "refresh";
			break;
		case PlayerMessageUtility.NETWORK_OFFLINE:
			new AlertDialog.Builder(this.context)
			.setTitle("알림")
			.setMessage(message)
			.setPositiveButton("확인", null).show();
			break;
		case PlayerMessageUtility.NETWORK_ALARM_OFF_MOVIE_OFF:
			new AlertDialog.Builder(this.context)
			.setTitle("알림")
			.setMessage(message)
			.setPositiveButton("확인", null).show();
			break;
		case PlayerMessageUtility.NETWORK_ALARM_OFF_MOVIE_ON:
			SangSangStep1(url);
			actionNm= "refresh";
			break;
		case PlayerMessageUtility.NETWORK_ALARM_ON_MOVIE_OFF:
			new AlertDialog.Builder(this.context)
			.setTitle("알림")
			.setMessage(message)
			.setPositiveButton("확인", null).show();
			break;
		case PlayerMessageUtility.NETWORK_ALARM_ON_MOVIE_ON:
			new AlertDialog.Builder(this.context)
			.setTitle("알림")
			.setMessage(message)
			.setPositiveButton("확인", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					actionNm= "refresh";
					SangSangStep1(url);
				}
			}).show();
			break;
		}
		
//		int network =  confirmMovNetwork();
//		if(network == 0 || network == 1){
//			SangSangStep1(url);
//			actionNm= "refresh";
//		}
//		else if(network == 2)
//		{
//			new AlertDialog.Builder(this.context)
//			.setTitle("알림")
//			.setMessage(R.string.player_msg_data_confirm)
//			.setPositiveButton("승인", new DialogInterface.OnClickListener() {
//				@Override
//				public void onClick(DialogInterface dialog, int which) 
//				{
//					actionNm= "refresh";
//					SangSangStep1(url);
//				}
//			})
//			.setNegativeButton("취소", new DialogInterface.OnClickListener() {
//				@Override
//				public void onClick(DialogInterface dialog, int which) {
//					dialog.dismiss();
//				}
//			})
//			.show();
//		}
//		else if(network == 3)
//		{
//			new AlertDialog.Builder(context)
//			.setTitle("알림")
//			.setMessage(R.string.player_msg_data_limit)
//			.setPositiveButton("확인", new DialogInterface.OnClickListener() {
//				@Override
//				public void onClick(DialogInterface dialog, int which) {
//				}
//			})
//			.show();
//		}
//		else
//		{
//			new AlertDialog.Builder(context)
//			.setTitle("알림")
//			.setMessage(R.string.player_msg_none_network)
//			.setPositiveButton("확인", new DialogInterface.OnClickListener() {
//				@Override
//				public void onClick(DialogInterface dialog, int which) {
//				}
//			})
//			.show();
//		}
	}
	public void ConfirmDrmPlayer(final String strUrl)
	{
		int network = PlayerMessageUtility.getNetworkSettings(this.context);
		String message = PlayerMessageUtility.getPlayMessage(this.context, network);

		switch (network) {
		case PlayerMessageUtility.NETWORK_WIFI:
			PlayerStep1(strUrl);
			actionNm= "refresh";
			break;
		case PlayerMessageUtility.NETWORK_OFFLINE:
			new AlertDialog.Builder(this.context)
			.setTitle("알림")
			.setMessage(message)
			.setPositiveButton("확인", null).show();
			break;
		case PlayerMessageUtility.NETWORK_ALARM_OFF_MOVIE_OFF:
			new AlertDialog.Builder(this.context)
			.setTitle("알림")
			.setMessage(message)
			.setPositiveButton("확인", null).show();
			break;
		case PlayerMessageUtility.NETWORK_ALARM_OFF_MOVIE_ON:
			PlayerStep1(strUrl);
			actionNm= "refresh";
			break;
		case PlayerMessageUtility.NETWORK_ALARM_ON_MOVIE_OFF:
			new AlertDialog.Builder(this.context)
			.setTitle("알림")
			.setMessage(message)
			.setPositiveButton("확인", null).show();
			break;
		case PlayerMessageUtility.NETWORK_ALARM_ON_MOVIE_ON:
			new AlertDialog.Builder(this.context)
			.setTitle("알림")
			.setMessage(message)
			.setPositiveButton("확인", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					PlayerStep1(strUrl);
					actionNm= "refresh";
				}
			}).show();
			break;
		}
		
//		int network =  confirmMovNetwork();
//		if(network == 0 || network == 1){
//			PlayerStep1(strUrl);
//			actionNm= "refresh";
//		}
//		else if(network == 2)
//		{
//			new AlertDialog.Builder(this.context)
//			.setTitle("알림")
//			.setMessage(R.string.player_msg_data_confirm)
//			.setPositiveButton("승인", new DialogInterface.OnClickListener() {
//				@Override
//				public void onClick(DialogInterface dialog, int which) 
//				{
//					PlayerStep1(strUrl);
//					actionNm= "refresh";
//				}
//			})
//			.setNegativeButton("취소", new DialogInterface.OnClickListener() {
//				@Override
//				public void onClick(DialogInterface dialog, int which) {
//					dialog.dismiss();
//				}
//			})
//			.show();
//		}
//		else if(network == 3)
//		{
//			new AlertDialog.Builder(this.context)
//			.setTitle("알림")
//			.setMessage(R.string.player_msg_data_limit)
//			.setPositiveButton("확인", new DialogInterface.OnClickListener() {
//				@Override
//				public void onClick(DialogInterface dialog, int which) {
//					dialog.dismiss();
//				}
//			})
//			.show();
//		}
//		else
//		{
//			new AlertDialog.Builder(this.context)
//			.setTitle("알림")
//			.setMessage(R.string.player_msg_none_network)
//			.setPositiveButton("확인", new DialogInterface.OnClickListener() {
//				@Override
//				public void onClick(DialogInterface dialog, int which) {
//					dialog.dismiss();
//				}
//			})
//			.show();
//		}
	}

	public void ConfirmB2CDrmPlayer(final String strUrl)
	{
		int network =  Network.GetCurrentUseNetwork(context);
		if(network == 0 || network == 1){
			PlayerStep1(strUrl);
			actionNm= "refresh";
		}
		else if(network == 2)
		{
			new AlertDialog.Builder(this.context)
			.setTitle("알림")
			.setMessage(R.string.player_msg_data_confirm)
			.setPositiveButton("승인", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) 
				{
					PlayerStep1(strUrl);
					actionNm= "refresh";
				}
			})
			.setNegativeButton("취소", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			})
			.show();
		}
		else if(network == 3)
		{
			new AlertDialog.Builder(this.context)
			.setTitle("알림")
			.setMessage(R.string.player_msg_data_limit)
			.setPositiveButton("확인", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			})
			.show();
		}
		else
		{
			new AlertDialog.Builder(this.context)
			.setTitle("알림")
			.setMessage(R.string.player_msg_none_network)
			.setPositiveButton("확인", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			})
			.show();
		}
	}

	public void ShowDialog(Context context, String msg, boolean cancelable) {
		if (context == null || loadingDialog != null)
			return;

		loadingDialog = new ProgressDialog(context);
		loadingDialog.setMessage("".equals(msg) ? "로딩하는 중입니다..." : msg);
		loadingDialog.setIndeterminate(true);
		loadingDialog.setCancelable(cancelable);
		loadingDialog.show();
	}

	public void HideDialog() {
		if (loadingDialog != null) {
			try {
				loadingDialog.dismiss();
				loadingDialog = null;
			} catch (Exception e) {
			}
		}
	}

	private void PlayerStep1(String strUrl)
	{
		String[] strTemp = strUrl.replace("drmplay://", "").split("/");

		if(strTemp.length < 4)
			return;		
		commonData.courseCd 		= strTemp[0];
		commonData.takeCourseSeq 	= strTemp[1];
		commonData.chapterNo 		= strTemp[2];
		commonData.frameNo 		= strTemp[3];

		String strReqUrl 	= DomainAddress.getUrlApps() + "/Hunet_Player/Android.aspx";
		String strReqParam 	= String.format("action=SelectProgressInfoNew&user_id=%s&course_cd=%s&chapter_no=%s&frame_no=%s&take_course_seq=%s"
				, userId, commonData.courseCd, commonData.chapterNo, commonData.frameNo, commonData.takeCourseSeq); 

		ShowDialog(context, context.getString(R.string.player_msg_request_info), true);
		asyncReqData.Request(step1ProgressInfo, strReqUrl, strReqParam, null);
	}
	/*
	private void PlayerStep2(String courseType)
	{						
		String strReqUrl 	= DomainAddress.getUrlApps() + "/Hunet_Player/Android.aspx";
		String strReqParam 	= "action=" + (courseType.equals("3") ? "ContentDetailViewByTakeCourseSeq" : "FrameDetailViewByTakeCourseSeq")
				+ "&course_cd=" + commonData.courseCd
				+ "&chapter_no=" + commonData.chapterNo
				+ "&take_course_seq="+ commonData.takeCourseSeq
				+ (courseType.equals("3") ? "&contents_no=" : "&frame_no=") + commonData.frameNo;	

		asyncReqData.Request(step2ProgressInfoUrlInfo, strReqUrl, strReqParam, null);
	}
	 */
	private void PlayerStep3()
	{
		String strReqUrl 	= DomainAddress.getUrlApps() + "/Hunet_Player/Android.aspx";
		String strReqParam 	= "action=MarkList&course_cd=" + commonData.courseCd 
				+ "&chapter_no=" + commonData.chapterNo 
				+ "&frame_no=" + commonData.frameNo 
				+ "&take_course_seq=" + commonData.takeCourseSeq;

		asyncReqData.Request(step3MarkList, strReqUrl, strReqParam, null);
	}
	@Override
	public void AsyncRequestDataResult(HttpData result) {
		// TODO Auto-generated method stub
		if("YES".equals(result.GetJsonValue("IsSuccess")) == false)
		{
			HideDialog();

			ShowDialog(context, context.getString(R.string.player_msg_response_fail), true);
			return;
		}

		switch(result.id)
		{
		case step1ProgressInfo:
		{
			String url = result.GetJsonValue("contents_url", "").toLowerCase().replace("http://hunet2.hscdn.com/hunetvod/_definst_/mp4:m_learning/","http://m.hunet.hscdn.com/hunet/M_learning/");
			url = url.replace("/playlist.m3u8", "");
			String courseType = result.GetJsonValue("course_type", "0");
			commonData.progressInfo = result.GetData();
			commonData.playUrl = url;
			commonData.title = result.GetJsonValue("title", "");
			PlayerStep3();
		}
		break;
		/*
		case step2ProgressInfoUrlInfo:
			{
				commonData.playUrl = result.GetData();
				PlayerStep3();
			}
			break;
		 */
		case step3MarkList: 		
		{
			HideDialog();
			commonData.markList = result.GetData();

			Intent intent = new Intent(context, HunetStwPlayerActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
			intent.putExtra("defaultInfo"
					, String.format("%s/%s/%s/%s/%s", commonData.courseCd, commonData.takeCourseSeq, commonData.chapterNo, commonData.frameNo, userId));

			intent.putExtra("jsonProgressInfo",commonData.progressInfo);
			intent.putExtra("jsonMarkList", 	commonData.markList);
			intent.putExtra("jsonPlayUrl", 		commonData.playUrl);
			intent.putExtra("title", commonData.title);

			context.startActivity(intent);
		}
		break;
		case step1SangSangInfo:{
			HideDialog();
			Intent intent = new Intent(context, HunetStwPlayerActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
			intent.putExtra("jsonDataLoaded", result.GetData());
			intent.putExtra("playerType", "sangsangWeb");			
			intent.putExtra("companySeq", companySeq);
			context.startActivity(intent);
		}
		break;
		}
	}

}
