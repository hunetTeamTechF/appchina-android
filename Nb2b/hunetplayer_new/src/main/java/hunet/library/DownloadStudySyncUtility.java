package hunet.library;

import hunet.data.SQLiteManager;
import hunet.domain.DomainAddress;
import hunet.drm.models.StudyIndexModel;
import hunet.drm.models.TakeCourseModel;
import hunet.net.AsyncHttpRequestData;
import hunet.net.HttpData;
import hunet.net.IAsyncHttpRequestData;
import android.content.Context;
import android.widget.Toast;

public class DownloadStudySyncUtility {

	private SQLiteManager sqlManager;
	
	/**
	 * 오프라인 진도 데이터를 전송하지 못한 목록을 재전송 합니다.
	 */
	public void doSendFailedProgressData(final Context context) {
		
		sqlManager = new SQLiteManager(context);
		
		AsyncHttpRequestData request = new AsyncHttpRequestData();
		request.SetEventListener(new IAsyncHttpRequestData() {
			
			@Override
			public void AsyncRequestDataResult(HttpData result) {
				StudyIndexModel model = (StudyIndexModel) result.tag;
				
				if("YES".equals(result.GetJsonValue("IsSuccess")))
				{			
					UpdateDownStudyIndexData(model.take_course_seq, model.chapter_no
							, Integer.parseInt(result.GetJsonValue("max_study_sec", "0")),0, model.external_memory);
					
					Toast.makeText(context, "오프라인 환경에서 학습하신 내역이 반영되었습니다.", Toast.LENGTH_LONG).show();

				}
			}
		});
		
		// 동기화 하지 못한 진도가 있을 경우 일괄 전송 하며
		// 실패시 다음 앱 실행 시 재시도 함
		for (StudyIndexModel model : sqlManager.GetDownStudyDataList()) {
			StudyIndexModel studyIndexModel = sqlManager.GetDownStudyIndexData(model.take_course_seq, model.chapter_no, model.external_memory);
			TakeCourseModel takeCourseModel = sqlManager.GetDownTakeCourseData(model.take_course_seq);
			String 				strReqUrl		= DomainAddress.getUrlApps() + "/Hunet_Player/Android.aspx";
	    	String 				strReqParam 	= String.format("action=ProgressSyncNew&courseCd=%s&takeCourseSeq=%d&chapterNo=%s&studySec=%d&deviceNm=android&regId=%s&lastStudySec=%d"
	    													, takeCourseModel.course_cd, model.take_course_seq
	    													, model.chapter_no, studyIndexModel.study_sec, "", 
	    													//studyIndexModel.max_view_sec
	    													(studyIndexModel.max_view_sec > studyIndexModel.last_view_sec ? studyIndexModel.max_view_sec : studyIndexModel.last_view_sec)
	    													);
	    	request.Request(0, strReqUrl, strReqParam, model);
		}
	}
	
	private void UpdateDownStudyIndexData(int take_course_seq, String chapter_no, int max_view_sec, int study_sec, String external_memory)
	{
		StudyIndexModel studyIndexModel = new StudyIndexModel();
		studyIndexModel.take_course_seq 	= take_course_seq;
		studyIndexModel.chapter_no 			= chapter_no;
		studyIndexModel.max_view_sec 		= max_view_sec;
		studyIndexModel.study_sec 			= study_sec;
		studyIndexModel.external_memory 	= external_memory;
		
		sqlManager.SetDownStudyIndexData(studyIndexModel);
	}
}
