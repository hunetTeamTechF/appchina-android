
package hunet.player;



import hunet.library.hunetplayer.R;
import hunet.player.stwplayer.HunetStwPlayerActivity;

import java.util.Locale;

import net.stway.sniperplayer.SniperPlayer;
import net.stway.sniperplayer.SniperPlayerVideoView;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.os.PowerManager;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

public class VideoActivity extends Activity {
    
    // App package占쏙옙筌욑옙?숋옙?롫뮉 占쎌눘?좈뇡??쑎?깍옙占쎈챷弛놅옙占?   
	private final String libraryKey = "37a4a54614d54859abc78f53622bf31e";
    private final static String LOG_TAG = "VideoActivity";

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	/*super.onCreate(savedInstanceState);
    	Intent intent2 = new Intent(VideoActivity.this, HunetStwPlayerActivity.class);
    	intent2.putExtra("defaultInfo"
				, String.format("%s/%s/%s/%s/%s", "HLSC01981", "3300023", "0101", "1", "mjg7822"));
	
    	intent2.putExtra("jsonProgressInfo", "{\"IsSuccess\":\"YES\",\"review_end_date\":\"2015-02-19\",\"course_type\":\"1\",\"parent_cd\":\"\",\"seamless_yn\":\"Y\",\"progress_no\":\"1\",\"last_mark_no\":\"1\",\"list_mov_yn\":\"N\",\"progress_restriction_yn\":\"Y\",\"study_sec\":\"5093\",\"mobile_study_sec\":\"5093\"}");
    	intent2.putExtra("jsonMarkList", 	"{\"IsSuccess\":\"YES\",\"List\":[{\"mark_no\":\"1\",\"mark_nm\":\"?숈쁺??",\"mark_value\":\"1\",\"ppt_url\":\"notPt\",\"real_frame_no\":\"1\",\"progress_yn\":\"Y\"},{\"mark_no\":\"2\",\"mark_nm\":\"?숈쁺??",\"mark_value\":\"60000\",\"ppt_url\":\"notPt\",\"real_frame_no\":\"1\",\"progress_yn\":\"Y\"},{\"mark_no\":\"3\",\"mark_nm\":\"?숈쁺??",\"mark_value\":\"120000\",\"ppt_url\":\"notPt\",\"real_frame_no\":\"1\",\"progress_yn\":\"Y\"},{\"mark_no\":\"4\",\"mark_nm\":\"?숈쁺??",\"mark_value\":\"180000\",\"ppt_url\":\"notPt\",\"real_frame_no\":\"1\",\"progress_yn\":\"Y\"},{\"mark_no\":\"5\",\"mark_nm\":\"?숈쁺??",\"mark_value\":\"240000\",\"ppt_url\":\"notPt\",\"real_frame_no\":\"1\",\"progress_yn\":\"Y\"},{\"mark_no\":\"6\",\"mark_nm\":\"?숈쁺??",\"mark_value\":\"300000\",\"ppt_url\":\"notPt\",\"real_frame_no\":\"1\",\"progress_yn\":\"Y\"},{\"mark_no\":\"7\",\"mark_nm\":\"?숈쁺??",\"mark_value\":\"360000\",\"ppt_url\":\"notPt\",\"real_frame_no\":\"1\",\"progress_yn\":\"Y\"},{\"mark_no\":\"8\",\"mark_nm\":\"?숈쁺??",\"mark_value\":\"420000\",\"ppt_url\":\"notPt\",\"real_frame_no\":\"1\",\"progress_yn\":\"Y\"},{\"mark_no\":\"9\",\"mark_nm\":\"?숈쁺??",\"mark_value\":\"480000\",\"ppt_url\":\"notPt\",\"real_frame_no\":\"1\",\"progress_yn\":\"Y\"},{\"mark_no\":\"10\",\"mark_nm\":\"?숈쁺??",\"mark_value\":\"540000\",\"ppt_url\":\"notPt\",\"real_frame_no\":\"1\",\"progress_yn\":\"Y\"},{\"mark_no\":\"11\",\"mark_nm\":\"?숈쁺??",\"mark_value\":\"600000\",\"ppt_url\":\"notPt\",\"real_frame_no\":\"1\",\"progress_yn\":\"Y\"},{\"mark_no\":\"12\",\"mark_nm\":\"?숈쁺??",\"mark_value\":\"660000\",\"ppt_url\":\"notPt\",\"real_frame_no\":\"1\",\"progress_yn\":\"Y\"},{\"mark_no\":\"13\",\"mark_nm\":\"?숈쁺??",\"mark_value\":\"720000\",\"ppt_url\":\"notPt\",\"real_frame_no\":\"1\",\"progress_yn\":\"Y\"},{\"mark_no\":\"14\",\"mark_nm\":\"?숈쁺??",\"mark_value\":\"780000\",\"ppt_url\":\"notPt\",\"real_frame_no\":\"1\",\"progress_yn\":\"Y\"},{\"mark_no\":\"15\",\"mark_nm\":\"?숈쁺??",\"mark_value\":\"840000\",\"ppt_url\":\"notPt\",\"real_frame_no\":\"1\",\"progress_yn\":\"Y\"},{\"mark_no\":\"16\",\"mark_nm\":\"?숈쁺??",\"mark_value\":\"912000\",\"ppt_url\":\"notPt\",\"real_frame_no\":\"1\",\"progress_yn\":\"Y\"}]}");
    	intent2.putExtra("jsonPlayUrl", 		"{\"IsSuccess\":\"YES\",\"contents_url\":\"http://m.hunet.hscdn.com/hunet/M_Learning/HLSC01981/01.mp4\"}");
	
		startActivity(intent2);
		return;
      */
		
        Log.i(LOG_TAG, "onCreate");
        
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);
        
        //PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        //wake_lock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK, "SniperPlayer");
        //wake_lock.setReferenceCounted(false);
        Intent intent = getIntent();
        uripassed = intent.getData();
        
        
        timeView = (TextView)findViewById(R.id.textView1);
        timeSlider = (SeekBar)findViewById(R.id.seekBar1);
        
        webView = (WebView)findViewById(R.id.webView1);
        webView.getSettings().setJavaScriptEnabled(false);
        webView.setHorizontalScrollBarEnabled(false);
        webView.setVerticalScrollBarEnabled(false);
        webView.getSettings().setDefaultTextEncodingName("UTF-8");
        webView.setBackgroundColor(0);
        webView.setVisibility(View.INVISIBLE);
        
        SniperPlayerVideoView videoView = (SniperPlayerVideoView) this.findViewById(R.id.videoview);
        
        /*
        try {
            player = new SniperPlayer(this, videoView, libraryKey);
        } catch (SniperPlayer.CreateFailedException e) {
            // TODO: messgage box...
            finish();
            return;
        }
        */
        player.setCallback(this.calback);
        player.setMovieUrl(this.uripassed.getQueryParameter("uri"));
        
        int drmResult = player.checkDRM();
        if (drmResult == -1) {
            Log.e(LOG_TAG, "Cannot find drm information...");
            finish();
            return;
        }
        else if (drmResult > 1) {
            // we have drm info but do not show it
            if ((drmResult & 2) == 2) {
                Log.e(LOG_TAG, "Expired");
            }
            if ((drmResult & 4) == 4) {
                Log.e(LOG_TAG, "Cheating");
            }
            if ((drmResult & 1) == 1) {
                Log.e(LOG_TAG, "UpdateRequired");
            }
//            finish();
//            return;
        }

        if ((drmResult & 1) == 1) {
            Log.w(LOG_TAG, "UpdateRequired but play....???");
        }

        int ret = player.open();
        Log.i(LOG_TAG, "open... " + ret);
       
       
       
    }
   
   
    private PowerManager.WakeLock wake_lock = null;
    private SniperPlayer player = null;
    private Uri uripassed;

    private TextView timeView;
    private SeekBar timeSlider;
    private WebView webView;
    
    private double playbackRate = 1.0;
    private double duration = -1.0;
    private boolean seekStarted = false;
    private boolean fitToScreen = false;
    
    private boolean sliderMovingByUser = false;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    

    @Override
    protected void onStart() {
        Log.i(LOG_TAG, "onStart");
        //wake_lock.acquire();
        super.onStart();
    }
    
    @Override
    protected void onResume() {
        Log.i(LOG_TAG, "onResume");
        super.onResume();
    }
    
    @Override
    protected void onPause() {
        Log.i(LOG_TAG, "onPause");
        
      
        
        player.pause();
        
        super.onPause();
    }

   

    @Override
    protected void onDestroy() {
        Log.i(LOG_TAG, "onDestroy");
        
        player.dispose();
        player = null;
        
        super.onDestroy();
    }
    
   
      
   @Override
    public void onConfigurationChanged(Configuration newConfig) {
        Log.i(LOG_TAG, "onConfigurationChanged()");
        super.onConfigurationChanged(newConfig);
    }
    
 	@Override
    protected void onRestart() {
        Log.i(LOG_TAG, "onRestart");
        super.onRestart();
    }

    @Override
    protected void onStop() {
        Log.i(LOG_TAG, "onStop");
        super.onStop();
    }
    
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        uripassed = Uri.parse(savedInstanceState.getString("uri"));

        Log.i(LOG_TAG, "onRestoreInstanceState " + uripassed.toString());

        super.onRestoreInstanceState(savedInstanceState);
    }
     
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString("uri", uripassed.toString());
        
        Log.i(LOG_TAG, "onSaveInstanceState " + uripassed.toString());
        
        super.onSaveInstanceState(outState);
    }
   
    private SniperPlayer.Callback calback = new SniperPlayer.Callback() {
        
        @Override
        public void timeChanged(final double curTime, final double duration) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                  
                    
                }
            });
        }
        
        @Override
        public void subtitle(final String subtitle) {
            
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String html = "<font color=\"white\">" + subtitle + "</font>";
                    //Log.i(LOG_TAG, "subtitle - " + html);
                    webView.loadData(html, "text/html; charset=UTF-8", null);
                }
            });
        }
        
        @Override
        public void seekStarted() {
            Log.i(LOG_TAG, "seekStarted!!!");
            
        }
        
        @Override
        public void seekDone() {
            Log.i(LOG_TAG, "seekDone!!!");
            seekStarted = false;
        }
        
        @Override
        public void playing() {
            Log.i(LOG_TAG, "playing!!!");
            
        }
        
        @Override
        public void playbackRateChanged(double rate) {
            Log.i(LOG_TAG, "playbackRate "+rate);
            VideoActivity.this.playbackRate = rate;
        }
        
        @Override
        public void paused() {
            Log.i(LOG_TAG, "paused!!!");
            
        }
        
        @Override
        public void mediaSizeChanged(int width, int height) {
            Log.i(LOG_TAG, "mediaSize : " + width + "x" + height);
        }
        
        @Override
        public void loaded() {
            Log.i(LOG_TAG, "loaded!!!");
        }
        
        @Override
        public void error(int err) {
            Log.e(LOG_TAG, "error!!! - " + err);
        }
        
        @Override
        public void ended() {
            Log.i(LOG_TAG, "ended!!!");
            player.setCurrentTime(0);
        }
        
        @Override
        public void durationChanged(final double duration) {
            Log.i(LOG_TAG, "duration : " + duration);
            VideoActivity.this.duration = duration;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    timeSlider.setMax((int) (VideoActivity.this.duration * 1000));
                }
            });
        }
        
        @Override
        public void canPlay() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (player.hasSubtitle())
                        webView.setVisibility(View.VISIBLE);
                    player.play();
                }
            });
        }
    };

    ///////
    
    private final static boolean bShowMilli = true;
    
    static private String formatTime(long millisec, long duration, double playbackRate) {
        return formatTime(millisec, duration, playbackRate, bShowMilli);
    }
    
    static private String formatTime(long millisec, long duration, double playbackRate, boolean bShowMilli) {
        boolean bShowHour = (duration > 3600000);
        return String.format(Locale.KOREA, "(%.1fx) %s / %s", playbackRate, formatTime(millisec, bShowHour, bShowMilli), formatTime(duration, bShowHour, bShowMilli));
    }
    
    static private String formatTime(long millisec, boolean bShowHour, boolean bShowMilli) {
        String message;
        
        int hours =  (int) ((millisec / 1000) / 3600);
        int minutes = (int) (((millisec / 1000) % 3600) / 60);
        int seconds = (int) ((millisec / 1000) % 60);
        int milliseconds = (int) (millisec % 1000);

        if (bShowMilli) {
            if (bShowHour)
                message = String.format(Locale.KOREA, "%d:%02d:%02d.%03d", hours, minutes, seconds, milliseconds);
            else
                message = String.format(Locale.KOREA, "%02d:%02d.%03d", minutes, seconds, milliseconds);
        }
        else {
            if (bShowHour)
                message = String.format(Locale.KOREA, "%d:%02d:%02d", hours, minutes, seconds);
            else
                message = String.format(Locale.KOREA, "%02d:%02d", minutes, seconds);
        }
        
        return message;
    }
 
}
