package hunet.player.model.restriction;

/**
 * Created by zeple on 2015. 9. 16..
 */
public class RestrictionEntity {
    public String key;
    public ItemType itemType;
    public RestrictionType restrictionType;

    public RestrictionEntity(String key, ItemType itemType, RestrictionType restrictionType) {
        this.key = key;
        this.itemType = itemType;
        this.restrictionType = restrictionType;
    }
}
