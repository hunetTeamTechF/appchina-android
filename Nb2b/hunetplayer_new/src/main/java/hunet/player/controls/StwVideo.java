package hunet.player.controls;

import android.content.Context;
import android.util.AttributeSet;
import net.stway.sniperplayer.SniperPlayerVideoView;

public class StwVideo extends SniperPlayerVideoView
{	
	public StwVideo(Context context) { super(context); }
	public StwVideo(Context context, AttributeSet attrs) { super(context, attrs); }
}
