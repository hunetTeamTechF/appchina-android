package hunet.player;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.PowerManager;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ProgressBar;
import android.widget.Toast;

import net.stway.sniperplayer.SniperPlayer;
import net.stway.sniperplayer.SniperPlayerUtil;
import net.stway.sniperplayer.SniperPlayerVideoView;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import hunet.core.HRunnable;
import hunet.core.IHunetRunnableEventListener;
import hunet.domain.DomainAddress;
import hunet.library.PlayerMessageUtility;
import hunet.library.Utilities;
import hunet.library.hunetplayer.R;
import hunet.net.Network;
import hunet.player.controls.ControlBar;
import hunet.player.controls.IndexViewer;
import hunet.player.controls.PptViewer;
import hunet.player.controls.StwVideo;
import hunet.player.interfaces.IControlBarEventListener;
import hunet.player.interfaces.IIndexViewerEventListener;
import hunet.player.interfaces.IPlayerStudyDataEventListener;
import hunet.player.interfaces.IPptViewerEventListener;
import hunet.player.model.MarkDataModel;
import hunet.player.model.PlayerModelBase;
import hunet.player.model.PlayerModelFactory;
import hunet.player.stwplayer.LicenseFactory;

public class PlayerActivity extends Activity implements
        IControlBarEventListener, IHunetRunnableEventListener,
        DialogInterface.OnClickListener, IPlayerStudyDataEventListener,
        IPptViewerEventListener, IIndexViewerEventListener {
    private static final String TAG = "PlayerActivity";
    private String libraryKey = "";

    private MarkDataModel pptMarkDataModel;

    private ProgressBar pgbBuffering = null;

    private StwVideo stwVideoView = null;
    private ControlBar playerControlbar;
    private PptViewer pptViewer = null;
    private IndexViewer indexViewer = null;
    private AudioManager audioManager;
    private PowerManager powerManager;
    private HRunnable runnableBuffering;
    // private LinearLayout voumnbar_test= null; 나중에 다시 구현
    private boolean bInitialized = false;
    private PlayerModelBase playerModelBase;
    private Timer timer;
    private TimerTask timerTask;

    private Toast toast = null;
    private SniperPlayer player = null;
    private int maxVolume = 0;


    private int playerStartSec = 0;
    private int useNetwork = 0;
    private boolean isProgressBarDrag = false;
    private int duration = 0;
    // 받아야하는 파라미터
    String userId = "";
    String playerType = "";

    double startTime = 0;
    double endTime = 0;
    boolean repeat = false;

    private float mediaRateMax = (float) 2.0;
    private float mediaRateMin = (float) 0.8;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String packageName = getApplicationContext().getPackageName();
        libraryKey = LicenseFactory.GetLicense(packageName);
        this.setContentView(R.layout.library_player);
        getWindow().addFlags(
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                        | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
                        | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                        | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);

        InflateControl();
        SetValiableFromIntent();
        SetEvent();
        InitControl();
        InitPlayer(playerModelBase.GetPlayerUrl());

        if (TextUtils.equals(playerType, "localSangSang") || TextUtils.equals(playerType, "sangsangWeb")) {
            mediaRateMax = (float) 1.5;
        }

        Log.i(TAG, "onCreate");
    }

    @Override
    public void onBackPressed() {
        if (playerModelBase.ShowCompletedButton())
            super.onBackPressed();
    }

    private void SetPlayer() {
        SniperPlayerVideoView videoView = (SniperPlayerVideoView) this
                .findViewById(R.id.stwVideoView);
        try {
            String udid = SniperPlayerUtil.readDeviceId(this) + libraryKey;
            player = new SniperPlayer(this, videoView, libraryKey, udid, true);
        } catch (SniperPlayer.CreateFailedException e) {
            Log.i(TAG, "SetPlayer");
            finish();
            return;
        }
    }

    private void InitControl() {
        playerControlbar.InitControlBar(playerModelBase.HasPpt(),
                playerModelBase.title);
        if (playerModelBase.title.length() < 28)//padding
        {
            for (int i = 28 - playerModelBase.title.length(); i > 0; i--)
                playerModelBase.title += "&nbsp";
        }
        indexViewer.SetIndexContetns(playerModelBase.m_arMarkList);

        if (!playerModelBase.ShowSpeedControl())
            playerControlbar.HideSpeedLayout();

        if (!playerModelBase.ShowCompletedButton())
            playerControlbar.HideCompletedButton();
    }

    private void SetEvent() {
        runnableBuffering.SetEventListener(this);
        playerModelBase.SetEventListener(this);
        pptViewer.SetEventListener(this);
        indexViewer.SetEventListener(this);
    }


    private void InflateControl() {
        stwVideoView = (StwVideo) findViewById(R.id.stwVideoView);
        playerControlbar = (ControlBar) findViewById(R.id.playerControlbar);
        pptViewer = (PptViewer) findViewById(R.id.pptViewer);
        indexViewer = (IndexViewer) findViewById(R.id.indexViewer);
        pgbBuffering = (ProgressBar) findViewById(R.id.pgbBuffering);

        playerControlbar.setVisibility(View.GONE);
    }


    private void SetValiableFromIntent() {
        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
        timer = new Timer();
        runnableBuffering = new HRunnable(1);
        maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        //		useNetwork = Network.GetCurrentUseNetwork(this);
//		useNetwork = PlayerMessageUtility.getNetworkSettings(PlayerActivity.this);

        if (isConnectedWifi())
            useNetwork = 0;
        else if (isConnectedMobile())
            useNetwork = 1;
        else
            useNetwork = -1;

        toast = toast.makeText(this, "", toast.LENGTH_SHORT);
        Bundle bundle = getIntent().getExtras();
        playerType = "web";

        if (bundle != null && bundle.containsKey("userId"))
            userId = bundle.getString("userId");
        if (bundle != null && bundle.containsKey("playerType"))
            playerType = bundle.getString("playerType");
        playerModelBase = PlayerModelFactory.Instance().Get(this, playerType);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        if (hasFocus == false)
            return;

        int curVolumeValue = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        int nPercent = (int) Math.round((curVolumeValue / (double) maxVolume) * 100);

        playerControlbar.onWindowFocusChanged(hasFocus);
        playerControlbar.SetVolume(nPercent);
    }

    @Override
    public void onResume() {
        super.onResume();
        playerControlbar.setVisibility(View.VISIBLE);
        InitPlayer(playerModelBase.GetPlayerUrl());
    }

    @Override
    public void onPause() {
        if (player != null && playerControlbar != null) {
            playerControlbar.Pause();
            player.pause();
        }

        super.onPause();

//		playerControlbar.Pause();
//
//		if (player != null)
//			StartServiceStudyProgress();
//
//		if (!CheckScreenOn())			
//			finish();
//
//		super.onPause();
    }

    @Override
    public void finish() {
        StopTimer();

        Log.i(TAG, "finish ");
        if (player != null) {
            StartServiceStudyProgress();
            player.dispose();
            player = null;
        }

        maxVolume = 0;
        playerStartSec = 0;
        useNetwork = 0;
        isProgressBarDrag = false;
        duration = 0;
        userId = "";
        playerType = "";
        startTime = 0;
        endTime = 0;
        repeat = false;

        super.finish();
    }

    @Override
    public void onDestroy() {

        if (player != null) {
            player.dispose();
            player = null;
        }
        super.onDestroy();
    }

    private void InitPlayer(String strFileName) {
        bInitialized = false;

        if (TextUtils.isEmpty(strFileName)) {
            Toast.makeText(getBaseContext(), "영상 파일이 존재하지 않습니다. 고객행복센터로 문의주시기 바랍니다.", Toast.LENGTH_LONG).show();
            finish();
            return;
        }

        if (player != null) {
            bInitialized = true;
            onPlayStarted(false);
            return;
        }
        SetPlayer();
        strFileName = strFileName.replace("m.hunet", "m2.hunet");

        player.setCallback(this.calback);
        player.setMovieUrl(strFileName);

        int drmResult = player.checkDRM();
        if (drmResult == SniperPlayer.DRM_NOT_FOUND) {
            Log.e(TAG, "Cannot find drm information");
            finish();
            return;
        }

        if ((drmResult & SniperPlayer.DRM_UPDATE_REQUIRED) != 0) {
            Log.e(TAG, "UpdateRequired");
        }

        if ((drmResult & SniperPlayer.DRM_EXPIRED) != 0) {
            Log.e(TAG, "Expired");
            finish();
            return;
        }

        if ((drmResult & SniperPlayer.DRM_CHEATING) != 0) {
            Log.e(TAG, "current time is prior to last accessed time or DRM start date");
            finish();
            return;
        }

        int ret = player.open();
        if (ret != SniperPlayer.PLAYER_SUCCESS) {
            Log.i(TAG, String.format("open : %s / %s", ret, libraryKey));
            finish();
            return;
        }
        bInitialized = true;
    }

    public void onPlayStarted(boolean bIsInit) {

        if (bIsInit == false) {
            playerControlbar.Play();
            return;
        }
        int rate = (int) (player.playbackRate() * 10);
        int totalViewSec = (int) player.duration();
        int nLastViewSec = playerModelBase.GetLastViewSec();

        playerControlbar.SetSeekBarMax(totalViewSec);
        playerControlbar.SetTotalTimeText(Utilities.GetTimeString(
                player.duration() * 1000, false));
        playerControlbar.SetEventListener(PlayerActivity.this);
        playerControlbar.SetMediaRateText((rate / 10.0f));

        if (nLastViewSec >= totalViewSec)
            nLastViewSec = 0;

        if (nLastViewSec > 0) {
            try {
                player.setCurrentTime(nLastViewSec, false);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        playerControlbar.Play();

    }

    private SniperPlayer.Callback calback = new SniperPlayer.Callback() {

        @Override
        public void timeChanged(final double curTime, final double duration) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                }
            });
        }

        @Override
        public void subtitle(final String subtitle) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.i(TAG, "subtitle ");

                }
            });
        }

        @Override
        public void seekStarted() {
            Log.i(TAG, "seekStarted ");
        }

        @Override
        public void seekDone() {
            Log.i(TAG, "seekDone ");
        }

        @Override
        public void playing() {
            Log.i(TAG, "playing ");
        }

        @Override
        public void playbackRateChanged(double rate) {
            Log.i(TAG, "playbackRateChanged ");
        }

        @Override
        public void paused() {
            Log.i(TAG, "paused ");
        }

        @Override
        public void mediaSizeChanged(int width, int height) {
            Log.i(TAG, "mediaSizeChanged ");
        }

        @Override
        public void loaded() {
            Log.i(TAG, "loaded ");
            player.pause();
        }

        @Override
        public void error(int err) {
            Log.i(TAG, "error ");
        }

        @Override
        public void ended() {
            Log.i(TAG, "ended ");
            if (playerType.equals("htmlWeb")) {
                Intent result = new Intent();
                result.putExtra("completed", true);
                setResult(Activity.RESULT_OK, result);
            }
            finish();
        }

        @Override
        public void durationChanged(final double duration) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                }
            });
        }

        @Override
        public void canPlay() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (player != null) {
                        player.pause();
                        duration = (int) player.duration();
                        onPlayStarted(true);
                    } else {
                        toast.setText("일시적인 현상으로 인하여 동영상을 불러오지 못하였습니다.\n다시 시도해 주시기 바랍니다.");
                        toast.show();
                        finish();
                    }
                }
            });
        }
    };

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_VOLUME_UP:
            case KeyEvent.KEYCODE_VOLUME_DOWN: {
                int curVolumeValue = audioManager
                        .getStreamVolume(AudioManager.STREAM_MUSIC);
                int nPercent = (int) Math
                        .round((curVolumeValue / (double) maxVolume) * 100);

                playerControlbar.SetVolume(nPercent);
            }
            break;
        }

        return super.onKeyUp(keyCode, event);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK: {
                if (playerModelBase.ShowCompletedButton()) {
                    playerControlbar.Pause();
                    new AlertDialog.Builder(PlayerActivity.this)
                            .setMessage("동영상 강의를 종료 하시겠습니까?")
                            .setCancelable(false)
                            .setPositiveButton("종료",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(
                                                DialogInterface dialog,
                                                int which) {
                                            if (playerType.equals("htmlWeb")) {
                                                Intent result = new Intent();
                                                result.putExtra("completed", true);
                                                setResult(Activity.RESULT_OK, result);
                                            }
                                            finish();
                                        }
                                    })
                            .setNegativeButton("취소",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(
                                                DialogInterface dialog,
                                                int which) {
                                            playerControlbar.Play();
                                            player.play();
                                        }
                                    }).show();
                }
            }
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    private void StartTimer() {
        StopTimer();

        timerTask = new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(OnTimerViewUpdate);
            }
        };
        timer.schedule(timerTask, 0, 1000);
    }

    private void StopTimer() {
        if (timerTask == null)
            return;

        timerTask.cancel();
        timerTask = null;
    }

    private boolean CheckScreenOn() {
        boolean isScreenOn = false;

        try {
            isScreenOn = powerManager.isScreenOn();
        } catch (Exception e) {
            isScreenOn = true;
        }

        return isScreenOn;
    }

    private boolean isConnectedWifi() {
        return Network.connectWifiConfirm(PlayerActivity.this);
    }

    private boolean isConnectedMobile() {
        return Network.connect3GConfirm(PlayerActivity.this) || Network.connectWimaxConfirm(PlayerActivity.this);
    }

    private Runnable OnTimerViewUpdate = new Runnable() {
        public void run() {
            if (player == null || player.paused()) {
                Log.i(TAG, "player == null || !player.playing() ");
                return;
            }

            playerStartSec++;

            if (playerStartSec > 5 && playerStartSec % 3 == 0) {

                int curUseNetwork = 0;

                if (isConnectedWifi())            // 와이파이
                    curUseNetwork = 0;
                else if (isConnectedMobile())    // 이동통신사 망
                    curUseNetwork = 1;
                else                            // 오프라인
                    curUseNetwork = -1;

                int currentNetworkConfig = PlayerMessageUtility.getNetworkSettings(PlayerActivity.this);
                String message = PlayerMessageUtility.getNetworkChangeMessage(PlayerActivity.this, currentNetworkConfig);

//				int nCurUseNetwork = Network.GetCurrentUseNetwork(getApplicationContext());			

                if (curUseNetwork != useNetwork && !("local".equals(playerType) || "localSangSang".equals(playerType))) {
                    useNetwork = curUseNetwork;

                    if (currentNetworkConfig != PlayerMessageUtility.NETWORK_ALARM_OFF_MOVIE_ON) {

                        switch (currentNetworkConfig) {
                            case PlayerMessageUtility.NETWORK_ALARM_OFF_MOVIE_OFF:
                                playerControlbar.Pause();
                                player.pause();

                                new AlertDialog.Builder(PlayerActivity.this)
                                        .setTitle("알림")
                                        .setMessage(message)
                                        .setPositiveButton("확인", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                PlayerMessageUtility.setOnMovieStatus(PlayerActivity.this);
                                                playerControlbar.Play();
                                                player.play();
                                            }
                                        }).setNegativeButton("취소", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        finish();
                                    }
                                }).show();
                                break;
                            case PlayerMessageUtility.NETWORK_ALARM_ON_MOVIE_OFF:
                                playerControlbar.Pause();
                                player.pause();

                                new AlertDialog.Builder(PlayerActivity.this)
                                        .setTitle("알림")
                                        .setMessage(message)
                                        .setPositiveButton("확인", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                PlayerMessageUtility.setOnMovieStatus(PlayerActivity.this);
                                                playerControlbar.Play();
                                                player.play();
                                            }
                                        }).setNegativeButton("취소", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        finish();
                                    }
                                }).show();
                                break;
                            case PlayerMessageUtility.NETWORK_ALARM_ON_MOVIE_ON:
                                playerControlbar.Pause();
                                player.pause();

                                new AlertDialog.Builder(PlayerActivity.this)
                                        .setTitle("알림")
                                        .setMessage(message)
                                        .setPositiveButton("확인", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                PlayerMessageUtility.setOnMovieStatus(PlayerActivity.this);
                                                playerControlbar.Play();
                                                player.play();
                                            }
                                        }).show();
                                break;
                        }
                    }
                }
            }

            double curTime = player.currentTime();
            int nTime = (int) (curTime);
            if (repeat && startTime > 0 && endTime > 0 && (int) endTime <= nTime) {
                player.setCurrentTime(startTime);
            }
            if (!isProgressBarDrag)
                playerControlbar.SetProgress(nTime);


            if (playerControlbar.txtIndexNm != null) {
                Date dt = new Date();
                int hours = dt.getHours();
                int minutes = dt.getMinutes();
                int seconds = dt.getSeconds();
                String oo = hours > 12 ? "下午" : "上午";
                String currentTime = String.format("%d:%02d:%02d", hours > 12 ? hours - 12 : hours, minutes, seconds);
                playerControlbar.txtIndexNm.setText(Html.fromHtml(String.format("<b>%s (%s)%s</b>", playerModelBase.title, oo, currentTime)));
            }

            // 1초 단위로 중복학습 여부 체크 (스테이징 서버 모드인 경우는 체크하지 않음)
            if (!DomainAddress.IS_STAGING && playerModelBase.CheckOverlapStudyRestriction()) {
                // 다중기기 중복학습제약이 설정되어 있는 경우
                if (playerModelBase.IsOverlapStudy()) {
                    playerControlbar.Pause();
                    player.pause();
                    new AlertDialog.Builder(PlayerActivity.this)
                            .setMessage("因他人用同账号登录，您被强制推出！")
                            .setPositiveButton("确认", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            })
                            .setCancelable(false)
                            .show();
                }
            }

//			Log.i(TAG,"currentTime : (int) (player.currentTime() : " + player.currentTime() + " playerStartSec : " + playerStartSec);
            if (!playerModelBase.CheckSendProgress((int) (player.currentTime()), playerStartSec)) {
                /*
				if(nTime == duration)
				{
					StopTimer();
					if (playerType.equals("htmlWeb")) {
						Intent result = new Intent();
						result.putExtra("completed", true);
						setResult(Activity.RESULT_OK, result);
					}						
					finish();
				}*/
                return;
            }

            StartServiceStudyProgress();
        }
    };

    private void StartServiceStudyProgress() {
        double currentTime = Math.round(player.currentTime());
        playerModelBase.SaveProgress((int) currentTime, playerStartSec);

        if ("localSangSang".equals(playerType))
            playerStartSec = 0;
    }

    public void OnChangeVolume(ControlBar controlBar, int nValue) {
        int nVolumeValue = (int) Math
                .round(((double) nValue * (double) maxVolume) / 100.0);
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, nVolumeValue,
                AudioManager.FLAG_SHOW_UI);
    }

    public boolean OnClickPlay(ControlBar controlBar) {
        StartTimer();

        if (player.playing()) {
            return true;
        }

        player.play();

        return true;
    }

    public boolean OnClickPause(ControlBar controlBar) {
        StopTimer();

        if (player.paused())
            return true;

        player.pause();

        return true;
    }

    public boolean OnClickBack(ControlBar controlBar) {

        try {
            double currentTime = player.currentTime();
            double backward = currentTime - 15;
            if (backward > 0) {
                player.setCurrentTime(backward);
            } else {
                player.setCurrentTime(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return true;
    }

    public boolean OnClickForward(ControlBar controlBar) {
        if (playerModelBase.CheckProgressRestriction() == false)
            return true;

        try {
            double totalTime = player.duration();
            double currentTime = player.currentTime();
            double forward = currentTime + 15;
            if (totalTime <= forward) {
                player.setCurrentTime(totalTime);
            } else {
                player.setCurrentTime(forward);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return true;
    }

    @Override
    public boolean OnClickSpeedUp(ControlBar controlBar) {
        double rate = player.playbackRate() + 0.1;

        if (rate > mediaRateMax)
            rate = mediaRateMax;

        playerControlbar.SetMediaRateText((float) rate);
        player.setPlaybackRate(rate);

        StartTimer();
        return true;
    }

    @Override
    public boolean OnClickSpeedDown(ControlBar controlBar) {
        double rate = player.playbackRate() - 0.1;

        if (rate < mediaRateMin)
            rate = mediaRateMin;

        playerControlbar.SetMediaRateText((float) rate);
        player.setPlaybackRate(rate);

        StartTimer();
        return true;
    }

    public boolean OnClickSound(ControlBar controlBar) {
        return true;
    }

    public void OnStartTrackingTouch(ControlBar controlBar, int progress) {
        isProgressBarDrag = true;
    }

    public void OnStopTrackingTouch(ControlBar controlBar, int progress) {
        if (playerModelBase.CheckProgressRestriction() == false) {
            toast.setText("최초 학습 완료 시 이동이 가능합니다.");
            toast.show();
        } else {
            try {
                player.setCurrentTime(progress, true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        isProgressBarDrag = false;
    }

    public void OnProgressChanged(ControlBar controlBar, int progress) {
        controlBar.SetTimeText(Utilities.GetTimeString(progress * 1000,
                false));
    }

    @Override
    public void OnRun(int nId, Object tag) {
        switch (nId) {
            case 1:
                ShowBuffering((Boolean) tag);
                break;
            case 2:
                finish();
                break;
        }
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        if (which != DialogInterface.BUTTON_POSITIVE)
            return;

        dialog.dismiss();
        if (playerType.equals("htmlWeb")) {
            Intent result = new Intent();
            result.putExtra("completed", true);
            setResult(Activity.RESULT_OK, result);
        }

        finish();

    }

    private boolean SetMediaPlayerSeek(int nMillisec) {
        if (playerModelBase.CheckProgressRestriction() == false)
            return true;
        try {
            player.setCurrentTime(nMillisec / 1000.);

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (player.paused())
            playerControlbar.Play();

        return true;
    }

    private void ShowIndexList(boolean bShow) {
        if (bShow) {
            Animation animation = new TranslateAnimation(
                    -indexViewer.getWidth(), 0, 0, 0);
            animation.setDuration(500);
            indexViewer.setVisibility(View.VISIBLE);
            indexViewer.startAnimation(animation);
            playerControlbar.Hide();
			/*
			 * layoutIndexBG.setVisibility(View.VISIBLE);
			 * layoutIndexBG.startAnimation(animation);
			 */
        } else {
            Animation animation = new TranslateAnimation(0,
                    -indexViewer.getWidth(), 0, 0);
            animation.setDuration(500);
            indexViewer.setVisibility(View.INVISIBLE);
            indexViewer.startAnimation(animation);
			/*
			 * layoutIndexBG.setVisibility(View.INVISIBLE);
			 * layoutIndexBG.startAnimation(animation);
			 */
        }
    }

    private void ShowPptViewer(boolean bShow) {
        if (bShow) {
            Animation animation = new TranslateAnimation(-pptViewer.getWidth(),
                    0, 0, 0);
            animation.setDuration(500);
            pptViewer.setVisibility(View.VISIBLE);
            pptViewer.startAnimation(animation);
            pptMarkDataModel = playerModelBase.m_CurMarkData;
            pptViewer.SetLoadWebView(playerModelBase.m_CurMarkData.pptUrl);
            playerControlbar.Hide();

        } else {
            Animation animation = new TranslateAnimation(0,
                    -pptViewer.getWidth(), 0, 0);
            animation.setDuration(500);
            pptViewer.setVisibility(View.INVISIBLE);
            pptViewer.startAnimation(animation);
        }
    }

    @Override
    public boolean OnClickIndexFull(ControlBar controlBar, boolean bIsFull) {
        if (playerModelBase.HasPpt()) {
            ShowIndexList(true);
        }
        return true;
    }

    @Override
    public boolean OnClickPptFull(ControlBar controlBar, boolean bIsFull) {
        if (playerModelBase.HasPpt()) {
            // SetScreen(layoutWebView, bIsFull);
            ShowPptViewer(true);
        }
        return true;
    }

    @Override
    public boolean OnPptLeftMove() {
        // TODO Auto-generated method stub
        if (playerModelBase.m_arMarkList != null && pptMarkDataModel != null
                && playerModelBase.m_arMarkList.size() > 0) {
            MarkDataModel data = pptMarkDataModel;
            int index = playerModelBase.m_arMarkList.indexOf(data);

            for (int nIndex = index; 0 <= nIndex; nIndex--) {
                MarkDataModel temp = playerModelBase.m_arMarkList.get(nIndex);
                if (temp.pptUrl != null && !temp.pptUrl.equals("notPt")
                        && data.markNo != temp.markNo
                        && !data.pptUrl.equals(temp.pptUrl)) {
                    pptMarkDataModel = temp;
                    pptViewer.SetLoadWebView(temp.pptUrl);
                    break;
                }
            }
        }
        return false;
    }

    @Override
    public boolean OnPptRightMove() {
        // TODO Auto-generated method stub
        if (playerModelBase.m_arMarkList != null && pptMarkDataModel != null
                && playerModelBase.m_arMarkList.size() > 0) {
            MarkDataModel data = pptMarkDataModel;
            int index = playerModelBase.m_arMarkList.indexOf(data);
            int size = playerModelBase.m_arMarkList.size();

            for (int nIndex = index; nIndex < size; nIndex++) {
                MarkDataModel temp = playerModelBase.m_arMarkList.get(nIndex);
                if (temp.pptUrl != null && !temp.pptUrl.equals("notPt")
                        && data.markNo != temp.markNo
                        && !data.pptUrl.equals(temp.pptUrl)) {
                    pptMarkDataModel = temp;
                    pptViewer.SetLoadWebView(temp.pptUrl);
                    break;
                }
            }
        }
        return false;
    }

    @Override
    public boolean OnPptPlay() {
        // TODO Auto-generated method stub
        if (pptMarkDataModel != null) {
            SetMediaPlayerSeek(pptMarkDataModel.markValue);
            ShowPptViewer(false);
        }
        return false;
    }

    @Override
    public boolean OnPptClose() {
        // TODO Auto-generated method stub
        ShowPptViewer(false);
        return false;
    }

    @Override
    public boolean OnIndexClose() {
        // TODO Auto-generated method stub
        ShowIndexList(false);
        return false;
    }

    @Override
    public boolean OnIndexChnaged(int markValue) {
        // TODO Auto-generated method stub
        SetMediaPlayerSeek(markValue);
        ShowIndexList(false);
        return false;
    }

    @Override
    public void OnClickComplete(ControlBar controlBar) {
        if (playerType.equals("htmlWeb")) {
            Intent result = new Intent();
            result.putExtra("completed", true);
            setResult(Activity.RESULT_OK, result);
        }
        this.finish();
    }

    @Override
    public void OnClickScreenFit(boolean fit) {
        player.setFitToScreen(fit);

    }

    private void ShowBuffering(boolean bIsShow) {
        if (bIsShow)
            playerControlbar.Pause();
        else
            playerControlbar.Play();

        if (bIsShow == (pgbBuffering.getVisibility() == View.VISIBLE))
            return;

        pgbBuffering.setVisibility(bIsShow ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    public void OnClickRepeat(String status) {
        // TODO Auto-generated method stub
        if (status == "1") {
            startTime = player.currentTime();
        } else if (status == "2") {
            endTime = player.currentTime();
            repeat = true;
            if (repeat && startTime > 0 && endTime > 0) {
                player.setCurrentTime(startTime);
            }
        } else if (status == "0") {
            startTime = 0;
            endTime = 0;
            repeat = false;
        }
    }
}
