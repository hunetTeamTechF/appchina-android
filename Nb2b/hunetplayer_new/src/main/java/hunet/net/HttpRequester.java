package hunet.net;

import java.io.InputStream;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.params.HttpParams;

public class HttpRequester 
{	
	public static HttpData GetPostData(HttpData data)
	{						
		try {
			HttpClient 		httpClient 		= new DefaultHttpClient();
			HttpParams 		httpParam 		= httpClient.getParams();
			httpParam.setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, 5000);
			httpParam.setParameter(CoreConnectionPNames.SO_TIMEOUT, 5000);
			
			HttpPost 		httpPost 		= new HttpPost(data.GetUrl());
			HttpResponse 	httpResponse 	= httpClient.execute(httpPost);
			InputStream 	inputStream 	= httpResponse.getEntity().getContent();
			
			data.SetDataInfo(httpClient, httpResponse, inputStream);
		}
		catch (Exception e) 
		{
			data.SetDataInfo(null, null, null);			
		}
		
		return data;
	}
}
