package hunet.net;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class HttpData 
{
	public 	String 			httpType	= "POST";
	public 	int				id			= -1;
	public 	String 			url		= "";
	public 	String 			param		= "";
	public 	String 			data		= "";
	public 	InputStream 	inputStream	= null;
	public 	HttpClient 		httpClient	= null;
	public 	HttpResponse 	httpResponse	= null;
	public 	Object			tag		= null;
	private JSONObject 		jsonObject	= null;
	public String EncodingString = "";
	
	public void SetDataInfo(HttpClient httpClient, HttpResponse httpResponse, InputStream inputStream)
	{
		this.httpClient	= httpClient;
		this.httpResponse 	= httpResponse;
		this.inputStream 	= inputStream;
		
		SetData();		
		//HunetLog.Write("StatusCode : " + StatusCode() + ", Url : " + GetUrl() + "\n\nData : " + GetData());
	}
	
	public String GetUrl()
	{
		if("".equals(param))
			return url;
		
		return url + "?" + param;
	}
	
	public int StatusCode()
	{
		if(httpResponse == null)
			return 0;
		
		return httpResponse.getStatusLine().getStatusCode();
	}
	
	public void SetData()
	{
		if(inputStream == null)
			return;
		
		BufferedReader 	br 			= null;
		StringBuilder 	strBuilder 	= new StringBuilder();
		if(EncodingString != ""){
			try {
				br = new BufferedReader(new InputStreamReader(inputStream,EncodingString));
			} catch (UnsupportedEncodingException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		else{
		br = new BufferedReader(new InputStreamReader(inputStream));
		}
	    
	    try 
	    {
	    	String strData;
	    	
			while ((strData = br.readLine()) != null)
			{
				strBuilder.append(strData);
			}			
			
		} catch (IOException e) { e.printStackTrace(); } 
	    
	    data = strBuilder.toString();
	}
	
	public String GetData()
	{
		return data;
	}
	
	public String GetJsonValue(String strKey)
	{
		if(jsonObject == null)
			jsonObject = ConvertJsonObject();
		
		String strValue = "";
		
		if(jsonObject == null)
			return "";
		
		try {
			strValue = jsonObject.getString(strKey);
		} catch (JSONException e) {
			strValue = "";
			e.printStackTrace();
		}
		
		return strValue;
	}
	
	public String GetJsonValue(String strKey, String strDefault)
	{
		if(jsonObject == null)
			jsonObject = ConvertJsonObject();
		
		String strValue = "";
		
		if(jsonObject == null)
			return strDefault;
		
		try 
		{
			strValue = jsonObject.getString(strKey);
		} 
		catch (JSONException e) { e.printStackTrace(); }
		
		if("".equals(strValue))
			strValue = strDefault;
		
		return strValue;
	}
	
	public JSONArray GetJsonArray(String strKey)
	{
		if(jsonObject == null)
			jsonObject = ConvertJsonObject();
		
		if(jsonObject == null)
			return null;
		
		JSONArray jsonArray = null;
		
		try 
		{
			jsonArray = jsonObject.getJSONArray(strKey);
		} 
		catch (JSONException e) { jsonArray = null; }
		
		return jsonArray;
	}
	
	public String GetJsonValue(JSONObject json, String strKey, String strDefault)
	{
		if(json == null)
			return strDefault;
		
		String strValue = "";
		
		try 
		{
			strValue = json.getString(strKey);
		} 
		catch (JSONException e) { e.printStackTrace(); }
		
		if("".equals(strValue))
			strValue = strDefault;
		
		return strValue;
	}
	
	private JSONObject ConvertJsonObject()
	{
		JSONObject JO = null;
		
		if("".equals(data))
			return null;
			    
	    try 
	    {			
			JO = new JSONObject(data);
		} catch (JSONException e) { e.printStackTrace(); }
				
		return JO;
	}
	
}
