package hunet.controls;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Rect;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.widget.TextView;

public class AutoSizeTextView extends TextView 
{
	private Paint mTestPaint;

	public AutoSizeTextView(Context context) {
	    this(context, null);
	    initialize();
	}
	
	public AutoSizeTextView(Context context, AttributeSet attrs) {
	    super(context, attrs);
	    initialize();
	}
	
	private void initialize() {
        mTestPaint = new Paint();
        mTestPaint.set(this.getPaint());

        //max size defaults to the initially specified text size unless it is too small
    }


	private void refitText(String text, int textWidth, int textHeight) 
	{
		if (textWidth <= 0)
            return;
        int targetWidth = textWidth - this.getPaddingLeft() - this.getPaddingRight();
        int targetHeight = textHeight - this.getPaddingTop() - this.getPaddingBottom();
        float hi = Math.min(targetHeight,100);
        float lo = 2;
        final float threshold = 0.5f; // How close we have to be

        Rect bounds = new Rect();

        mTestPaint.set(this.getPaint());

        while((hi - lo) > threshold) {
            float size = (hi+lo)/2;
            mTestPaint.setTextSize(size);
            mTestPaint.getTextBounds(text, 0, text.length(), bounds);
            if((mTestPaint.measureText(text)) >= targetWidth || (1+(2*(size+(float)bounds.top)-bounds.bottom)) >=targetHeight) 
                hi = size; // too big
            else
                lo = size; // too small
        }
        // Use lo so that we undershoot rather than overshoot
        this.setTextSize(TypedValue.COMPLEX_UNIT_PX,(float) lo);

        
        
	}
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
	    super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	    int parentWidth = MeasureSpec.getSize(widthMeasureSpec);
	    int parentHeight = MeasureSpec.getSize(heightMeasureSpec);
	    refitText(this.getText().toString(), parentWidth, parentHeight);
	    this.setMeasuredDimension(parentWidth, parentHeight);
	}
	
	@Override
	protected void onTextChanged(final CharSequence text, final int start,
	        final int before, final int after) {
	    refitText(text.toString(), this.getWidth(), this.getHeight());
	}
	
	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
	    if (w != oldw) {
	        refitText(this.getText().toString(), w, h);
	    }
	}
}
