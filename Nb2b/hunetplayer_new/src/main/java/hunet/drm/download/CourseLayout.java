package hunet.drm.download;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import hunet.drm.models.TakeCourseModel;
import hunet.drm.models.TakeSangSangModel;
import hunet.library.hunetplayer.R;
import android.content.Context;
import android.text.Html;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.view.View.OnClickListener;

public class CourseLayout extends LinearLayout
{		
	private TextView 			title;
	private TextView 			description;
	private TakeCourseModel downTakeCourseModel;
	private TakeSangSangModel downTakeSangSangModel;
	public String type;
	
	public CourseLayout(Context context)
	{
		super(context);
		InitControl(context);
	}
	
	public CourseLayout(Context context, AttributeSet attrs) 
	{
		super(context, attrs);
		InitControl(context);
	}
	
	private void InitControl(Context context)
	{
		LayoutInflater li = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		li.inflate(R.layout.library_course_layout, this, true);
		
		title 		= (TextView)findViewById(R.id.tvTitle);
		description = (TextView)findViewById(R.id.tvDescription);
	}
	
	public void SetDownTakeCourseInfo(TakeCourseModel model, int nIndexCount)
	{
		type ="course";
		downTakeCourseModel = model;
		title.setText(model.course_nm);
		
		long 	day 	= GetRemainStudyDate(model.study_end_date);
		String 	strText = String.format("&middot; 학습기간 : %s까지&nbsp;%s"
				, model.study_end_date, day <= 0 ? "<font color=#ee0000>(학습 기간 종료)</font>" : "<font color=#b37e4d>(" + day +"일 남음)</font>");
		
		description.setText(Html.fromHtml(strText));
	}
	
	public void SetDownTakeSangSangInfo(TakeSangSangModel model, int nIndexCount)
	{
		type ="sangsang";
		downTakeSangSangModel  = model;
		title.setText(model.goods_nm);
		
		long 	day 	= GetRemainStudyDate(model.study_end_date);
		String 	strText = String.format("&middot; 학습기간 : %s까지&nbsp;%s"
				, model.study_end_date, day <= 0 ? "<font color=#ee0000>(학습 기간 종료)</font>" : "<font color=#b37e4d>(" + day +"일 남음)</font>");
		
		description.setText(Html.fromHtml(strText));
	}
	
	private long GetRemainStudyDate(String studyEndDate)
	{
		String[] arDate = studyEndDate.split("-");
		
		if(arDate.length != 3)
			return 0;
		
		int year 	= Integer.parseInt(arDate[0]);
		int month 	= Integer.parseInt(arDate[1]) - 1;
		int day 	= Integer.parseInt(arDate[2]);
		
		Calendar curCal 		= Calendar.getInstance();
		Calendar studyEndCal 	= Calendar.getInstance();
		studyEndCal.set(year, month, day);
		
		long remainDay = ((studyEndCal.getTime().getTime() - curCal.getTime().getTime()) / 86400000); // 1000 * 60 * 60 * 24
		
		return remainDay;
	}
	
	public TakeCourseModel getDownTakeCourseModel()
	{
		return downTakeCourseModel;
	}
	
	public TakeSangSangModel getDownTakeSangSangModel()
	{
		return downTakeSangSangModel;
	}
	
	public void SetTitle(String strText) 		{ title.setText(strText); }
	public void SetDescription(String strDesc) 	{ description.setText(strDesc); }
}
