package hunet.drm.download.library;

import hunet.data.SQLiteManager;
import hunet.domain.DomainAddress;
import hunet.drm.models.AccessModel;
import hunet.drm.models.StudyIndexModel;
import hunet.drm.models.TakeCourseModel;
import hunet.library.MemoryStatus;
import hunet.library.PlayerMessageUtility;
import hunet.library.Utilities;
import hunet.library.hunetplayer.R;
import hunet.net.AsyncHttpRequestData;
import hunet.net.HttpData;
import hunet.net.IAsyncHttpRequestData;
import hunet.player.stwplayer.LicenseFactory;

import java.io.File;

import net.stway.sniperplayer.SniperDownloader;
import net.stway.sniperplayer.SniperDownloader.CreateFailedException;
import net.stway.sniperplayer.SniperPlayerUtil;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.WebView;
import android.widget.Toast;

public class OnlineDrmDownload implements IAsyncHttpRequestData {

	protected static final String LOG_TAG = "FileDown";
	private final String libraryKey;
	private SQLiteManager sqlManager;
	private TakeCourseModel takeCourseModel;
	private StudyIndexModel studyIndexModel;
	private SniperDownloader stwDownloader;
	private ProgressDialog loadingDialog = null;
	private ProgressDialog progressDialog;
	private AsyncHttpRequestData asyncReqData;
	private final int downloadInfo = 1; // 다운로드 정보 가져 오기
	private final int downloadCompleted = 2; // 다운로드 정보 저장하기
	private Activity activity;
	private String userId = "";
	private WebView wv;
	private String courseCd;
	private String takeCourseSeq;
	private String chapterNo;
	private String frameNo;
	private String view_index;

	public OnlineDrmDownload(Activity activity, WebView wv, String userId) {
		String packageName = activity.getApplicationContext().getPackageName();
		libraryKey = LicenseFactory.GetLicense(packageName);
		this.activity = activity;
		this.wv = wv;
		sqlManager = new SQLiteManager(activity.getApplicationContext());
		takeCourseModel = new TakeCourseModel();
		studyIndexModel = new StudyIndexModel();
		asyncReqData = new AsyncHttpRequestData();
		asyncReqData.SetEventListener(this);
		this.userId = userId;
	}

	public void ShowDialog(String msg, boolean cancelable) {
		if (loadingDialog != null)
			return;

		loadingDialog = new ProgressDialog(activity);
		loadingDialog.setMessage("".equals(msg) ? "로딩하는 중입니다..." : msg);
		loadingDialog.setIndeterminate(true);
		loadingDialog.setCancelable(cancelable);
		loadingDialog.show();
	}

	public void HideDialog() {
		if (loadingDialog != null) {
			try {
				loadingDialog.dismiss();
				loadingDialog = null;
			} catch (Exception e) {
			}
		}
	}

	private SniperDownloader.Callback callback = new SniperDownloader.Callback() {

		@Override
		public void progress(final long sizeOnServer,
				final long sizeDownloaded, final long speed) {
			activity.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					if (progressDialog == null)
						return;
					progressDialog.setProgress((int) sizeDownloaded);
				}
			});
		}

		@Override
		public void canDownload(long arg0) {
			stwDownloader.startDownload();
		}

		@Override
		public void canceled(int failedReason) {
			// TODO Auto-generated method stub
			Log.i(LOG_TAG, "canceled!!!" + failedReason);
			activity.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					Toast.makeText(activity, "다운로드가 취소 되었습니다.", Toast.LENGTH_LONG)
					.show();
				}
			});
			return;
		}

		@Override
		public void cannotContinue(int failedReaso) {
			// TODO Auto-generated method stub
			Log.i(LOG_TAG, "cannotContinue!!!");
		}

		@Override
		public void cannotDownload(int failedReason) {
			// TODO Auto-generated method stub
			Log.i(LOG_TAG, "cannotDownload!!!");
		}

		@Override
		public void completed(long sizeDownloaded) {
			Log.i(LOG_TAG, "completed!!!");
			CloseProgressDialog();
			if (stwDownloader.canceled()) {
				
				try {
					File file = new File(studyIndexModel.full_path);
				
					if (file.exists())
						file.delete();
					
				
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			DownloadStep2();
		}		

		@Override
		public void queryOverwrite(long sizeOnServer, long sizeDownloaded) {
			stwDownloader.startDownload();
		}

		@Override
		public void queryOverwriteOrContinue(long arg0, long arg1) {
			// TODO Auto-generated method stub
			Log.i(LOG_TAG, "queryOverwriteOrContinue!!!");
			stwDownloader.startContinueDownload();
		}

		@Override
		public void started(final long sizeOnServer) {
			Log.i(LOG_TAG, "started!!!");
			// TODO Auto-generated method stub
			activity.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					if (progressDialog == null)
						return;
					if (!progressDialog.isShowing()) {
						final MemoryStatus m_MemoryStatus = new MemoryStatus();
//						long availableSize = m_MemoryStatus
//								.getAvailableExternalMemorySize();
						final long availableSize = m_MemoryStatus.getAvailableMemorySize(activity.getApplicationContext(), sqlManager);

						int tempSize = (int)sizeOnServer;
						Log.i(LOG_TAG, "availableSize!!! : "+ availableSize + " sizeOnServer : " + sizeOnServer + " tempSize: " + tempSize);
						
						if (availableSize <= sizeOnServer) {
							stwDownloader.cancelDownload();
							
							activity.runOnUiThread(new Runnable() {
								@Override
								public void run() {
									new AlertDialog.Builder(activity)
									.setTitle("저장 공간 부족 확인")
									.setMessage(
											String.format(
													"현재 선택하신 파일의 용량은 %s이며, 저장 가능한 공간은 %s 입니다.\n\n저장 공간을 확보 하신 후 다시 시도해 주세요.",
													m_MemoryStatus
															.getFileSizeFormat(sizeOnServer),
													m_MemoryStatus
															.getFileSizeFormat(availableSize)))
									.setPositiveButton("확인", null).show();
								}
							});
							return;
						}
						
						HideDialog();
						progressDialog.setMax((int) sizeOnServer);
						progressDialog.show();

					}
				}
			});
		}

	};

	public void ConfirmDownload(final String url) {
		int network = PlayerMessageUtility.getNetworkSettings(activity);
		String message = PlayerMessageUtility.getDownloadMessage(activity, network);

		switch (network) {
		case PlayerMessageUtility.NETWORK_WIFI:
			DownloadStep1(url);
			break;
		case PlayerMessageUtility.NETWORK_OFFLINE:
			new AlertDialog.Builder(this.activity)
			.setTitle("알림")
			.setMessage(message)
			.setPositiveButton("확인", null).show();
			break;
		case PlayerMessageUtility.NETWORK_ALARM_OFF_MOVIE_OFF:
			new AlertDialog.Builder(this.activity)
			.setTitle("알림")
			.setMessage(message)
			.setPositiveButton("확인", null).show();
			break;
		case PlayerMessageUtility.NETWORK_ALARM_OFF_MOVIE_ON:
			DownloadStep1(url);
			break;
		case PlayerMessageUtility.NETWORK_ALARM_ON_MOVIE_OFF:
			new AlertDialog.Builder(this.activity)
			.setTitle("알림")
			.setMessage(message)
			.setPositiveButton("확인", null).show();
			break;
		case PlayerMessageUtility.NETWORK_ALARM_ON_MOVIE_ON:
			new AlertDialog.Builder(this.activity)
			.setTitle("알림")
			.setMessage(message)
			.setPositiveButton("확인", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					DownloadStep1(url);
				}
			}).show();
			break;
		}

	}

	public void DownloadPlay(String url) {
		String[] strTemp = url.replace("downloadplay://", "").split("/");

		if (strTemp.length != 5)
			return;

		int take_course_seq = 0;
		String chapter_no = "";

		try {
			take_course_seq = Integer.parseInt(strTemp[1]);
			chapter_no = strTemp[2];
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		StudyIndexModel model = sqlManager.GetDownStudyIndexData(take_course_seq, chapter_no);
		
		if(!TextUtils.equals(model.external_memory, "0")){
			AccessModel accessModel = sqlManager.GetAccessData();
			if("1".equals(accessModel.external_memory)){ // 외부 메모리 사용
				if(Utilities.getExternalMemoryCheck(activity.getApplicationContext())){
					model = sqlManager.GetDownStudyIndexData(take_course_seq, chapter_no, "2");
				}else{
					activity.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							Toast.makeText(activity.getApplicationContext(), "이동식 메모리가 제거 되었습니다..", Toast.LENGTH_LONG).show();
						}
					});
					model = sqlManager.GetDownStudyIndexData(take_course_seq, chapter_no, "1");
				}
			}else{
				model = sqlManager.GetDownStudyIndexData(take_course_seq, chapter_no, "1");
			}
		}

		if (model.take_course_seq == 0) {
			ConfirmDownload("download://" + url.replace("downloadplay://", ""));
			return;
		}
		
		
//		String 	downFileName = "";
//		if(model.external_memory.equalsIgnoreCase("0")){
//			String externalDir = Environment.getExternalStorageDirectory() + "/hunet_mlc/";
//			downFileName = String.format("%s%d_%s.mp4", externalDir, take_course_seq,chapter_no);
//		}else{
//			downFileName = Utilities.getDownloadPullPath(activity.getApplicationContext(), 
//							String.valueOf(take_course_seq), chapter_no);
//		}
		
		String 	downFileName = model.full_path;
		File file = new File(downFileName);

		if (file.exists() == false) {
			ConfirmDownload("download://" + url.replace("downloadplay://", ""));
			return;
		}
		
		Intent intent = getDownloadPlayIntent(activity, take_course_seq);
		activity.startActivity(intent);
	}
	
	public Intent getDownloadPlayIntent(Activity activity, int take_course_seq) {
		Intent intent = new Intent(activity, hunet.drm.download.DownloadCourseActivity.class);
		intent.putExtra("take_course_seq", take_course_seq);
		
		return intent;
	}

	private void DownloadStep1(String url) {
		String[] strTemp = url.replace("download://", "").split("/");

		if (strTemp.length != 5)
			return;

		courseCd = strTemp[0];
		takeCourseSeq = strTemp[1];
		chapterNo = strTemp[2];
		frameNo = strTemp[3];
		view_index = strTemp[4];

		String strReqUrl = DomainAddress.getUrlApps() + "/Hunet_Player/Android.aspx";
		String strReqParam = String
				.format("action=DrmDownloadInfo&courseCd=%s&takeCourseSeq=%s&chapterNo=%s",
						courseCd, takeCourseSeq, chapterNo);

		ShowDialog(activity.getString(R.string.download_msg_request_info), true);
		asyncReqData.Request(downloadInfo, strReqUrl, strReqParam, null);
	}

	private void DownloadStep2() {
		String strReqUrl = DomainAddress.getUrlApps() + "/Hunet_Player/Android.aspx";
		String strReqParam = String
				.format("action=DrmDownloadComplete&courseCd=%s&takeCourseSeq=%s&chapterNo=%s&downloadType=2&deviceNm=android&regId=%s",
						courseCd, takeCourseSeq, chapterNo, userId);

		// ShowDialog(activity,"다운로드 정보 구성중입니다.\n\n잠시만 기다려주세요.", false);
		asyncReqData.Request(downloadCompleted, strReqUrl,
				strReqParam, null);

	}

	private void CreateProgressDialog() {
		if (progressDialog != null)
			return;

		progressDialog = new ProgressDialog(activity);
		progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		progressDialog.setTitle("다운로드");
		progressDialog.setProgress(0);
		progressDialog.setMax(0);
		progressDialog.setCancelable(false);
		progressDialog.setCanceledOnTouchOutside(false);

		progressDialog.setButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						CloseProgressDialog();

						if (stwDownloader != null)
							stwDownloader.cancelDownload();
					}
				});
	}

	private void CloseProgressDialog() {
		if (progressDialog == null)
			return;

		progressDialog.dismiss();
		progressDialog = null;
	}

	@Override
	public void AsyncRequestDataResult(HttpData result) {
		switch (result.id) {

		case downloadInfo: {

			takeCourseModel.take_course_seq = Integer.parseInt(result.GetJsonValue("take_course_seq", "0"));
			takeCourseModel.course_cd = result.GetJsonValue("course_cd");
			takeCourseModel.course_nm = result.GetJsonValue("course_nm");
			takeCourseModel.study_end_date = result.GetJsonValue("study_end_date");

			studyIndexModel.take_course_seq = Integer.parseInt(result.GetJsonValue("take_course_seq", "0"));
			studyIndexModel.chapter_no = result.GetJsonValue("chapter_no");
			studyIndexModel.index_no = Integer.parseInt(result.GetJsonValue("index_no", "0"));
			studyIndexModel.index_nm = result.GetJsonValue("index_nm");
			studyIndexModel.max_mark_sec = Integer.parseInt(result.GetJsonValue("mark_sec", "0"));
			studyIndexModel.view_index = "".equals(view_index) ? 0 : Integer.parseInt(view_index);
			
			String downFileName = Utilities.getDownloadPullPath(activity.getApplicationContext(), 
									String.valueOf(studyIndexModel.take_course_seq), studyIndexModel.chapter_no);
			
			studyIndexModel.full_path = downFileName;
			
			String downHttpUrl = result.GetJsonValue("mov_url").replace("m.hunet", "m2.hunet");

			CreateProgressDialog();
			progressDialog.setMessage(String.format("%s",
					takeCourseModel.course_nm));

			try {
				String udid = SniperPlayerUtil.readDeviceId(this.activity)
						+  libraryKey;
				stwDownloader = new SniperDownloader(this.activity, libraryKey,
						udid, true);
			} catch (CreateFailedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			stwDownloader.setCallback(this.callback);
			stwDownloader.setMovieUrl(downHttpUrl);
			stwDownloader.setFileLocation(downFileName);

			String drmKey = String
					.format("<drmKey><UserID value=\"%s\" /><MovieID value=\"%s\" /><PayID value=\"%d\" /></drmKey>",
							userId, takeCourseModel.course_cd,
							takeCourseModel.take_course_seq);
			String startDate = "2014-1-1";
			String endDate = takeCourseModel.study_end_date;

			stwDownloader.setDrmInfo(startDate, endDate, drmKey);
			stwDownloader.prepareDownload();
		}
			break;
		case downloadCompleted: {
			HideDialog();
			
			AccessModel accessModel = sqlManager.GetAccessData();
			if("1".equals(accessModel.external_memory)){ // 외부 메모리 사용
				if(Utilities.getExternalMemoryCheck(activity.getApplicationContext())){
					studyIndexModel.external_memory = "2";
				}else{
					studyIndexModel.external_memory = "1";
				}
			}else{
				studyIndexModel.external_memory = "1";
			}

			studyIndexModel.last_view_sec = Integer.parseInt(result.GetJsonValue("max_study_sec", "0"));
			
			sqlManager.SetDownTakeCourseData(takeCourseModel);
			sqlManager.SetDownStudyIndexData(studyIndexModel);

			Toast.makeText(activity, "다운로드가 완료 되었습니다.", Toast.LENGTH_LONG)
					.show();

			wv.reload();
		}
			break;
		}

	}

}
