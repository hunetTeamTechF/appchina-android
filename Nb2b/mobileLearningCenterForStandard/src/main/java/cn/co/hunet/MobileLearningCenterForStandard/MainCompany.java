package cn.co.hunet.MobileLearningCenterForStandard;

import java.net.URLEncoder;

import cn.co.HunetLib.Company.Company;
import cn.co.HunetLib.Control.TabControl;
import hunet.domain.DomainAddress;

public class MainCompany extends Company {
    public MainCompany() {
        super();
        AppName = "修耐微学院";
        Seq = 0;
        DrmUse = true;
        GCMSendManageEnabled = true;
        EnabledSangSangOverlapStudy = true;

        LoginTop = 190;
        LoginBottom = 220;
        LoginDrmTop = 0;
        LoginDrmBottom = 30;
        RuleId = "";
        eURL_DEFAULT_HOST = "";
    }

    @Override
    protected void CreateViewModel() {
        setViewModel(eTAB_NOTICE, "学员公告");
        setViewModel(eTAB_QNA, "在线答疑");
        setViewModel(eTAB_MEMBERINFO, "个人信息");
        //setViewModel(eTAB_PASSWORD, "修改密码");
        //setViewModel(eTAB_SCHEDULE, "스케줄 알리미");
        setViewModel(eTAB_CONFIG, "环境设置");
        setViewModel(eTAB_VERSION, "版本信息");
        setViewModel(eTAB_DOWNLOAD_CENTER, "下载中心");
    }

    @Override
    public Class<?> getNextActivity(int type) {
        Class<?> tempClass = null;

        switch (type) {
            case eNEXT_ACTIVITY_INTRO:
            case eNEXT_ACTIVITY_NOTIFY_ALARM:
            case eACTIVITY_LOGIN:
                tempClass = LoginActivity.class;
                break;
            case eNEXT_ACTIVITY_LOGIN:
                tempClass = HomeActivity.class;
                break;
            case eACTIVITY_HOME:
                tempClass = HomeActivity.class;
                break;

            default:
                tempClass = super.getNextActivity(type);
                break;
        }

        return tempClass;
    }

    @Override
    public String getUrl(int type) {
        String url;
        switch (type) {
            case eURL_LOGIN:
                url = String.format("%s/JLog", DomainAddress.getUrlMLC());
                break;
            case eURL_HOME: 				url = String.format("%s/JLog?type=4&uid=%s&pw=%s", DomainAddress.getUrlMLC(), URLEncoder.encode(LoginModel.id), URLEncoder.encode(LoginModel.pwd)); break;

            case eURL_LECTURE:				url = String.format("%s/JLog?type=5&uid=%s&pw=%s", DomainAddress.getUrlMLC(), URLEncoder.encode(LoginModel.id), URLEncoder.encode(LoginModel.pwd)); break;
            case eURL_LECTURE_INMUN:		url = String.format("%s/JLog?type=7&uid=%s&pw=%s", DomainAddress.getUrlMLC(), URLEncoder.encode(LoginModel.id), URLEncoder.encode(LoginModel.pwd)); break;
            case eURL_SELFSTUDY:			url = String.format("%s/Imagine", eURL_DEFAULT_HOST); break;
            case eURL_CLASSROOM:			url = String.format("%s/JLog?type=8&uid=%s&pw=%s", DomainAddress.getUrlMLC(), URLEncoder.encode(LoginModel.id), URLEncoder.encode(LoginModel.pwd)); break;
            case eURL_SANGSANG:				url = String.format("%s/JLog?type=17&uid=%s&pw=%s", DomainAddress.getUrlMLC(), URLEncoder.encode(LoginModel.id), URLEncoder.encode(LoginModel.pwd)); break;
            case eURL_SANGSANG_SELFSTUDY:	url = String.format("%s/JLog?type=35&uid=%s&pw=%s", DomainAddress.getUrlMLC(), URLEncoder.encode(LoginModel.id), URLEncoder.encode(LoginModel.pwd)); break;

            case eURL_MORE_NOTICE: 			url = String.format("%s/JLog?type=13&uid=%s&pw=%s", DomainAddress.getUrlMLC(), URLEncoder.encode(LoginModel.id), URLEncoder.encode(LoginModel.pwd)); break;
            case eURL_MORE_FAQ:				url = String.format("%s/JLog?type=14&uid=%s&pw=%s", DomainAddress.getUrlMLC(), URLEncoder.encode(LoginModel.id), URLEncoder.encode(LoginModel.pwd)); break;
            case eURL_MORE_QNA: 			url = String.format("%s/JLog?type=15&uid=%s&pw=%s", DomainAddress.getUrlMLC(), URLEncoder.encode(LoginModel.id), URLEncoder.encode(LoginModel.pwd)); break;
            case eURL_MORE_MEMBERINFO: 		url = String.format("%s/JLog?type=16&uid=%s&pw=%s", DomainAddress.getUrlMLC(), URLEncoder.encode(LoginModel.id), URLEncoder.encode(LoginModel.pwd)); break;

            case eURL_APP_VERSION:			url = String.format("%s/JLog?type=28&device=android", DomainAddress.getUrlMLC()) + "&app=%s"; break;
            case eURL_HREMINDER:			url = String.format("http://study.hunet.co.kr/Study/HTools/Gate.aspx?userId=%s&typeCd=0&isMobile=Y", URLEncoder.encode(LoginModel.id)); break;
            case eURL_HSTORY:				url = String.format("%s/JLog?type=37&uid=%s&pw=%s", DomainAddress.getUrlMLC(), URLEncoder.encode(LoginModel.id), URLEncoder.encode(LoginModel.pwd)); break;
            case eURL_PASSWORD:				url = String.format("%s/JLog?type=40&uid=%s&pw=%s", DomainAddress.getUrlMLC(), URLEncoder.encode(LoginModel.id), URLEncoder.encode(LoginModel.pwd)); break;
            case eURL_NOTIFY_EVENT:			url = String.format("%s/JLog?type=1004", DomainAddress.getUrlMLC()); break;
            case eURL_H_LEADERSHIP_CLASSROOM:	url = String.format("http://h-leadership.hunet.co.kr/App/MyPage/%s", URLEncoder.encode(LoginModel.id)); break;
            case eURL_SET_PUSHINFO:			url = String.format("%s/JLwog/SavePushClientInfo", DomainAddress.getUrlMLC()); break;
            default:
                url = super.getUrl(type);
        }
        return url;
    }

    public TabControl getTabControl() {
        return new StandardTagControl();
    }
}
