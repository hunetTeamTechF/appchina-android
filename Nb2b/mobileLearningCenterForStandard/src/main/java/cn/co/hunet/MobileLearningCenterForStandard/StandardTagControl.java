package cn.co.hunet.MobileLearningCenterForStandard;

import cn.co.HunetLib.Company.Company;
import cn.co.HunetLib.Control.TabControl;
import cn.co.HunetLib.Control.TabItem;

public class StandardTagControl extends TabControl {

    public StandardTagControl() {
        super("#ffadadad", "#ffefefef", "#ffefefef", "#ff305592", "#ff305592");
    }

    @Override
    public void InitTab(int nSelectTab) {
        selected_tab = nSelectTab;

        if(nSelectTab == Company.eTAB_SANGSANG)
        {
            CreateSangSangTab();
            return;
        }
        else if(nSelectTab == Company.eTAB_H_LEADERSHIP_CLASSROOM)
        {
            CreateHLeadership();
            return;
        }

        InitTabContainer();

        /* 탭 메뉴 */
        TabItem tab_1 = createTabItem();
        TabItem tab_2 = createTabItem();
        TabItem tab_3 = createTabItem();
        TabItem tab_4 = createTabItem();

        tab_1.tabLayout.setTag(Company.eTAB_HOME);
        tab_4.tabLayout.setTag(nSelectTab >= Company.eTAB_MORE ? nSelectTab : Company.eTAB_MORE);

        tab_1.init(cn.co.HunetLib.R.drawable.tab_home, tab_container.getContext().getString(cn.co.HunetLib.R.string.tab_home));
        tab_2.init(cn.co.HunetLib.R.drawable.tab_classroom, tab_container.getContext().getString(cn.co.HunetLib.R.string.tab_my));
        tab_3.init(cn.co.HunetLib.R.drawable.tab_download, tab_container.getContext().getString(cn.co.HunetLib.R.string.tab_download));
        tab_4.init(cn.co.HunetLib.R.drawable.tab_setting, tab_container.getContext().getString(cn.co.HunetLib.R.string.tab_setting));

        if (nSelectTab == Company.eTAB_HOME)
        tab_1.selected();
        else if (nSelectTab == Company.eTAB_CLASSROOM)
        tab_2.selected();
        else if (nSelectTab == Company.eTAB_DOWNLOAD_CENTER)
        tab_3.selected();
        else if (nSelectTab >= Company.eTAB_MORE)
        tab_4.selected();

        tab_2.tabLayout.setTag(Company.eTAB_CLASSROOM);
        tab_3.tabLayout.setTag(Company.eTAB_DOWNLOAD_CENTER);

        tab_1.tabLayout.setOnClickListener(this);
        tab_2.tabLayout.setOnClickListener(this);
        tab_3.tabLayout.setOnClickListener(this);
        tab_4.tabLayout.setOnClickListener(this);

        tab_container.addView(tab_1.tabLayout);
        tab_container.addView(tab_2.tabLayout);
        tab_container.addView(tab_3.tabLayout);
        tab_container.addView(tab_4.tabLayout);
    }
}
