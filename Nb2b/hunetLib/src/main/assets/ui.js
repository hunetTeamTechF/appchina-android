/*
* UI JS
*/

$(document).ready(function() {
  // search open & close
  $('.js-search-open').on('click', function() {
    $(this).next('.js-search-form').show();
  })
  $('.js-search-close').on('click', function() {
    $(this).parent('.js-search-form').hide();
  })
})