package cn.co.HunetLib.OldPlayer;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

/**
 * HTTP 통신 연결 부모 클래스.
 * 작업자 : 장형일
 * 작업일 : 2011.05
 * mlc 플레이어 인앱 수정. 2011.11.11 
 */
public class Hunet_Http_Post_Cls {
	
	//연결 url.
	private boolean blPlayer = false;
	private String url;
	private String type;
	private String param;
	private InputStream mInstream;
	
	public void setBlPlayer(boolean blPlayer) {
		this.blPlayer = blPlayer;
	}
	public boolean isBlPlayer() {
		return blPlayer;
	}
		
	//연결 url get.
	public String getUrl() {
		return url;
	}
	//연결 url set.
	public void setUrl(String url) {
		this.url = url;
	}
	
	//데이터 작업 구분값 get.
	public String getType() {
		return type;
	}
	//데이터 작업 구분값 set.
	public void setType(String type) {
		this.type = type;
	}
	
	//파라미터 get.
	public String getParam() {
		return param;
	}
	//파라미터 set.
	public void setParam(String param) {
		this.param = param;
	}
	
	/*
	 * 기본 생성자.
	 */
	public Hunet_Http_Post_Cls() {
	}
	
	/**
	 * 데이터조회.
	 */
	public InputStream hunetHttpPostData() {
		Thread mThread = null;
		Runnable mRunnable = new Runnable()
		{
			public void run()
			{
				try {
					String totalUrl;
					String actParam;
					if(blPlayer == true) 
						actParam = "action";
					else
						actParam = "type";
					
					totalUrl = TotalUrl(actParam);
					
					HttpClient httpClient = new DefaultHttpClient();
					HttpPost httpPost = new HttpPost(totalUrl);
					
					HttpResponse httpResponse = httpClient.execute(httpPost);
					HttpEntity httpEntity = httpResponse.getEntity();
					
					mInstream = httpEntity.getContent();
					
				}
				catch (IOException e) {
					// TODO Auto-generated catch block
					mInstream = null;
					
					e.printStackTrace();
				}
			}
		};
		
		mThread = new Thread(mRunnable);
		mThread.start();
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return mInstream;
	}
	
	/**
	 * 
	 * @param url
	 * @return
	 */
	public static Bitmap hunetHttpGetImage(String url)
	{
		try {
			URL iUrl = new URL(url);
			URLConnection conn = iUrl.openConnection();
			
			conn.connect();
			
			InputStream instream = conn.getInputStream();
			BufferedInputStream bStream = new BufferedInputStream(instream);
			Bitmap bm = BitmapFactory.decodeStream(bStream);
			bStream.close();
			instream.close();
			
			return bm;
		}
		catch(IOException e) {
			
		}
		
		return null;
	}
	
	
	/**
	 * 접속 URL.
	 */
	public String TotalUrl(String actParam)
	{
		String totalUrl;
		
		StringBuffer buffer = new StringBuffer();
			
		buffer.append(actParam).append("=").append(type);
		
		if(!"".equals(param))
			buffer.append("&").append(param);
					
		totalUrl = url + "?" + buffer.toString();
			
		return totalUrl;
	}
}
