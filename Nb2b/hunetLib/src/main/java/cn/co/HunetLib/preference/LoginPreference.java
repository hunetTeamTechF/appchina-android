package cn.co.HunetLib.preference;

import android.content.Context;
import android.content.SharedPreferences;

public class LoginPreference extends BasePreference {
    public LoginPreference(Context context) {
        super(context);
    }

    @Override
    protected String getPreferenceName() {
        return "login";
    }

    public boolean setLoginRuleId(String ruleId) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("login_rule_id", ruleId);
        return editor.commit();
    }

    public String getLoginRuleId() {
        return pref.getString("login_rule_id", "");
    }
}
