package cn.co.HunetLib.OldPlayer;

/**
 * 강의관련 HTTP 통신. 진도정보
 * 작업자 : 장형일
 * 작업일 : 2012.01 
 */
public class ProgressData {
	
	public ProgressData()
	{}
	
	private int progressNo = 0;
	private int lastMarkNo = 0;
	private boolean blListMov = false;
	private boolean blProgressRestrict = false;
	private int studySec = 0;
	private int mobileStudySec = 0;

	public int getProgressNo() {
		return this.progressNo;
	}

	public void setProgressNo(int progressNo) {
		this.progressNo = progressNo;
	}
	
	public int getLastMarkNo() {
		return lastMarkNo;
	}

	public void setLastMarkNo(int lastMarkNo) {
		this.lastMarkNo = lastMarkNo;
	}
	
	public boolean isBlListMov() {
		return blListMov;
	}

	public void setBlListMov(boolean blListMov) {
		this.blListMov = blListMov;
	}
	
	//과목정보
	private CourseData _CourseData = new CourseData();

	public CourseData get_CourseData() {
		return _CourseData;
	}

	public void set_CourseData(CourseData _CourseData) {
		this._CourseData = _CourseData;
	}
	
	public boolean isBlProgressRestrict() {
		return blProgressRestrict;
	}

	public void setBlProgressRestrict(boolean blProgressRestrict) {
		this.blProgressRestrict = blProgressRestrict;
	}
	
	public int getMobileStudySec() {
		return mobileStudySec;
	}

	public void setMobileStudySec(int mobileStudySec) {
		this.mobileStudySec = mobileStudySec;
	}
	

	public int getStudySec() {
		return studySec;
	}

	public void setStudySec(int studySec) {
		this.studySec = studySec;
	}
}
