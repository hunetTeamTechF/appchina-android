package cn.co.HunetLib;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.baidu.android.pushservice.PushConstants;
import com.baidu.android.pushservice.PushManager;

import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.List;

import cn.co.HunetLib.Base.BaseActivity;
import cn.co.HunetLib.Common.AppMain;
import cn.co.HunetLib.Common.Define;
import cn.co.HunetLib.Common.DeviceUtility;
import cn.co.HunetLib.Common.DownLoadManager;
import cn.co.HunetLib.Common.HunetNetwork;
import cn.co.HunetLib.Common.UpdataInfo;
import cn.co.HunetLib.Company.Company;
import cn.co.HunetLib.GCM.GCMRegistUtil;
import cn.co.HunetLib.Http.HunetHttpData;
import cn.co.HunetLib.SQLite.AccessModel;
import cn.co.HunetLib.SQLite.AppEventModel;
import cn.co.HunetLib.SQLite.LoginModel;
import cn.co.HunetLib.SQLite.SQLiteManager;
import cn.co.HunetLib.Service.HttpSenderService;
import cn.co.HunetLib.preference.LoginPreference;
import hunet.domain.DomainAddress;
import hunet.drm.download.DownloadCourseActivity;
import hunet.drm.models.AppSettingsModel;
import hunet.drm.models.DownloadCenterSettingsModel;
import hunet.library.Utilities;
import io.vov.vitamio.utils.Log;


public class HtmlLoginActivity extends BaseActivity {
    private WebView webView;

    private SQLiteManager m_SQLiteManager;
    private hunet.data.SQLiteManager manager;
    private LoginModel m_LoginModel;
    private boolean m_IsKillApp = false;
    private String m_NextUrl = "";
    private boolean m_SSo = false;

    private int m_TabIndex = 0;
    private String m_IndexDetail = "";

    int ClickCount = 0;

    private final int eLOGIN_INFO = 1;
    private final int eVERSION_INFO = 2;

    /*앱버전업데이트 관련*/
    private final String TAGU = this.getClass().getName();
    private final int UPDATA_NONEED = 0;
    private final int UPDATA_CLIENT = 1;
    private final int GET_UNDATAINFO_ERROR = 2;
    private final int DOWN_ERROR = 4;
    private UpdataInfo info;

    private String DownUrl;

    //public String PushApiKey = "smFxZnDUHe98Vc2ZBkVOYgt0"; // avene
    private String PushApiKey = "1234"; // lg

    private static final String TAG = HtmlLoginActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.htmllogin_activity);

        m_SQLiteManager = getSQLiteManager();
        manager = new hunet.data.SQLiteManager(this);

        registRunApplicationFlag();

        initWebView();

        InitIntent(this.getIntent());
    }

    protected boolean isSSO() {
        return m_SSo;
    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        InitIntent(intent);
    }

    private void InitIntent(Intent intent) {
        setFirstResume(true);
        Bundle bundle = intent.getExtras();
        m_IsKillApp = this.getBundleValue(bundle, "KILL_APP", false);
        m_NextUrl = this.getBundleValue(bundle, "NEXT_URL", "");
        m_SSo = this.getBundleValue(bundle, "sso", false);
        Define notify = this.getBundleValue(bundle, "DEFINE_NOTIFY", Define.NONE);
        m_TabIndex = this.getBundleValue(bundle, "TAB_INDEX", 0);
        m_IndexDetail = this.getBundleValue(bundle, "INDEX_DETAIL", "");
        m_LoginModel = m_SQLiteManager.GetLoginData();

        //SSO인지 체크
        if (m_SSo)
            sso_login(intent);

        // 아이디 저장 선택을 하지 않은 경우 자동로그인은 해제 (로그인 정보 삭제)
        if ("0".equals(m_LoginModel.save_id) || "0".equals(m_LoginModel.is_save_company_seq)) {
            m_LoginModel.id = "";
            m_LoginModel.pwd = "";
            m_LoginModel.auto_login = "0";
            m_SQLiteManager.SetLoginData(m_LoginModel);
        }

        switch (notify) {
            case NOTIFY_GCM_NOTI_REGIST: {
                String userSeq = this.getBundleValue(bundle, "USER_SEQ", "");

                if ("".equals(userSeq) == false) {
                    AppEventModel model = new AppEventModel();
                    model.max_count = 10;
                    model.url = getCompany().getUrl(Company.eURL_NOTIFY_EVENT) + "&eventType=click&userSeq=" + userSeq;
                    m_SQLiteManager.InsertAppEvent(model);
                }
            }
            break;
        }

        Intent httpSenderService = new Intent(this, HttpSenderService.class);
        this.startService(httpSenderService);
    }

    @Override
    protected void onFirstResume() {
        if (m_IsKillApp) {
            finish();
            return;
        }

        m_LoginModel = m_SQLiteManager.GetLoginData();

//        String loginId = m_LoginModel.id;
//        String ruleId = getCompany().getRuleId();
//
//        if (!TextUtils.isEmpty(ruleId)) {
//            if (loginId.toLowerCase().indexOf(ruleId.toLowerCase()) == 0)
//                loginId = loginId.replaceFirst("(?i)" + ruleId, "");
//
//            if (getCompany().getShowRuleId())
//                loginId = ruleId + loginId;
//        }

        // 자동로그인
        if ("1".equals(m_LoginModel.auto_login) && "".equals(m_LoginModel.pwd) == false && m_SSo == false) {
            DoLoginAction(m_LoginModel.id, m_LoginModel.pwd, m_LoginModel.company_seq);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        ClickCount = 0;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private boolean CheckLogin(String strUserId, String strUserPwd, String strUserCompanySeq) {
        // Step 1. 기업코드 검사
        if (TextUtils.isEmpty(strUserCompanySeq)) {
            ShowToast(getString(R.string.label_text_company_seq_check_msg));
            return false;
        }

        // Step 2. 아이디 검사
        if (TextUtils.isEmpty(strUserId)) {
            ShowToast(getString(R.string.label_text_login_check_msg));
            return false;
        }

        // Step 3. 비밀번호 검사
        if (TextUtils.isEmpty(strUserPwd)) {
            ShowToast(getString(R.string.label_text_pwd_check_msg));
            return false;
        }

        return true;
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void initWebView() {
        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.setAcceptCookie(true);

        webView = (WebView) findViewById(R.id.webview_login);
        WebSettings setting = webView.getSettings();
        setting.setUserAgentString(setting.getUserAgentString() + " HunetAndroid ScriptBridgeNew ");
        setting.setJavaScriptEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            setting.setMediaPlaybackRequiresUserGesture(false);
        }
        webView.addJavascriptInterface(new JSClass(), "ScriptBridge");
        webView.setHorizontalScrollBarEnabled(false);
        webView.setVerticalScrollBarEnabled(false);
//        setting.setCacheMode(WebSettings.LOAD_DEFAULT);
//        setting.setAppCacheEnabled(true);
        webView.setWebChromeClient(new WebChromeClient());
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                String lowerUrl = url.toLowerCase();
                Log.d(TAG, "::::::::: " + url);
                if (lowerUrl.startsWith("app://")) {
                    return handleCallScheme(url);
                } else if (lowerUrl.startsWith("modal://")) {
                    return handleModalScheme(url);
                }
                return super.shouldOverrideUrlLoading(view, url);
            }
        });
        webView.loadUrl("file:///android_asset/login.html");
    }

    private boolean handleModalScheme(String url) {
        try {
            Uri uri = Uri.parse(url);
            String paramUrl = uri.getQueryParameter("url");

            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(paramUrl)));

//            String paramTitle = TextUtils.isEmpty(uri.getQueryParameter("title")) ? "" : uri.getQueryParameter("title");
//            boolean paramHeader = !TextUtils.isEmpty(paramTitle);
//
//            if (!paramHeader)
//                paramHeader = "y".equalsIgnoreCase(uri.getQueryParameter("header"));
//
//            if (DomainAddress.IS_STAGING) {
//                paramUrl = paramUrl.replace("http://", "http://test").replace("https://", "https://test");
//            }
//
//            getCompany().setStartActivity(HtmlLoginActivity.this, Company.eACTIVITY_MODAL, String.format("LOAD_URL=%s&SHOW_HEADER=%s&HEADER_TITLE=%s", paramUrl, paramHeader ? "Y" : "N", paramTitle), false, 0);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private boolean handleCallScheme(String url) {
        boolean success = false;
        JSONObject obj = null;
        try {
            String decodedUrl = URLDecoder.decode(url.replace("app://", ""), "utf-8");
            obj = new JSONObject(decodedUrl);
            if (obj.has("type") && TextUtils.equals(obj.getString("type"), "scriptbridge")) {
                success = actionAppScheme(obj);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            if (obj != null) {
                callAppSchemeCallback(obj, false, e.getMessage());
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            if (obj != null) {
                callAppSchemeCallback(obj, false, e.getMessage());
            }
        }

        return success;
    }

    private boolean actionAppScheme(JSONObject req) throws JSONException {
        boolean action = false;
        String command = req.getString("command");
        String lowCommand = command.toLowerCase();
        JSONObject param = req.getJSONObject("param");
        if (TextUtils.equals(lowCommand, "isautologin")) {
            boolean ret = !"0".equals(m_LoginModel.auto_login);
            callAppSchemeCallback(req, true, ret);
            action = true;

        } else if (TextUtils.equals(lowCommand, "issaveid")) {
            boolean ret = !"0".equals(m_LoginModel.save_id);
            callAppSchemeCallback(req, true, ret);
            action = true;

        } else if (TextUtils.equals(lowCommand, "issavecompanyseq")) {
            boolean ret = !"0".equals(m_LoginModel.is_save_company_seq);
            callAppSchemeCallback(req, true, ret);
            action = true;

//        } else if (TextUtils.equals(lowCommand, "isstaging")) {
//            callAppSchemeCallback(req, true, DomainAddress.IS_STAGING);
//            action = true;

//        } else if (TextUtils.equals(lowCommand, "isonline")) {
//            boolean ret = PlayerMessageUtility.getNetworkSettings(m_Activity) != PlayerMessageUtility.NETWORK_OFFLINE;
//            callAppSchemeCallback(req, true, ret);
//            action = true;

//        } else if (TextUtils.equals(lowCommand, "ishdrm")) {
//            boolean ret = Utilities.isHdrmPlayer() && Utilities.isModuleDownload(m_Activity);
//            callAppSchemeCallback(req, true, ret);
//            action = true;

        } else if (TextUtils.equals(lowCommand, "hasdownloadcontents")) {
            boolean ret = false;
            if (getCompany().getDrmUse() && getCompany().getShowButtonLoginDrm()) {
                ret = isFileExists(getApplicationContext());
            }
            callAppSchemeCallback(req, true, ret);
            action = true;

//        } else if (TextUtils.equals(lowCommand, "existsdownloadfileofcourse")) {
//            callAppSchemeCallback(req, true, existsDownloadFileOfCourse(param).toString());
//            action = true;
//
//        } else if (TextUtils.equals(lowCommand, "existsinstalledapp")) {
//            boolean ret = m_Activity.getPackageManager().getLaunchIntentForPackage(param.getString("urlScheme")) != null;
//            callAppSchemeCallback(req, true, ret);
//            action = true;
//
//        } else if (TextUtils.equals(lowCommand, "getappversion")) {
//            callAppSchemeCallback(req, true, m_Activity.getPackageVersion());
//            action = true;
//
//        } else if (TextUtils.equals(lowCommand, "gotologin")) {
//            LoginPreference loginPreference = new LoginPreference(m_Activity);
//            loginPreference.setAutoLogin(false);
//            m_Company.setLoginBoolean(false);
//
//            Intent intent = new Intent(m_Activity, LoginActivity.class);
//            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            m_Activity.startActivity(intent);
////            callAppSchemeCallback(req, true, "");
//            action = true;

//        } else if (TextUtils.equals(lowCommand, "changepassword")) {
//            String newPwd = param.getString("newPwd");
//            LoginPreference loginPreference = new LoginPreference(m_Activity);
//            loginPreference.setPassword(newPwd);
//
//            callAppSchemeCallback(req, true, "");
//            // 기본적으로 창을 닫아야 하는데, history back 이 필요할 수도 있으므로 BackPressed()를 호출
//            m_Activity.onBackPressed();
//            action = true;

        } else if (TextUtils.equals(lowCommand, "dologin")) {
            // 로그인 처리
            readyLogin(req, param);
            action = true;

        } else if (TextUtils.equals(lowCommand, "gotodownloadcenter")) {
            Intent intent = new Intent(this, DownloadCourseActivity.class);
            startActivity(intent);
//            callAppSchemeCallback(req, true, "");
            action = true;

        }
        return action;
    }

    private void readyLogin(JSONObject req, JSONObject param) throws JSONException {
        // parse parameter
        if (param.has("isSaveId") || param.has("isAutoLogin") || param.has("isSaveCompanySeq")) {
            if (param.has("isSaveId")) {
                boolean saveId = param.getBoolean("isSaveId");
                m_LoginModel.save_id = saveId? "1" : "0";
            }
            if (param.has("isAutoLogin")) {
                boolean autoLogin = param.getBoolean("isAutoLogin");
                m_LoginModel.auto_login = autoLogin? "1" : "0";
            }
            if (param.has("isSaveCompanySeq")) {
                boolean isSaveCompanySeq = param.getBoolean("isSaveCompanySeq");
                m_LoginModel.is_save_company_seq = isSaveCompanySeq? "1" : "0";
            }
        }

        String id = param.getString("id");
        String password = param.getString("pwd");
        String companySeq = param.getString("companySeq");

        // action login
        DoLoginAction(id, password, companySeq);
    }

    public void callAppSchemeCallback(JSONObject req, boolean success, Object result) {
        String successCallback = null;
        String errorCallback = null;
        try {
            String command = req.getString("command");
            if (req.has("successCallbackName")) {
                successCallback = req.getString("successCallbackName");
                errorCallback = req.getString("errorCallbackName");
                JSONObject jsonObj = new JSONObject();
                jsonObj.put("result", result);

                StringBuilder builder = new StringBuilder("javascript:");
                if (success) {
                    builder.append(successCallback);
                } else {
                    builder.append(errorCallback);
                }
                builder.append("(");
                builder.append(jsonObj.toString());
                builder.append(");");
//                Log.d(TAG, "reqUrl  " + req.toString());
                Log.d(TAG, "loadUrl " + builder.toString());
                webView.loadUrl(builder.toString());
            } else {
                Toast.makeText(this, "command : " + command + "\nparam : " + req.get("param"), Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void DoLoginAction(String id, String pwd, String companySeq) {
        String ruleId = AppMain.CompanyInfo.getRuleId();
        m_LoginModel.id = id.trim();
        m_LoginModel.pwd = pwd.trim();
        m_LoginModel.company_seq = companySeq.trim();

        // Step 1. 아이디, 비번 검사
        if (CheckLogin(m_LoginModel.id, m_LoginModel.pwd, m_LoginModel.company_seq) == false)
            return;

        if (HunetNetwork.GetCurrentUseNetwork(this) == 0) {
            ShowToast(this.getString(R.string.app_msg_none_network));
            return;
        }

        if ("".equals(ruleId) == false) {
            if (m_LoginModel.id.toLowerCase().indexOf(ruleId.toLowerCase()) == 0)
                m_LoginModel.id = m_LoginModel.id.replaceFirst("(?i)" + ruleId, "");

            m_LoginModel.id = ruleId + m_LoginModel.id;
        }

        ShowDialog(this, "", false);

        String param = String.format("type=1&uid=%s&pw=%s&cseq=%s", URLEncoder.encode(m_LoginModel.id), URLEncoder.encode(m_LoginModel.pwd), m_LoginModel.company_seq);
        AsyncExecute(eLOGIN_INFO, getCompany().getUrl(Company.eURL_LOGIN, param), null);
    }

    @Override
    public void AsyncRequestDataResult(HunetHttpData result) {
        super.AsyncRequestDataResult(result);

        switch (result.m_nId) {
            case eLOGIN_INFO:
                // Step 1. 로그인 성공했는지 검사
                if ("YES".equals(result.GetJsonValue("IsSuccess")) == false) {
                    HideDialog();
                    ShowToast(getString(R.string.loginError));
                    return;
                }

                // Step 2. 로그인 정보 DB 저장
                AccessModel accessModel = m_SQLiteManager.GetAccessData();

                // 동영상 강의 시청 (설정 값이 없을 경우 기본 값 셋팅)
                if ("".equals(accessModel.access_mov) && "Y".equalsIgnoreCase(getString(R.string.label_text_config_3g_mov_default_value)))
                    accessModel.access_mov = "1";
                // 3G/4G 알림 팝업 허용 (설정 값이 없을 경우 기본 값 셋팅)
                if ("".equals(accessModel.access_text) && "Y".equalsIgnoreCase(getString(R.string.label_text_config_3g_textdata_default_value)))
                    accessModel.access_text = "1";
                // 신규 소식, 결재 등 PUSH 알림 수신 (설정 값이 없을 경우 기본 값 셋팅)
                if ("".equals(accessModel.push_type) && "Y".equalsIgnoreCase(getString(R.string.label_text_config_gcm_default_value)))
                    accessModel.push_type = "1";
                // 이동식 메모리 설정
                if ("".equals(accessModel.external_memory))
                    accessModel.external_memory = "0";

                //			accessModel.access_mov 	= "".equals(accessModel.access_mov) ? "0" : accessModel.access_mov;
                //			accessModel.access_text = "".equals(accessModel.access_text) ? "1" : accessModel.access_text;
                //			accessModel.push_type 	= "".equals(accessModel.push_type) ? "1" : accessModel.push_type;
                m_SQLiteManager.SetAccessData(accessModel);
                m_LoginModel.id = result.GetJsonValue("UserId", "");
                m_LoginModel.pwd = result.GetJsonValue("Password", "");

                m_LoginModel.id = m_LoginModel.id.toString().trim();

                new LoginPreference(getBaseContext()).setLoginRuleId(result.GetJsonValue("UserIDRule"));
                m_SQLiteManager.DeleteLoginData();
                m_LoginModel.loginYn = "Y";
                m_SQLiteManager.SetLoginData(m_LoginModel);
                getCompany().LoginModel = m_LoginModel;

                // Step 3. 로그인 정보 저장
                getCompany().setDrmUse("Y".equals(result.GetJsonValue("DrmUseType").toUpperCase()));
                getCompany().setHReminderUseType(result.GetJsonValue("HReminderUseType").toUpperCase());
                getCompany().setHStoryUseType(result.GetJsonValue("HStoryUseType").toUpperCase());
                getCompany().setEduType(result.GetJsonValue("EduType").toUpperCase());
                getCompany().setDefaultHost(result.GetJsonValue("Url"));
                getCompany().setLoginSeq(Integer.parseInt(result.GetJsonValue("CompanySeq", "0")));

                if ("1".equals(getCompany().getHStoryUseType()))
                    getCompany().setViewModel(Company.eTAB_HSTORY, "H-스토리");

                if ("1".equals(getCompany().getHReminderUseType()))
                    getCompany().setViewModel(Company.eTAB_HREMINDER, "H-리마인더");

                // Step 4. 로그인 쿠키 굽기
                List<Cookie> cookies = ((DefaultHttpClient) result.m_HttpClient).getCookieStore().getCookies();

                if (cookies.isEmpty() == false) {
                    CookieSyncManager.createInstance(this);
                    CookieManager cookieManager = CookieManager.getInstance();
                    cookieManager.removeSessionCookie();

                    try {
                        Thread.sleep(500);
                    } catch (Exception e) {
                    }

                    for (int nIndex = 0; nIndex < cookies.size(); nIndex++) {
                        Cookie cookie = cookies.get(nIndex);
                        String strCookie = "";

                        strCookie = cookie.getName() + "=" + cookie.getValue();
                        strCookie += "; path=" + cookie.getPath() + "; domain=" + cookie.getDomain();

                        cookieManager.setCookie("xiunaichina.com", strCookie);
                    }

                    CookieSyncManager.getInstance().startSync();
                }

                DownloadCenterSettingsModel model = new DownloadCenterSettingsModel();
                model.company_seq = Integer.parseInt(result.GetJsonValue("CompanySeq"));
                model.only_imagine_yn = result.GetJsonValue("OnlyImagineYn");
                model.tab1_text = result.GetJsonValue("ProcessMenuAlias");
                model.tab2_text = result.GetJsonValue("ImagineMenuAlias");
                manager.SetDownloadCenterSettings(model);

                AppSettingsModel appSetting = new AppSettingsModel();
                appSetting.company_seq = Integer.parseInt(result.GetJsonValue("CompanySeq", "0"));
                appSetting.enable_sangsang_player_speedbar = result.GetJsonValue("EnableSSPlayerSpeedBar", "N");
                appSetting.enable_sangsang_player_overlap_study = getCompany().getEnabledSangSangOverlapStudy() ? "Y" : "N";
                manager.SetAppSettings(appSetting);

                GCMRegistUtil.registGcm(this);
                PushManager.startWork(getApplicationContext(),PushConstants.LOGIN_TYPE_API_KEY,PushApiKey);
                String url = String.format(getCompany().getUrl(Company.eURL_APP_VERSION), getCompany().getPackageName(this).replace("cn.co.hunet.MobileLearningCenterFor", ""));
                String ClientId = BaiduUtils.getMetaValue(HtmlLoginActivity.this, PushApiKey);


                // Set Tags
                String companySeq = result.GetJsonValue("CompanySeq");
                String WorkAreaCode = result.GetJsonValue("WorkAreaCode");
                String TagStaring = "tag"+companySeq+WorkAreaCode;
                List<String> tags = BaiduUtils.getTagsList(TagStaring);
                PushManager.setTags(getApplicationContext(), tags);


                AsyncExecute(eVERSION_INFO, url, null);
                break;
            case eVERSION_INFO:
                HideDialog();
                DownUrl = result.GetJsonValue("DownloadUrl","");
                String updateVersion = result.GetJsonValue("Version", "0.0.0");

                if (CheckUpdateVersion(updateVersion)) {
                    versionUpgradeMsgAlert(updateVersion);
                    return;
                }

                NextActivity();
                break;
        }
    }

    private void versionUpgradeMsgAlert(String updateVersion) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("有新版本").setMessage(this.getString(R.string.app_msg_confirm_update))
                .setPositiveButton("更新", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
//                        String url = "market://details?id=" + getPackageName();
//                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
//                        startActivity(intent);
//                        finish();
                        downLoadApk();
                    }
                })
                .setNeutralButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        NextActivity();
                    }
                })
                .show();
    }

    private void NextActivity() {
        Bundle bundle = getIntent().getExtras();
        String companySeq = this.getBundleValue(bundle, "cSeq", "");
        String Id = this.getBundleValue(bundle, "userId", "");
        String processCd = this.getBundleValue(bundle, "processCd", "");
        String processYear = this.getBundleValue(bundle, "processYear", "");
        String processTerm = this.getBundleValue(bundle, "processTerm", "");
        String action = this.getBundleValue(bundle, "action", "");

        // 녹십자 App to App
        if (isSSO() && ("13830".equals(companySeq) || action.equalsIgnoreCase("classroom")) && !"".equals(processCd) && !"".equals(processYear)) {
            NextActivity_MyClassroom(processCd, processYear, processTerm, companySeq);
        } else {
            Intent intent = new Intent(this, getCompany().getNextActivity(getCompany().eNEXT_ACTIVITY_LOGIN));

            if ("".equals(m_NextUrl) == false)
                intent.putExtra("NEXT_URL", m_NextUrl);

            // Code begin - SSO 기업들 차후 삭제할 코드
            if (getCompany().getSeq() == 9242 || getCompany().getSeq() == 1075 || getCompany().getSeq() == 214)
                m_TabIndex = 2;
            // Code end

            intent.putExtra("TAB_INDEX", m_TabIndex);
            intent.putExtra("INDEX_DETAIL", m_IndexDetail);
            intent.putExtra("IS_SSO", isSSO());

            m_NextUrl = m_IndexDetail = "";
            m_TabIndex = 0;

            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            startActivity(intent);
        }
    }

    /**
     * 해당 기업의 해당 과정 학습방으로 이동 합니다.
     *
     * @param processCd   과정코드
     * @param processYear 년도
     * @param processTerm 기수
     * @param companySeq  기업코드
     */
    private void NextActivity_MyClassroom(String processCd, String processYear, String processTerm, String companySeq) {
        // 학습방으로 이동
        Intent intent = new Intent(this, getCompany().getNextActivity(getCompany().eACTIVITY_CLASSROOM));

        intent.putExtra("type", "userCourseRoom");
        intent.putExtra("processCd", processCd);
        intent.putExtra("processYear", processYear);
        intent.putExtra("processTerm", processTerm);
        intent.putExtra("cSeq", companySeq);
        intent.putExtra("userId", m_LoginModel.id);

        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }

    private void registRunApplicationFlag() {
        DeviceUtility deviceUtility = new DeviceUtility(this);
        String packageNm = "";
        String modelName = "";
        String androidID = "";

        try {
            packageNm = URLEncoder.encode(getPackageName(), "utf-8");
            modelName = URLEncoder.encode(deviceUtility.getModel(), "utf-8");
            androidID = URLEncoder.encode(deviceUtility.getAndroidId(), "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        AppEventModel model = new AppEventModel();
        model.url = String.format(DomainAddress.getUrlMLC() + "/App/JLog?type=1002&userId=%s&deviceModel=%s&androidId=%s&gcmId=%s&packageNm=%s&companySeq=%d"
                , "", modelName, androidID, "", packageNm, getCompany().getSeq());

        m_SQLiteManager.InsertAppEvent(model);

        startService(new Intent(this, HttpSenderService.class));
    }

    private void sso_login(Intent intent) {
        Bundle bundle = intent.getExtras();

        String companySeq = this.getBundleValue(bundle, "cSeq", "");
        String Id = this.getBundleValue(bundle, "userId", "");

        String param = String.format("type=24&etc3=%s&etc1=%s", URLEncoder.encode(Id), companySeq);
        ShowDialog(this, "", false);
        AsyncExecute(eLOGIN_INFO, getCompany().getUrl(Company.eURL_LOGIN, param), null);
    }

    private boolean isFileExists(Context context) {
        boolean bExist = false;
        File file = new File(AppMain.GetExternalDir()); // hunet_mlc
        bExist = isFileExist(file);

        if (bExist)
            return true;

        if (Utilities.getExternalMemoryCheck(context)) {
            file = Utilities.getExternalMemoryFileHandle(context);
            bExist = isFileExist(file);
            if (bExist)
                return true;
        }

        file = Utilities.getInternalMemoryFileHandle(context);
        bExist = isFileExist(file);

        return bExist;
    }

    private boolean isFileExist(File file) {
        boolean bExist = false;

        if (file == null || (file.exists() == false))
            return bExist;

        File[] fileList = file.listFiles();

        if (fileList.length <= 0) {
            if (file.exists())
                file.delete();
            return false;
        }

        String message = null;

        for (int i = 0; i < fileList.length; i++) {

            if (fileList[i].isDirectory()) {
                message = "[디렉토리] ";
                message += fileList[i].getName();
                //Log.d(TAG, message);

                try {
                    if (!subDirList(fileList[i].getCanonicalPath().toString())) {
                        // 비어있는 폴더 삭제
                        if (fileList[i].exists())
                            fileList[i].delete();
                        //Log.d(TAG, "폴더 삭제");
                    } else {
                        bExist = true;
                        break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                message = "[파일] ";
                message += fileList[i].getName();
                bExist = true;
                //Log.d(TAG, message);
                break;
            }
        }
        return bExist;
    }

    private static boolean subDirList(String source) {
        File dir = new File(source);
        File[] fileList = dir.listFiles();

        if (fileList.length <= 0) {
            return false;
        }

        try {
            for (int i = 0; i < fileList.length; i++) {
                File file = fileList[i];
                if (file.isFile()) {
                    // 파일이 있다면 파일 이름 출력
                    //Log.d(TAG, "[파일 이름] = " + file.getName());
                    return true;
                } else if (file.isDirectory()) {
                    // 서브디렉토리가 존재하면 재귀적 방법으로 다시 탐색
                    if (!subDirList(file.getCanonicalPath().toString())) {
                        if (file.exists())
                            file.delete();
                        return false;
                    } else {
                        return true;
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }


    /** 앱접속시 최초 버전체크 및 다운로드 설치 */
    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case UPDATA_NONEED:
                    Toast.makeText(getApplicationContext(), getString(R.string.update_no_update_version),
                            Toast.LENGTH_SHORT).show();
                case UPDATA_CLIENT:
                    //对话框通知用户升级程序
                    showUpdataDialog();
                    break;
                case GET_UNDATAINFO_ERROR:
                    //服务器超时
                    Toast.makeText(getApplicationContext(),  getString(R.string.update_error), Toast.LENGTH_LONG).show();
                    break;
                case DOWN_ERROR:
                    //下载apk失败
                    Toast.makeText(getApplicationContext(), getString(R.string.download_error), Toast.LENGTH_LONG).show();
                    break;
            }
        }
    };


    /*
     *
     * 弹出对话框通知用户更新程序
     *
     * 弹出对话框的步骤：
     *  1.创建alertDialog的builder.
     *  2.要给builder设置属性, 对话框的内容,样式,按钮
     *  3.通过builder 创建一个对话框
     *  4.对话框show()出来
     */
    private void showUpdataDialog() {
        AlertDialog.Builder builer = new AlertDialog.Builder(this);
        builer.setTitle(getString(R.string.version_update));
        builer.setMessage(info.getDescription());
        //当点确定按钮时从服务器上下载 新的apk 然后安装   װ
        builer.setPositiveButton(getString(R.string.btn_confirm), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                //Log.i(TAGU, "下载apk,更新");
                downLoadApk();
            }
        });
        builer.setNegativeButton(getString(R.string.btn_cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                //do sth
            }
        });
        AlertDialog dialog = builer.create();
        dialog.show();
    }

    /*
     * 从服务器中下载APK
     */
    private void downLoadApk() {

        final ProgressDialog pd;    //进度条对话框
        pd = new  ProgressDialog(this);
        pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        pd.setMessage(getString(R.string.download_ing));
        pd.show();

        new Thread(){
            @Override
            public void run() {
                try {
                    File file = DownLoadManager.getFileFromServer(DownUrl, pd);
                    sleep(3000);
                    installApk(file);
                    pd.dismiss(); //结束掉进度条对话框
                } catch (Exception e) {
                    Message msg = new Message();
                    msg.what = DOWN_ERROR;
                    handler.sendMessage(msg);
                    e.printStackTrace();
                }
            }}.start();
    }

    //安装apk
    private void installApk(File file) {
        Intent intent = new Intent();
        //执行动作
        intent.setAction(Intent.ACTION_VIEW);
        //执行的数据类型
        intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
        startActivity(intent);
    }

    public class JSClass {
        @JavascriptInterface
        @Override
        public String toString() {
            return "ScriptBridge";
        }

//        @JavascriptInterface
//        public void DoLogin(String id, String password, boolean isSaveId, boolean isAutoLogin) {
//            config.isSaveId = isSaveId;
//            config.isAutoLogin = isAutoLogin;
//            DoLoginAction(id, password);
//        }
//
//        @JavascriptInterface
//        public void DoLogin(String id, String password, boolean isSaveId, boolean isAutoLogin, String company_seq) {
//            config.isSaveId = isSaveId;
//            config.isAutoLogin = isAutoLogin;
//
//            // 숫자형이 들어와야 되는데 안들어올 경우
//            try{
//                int companySeq = Integer.parseInt(company_seq);
//                getCompany().setLoginSeq(companySeq);
//            }catch(Exception e){
//                e.printStackTrace();
//            }
//
//            DoLogin(id, password);
//        }

        @JavascriptInterface
        public boolean isSaveId() {
            return !"0".equals(m_LoginModel.save_id);
        }

        @JavascriptInterface
        public boolean isAutoLogin() {
            return !"0".equals(m_LoginModel.auto_login);
        }

        @JavascriptInterface
        public boolean isSaveCompanySeq() {
            return !"0".equals(m_LoginModel.is_save_company_seq);
        }

        @JavascriptInterface
        public boolean hasDownloadContents() {
            boolean ret = false;
            if (getCompany().getDrmUse() && getCompany().getShowButtonLoginDrm()) {
                ret = isFileExists(getApplicationContext());
            }
            return ret;
        }

//        @JavascriptInterface
//        public void goToDownloadCenter() {
//            showDownloadCenter();
//        }

        @JavascriptInterface
        public void DoLogin(String id, String pwd, boolean isSaveCompanySeq, boolean isSaveId, boolean isAutoLogin, String companySeq) {
            m_LoginModel.save_id = isSaveId? "1" : "0";
            m_LoginModel.auto_login = isAutoLogin? "1" : "0";
            m_LoginModel.is_save_company_seq = isSaveCompanySeq? "1" : "0";

            // action login
            DoLoginAction(id, pwd, companySeq);
        }

        @JavascriptInterface
        public String getUserId() {
            String ruleId = new LoginPreference(getBaseContext()).getLoginRuleId();
            String id = m_LoginModel.id;
            if (m_LoginModel.id.startsWith(ruleId)) {
                id = m_LoginModel.id.replace(ruleId, "");
            }
            return id;
        }

        @JavascriptInterface
        public String getCompanySeq() {
            return m_LoginModel.company_seq;
        }
    }
}
