package cn.co.HunetLib;

import java.util.ArrayList;

import cn.co.HunetLib.Gallery.LruMemoryCache;
import cn.co.HunetLib.Gallery.ThumbImageInfo;
import cn.co.HunetLib.R;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;

public class GalleryActivity extends Activity implements ListView.OnScrollListener, GridView.OnItemClickListener, View.OnClickListener
{
	private ArrayList<ThumbImageInfo> mThumbImageInfoList;
	private boolean 		mBusy = false;
	private GridView 		mGvImageList;
	private ImageAdapter 	mListAdapter;
	private Button 			m_btnSelectOk;
	private int				max_upload_count = 4;
  
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.gallery_list);
		    
		mThumbImageInfoList = new ArrayList<ThumbImageInfo>();
		mGvImageList 		= (GridView)findViewById(R.id.gvImageList);
		m_btnSelectOk 		= (Button)findViewById(R.id.btnSelectOk);
		
		mGvImageList.setOnScrollListener(this);
		mGvImageList.setOnItemClickListener(this);
		m_btnSelectOk.setOnClickListener(this);
		m_btnSelectOk.setEnabled(false);
		
		getIntent(getIntent());
	}
	
	@Override
	protected void onResume()
	{	
		super.onResume();
		m_btnSelectOk.setText(String.format("전송 (0/%d)", max_upload_count));
		new DoFindImageList().execute();
	}
	
	@Override
	protected void onNewIntent(Intent intent) {
		getIntent(intent);
	}
	
	private void getIntent(Intent intent) {
		if (intent != null) {
			Bundle bundle = intent.getExtras();
			max_upload_count = bundle.getInt("max_upload_count", 4);
		}
	}
	
	@Override
	protected void onPause()
	{
		super.onPause();
	}
	
	@Override
	public void onClick(View v) 
	{
		Intent intent = getIntent();		
		intent.putExtra("checkData", CheckItemData());
		setResult(RESULT_OK, intent);
		finish();
	}
  
	private long findThumbList()
	{
		long returnValue = 0;
		mThumbImageInfoList.clear();
		
		String[] projection = { MediaStore.Images.Media._ID, MediaStore.Images.Media.DATA };		
		
		Cursor imageCursor = managedQuery(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, projection, null, null, MediaStore.Images.Media.DATE_ADDED + " desc ");
		 
		if (imageCursor != null && imageCursor.getCount() > 0)
		{
			int imageIDCol 		= imageCursor.getColumnIndex(MediaStore.Images.Media._ID); 
			int imageDataCol 	= imageCursor.getColumnIndex(MediaStore.Images.Media.DATA);
			
			while (imageCursor.moveToNext())
			{
				ThumbImageInfo thumbInfo = new ThumbImageInfo();
				thumbInfo.setId(imageCursor.getString(imageIDCol));
				thumbInfo.setData(imageCursor.getString(imageDataCol));
				thumbInfo.setCheckedState(false);
				        
				mThumbImageInfoList.add(thumbInfo);
				returnValue++;
			}
		}
		
		return returnValue;
	}

	private void updateUI()
	{
		mListAdapter = new ImageAdapter(this, R.layout.gallery_cell, mThumbImageInfoList);
		mGvImageList.setAdapter(mListAdapter);
	}
  
	public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount)
	{}
  
	public void onScrollStateChanged(AbsListView view, int scrollState)
	{
		switch (scrollState)
		{
		case OnScrollListener.SCROLL_STATE_IDLE:
			mBusy = false;
			mListAdapter.notifyDataSetChanged();
			break;
		case OnScrollListener.SCROLL_STATE_TOUCH_SCROLL:
			mBusy = true;
			break;
		case OnScrollListener.SCROLL_STATE_FLING:
			mBusy = true;
			break;
		}
	}
  
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3)
	{				
		ImageAdapter 	adapter = (ImageAdapter) arg0.getAdapter();
		ThumbImageInfo 	rowData = (ThumbImageInfo)adapter.getItem(position);
		boolean curCheckState 	= rowData.getCheckedState();
		int 	nCheckCount 	= CheckItemCount();
		
		if(curCheckState == false && nCheckCount >= max_upload_count)
			return;

		rowData.setCheckedState(!curCheckState);

		mThumbImageInfoList.set(position, rowData);
		adapter.notifyDataSetChanged();
		
		nCheckCount = CheckItemCount();
		
		if(nCheckCount > 0)
			m_btnSelectOk.setEnabled(true);
		else
			m_btnSelectOk.setEnabled(false);
		
		m_btnSelectOk.setText(String.format("전송 (%d/%d)", nCheckCount, max_upload_count));
	}
	
	private String CheckItemData()
	{
		String strPath = "";
		for(int nIndex = 0; nIndex < mThumbImageInfoList.size(); nIndex++)
		{
			ThumbImageInfo info = mThumbImageInfoList.get(nIndex);
			
			if(info.getCheckedState())
				strPath += info.getData() + ",";
		}
		
		return strPath;
	}
	
	private int CheckItemCount()
	{
		int nChecked = 0;
		
		for(int nIndex = 0; nIndex < mThumbImageInfoList.size(); nIndex++)
		{
			ThumbImageInfo info = mThumbImageInfoList.get(nIndex);
			
			if(info.getCheckedState())
				nChecked++;
		}
		
		return nChecked;
	}

	static class ImageViewHolder
	{
		ImageView ivImage;
		CheckBox chkImage;
	}

	private class ImageAdapter extends BaseAdapter
	{
		static final int VISIBLE = 0x00000000; 
		static final int INVISIBLE = 0x00000004; 
		private Context mContext;
		private int mCellLayout;
		private LayoutInflater mLiInflater;
		private ArrayList<ThumbImageInfo> mThumbImageInfoList;
		private LruMemoryCache mCache;
		
		public ImageAdapter(Context c, int cellLayout, ArrayList<ThumbImageInfo> thumbImageInfoList)
		{
			mContext 			= c;
			mCellLayout 		= cellLayout;
			mThumbImageInfoList = thumbImageInfoList;
			mLiInflater 		= (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			mCache 				= new LruMemoryCache();
		}

		public int getCount()
		{
			return mThumbImageInfoList.size();
		}

		public Object getItem(int position)
		{
			return mThumbImageInfoList.get(position);
		}

		public long getItemId(int position)
		{
			return position;
		}

		public View getView(int position, View convertView, ViewGroup parent)
		{
			if (convertView == null)
			{
				convertView = mLiInflater.inflate(mCellLayout, parent, false);
				ImageViewHolder holder = new ImageViewHolder();

				holder.ivImage 	= (ImageView)convertView.findViewById(R.id.ivImage);
				holder.chkImage = (CheckBox)convertView.findViewById(R.id.chkImage);

				convertView.setTag(holder);
			}

			final ImageViewHolder holder = (ImageViewHolder) convertView.getTag();
			ThumbImageInfo thumInfo = ((ThumbImageInfo)mThumbImageInfoList.get(position));

			if (thumInfo.getCheckedState())
				holder.chkImage.setChecked(true);
			else
				holder.chkImage.setChecked(false);

			if (!mBusy)
			{
				try
				{
					Bitmap bmp = (Bitmap)mCache.getBitmapFromMemCache(thumInfo.getId());					

					if (bmp != null)
					{	
						holder.ivImage.setImageBitmap(bmp);
						holder.ivImage.setVisibility(VISIBLE);
					}
					else
					{
				        BitmapWorkerTask task = new BitmapWorkerTask(holder);
				        task.execute(Integer.parseInt(thumInfo.getId()));
					}

					setProgressBarIndeterminateVisibility(false);
				}
				catch (Exception e)
				{
					e.printStackTrace();
					setProgressBarIndeterminateVisibility(false);
				}
			}
			else
			{
				setProgressBarIndeterminateVisibility(true);
				holder.ivImage.setVisibility(INVISIBLE);
			}

			return convertView;
		}
		
		private class BitmapWorkerTask extends AsyncTask<Integer, Void, Bitmap> 
		{
			private ImageViewHolder m_holder;
			
			public BitmapWorkerTask(ImageViewHolder holder)
			{
				m_holder = holder;
			}

			@Override
			protected Bitmap doInBackground(Integer... params) 
			{
		        Bitmap bitmap = MediaStore.Images.Thumbnails.getThumbnail(
						getContentResolver(), params[0],
						MediaStore.Images.Thumbnails.MICRO_KIND, null);
		        
		        mCache.addBitmapToMemoryCache(String.valueOf(params[0]), bitmap);
		        return bitmap;
		    }

			@Override
			protected void onPostExecute(Bitmap result)
			{
				m_holder.ivImage.setImageBitmap(result);
				m_holder.ivImage.setVisibility(VISIBLE);
			}
		}
	}	
	
	private class DoFindImageList extends AsyncTask<String, Integer, Long>
	{
		@Override
		protected Long doInBackground(String... arg0)
		{
			long returnValue = 0;
			returnValue = findThumbList();
			return returnValue;
		}

		@Override
		protected void onPostExecute(Long result)
		{
			updateUI();
		}
	}
}
