package cn.co.HunetLib.SQLite;

public class AppEventModel 
{
	public int event_seq = 0;
	public String url = "";
	public String reg_date = "";
	public int try_count = 0;
	public int max_count = 10;
	
	@Override
	public String toString()
	{
		return String.format("event_seq : %d, try_count : %d, max_count : %d, reg_date : %s", event_seq, try_count, max_count, reg_date);
	}
}
