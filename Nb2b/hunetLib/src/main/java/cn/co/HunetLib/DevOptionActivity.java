package cn.co.HunetLib;

import hunet.domain.DomainAddress;
import cn.co.HunetLib.Base.TabActivity;
import cn.co.HunetLib.Company.Company;
import cn.co.HunetLib.Interface.ITabControlEventListener;
import cn.co.HunetLib.R;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import android.widget.CompoundButton.OnCheckedChangeListener;

public class DevOptionActivity extends TabActivity implements ITabControlEventListener, OnCheckedChangeListener
{
	LinearLayout 	ll_config_back;		
	CheckBox		dev_option_staging;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	
	    setContentView(R.layout.devoption_activity);
	    
	    ll_config_back 		= (LinearLayout)findViewById(R.id.more_ll_config_back);
	    dev_option_staging 	= (CheckBox)findViewById(R.id.dev_option_staging);
		
		CreateTabControl((LinearLayout)findViewById(R.id.more_ll_config_tab));
		setSelectTab(Company.eTAB_DEVOPTION);
		
		dev_option_staging.setChecked(DomainAddress.IS_STAGING);
		dev_option_staging.setOnCheckedChangeListener(this);
		
		ll_config_back.setOnClickListener(new OnClickListener() { @Override
		public void onClick(View v) 
		{
	    	finish();
		}});
	}
	
	@Override
	protected void onFirstResume() 
	{
	}
	
	@Override
	protected void onResume()
	{
		super.onResume();
	}
	
	@Override
	protected void onPause()
	{
		super.onPause();
	}	

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) 
	{
		DomainAddress.IS_STAGING = isChecked;
//		this.getCompany().setTestHost(isChecked);
	}
}
