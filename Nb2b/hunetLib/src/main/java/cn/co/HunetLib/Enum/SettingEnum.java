package cn.co.HunetLib.Enum;

public enum SettingEnum {
	Check("1", "체크"),
	NoneCheck("0", "비체크"),
	Blank("", "빈값");
	
	
	private String code;
	private String description;
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	
	SettingEnum(String cd, String desc)
	{
		this.code = cd;
		this.description = desc;
	}
}
