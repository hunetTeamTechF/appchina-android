package cn.co.HunetLib.OldPlayer;

/**
 * 강의관련 HTTP 통신. 플레이정보
 * 작업자 : 장형일
 * 작업일 : 2011.11
 * mlc 플레이어 인앱 수정. 2011.11.11 
 */
public class CourseData {

	private int CourseType = 0;
	private String reviewEndDate = "";

	public void setCourseType(int courseType) {
		CourseType = courseType;
	}

	public int getCourseType() {
		return CourseType;
	}

	public void setReviewEndDate(String reviewEndDate) {
		this.reviewEndDate = reviewEndDate;
	}

	public String getReviewEndDate() {
		return reviewEndDate;
	}
	
	public CourseData()
	{
		
	}
	
	public CourseData(String reviewEndDate, int CourseType)
	{
		this.CourseType = CourseType;
		this.reviewEndDate = reviewEndDate;
	}
}
