package cn.co.HunetLib.Common;

import java.io.File;
import java.net.URLEncoder;
import java.util.HashMap;

import cn.co.HunetLib.GalleryActivity;
import cn.co.HunetLib.HLeaderShipActivity;
import cn.co.HunetLib.HtmlCourseActivity;
import cn.co.HunetLib.SQLite.LoginModel;
import cn.co.HunetLib.Stw.SangSangStw;
import cn.co.HunetLib.HtmlLanguageCourseActivity;
import cn.co.HunetLib.Base.BaseActivity;
import cn.co.HunetLib.Company.Company;
import cn.co.HunetLib.HttpFileDownload.FileDownload;
import cn.co.HunetLib.HttpFileDownload.FileDownload.downloadFileEventListener;
import cn.co.HunetLib.Stw.PlayerStw;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.text.TextUtils;
import android.webkit.WebView;

public class UrlAction 
{
	private BaseActivity 	m_Activity;
	private WebView 		m_Webview;
	private Company		m_Company;
	private int 			m_TabId;
	private SangSangStw m_SangSangStw;
	private PlayerStw		m_PlayerStw;
	private String 		m_ActionNm = "";
	
	public UrlAction()
	{
		
	}
	
	public void Init(BaseActivity activity, WebView webview)
	{
		m_Activity 		= activity;
		m_Webview		= webview;
		m_Company 		= activity.getCompany();
		m_SangSangStw	= m_Company.getSangSangStw(activity, webview, m_Company.getSeq(), m_Company.LoginModel.id);
		m_PlayerStw		= m_Company.getPlayerStw(activity, webview, m_Company.getSeq(), m_Company.LoginModel.id);
	}

	public void Clear()
	{
		if(m_SangSangStw != null)
			m_SangSangStw.Destroy();
		
		if(m_PlayerStw != null)
			m_PlayerStw.Destroy();
		
		m_SangSangStw = null;
		m_PlayerStw = null;
		m_Activity = null;
		m_Company = null;		
	}
	
	public void setTabId(int tabId)
	{
		m_TabId = tabId;
	}	
	
	public void CheckAction()
	{
		if(m_PlayerStw != null)
			m_PlayerStw.CheckAction();
		
		if(m_SangSangStw != null)
			m_SangSangStw.CheckAction();
		
		String actionNm = m_ActionNm;
		m_ActionNm = "";
		
		if("".equals(actionNm))
			return;
		
		if(m_Webview != null && "refresh".equals(actionNm))
			m_Webview.reload();
	}
	
	public boolean CheckUrl(String url)
	{		
		String tempUrl = url.toLowerCase();
		
		boolean isStop = false;

		switch(m_TabId)
		{
		case Company.eTAB_HOME: isStop = CheckHomeUrl(url); break;
		}
		
		if(isStop)
			return true;
		
		if(m_SangSangStw != null && m_SangSangStw.HasSangSangProtocal(tempUrl))
			return true;
		else if(m_PlayerStw != null && m_PlayerStw.HasProtocal(tempUrl))
			return true;
		else if(tempUrl.endsWith(".pdf")) {
			File folderPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
			File downloadPath = new File(folderPath, new File(url).getName());
			
			if (!folderPath.exists())
				folderPath.mkdir();
			
			m_Activity.ShowDialog(m_Activity, "", true);
			
			new FileDownload().download(url, downloadPath.getPath(), new downloadFileEventListener() {
				
				@Override
				public void completed(File downloadFile) {
					Uri uri = Uri.fromFile(downloadFile);
					Intent intent = new Intent(Intent.ACTION_VIEW);
					intent.setDataAndType(uri, "application/pdf");
					
					m_Activity.HideDialog();
					m_Activity.startActivity(intent);
				}
			});
			return true;
		} else if(tempUrl.startsWith("modal://")) {
			try {
				Uri uri = Uri.parse(url);
				String paramUrl = URLEncoder.encode(uri.getQueryParameter("url"), "utf-8");
				boolean paramHeader = "y".equalsIgnoreCase(uri.getQueryParameter("header"));
				String paramTitle = TextUtils.isEmpty(uri.getQueryParameter("title")) ? "" : uri.getQueryParameter("title");

				m_Company.setStartActivity(m_Activity, Company.eACTIVITY_MODAL, String.format("LOAD_URL=%s&SHOW_HEADER=%s&HEADER_TITLE=%s", paramUrl, paramHeader ? "Y" : "N", paramTitle), false, 0);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return true;
		} else if(url.contains("htmlplay://")) {
			String studyUrl = url.replace("htmlplay://", "");	
			Intent intent = new Intent(m_Activity, HtmlCourseActivity.class);
			intent.putExtra("url", studyUrl);
			
			m_ActionNm = "refresh";
			m_Activity.startActivity(intent);
			return true;
		}
		else if(url.contains("study.hunet.co.kr") && tempUrl.contains("/study/htools/") == false)
		{
			Uri parseUri = Uri.parse(url);
			String screenOrientation = parseUri.getQueryParameter("contentsType");
			String studyUrl = url.replace("http://", "");

			Intent intent = new Intent(m_Activity, HtmlLanguageCourseActivity.class);
			intent.putExtra("url", studyUrl);
			intent.putExtra("orientation", screenOrientation);
			intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
			
			m_ActionNm = "refresh";
			m_Activity.startActivity(intent);
			return true;
		}
		else if(tempUrl.contains("finish://") || tempUrl.contains("close://"))
		{
			m_Activity.finish();
			return true;
		}
		else if(tempUrl.contains("webtoon://"))
		{			    		
    		m_Company.setStartActivity(m_Activity, Company.eACTIVITY_WEBTOON, String.format("webtoonUrl=%s",  URLEncoder.encode(url.replace("webtoon://", ""))), false, Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
    		return true;
		}
		else if(tempUrl.contains("/home/home"))
		{
			if(Company.eTAB_HOME == m_TabId)
				return false;
				
			m_Company.onChangeTab(m_Activity, Company.eTAB_HOME, m_TabId);
			return true;
		}
		else if(tempUrl.contains("downloadcenter://"))
		{
			m_Company.onChangeTab(m_Activity, Company.eTAB_DOWNLOAD_CENTER, m_TabId);
			return true;
		}
    	else if(tempUrl.contains("/more/more.aspx")) //더보기 메뉴
    	{				    		
    		m_Company.onChangeTab(m_Activity, Company.eTAB_MORE, m_TabId);
			return true;
    	}
    	else if(tempUrl.indexOf("historyback://") == 0 || tempUrl.indexOf("back://") == 0)
    	{
    		if(m_Webview.canGoBack())
    			m_Webview.goBack();
    		else 
    			m_Activity.finish();
    		
    		return true;
    	}
    	else if(tempUrl.contains("/hstory/detail"))
    	{
    		m_Company.setStartActivity(m_Activity, Company.eACTIVITY_HSTORY, String.format("SHOW_HEADER=N&SHOW_TAB=N&TAB_NUMBER=%d&LOAD_URL=%s", Company.eTAB_HSTORY, URLEncoder.encode(url)), false, 0);
    		return true;
    	}
    	else if(tempUrl.indexOf("jscall://callphotoview") > -1)
		{
    		Uri redirectUri = Uri.parse(url);
    		int max_upload_count = redirectUri.getQueryParameter("count") == null ? 4 : Integer.parseInt(redirectUri.getQueryParameter("count"));
    		
			Intent intent = new Intent(m_Activity, GalleryActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
			intent.putExtra("max_upload_count", max_upload_count);
			m_Activity.startActivityForResult(intent, Company.eACTIVITY_RESULT_GALLERY_IMG);
			return true;
		}
    	else if(tempUrl.indexOf("logout://") > -1)
    	{
    		DoLogout();
    		return true;
    	}
    	else if(tempUrl.indexOf("h-leadership.hunet.co.kr/app/mypage/") > -1)
    	{
    		Intent intent = new Intent(m_Activity, HLeaderShipActivity.class);
    		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    		m_Activity.startActivity(intent);
    		return true;
    	}
    	else if(CheckActionView(url))
    	{
			return true;
    	}
    	else if(url.contains("tel:0215886559"))
		{			
			Intent intent = new Intent(Intent.ACTION_DIAL);
			intent.setData(Uri.parse("tel:0215886559"));
			m_Activity.startActivity(intent);
			return true;
		}
    	else if(CheckMp4File(tempUrl) || CheckM3U8File(tempUrl))
		{
			Intent intent = new Intent(Intent.ACTION_VIEW);
			Uri uri = Uri.parse(url);
			intent.setDataAndType(uri, "video/*");
			m_Activity.startActivity(intent);
			return true;
		}
		else if (tempUrl.startsWith("intent:")) {
			int customUrlStartIndex = "intent://".length();
			int customUrlEndIndex = url.indexOf("#Intent;");
			if (customUrlEndIndex < 0) {
				return false;
			} else {
				int packageStartIndex = customUrlEndIndex + "#Intent;".length();
				int packageEndIndex = url.indexOf(";end");

				String[] intentInfo = url.substring(packageStartIndex, packageEndIndex < 0 ? url.length() : packageEndIndex).split(";");
				HashMap<String, String> intentParams = new HashMap<String, String>();
				for (String s : intentInfo) {
					String[] item = s.split("=");
					intentParams.put(item[0], item[1]);
				}

				try {
					String customUrl = url.substring(customUrlStartIndex, customUrlEndIndex);
					m_Activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(intentParams.get("scheme") + "://" + customUrl)));
				} catch (ActivityNotFoundException e) {
					m_Activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + intentParams.get("package"))));
				}
				return true;
			}
		}
		
		return false;
	}
	
	private boolean CheckActionView(String url)
	{
		String tempUrl = url.toLowerCase();
		String viewUri = "";
		
		if(tempUrl.indexOf("appstore://") > -1)
    	{
    		HunetProtocal protocal = new HunetProtocal();
    		protocal.TryParse(url);
    		
    		viewUri = "market://details?id=" + protocal.GetValue("android");
    	}
    	else if(tempUrl.contains("market://"))
    		viewUri = url;
    	else if(tempUrl.contains("outsideurl://"))
    		viewUri = url.replace("outsideurl://", "");
    	else if(tempUrl.contains("escape://"))    	
    		viewUri = url.replace("escape://", "http://");
		
		if("".equals(viewUri))
			return false;
		
		Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(viewUri));
		m_Activity.startActivity(intent);
		
		return true;
	}

	private boolean CheckMp4File(String url)
	{
		if(url.contains(".mp4") == false)
			return false;
			
		int pos = url.lastIndexOf( "." );
		
		if(pos == -1)
			return false;
		
		String fileExt = url.substring( pos + 1 ).toLowerCase();
		
		return "mp4".equals(fileExt);
	}

	private boolean CheckM3U8File(String url)
	{
		if(url.contains(".m3u8") == false)
			return false;
			
		int pos = url.lastIndexOf(".");
		
		if(pos == -1)
			return false;
		
		String fileExt = url.substring( pos + 1 ).toLowerCase();
		
		return "m3u8".equals(fileExt);
	}

	private boolean CheckHomeUrl(String url)
	{
		String tempUrl = url.toLowerCase();
		
		if(tempUrl.startsWith("modal://")) {
			try {
				Uri uri = Uri.parse(url);
				String paramUrl = URLEncoder.encode(uri.getQueryParameter("url"), "utf-8");
				String paramTitle = TextUtils.isEmpty(uri.getQueryParameter("title")) ? "" : uri.getQueryParameter("title");
				String paramHeader = TextUtils.isEmpty(paramTitle) ? "N" : "Y";

				m_Company.setStartActivity(m_Activity, Company.eACTIVITY_MODAL, String.format("LOAD_URL=%s&SHOW_HEADER=%s&HEADER_TITLE=%s", paramUrl, paramHeader, paramTitle), false, 0);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return true;
		} else if(tempUrl.indexOf("/my/my.aspx") > -1 || tempUrl.contains("/imgclassroom/studysummary")) {
			m_Company.onChangeTab(m_Activity, Company.eTAB_CLASSROOM, m_TabId);
			return true;
		}
		else if(tempUrl.indexOf("mlc.hunet.co.kr/My/MyCourseHome.aspx") > -1)
		{
			String[] 	strTemp = url.substring(url.indexOf("?")+1).split("&");
    		String 		putData = "";    		
    		    		
    		putData = "type=userMenu";
    		putData += "&processCd=" + strTemp[0].split("=")[1];
    		putData += "&processYear=" + strTemp[1].split("=")[1];
    		putData += "&processTerm=" + strTemp[2].split("=")[1];
    		    		
    		if(strTemp.length > 3) 
    			putData += "&courseCd=" + strTemp[3].split("=")[1];
    		
    		if(strTemp.length > 4) 
    			putData += "&takeCourseSeq=" + strTemp[4].split("=")[1];
    		
    		m_Company.setStartActivity(m_Activity, Company.eTAB_CLASSROOM
    				, putData
    				, false, Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
		}
		else if(tempUrl.contains("/sangsang/sangsanglink.aspx"))
		{
			switch(m_Company.getEduType())
			{
			case Company.eIMAGINE_CONTRACT_LEARNINGANDIMAGINE:
				m_Company.onChangeTab(m_Activity, Company.eTAB_SANGSANG_SELFSTUDY, m_TabId);
				break;
			case Company.eIMAGINE_CONTRACT_NONE:
			case Company.eIMAGINE_CONTRACT_ONLYLEARNING:
				m_Company.onChangeTab(m_Activity, Company.eTAB_SANGSANG, m_TabId);
				break;
			}
							
			return true;
		}
		else if(tempUrl.indexOf("/imgcategory/contents") > -1 || tempUrl.indexOf("/imgclassroom/series") > -1 || tempUrl.indexOf("/imgclassroom/contents") > -1)
		{	
			String nextParam = url.substring(url.indexOf("?") + 1, url.length());
			
			if(tempUrl.indexOf("childcategoryyn=y") > -1)
			{
				m_Company.setStartActivity(m_Activity, Company.eACTIVITY_LECTURE, String.format("TAB_ID=%d&NEXT_PARAM=%s", Company.eTAB_LECTURE, URLEncoder.encode(nextParam)), false, Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
			}
			else
			{			
				m_Company.setStartActivity(m_Activity, Company.eACTIVITY_LECTURE, String.format("TAB_ID=%d&NEXT_PARAM=%s&NEXT_URL=%s",  Company.eTAB_LECTURE, URLEncoder.encode(nextParam), URLEncoder.encode(url)), false, Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
			}
			
			return true;			
		}
		else if(tempUrl.contains("mlc.hunet.co.kr/lecture/category_inmun"))
		{
			m_Company.setStartActivity(m_Activity, Company.eACTIVITY_LECTURE_INMUN, String.format("TAB_ID=%d", Company.eTAB_LECTURE_INMUN), false, Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
			return true;
		}
		else if(tempUrl.contains("/lecture/category")) //과정별 카테고리 메뉴
    	{
			m_Company.setStartActivity(m_Activity, Company.eACTIVITY_LECTURE, String.format("TAB_ID=%d", Company.eTAB_LECTURE), false, Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
			return true;
    	}
    	else if(tempUrl.contains("mlc.hunet.co.kr/counsel/counsel_noticelist.aspx")) //공지사항 메뉴
    	{				    		
    		m_Company.setStartActivity(m_Activity, Company.eACTIVITY_NOTICE, String.format("SHOW_HEADER=Y&SHOW_TAB=Y&TAB_NUMBER=%d", Company.eTAB_NOTICE), false, 0);
    		return true;
    	}
    	else if(tempUrl.contains("mlc.hunet.co.kr/counsel/counsel_noticedetail.aspx"))
    	{
    		/*
    		String[] strTemp;
    		strTemp = url.substring(url.indexOf("?")+1).split("&");
    		int noticeSeq =  Integer.parseInt(strTemp[0].split("=")[1]);		    		
    		Intent intent = new Intent(HomeActivity.this, NoticeActivity.class);
    		intent.putExtra("type", "view");
    		intent.putExtra("noticeSeq", noticeSeq);
    		startActivity(intent);
    		*/
    	} else if (tempUrl.contains("/classroom/online")) {
    		m_Company.onChangeTab(m_Activity, Company.eTAB_CLASSROOM, m_TabId);
    		return true;
    	}
		else if (tempUrl.startsWith("intent:")) {
			int customUrlStartIndex = "intent://".length();
			int customUrlEndIndex = url.indexOf("#Intent;");
			if (customUrlEndIndex < 0) {
				return false;
			} else {
				int packageStartIndex = customUrlEndIndex + "#Intent;".length();
				int packageEndIndex = url.indexOf(";end");

				String[] intentInfo = url.substring(packageStartIndex, packageEndIndex < 0 ? url.length() : packageEndIndex).split(";");
				HashMap<String, String> intentParams = new HashMap<String, String>();
				for (String s : intentInfo) {
					String[] item = s.split("=");
					intentParams.put(item[0], item[1]);
				}

				try {
					String customUrl = url.substring(customUrlStartIndex, customUrlEndIndex);
					m_Activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(intentParams.get("scheme") + "://" + customUrl)));
				} catch (ActivityNotFoundException e) {
					m_Activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + intentParams.get("package"))));
				}
				return true;
			}
		}
		
		return false;
	}
	
	private void DoLogout() {
		LoginModel model 	= m_Activity.getSQLiteManager().GetLoginData(m_Company.LoginModel.id);
		model.auto_login 	= "0";
		model.loginYn		= "N";
		m_Activity.getSQLiteManager().SetLoginData(model);
		m_Company.LoginModel = null;
		
		Intent intent = new Intent(m_Activity, m_Company.getNextActivity(Company.eACTIVITY_LOGIN));
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		m_Activity.startActivity(intent);
		m_Activity.finish();
	}
}
