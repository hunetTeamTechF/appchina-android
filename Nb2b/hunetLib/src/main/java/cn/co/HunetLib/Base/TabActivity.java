package cn.co.HunetLib.Base;

import android.os.Bundle;
import android.widget.LinearLayout;

import cn.co.HunetLib.Control.TabControl;
import cn.co.HunetLib.Interface.ITabControlEventListener;

public abstract class TabActivity extends BaseActivity implements ITabControlEventListener
{
	protected TabControl m_TabControl;

	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
	    super.onCreate(savedInstanceState);	    
	}
	
	protected void CreateTabControl(LinearLayout tabContainer)
	{
		m_TabControl = getCompany().getTabControl();
		m_TabControl.setTabContainer(tabContainer);
	    m_TabControl.setEventListener(this);
	}
	
	protected void setSelectTab(int selectTab)
	{
		if(m_TabControl != null)
			m_TabControl.InitTab(selectTab);
	}
		
	public void onClickTab(int selectTab, int beforeSelectTab) 
	{
		if(selectTab == beforeSelectTab)
		{
			onFirstResume();
			return;
		}
		
		getCompany().onChangeTab(this, selectTab, beforeSelectTab);
	}
	
	@Override
	protected void onDestroy()
	{
		super.onDestroy();
		
		if(m_TabControl == null)
			return;
		
		m_TabControl.setTabContainer(null);
		m_TabControl.setEventListener(null);
		m_TabControl = null;
	}
}
