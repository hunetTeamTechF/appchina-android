package cn.co.HunetLib.Http;

import java.io.InputStream;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.params.HttpParams;

public class HunetHttpRequester 
{	
	public static HunetHttpData GetPostData(HunetHttpData data)
	{						
		try {
			HttpClient 		httpClient 		= new DefaultHttpClient();
			HttpParams 		httpParam 		= httpClient.getParams();
			httpParam.setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, 30000);
			httpParam.setParameter(CoreConnectionPNames.SO_TIMEOUT, 30000);
			
			HttpPost	 httpPost 	= new HttpPost(data.m_strUrl);
			StringEntity entity 	= new StringEntity(data.m_strParam);
			entity.setContentType("application/x-www-form-urlencoded");
			//UrlEncodedFormEntity entity = new UrlEncodedFormEntity(protocal.QueryStrList(), HTTP.UTF_8);
			httpPost.setEntity(entity);
			
			HttpResponse 	httpResponse 	= httpClient.execute(httpPost);
			InputStream 	inputStream 	= httpResponse.getEntity().getContent();
			
			data.SetDataInfo(httpClient, httpResponse, inputStream);
		}
		catch (Exception e)
		{
			data.SetDataInfo(null, null, null);
			//Log.i("hunet", "HunetHttpData Url : " + data.GetUrl() + "\nError : " + e.getMessage());
		}
		
		return data;
	}
}
