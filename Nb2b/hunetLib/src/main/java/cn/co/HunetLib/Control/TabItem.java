package cn.co.HunetLib.Control;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import cn.co.HunetLib.Base.BaseActivity;
public class TabItem {
	public LinearLayout tabLayout;
	private ImageView imageView;
	private TextView textView;
	
	private int icon_selected_color;
	private int text_selected_color;

	public TabItem(Context context, int icon_color, int text_color, int icon_selected_color, int text_selected_color) {

		int Dp7 = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 7, context.getResources().getDisplayMetrics())+10;

		this.icon_selected_color = icon_selected_color;
		this.text_selected_color = text_selected_color;
		
		// 탭 레이아웃 생성
		tabLayout = new LinearLayout(context);
		tabLayout.setOrientation(LinearLayout.VERTICAL);
		tabLayout.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.TOP);
		tabLayout.setLayoutParams(new LinearLayout.LayoutParams(0, LayoutParams.MATCH_PARENT, 1));
		tabLayout.setPadding(0, Dp7, 0, Dp7);

		// 이미지 뷰 생성
		imageView = new ImageView(context);
		imageView.setAdjustViewBounds(true);
		imageView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
		imageView.setColorFilter(new LightingColorFilter(0, icon_color));

		// 텍스트 뷰 생성
		textView = new TextView(context);
		textView.setGravity(Gravity.CENTER);
		textView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT, 1));

		if (context instanceof Activity) {
			DisplayMetrics dm = new DisplayMetrics();
			((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(dm);

			if (dm.xdpi <= 320) // 화면 가로의 인치당 실제 픽셀 수
				textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
			else
				textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
		}

		textView.setTextColor(text_color);


		int comSeq = BaseActivity.getCompany2().getSeq();
		if(comSeq==1011 || comSeq==1030 || comSeq==1033) {
			textView.setPadding(0,30,0,30);
		} else {
			tabLayout.addView(imageView);
		}
		tabLayout.addView(textView);


	}

	public void init(int resId, String text) {
		imageView.setImageResource(resId);
		textView.setText(text);
	}

	public void selected() {
		imageView.setColorFilter(new LightingColorFilter(0, icon_selected_color));
		textView.setTextColor(text_selected_color);
		if(BaseActivity.getCompany2().getSeq()==1011) {
			tabLayout.setBackgroundColor(Color.parseColor("#EF8970"));
		} else if (BaseActivity.getCompany2().getSeq()==1013 || BaseActivity.getCompany2().getSeq()==1015 || BaseActivity.getCompany2().getSeq()==1016 || BaseActivity.getCompany2().getSeq()==1017) {
			tabLayout.setBackgroundColor(Color.parseColor("#003b83"));
		} else if (BaseActivity.getCompany2().getSeq()==1030) {
			tabLayout.setBackgroundColor(Color.parseColor("#003B83"));
		} else if  (BaseActivity.getCompany2().getSeq()==1014) {

		} else if  (BaseActivity.getCompany2().getSeq()==1033) {
			tabLayout.setBackgroundColor(Color.parseColor("#2e6432"));
		} else {
			tabLayout.setBackgroundColor(Color.parseColor("#003b83"));
		}
	}
}
