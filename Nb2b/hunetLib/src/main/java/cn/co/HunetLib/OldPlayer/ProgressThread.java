package cn.co.HunetLib.OldPlayer;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import cn.co.HunetLib.R;
import cn.co.HunetLib.Common.AppMain;
import android.app.Activity;
import android.graphics.Color;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

/**
 * 강의관련 HTTP 통신. 플레이정보
 * 작업자 : 장형일
 * 작업일 : 2011.11
 * mlc 플레이어 인앱 수정. 2011.11.11 
 */
public class ProgressThread extends Thread {
	
	private VideoView videoView;
	private TextView txtTimePosition;
	private SeekBar seekBar;
	private LinearLayout vcontrol;
	private WebView wbHtml;
	
	private LinearLayout layoutListView;
	private LinearLayout layoutIndex;
	private ArrayList<MarkData> viewList;
	
	private LinearLayout layoutSound;
	private LinearLayout layoutMenu;
	
	private String course_cd = "";
	private int take_course_seq = 0;
	private int progress_no = 0;
	private int frame_no = 0;
	private String chapter_no = "";
	private String user_id = "";
	private int lastMarkNo = 0;
	private int studySec = 0;
	private int mobileStudySec = 0;
	boolean blPtMov = true;
	boolean blListMov = false;
	boolean bFinish = false;
	
	private int seekBarVisibleMark = 0;
	
	Activity act; //데이터 확인용 추가 변수.
	
	TranslateAnimation ani = null;
	Hunet_http_Contents_Cls content = new Hunet_http_Contents_Cls();
	MarkData view = new MarkData();
	
	String toastYn;
	
	private int playSec;  //학습시간(초), 마크값과 마크값 사이 초.
	private int prePlaySec;
	private int preWhat;
	private int skipWhat;
	
	private int WaitCnt;
	
	Handler mHandler = new Handler(){
		@Override
		public void handleMessage(android.os.Message msg){
			//Log.i("handlerMessage", "msg : " + msg.what);
			
			//네트워크 변경 여부 확인. 변경 되었으면. 종료
			if(AppMain.getNetworkChangeYn().equals("Y"))
			{
				try
				{
					if(toastYn.equals("N"))
					{
						toastYn = "Y";
						Toast.makeText(act.getBaseContext(), "네트워크가 전환되었습니다..", Toast.LENGTH_SHORT).show();
					}
				} catch (NullPointerException e) {
					toastYn = "N";
				}
				
				//act.finish();
			}
			else
			{
				CtlSeekBar(msg.what);  //프로그레스바 컨트롤
				CtlPT(msg.what);       //PT 컨트롤
			}
		};
	};
	
	public ProgressThread(VideoView videoView, TextView txtTimePosition, SeekBar seekBar, LinearLayout vcontrol, WebView wbHtml, LinearLayout layoutListView, LinearLayout layoutIndex, LinearLayout layoutSound, LinearLayout layoutMenu, ArrayList<MarkData> viewList
			, String course_cd, int take_course_seq, int frame_no, String chapter_no, int progress_no, String user_id, int lastMarkNo, int studysec, int mobileStudySec, boolean blPtMov, boolean blListMov, Activity act){
		
		this.videoView = videoView;
		this.txtTimePosition = txtTimePosition;
		this.seekBar = seekBar;
		this.vcontrol = vcontrol;
		this.wbHtml = wbHtml;
		this.layoutListView = layoutListView;
		this.layoutIndex = layoutIndex;
		this.layoutSound = layoutSound;
		this.layoutMenu = layoutMenu;
		this.viewList = viewList;
		this.course_cd = course_cd;
		this.take_course_seq = take_course_seq;
		this.progress_no = progress_no;
		this.frame_no = frame_no;
		this.chapter_no = chapter_no;
		this.user_id = user_id;
		this.lastMarkNo = lastMarkNo;
		this.mobileStudySec = mobileStudySec;
		this.blPtMov = blPtMov;
		this.blListMov = blListMov;
		
		this.act = act;
		
		toastYn = "N";
		
		this.studySec = studysec;
		this.mobileStudySec = mobileStudySec;
		this.playSec = 0;
		this.prePlaySec = 0;		
		this.preWhat = 0;
		this.skipWhat = 0;
		
		this.WaitCnt = 10;
	}
	
	public void Finish()
	{
		bFinish = true;
	}

	@Override
	public void run(){
		while(true)
		{
			if(bFinish)
				break;
			
			if(HunetNetworkMethod.connectAllConfirm(act.getBaseContext()) == false)
			{
				if(WaitCnt > 0)
				{
					videoView.pause();
					WaitCnt = WaitCnt - 1;
				}
				else
				{
					Toast.makeText(act.getBaseContext(), "네트워크가 불안정하여 재생을 종료합니다.", Toast.LENGTH_SHORT).show();
					act.finish();
				}
			}
			else
			{
				//현재 네트워크 기다리는 중 ~ 조건. 조건절 수정 말 것.
				if(!videoView.isPlaying() && WaitCnt < 10)
				{
					videoView.start();
				}
				if(WaitCnt < 10) WaitCnt = 10;
				
				mHandler.sendEmptyMessage(videoView.getCurrentPosition());
			}
			
			try
			{
				Thread.sleep(1000);
			}
			catch(InterruptedException e){
				e.printStackTrace();
			}
		}
	}
	
	/***
	 * SeekBar 컨트롤
	 * @param what
	 */
	protected void CtlSeekBar(int what)
	{
		txtTimePosition.setText(HunetCommonMethod.MarkToTimeString(what));
		
		seekBar.setProgress(what);
		if(vcontrol.getVisibility() == View.VISIBLE && seekBarVisibleMark == 0)
		{
			seekBarVisibleMark = what;
		}
		else if(vcontrol.getVisibility() == View.VISIBLE && what - seekBarVisibleMark >= 5000 && what - seekBarVisibleMark < 6000)
		{
			ani = new TranslateAnimation(0,0,0,100);
	        ani.setDuration(500);
	        vcontrol.setAnimation(ani);
			vcontrol.setVisibility(View.INVISIBLE);
			layoutSound.setVisibility(View.INVISIBLE);
			layoutMenu.setVisibility(View.INVISIBLE);
			
			seekBarVisibleMark = 0;
		}
		else if(vcontrol.getVisibility() == View.VISIBLE && what - seekBarVisibleMark > 10000)
		{
			seekBarVisibleMark = 0;
		}
	}
	
	/***
	 * PT컨트롤 및 진도 저장
	 * @param what
	 */
	protected void CtlPT(int what)
	{
		int index = 0;
		wbHtml.getSettings().setJavaScriptEnabled(true);
		
		if(what - preWhat >= 1000)
		{
			if(what - preWhat >= 10000)
			{
				skipWhat += (what - preWhat);
			}
			
			playSec = (what - skipWhat) / 1000;
			
			preWhat = what;	
		}
		else if(what - preWhat < 0)
		{
			preWhat = what;
			prePlaySec += playSec;
			skipWhat = what;
		}
		
		for(int i=viewList.size(); i > 0; i--)
		{
			view = viewList.get(i-1);
			
			if(what >= view.getMark_value())
			{
				view = viewList.get(i-1);
				index = i;
				break;
			}
			else
				view = new MarkData();
		}
		
		if(view.getMark_no() != lastMarkNo && view.getMark_value() > 0)
		{
			//진도저장
			int markNo = view.getMark_no();
			//HunetAlertMethod.AlertMessage(user_id + "/" + course_cd + "/" + String.valueOf(take_course_seq) + "/" + chapter_no + "/" + String.valueOf(progress_no) + "/" + String.valueOf(frame_no) + "/" + String.valueOf(markNo) + "/" + String.valueOf((studySec + playSec + prePlaySec)) + "/" + String.valueOf(mobileStudySec + playSec + prePlaySec), act, "A");
			content.ProgressUpdate(user_id, course_cd, take_course_seq, chapter_no, progress_no, frame_no, markNo, (studySec + playSec + prePlaySec), (mobileStudySec + playSec + prePlaySec));
			
			//PT호출
			if(blPtMov)
			{
				String strImg = "";
				try {
					strImg = URLEncoder.encode(view.getPpt_url(), "EUC_KR");
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				wbHtml.loadUrl("http://study.hunet.co.kr/mobile/mobilePtGate.aspx?imgPtUrl=" + strImg);
			}
			
			lastMarkNo = markNo;
			
			if(blPtMov) SetIndexSelect(index-1);
			if(blListMov) SetLayoutIndex(index-1);
		}
	}
	
	/***
	 * 화면의 목차부, 현재 영상의 목차에 선택값 부여
	 * @param i
	 */
	protected void SetIndexSelect(int i)
	{
		TextView tvIndex;
		
		for(int l=0; l < layoutListView.getChildCount(); l++)
		{
			tvIndex = (TextView) layoutListView.getChildAt(l);
			
			if(l == i)
			{
				tvIndex.setTextColor(Color.parseColor("#ff0000"));
			}
			else
				tvIndex.setTextColor(Color.parseColor("#000000"));
		}
	}
	
	//목차보기 레이아웃. 진도에 따른, 변경
	protected void SetLayoutIndex(int i)
	{
		ImageView ivIndex;
		
		for(int l=0; l < layoutIndex.getChildCount(); l++)
		{
			if(l == i)
			{
				ivIndex = (ImageView) ((LinearLayout)layoutIndex.getChildAt(l)).getChildAt(0);
				ivIndex.setBackgroundResource(R.drawable.icon_check_color);
			}
		}
	}
}
