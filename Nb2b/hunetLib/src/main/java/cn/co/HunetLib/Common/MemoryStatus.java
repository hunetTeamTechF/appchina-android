package cn.co.HunetLib.Common;

import java.io.File;

import android.os.Environment;
import android.os.StatFs;

public class MemoryStatus 
{
	private final int ERROR = -1;
	
	private boolean getExternalMemoryAvailable()
	{
		return android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
	}

	private long getAvailableInternalMemorySize()
	{
		File 	path 			= Environment.getDataDirectory();
		StatFs 	stat 			= new StatFs(path.getPath());
		long 	blockSize 		= stat.getBlockSize();
		long 	availableBlocks = stat.getAvailableBlocks();
		
		return availableBlocks * blockSize;
	}

	private long getTotalInternalMemorySize()
	{
		File 	path 		= Environment.getDataDirectory();
		StatFs 	stat 		= new StatFs(path.getPath());
		long 	blockSize 	= stat.getBlockSize();
		long 	totalBlocks = stat.getBlockCount();
		
		return totalBlocks * blockSize;
	}

	private long getAvailableExternalMemorySize()
	{
		if(getExternalMemoryAvailable() == false)
			return ERROR;
		
		File 	path 			= Environment.getExternalStorageDirectory();
		StatFs 	stat 			= new StatFs(path.getPath());
		long 	blockSize 		= stat.getBlockSize();
		long 	availableBlocks = stat.getAvailableBlocks();
		
		return availableBlocks * blockSize;
	}

	private long getTotalExternalMemorySize()
	{
		if(getExternalMemoryAvailable() == false)
			return ERROR;
		
		File 	path 			= Environment.getExternalStorageDirectory();
		StatFs 	stat 			= new StatFs(path.getPath());
		long 	blockSize 		= stat.getBlockSize();
		long 	totalBlocks 	= stat.getBlockCount();
		
		return totalBlocks * blockSize;
	}

	private String getFileSizeFormat(long size)
	{		
		double dbSize = size;
				
		if(dbSize <= 0)
			return String.format("%d%s", 0, "byte");
		
		if(dbSize < 1024)
			return String.format("%,.2f%s", dbSize, "bytes");
		
		dbSize /= 1024;
		
		if(dbSize < 1024)
			return String.format("%,.2f%s", dbSize, "KB");
		
		dbSize /= 1024;
		
		if(dbSize < 1024)
			return String.format("%,.2f%s", dbSize, "MB");
		
		dbSize /= 1024;
		
		if(dbSize < 1024)
			return String.format("%,.2f%s", dbSize, "GB");
		
		dbSize /= 1024;
		
		return String.format("%,.2f%s", dbSize, "TB");
	}
}
