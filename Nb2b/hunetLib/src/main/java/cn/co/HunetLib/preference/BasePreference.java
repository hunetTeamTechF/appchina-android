package cn.co.HunetLib.preference;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by zeple on 2015-10-12.
 */
public abstract class BasePreference {

    protected SharedPreferences pref;
    protected abstract String getPreferenceName();

    protected BasePreference(Context context) {
        pref = context.getSharedPreferences(getPreferenceName(), Context.MODE_PRIVATE);
    }
}
