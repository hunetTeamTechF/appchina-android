package cn.co.HunetLib;


import hunet.domain.DomainAddress;
import hunet.library.PlayerMessageUtility;

import java.util.Timer;
import java.util.TimerTask;

import cn.co.HunetLib.Common.AppMain;
import cn.co.HunetLib.Common.HunetWebChromeClient;
import cn.co.HunetLib.Http.AsyncHttpRequestData;
import cn.co.HunetLib.Http.HunetHttpData;
import cn.co.HunetLib.Http.IAsyncHttpRequestData;
import cn.co.HunetLib.R;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.KeyEvent;
import android.webkit.WebSettings.ZoomDensity;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class HtmlCourseActivity extends Activity implements IAsyncHttpRequestData {
	private WebView wvHtml;

	private int backButtonCount = 0;
	private int doubleBackClick = 0;
	Timer timer;
	private AsyncHttpRequestData asyncReqData = null;
	private final int playerInfo =1;
	String frameType;
	String playerUrl = "";
	String indexNo = "0";
	String totalFrame = "0";
	String currentFrame = "0";
	String courseCd = "";
	String takeCourseSeq  = "0";
	String progressNo   = "0";
	String lastViewSec    = "0";
	String userId = "0";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.html_course);
		TimerTask task = new TimerTask(){

			@Override
			public void run() {
				doubleBackClick++;
			}				
		};
		timer = new Timer();
		timer.schedule(task, 1000,1000);
		asyncReqData = new AsyncHttpRequestData();
		asyncReqData.SetEventListener(this);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if(timer != null){
			timer = null;			
		}
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(data != null && data.getBooleanExtra("completed", false) == true){
			wvHtml.loadUrl("javascript:window.main.fnMobileNext('"+frameType+"');");
		}
	}

	@SuppressLint("SetJavaScriptEnabled")
	@Override
	protected void onResume() {
		super.onResume();				
		if (wvHtml == null) {
			wvHtml = (WebView) findViewById(R.id.wvHtml);

			wvHtml.setHorizontalScrollBarEnabled(false);
			wvHtml.setVerticalScrollBarEnabled(false);
			wvHtml.getSettings().setJavaScriptEnabled(true);
			wvHtml.getSettings().setUseWideViewPort(true);
			wvHtml.getSettings().setLoadWithOverviewMode(true);
			wvHtml.getSettings().setSupportZoom(false);
			wvHtml.getSettings().setBuiltInZoomControls(false);
			wvHtml.getSettings().setDefaultZoom(ZoomDensity.FAR);

			Bundle bundle = getIntent().getExtras();
			String url = bundle.getString("url");
			if (url != null)
				wvHtml.loadUrl("http://" + url);

			wvHtml.setWebViewClient(new WebViewClient() {
				@Override
				public void onPageFinished(WebView view, String url) {
					super.onPageFinished(view, url);
				}

				@Override
				public void onPageStarted(WebView view, String url, Bitmap favicon) {
					super.onPageStarted(view, url, favicon);

					/*
					 * if(!url.contains(
					 * DomainAddress.getUrlMLC() + "/SangSang/SangSangLink.aspx") &&
					 * !url.contains("player://") &&
					 * !url.contains("http://hunet2.hscdn.com:1935/hunetvod") &&
					 * !url.contains("othersite://")) appMain.loading.show();
					 * 
					 * if(url.contains("My/MyCourse_Inmun.aspx") ||
					 * url.contains("My/MyChasiHome.aspx") ||
					 * url.contains("My/MyCourse_Type1") ||
					 * url.contains("Study/ChinaMiniCourse") ||
					 * url.contains("Study/ChinaCourse"))
					 * appMain.setCurrentUrl(url);
					 */
				}

				@Override
				public boolean shouldOverrideUrlLoading(WebView view, String url) {
					// 상상마루 이동

					if (url.contains("close://")) {
						finish();
						return true;
					} else if (url.contains("player://")) {
						return getUrlActionPlay(url);
					}

					return false;
				}
			});

			wvHtml.setWebChromeClient(new HunetWebChromeClient());
		}
	}

	private boolean getUrlActionPlay(final String url) {

		int network = PlayerMessageUtility.getNetworkSettings(this);
		String message = PlayerMessageUtility.getPlayMessage(this, network);

		switch (network) {
		case PlayerMessageUtility.NETWORK_WIFI:
			Play(url);
			break;
		case PlayerMessageUtility.NETWORK_OFFLINE:
			new AlertDialog.Builder(this)
			.setTitle("消息")
			.setMessage(message)
			.setPositiveButton("确认", null).show();
			break;
		case PlayerMessageUtility.NETWORK_ALARM_OFF_MOVIE_OFF:
			new AlertDialog.Builder(this)
			.setTitle("消息")
			.setMessage(message)
			.setPositiveButton("确认", null).show();
			break;
		case PlayerMessageUtility.NETWORK_ALARM_OFF_MOVIE_ON:
			Play(url);
			break;
		case PlayerMessageUtility.NETWORK_ALARM_ON_MOVIE_OFF:
			new AlertDialog.Builder(this)
			.setTitle("消息")
			.setMessage(message)
			.setPositiveButton("确认", null).show();
			break;
		case PlayerMessageUtility.NETWORK_ALARM_ON_MOVIE_ON:
			new AlertDialog.Builder(this)
			.setTitle("消息")
			.setMessage(message)
			.setPositiveButton("确认", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					Play(url);
				}
			}).show();
			break;
		}

//		int network = confirmMovNetwork();// 0 : wifi 연결,  1: 3G연결허용 및 알람비허용, 2:3G연결허용 및 알람허용,  3: 3G연결불가,  4: 네트워크 연결 안됨.
//		if(network == 0 || network == 1)
//		{
//			Play(url);
//		}
//		else if(network == 2)
//		{
//			new AlertDialog.Builder(HtmlCourseActivity.this)
//			.setTitle("알림")
//			.setMessage(getString(R.string.player_msg_data_confirm))
//			.setPositiveButton("확인", new DialogInterface.OnClickListener() {
//				@Override
//				public void onClick(DialogInterface dialog, int which) {
//					Play(url);
//				}
//			})
//			.setNegativeButton("취소", new DialogInterface.OnClickListener() {
//				@Override
//				public void onClick(DialogInterface dialog, int which) {
//				}
//			})
//			.show();
//		}
//		else if(network == 3)
//		{
//			new AlertDialog.Builder(HtmlCourseActivity.this)
//			.setTitle("알림")
//			.setMessage(getString(R.string.player_msg_data_limit))
//			.setPositiveButton("확인", new DialogInterface.OnClickListener() {
//				@Override
//				public void onClick(DialogInterface dialog, int which) {
//				}
//			})
//			.show();
//		}
//		else
//		{
//			new AlertDialog.Builder(HtmlCourseActivity.this)
//			.setTitle("알림")
//			.setMessage(getString(R.string.player_msg_none_network))
//			.setPositiveButton("확인", new DialogInterface.OnClickListener() {
//				@Override
//				public void onClick(DialogInterface dialog, int which) {
//				}
//			})
//			.show();
//		}

		return true;
	}

//	protected void Play(String url){
//		String temp = url.replace("player://", "");
//		String[] array = temp.split(";;");
//		playerUrl= array[0];
//		userId = AppMain.CompanyInfo.LoginModel.id;
//
//		int k = array.length;
//		if (array != null && array.length > 1) {
//			indexNo = array[1];
//			totalFrame = array[2];
//			currentFrame = array[3];
//			frameType = array[4];
//			courseCd = array[5];
//			takeCourseSeq = array[6];
//			progressNo = array[7];
//			lastViewSec = array[8];
//			String param = String.format("action=SelectLastViewSecByHtmlModule&takeCourseSeq=%s&courseCd=%s&progressNo=%s&frameNo=%s", takeCourseSeq,courseCd,progressNo,currentFrame);
//			asyncReqData.Request(playerInfo, DomainAddress.getUrlApps() + "/Hunet_Player/Android.aspx",param, null);
//		}
//
//	}

	private void Play(String url) {
		String temp = url.replace("player://", "");
		String[] array = temp.split(";;");
		playerUrl = array[0];
		userId = AppMain.CompanyInfo.LoginModel.id;

		indexNo = array[1];
		totalFrame = array[2];
		currentFrame = array[3];
		frameType = array[4];
		lastViewSec = "0";
		String[] GetSliceArrs = playerUrl.split("/");
		String[] arrays = playerUrl.split(GetSliceArrs[1]);
		String[] arrCourse = arrays[1].split(("_"));

		courseCd = (arrCourse[0] != "") ? arrCourse[0] : "";

		Intent intent = new Intent(HtmlCourseActivity.this, hunet.player.stwplayer.HunetStwPlayerActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
		intent.putExtra("indexNo", indexNo);
		intent.putExtra("currentFrame", currentFrame);
		intent.putExtra("totalFrame", totalFrame);
		intent.putExtra("lastViewSec", lastViewSec);
		intent.putExtra("progressRestrictionYN", "N");
		intent.putExtra("courseCd", "");
		intent.putExtra("takeCourseSeq", "");
		intent.putExtra("progressNo", "0");
		intent.putExtra("url", "http://" + playerUrl);
		intent.putExtra("playerType", "htmlWeb");
		intent.putExtra("userId", userId);
		startActivityForResult(intent, 0);
	}
//	private int confirmMovNetwork()
//	{
//		int rtnResult; // 0 : wifi 연결,  1: 3G연결허용,  2: 3G연결불가,  3: 네트워크 연결 안됨.
//		String[] rtnAccess;
//		Hunet_Sql_CRUD_HUNET_MLC_ACCESS_Cls AccessCls = new Hunet_Sql_CRUD_HUNET_MLC_ACCESS_Cls(this, AppMain.getId());
//
//		rtnAccess = AccessCls.Select_Access_Data();
//
//		AccessCls.CloseDB();
//
//		if(HunetNetworkMethod.connectWifiConfirm(this))
//		{
//			rtnResult = 0;
//		}
//		else if(HunetNetworkMethod.connect3GConfirm(this) || HunetNetworkMethod.connectWimaxConfirm(this))
//		{
//			if(!rtnAccess[2].equals("1"))
//				rtnResult = 3;
//			else
//			{
//				if(rtnAccess[1].equals("1"))
//					rtnResult = 2;
//				else
//					rtnResult = 1;
//			}
//		}
//		else
//			rtnResult = 4;
//
//		return rtnResult;
//	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		switch (keyCode) {
		case KeyEvent.KEYCODE_BACK:
			if(doubleBackClick > 2){
				doubleBackClick=0;
				Toast.makeText(this, "在按一次返回按钮会返回到学习首页", Toast.LENGTH_SHORT).show();
				return true;
			}
			else{
				finish();
				return true;
			} 
		default:
			return false;
		}
	}

	@Override
	public void AsyncRequestDataResult(HunetHttpData result) {
		switch(result.m_nId )
		{
		case playerInfo :
			if("YES".equals(result.GetJsonValue("IsSuccess")) == true)
			{
				lastViewSec = result.GetJsonValue("lastViewSec");
				Intent intent = new Intent(HtmlCourseActivity.this, hunet.player.stwplayer.HunetStwPlayerActivity.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
				intent.putExtra("indexNo", indexNo);
				intent.putExtra("currentFrame", currentFrame);
				intent.putExtra("totalFrame", totalFrame);
				intent.putExtra("lastViewSec", lastViewSec);
				intent.putExtra("progressRestrictionYN", result.GetJsonValue("progressRestrictionYN", "N"));
				intent.putExtra("courseCd", courseCd);
				intent.putExtra("takeCourseSeq", takeCourseSeq);
				intent.putExtra("progressNo", progressNo);
				intent.putExtra("url", "http://" + playerUrl);
				intent.putExtra("playerType", "htmlWeb");
				intent.putExtra("userId", userId);
				startActivityForResult(intent, 0);
			}
		}
	}
}

