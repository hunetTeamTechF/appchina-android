package cn.co.HunetLib;

import cn.co.HunetLib.Base.TabWebviewActivity;
import cn.co.HunetLib.Company.Company;
import cn.co.HunetLib.Interface.IWebViewEventListener;
import cn.co.HunetLib.R;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.KeyEvent;
import android.webkit.WebView;
import android.widget.LinearLayout;

public class HomeActivity extends TabWebviewActivity implements IWebViewEventListener
{	
	private long m_lastBackClickTime = 0;
	private boolean m_bClearHistory;
	
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
	    super.onCreate(savedInstanceState);
	    setWebViewEventListener(this);
	    m_WebView.clearCache(true);
	}
	
	@Override
	protected void CreateTabControl(LinearLayout tabContainer)
	{
		super.CreateTabControl(tabContainer);
		setSelectTab(Company.eTAB_HOME);
	}
	
	@Override
	protected void CheckNextUrl(Intent intent)
	{
		Bundle bundle 		= intent.getExtras();
		int 	tabIndex 	= this.getBundleValue(bundle, "TAB_INDEX", 0);
		String 	nextUrl 	= this.getBundleValue(bundle, "NEXT_URL", "");
		String 	indexDetail = this.getBundleValue(bundle, "INDEX_DETAIL", "");
		
		if(tabIndex == 0)
		{
			super.CheckNextUrl(intent);
			return;
		}
		
		int nextTabId = m_TabControl.GetChildTabId(tabIndex);
		
		if(nextTabId == -1)
		{
			super.CheckNextUrl(intent);
			return;
		}
		
		getCompany().onChangeTab(this, nextTabId, 0, nextUrl, indexDetail);
	}

	@Override
	protected void onFirstResume()
	{
		m_bClearHistory = true;
		
		super.onFirstResume();
	}
	
	@Override 
	public boolean onKeyDown(int keyCode, KeyEvent event) 
	{
		switch(keyCode)
		{
		case KeyEvent.KEYCODE_BACK:
			{	
				if(m_WebView != null && m_WebView.canGoBack())
				{
					m_WebView.goBack();
					return true;
				}
				
				long thisTime = System.currentTimeMillis();
				
				if(thisTime - m_lastBackClickTime <= 3000)
				{										
					Intent intent = new Intent(HomeActivity.this, getCompany().getNextActivity(Company.eACTIVITY_LOGIN));
					intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					intent.putExtra("KILL_APP", true);
					startActivity(intent);
					finish();
					return true;
				}				
				
				ShowToast(this.getString(R.string.app_msg_exit));
				m_lastBackClickTime = thisTime;
			}
			return true;
		}
		
		
		
		return false;
	}

	@Override
	public void OnPageFinished(WebView view, String url) 
	{
		if(m_bClearHistory == false)
			return;
		
		m_bClearHistory = false;
		view.clearHistory();
	}

	@Override
	public void OnPageStarted(WebView view, String url, Bitmap favicon) 
	{		
	}

	@Override
	public boolean OnShouldOverrideUrlLoading(WebView view, String url) 
	{
		return false;
	}

	@Override
	public void OnReceivedError(WebView view, int errorCode,
			String description, String failingUrl) 
	{		
	}
	
	@Override
    public void onDestroy() {		
        super.onDestroy();        
    } 
}
