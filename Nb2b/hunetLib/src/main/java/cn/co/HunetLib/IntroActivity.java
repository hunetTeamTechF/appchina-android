package cn.co.HunetLib;

import hunet.library.DownloadStudySyncUtility;
import hunet.library.StudySyncFailedUtility;

import java.util.Timer;
import java.util.TimerTask;

import cn.co.HunetLib.Base.BaseActivity;
import cn.co.HunetLib.R;

import android.content.Intent;
import android.os.Bundle;

public class IntroActivity extends BaseActivity 
{
//	private RelativeLayout m_IntroBackground;
	private Timer m_Timer;
	private TimerTask m_TimerTask;

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.intro_activity);
		
//		m_IntroBackground = (RelativeLayout)findViewById(R.id.introBackground);
//		m_IntroBackground.setBackgroundResource(R.drawable.background_intro);

		m_Timer 	= new Timer();
		m_TimerTask = new TimerTask()
		{
			@Override
			public void run()
			{
				NextActivity();
			}
		};		
	}
	
	@Override
	protected void onFirstResume()
	{
		
	}
		
	@Override
	protected void onResume()
	{
		super.onResume();
		m_Timer.schedule(m_TimerTask, 3000);
	}
	
	@Override
	protected void onPause()
	{
		super.onPause();
		finish();
	}
	
	@Override
	protected void onDestroy()
	{
		super.onDestroy();
		m_Timer.cancel();
	}
	
	private void NextActivity()
	{
		// 동기화 못한 학습 데이터 재 동기화
		StudySyncFailedUtility syncUtil = new StudySyncFailedUtility();
		syncUtil.doSendFailedProgressData(this);
		
		DownloadStudySyncUtility downsyncUtil = new DownloadStudySyncUtility();
		downsyncUtil.doSendFailedProgressData(this);
		
		Intent intent = new Intent(this, getCompany().getNextActivity(getCompany().eNEXT_ACTIVITY_INTRO));
        startActivity(intent);
        finish();
	}
}
