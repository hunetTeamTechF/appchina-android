package cn.co.HunetLib;

import cn.co.HunetLib.Base.TabWebviewActivity;
import cn.co.HunetLib.Company.Company;
import android.os.Bundle;

public class SelfStudyActivity extends TabWebviewActivity
{	
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
	    super.onCreate(savedInstanceState);
	    setSelectTab(Company.eTAB_SELFSTUDY);
	}
}
