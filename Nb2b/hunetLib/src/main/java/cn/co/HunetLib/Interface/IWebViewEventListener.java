package cn.co.HunetLib.Interface;

import android.graphics.Bitmap;
import android.webkit.WebView;

public interface IWebViewEventListener 
{
	public void OnPageFinished(WebView view, String url);
	public void OnPageStarted(WebView view, String url, Bitmap favicon);	
	public boolean OnShouldOverrideUrlLoading(WebView view, String url);
	public void OnReceivedError(WebView view, int errorCode, String description, String failingUrl);
}
