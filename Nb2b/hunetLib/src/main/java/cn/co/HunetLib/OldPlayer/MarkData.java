package cn.co.HunetLib.OldPlayer;

/**
 * 강의관련 HTTP 통신. 플레이정보
 * 작업자 : 장형일
 * 작업일 : 2011.11
 * mlc 플레이어 인앱 수정. 2011.11.11 
 */
public class MarkData {
	
	private int mark_no = 0;
	private String mark_nm = "";
	private int mark_value = 0;
	private String ppt_url = "";
	private int real_frame_no = 0;
	private String progress_yn = "";
	
	public int getMark_no() {
		return mark_no;
	}

	public void setMark_no(int mark_no) {
		this.mark_no = mark_no;
	}

	public String getMark_nm() {
		return mark_nm;
	}

	public void setMark_nm(String mark_nm) {
		this.mark_nm = mark_nm;
	}

	public int getMark_value() {
		return mark_value;
	}

	public void setMark_value(int mark_value) {
		this.mark_value = mark_value;
	}

	public String getPpt_url() {
		return ppt_url;
	}

	public void setPpt_url(String ppt_url) {
		this.ppt_url = ppt_url;
	}
	
	public void setReal_frame_no(int real_frame_no) {
		this.real_frame_no = real_frame_no;
	}

	public int getReal_frame_no() {
		return real_frame_no;
	}

	
	public void setProgress_yn(String progress_yn) {
		this.progress_yn = progress_yn;
	}

	public String getProgress_yn() {
		return progress_yn;
	}
	
	
	/***
	 * 생성자
	 */
	public MarkData()
	{
		
	}
	
	/***
	 * 생성자
	 * @param mark_no
	 * @param mark_nm
	 * @param mark_value
	 * @param ppt_url
	 */
	public MarkData(int mark_no, String mark_nm, int mark_value, String ppt_url, int real_frame_no, String progress_yn)
	{
		this.mark_no = mark_no;
		this.mark_nm = mark_nm;
		this.mark_value = mark_value;
		this.ppt_url = ppt_url;
		this.real_frame_no = real_frame_no;
		this.progress_yn = progress_yn;
	}


	
}
