package cn.co.HunetLib.Common;

import android.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by albert on 2017. 6. 27..
 */

public class EncryptUtility {

    private static final String KeyValue = "hunet906";

    public void sum(){
        System.out.println("aaa");
    }

    public String Dec(String text) {
        String result = text;
        try {
            result = Decrypt(text);
        } catch (Exception e) {}
        return result;
    }

    public String Enc(String text) {
        String result = text;
        try {
            result = Encrypt(text);
        } catch (Exception e) {}
        return result;
    }

    private String Decrypt(String text) throws Exception
    {

        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");

        byte[] keyBytes= new byte[16];

        byte[] b= KeyValue.getBytes("UTF-8");

        int len= b.length;

        if (len > keyBytes.length) len = keyBytes.length;

        System.arraycopy(b, 0, keyBytes, 0, len);

        SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");

        IvParameterSpec ivSpec = new IvParameterSpec(keyBytes);

        cipher.init(Cipher.DECRYPT_MODE,keySpec,ivSpec);

        byte [] results = cipher.doFinal(Base64.decode(text, 0));

        return new String(results ,"UTF-8");

    }

    private static String Encrypt(String text) throws Exception
    {

        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");

        byte[] keyBytes= new byte[16];

        byte[] b= KeyValue.getBytes("UTF-8");

        int len= b.length;

        if (len > keyBytes.length) len = keyBytes.length;

        System.arraycopy(b, 0, keyBytes, 0, len);

        SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");

        IvParameterSpec ivSpec = new IvParameterSpec(keyBytes);

        cipher.init(Cipher.ENCRYPT_MODE,keySpec,ivSpec);

        byte[] results = cipher.doFinal(text.getBytes("UTF-8"));

        return Base64.encodeToString(results, 0);

    }
}

