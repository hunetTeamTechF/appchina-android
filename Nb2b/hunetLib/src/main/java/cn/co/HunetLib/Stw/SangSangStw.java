package cn.co.HunetLib.Stw;

import hunet.data.SQLiteManager;
import hunet.drm.download.library.SangSangDrmDownload;
import hunet.drm.models.StudySangSangModel;
import hunet.drm.models.TakeSangSangModel;
import hunet.library.PlayerMessageUtility;
import hunet.net.AsyncHttpRequestData;
import cn.co.HunetLib.Common.AppMain;
import cn.co.HunetLib.OldPlayer.HunetPlayer;
import cn.co.HunetLib.OldPlayer.HunetSangSangVideoActivity;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Environment;
import android.webkit.WebView;
import android.widget.Toast;

public class SangSangStw 
{
	private ProgressDialog 			mLoadingDialog = null;
	private Activity 				mActivity;
	private WebView					mWebView;
	private String 					mActionNm 			= "";
	private SQLiteManager			sqlManager;
	private TakeSangSangModel 		takeSangSangModel;
	private StudySangSangModel     	studySangSangModel;	
	private ProgressDialog			progressDialog;
	private AsyncHttpRequestData 	asyncReqData;
	private final int 				downloadInfo		= 1; // 다운로드 정보 가져 오기
	private final int 				downloadCompleted 	= 2; // 다운로드 정보 저장하기
	private String 					goodsId 			= "";
	private String 					SeGoodsId 			= "";
	private int 					contentsSeq 		= 0;
	private int 					contractNo 			= 0;
	private String 					mCompanySeq			= "";
	private String 					mLoginId			= "";
	private String					mExternalDir		= ""; 					
	private boolean 				mIsFinish			= false;
	hunet.library.player player;
	SangSangDrmDownload downloader;

	public SangSangStw(Activity activity, WebView webView, int companySeq, String loginId)
	{
		this(activity, webView, companySeq, loginId, new SangSangDrmDownload(activity, webView, loginId, String.valueOf(companySeq)));
	}

	private SangSangStw(Activity activity, WebView webView, int companySeq, String loginId, SangSangDrmDownload downloader)
	{
		this.mActivity 			= activity;
		this.mWebView			= webView;
		this.sqlManager 			= new SQLiteManager(activity);
		this.takeSangSangModel 	= new TakeSangSangModel();
		this.studySangSangModel 	= new StudySangSangModel();
		this.asyncReqData 		= new AsyncHttpRequestData();		
		this.mExternalDir		= Environment.getExternalStorageDirectory() + "/hunet_mlc/";
		this.mCompanySeq			= String.valueOf(companySeq);
		this.mLoginId			= loginId;
		this.player = new hunet.library.player(activity, loginId);
		this.downloader = downloader;
	}

	private void ShowToast(String msg)
	{
		Toast.makeText(mActivity, msg, Toast.LENGTH_LONG).show();
	}

	private void ShowDialog(Context context, String msg, boolean cancelable)
	{
		if(mIsFinish || context == null || mLoadingDialog != null)
			return;

		mLoadingDialog = new ProgressDialog(context);
		mLoadingDialog.setMessage("".equals(msg) ? "加载中..." : msg);
		mLoadingDialog.setIndeterminate(true);
		mLoadingDialog.setCancelable(cancelable);
		mLoadingDialog.show();
	}

	private void HideDialog()
	{
		if(mLoadingDialog == null)
			return;

		try
		{
			mLoadingDialog.dismiss();
			mLoadingDialog = null;
		}
		catch(Exception e) { }
	}

	public void Destroy()
	{
		mActivity 	= null;
		mWebView 	= null;
		mIsFinish	= true;
	}

	public boolean HasSangSangProtocal(String url)
	{
		if(url.contains("sangsangplayer://"))
		{
			SangSangPlayer(url);
			return true;
		}
		else if(url.contains("sangsangdrmplayer://"))
		{
			player.ConfirmSangSangDrmPlayer(url, mCompanySeq);
			return true;
		}
		else if(url.contains("sangsangdownload://"))
		{
			//downloader.ConfirmDownload(url);
			downloader.DownloadPlay(url);
			return true;
		}
		else if(url.contains("sangsangdownloadplay://"))
		{
			downloader.DownloadPlay(url);
			return true;
		}
		else if(url.indexOf("player://") == 0)// || url.contains("http://hunet2.hscdn.com:1935/hunetvod"))
		{
			CheckNetwork(url);
			return true;
		}

		return false;
	}

	private void DoPlayer(String url)
	{
		Intent intent 	= null;
		mActionNm 		= "refresh";

//		if(url.contains("http://hunet2.hscdn.com:1935/hunetvod"))
//		{
//			intent = new Intent(mActivity, HunetPlayer.class);
//			intent.putExtra("url", url);
//			mActivity.startActivity(intent);
//		}
//		else if(CheckMp4File(url))
		if(CheckMp4File(url))
		{
			intent = new Intent(mActivity, hunet.player.stwplayer.HunetStwPlayerActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
			intent.putExtra("url", "http://" + url.replace("player://", ""));
			intent.putExtra("playerType", "htmlWeb");
			mActivity.startActivity(intent);
		}
		else
		{			
			intent = new Intent(mActivity, HunetPlayer.class);
			intent.putExtra("url", "");

			String[] strTemp;
			strTemp = url.replace("player://", "").split("/");

			AppMain.setCourse_cd(strTemp[0]);
			AppMain.setTake_course_seq(strTemp[1]);
			AppMain.setChapter_no(strTemp[2]);
			AppMain.setFrame_no(Integer.parseInt(strTemp[3]));
			mActivity.startActivity(intent);
		}
	}

	private boolean CheckMp4File(String url)
	{
		if(url.contains(".mp4") == false)
			return false;

		int pos = url.lastIndexOf( "." );

		if(pos == -1)
			return false;

		String fileExt = url.substring( pos + 1 ).toLowerCase();

		return "mp4".equals(fileExt);
	}

	private void CheckNetwork(final String url)
	{
		int network = PlayerMessageUtility.getNetworkSettings(mActivity);
		String message = PlayerMessageUtility.getPlayMessage(mActivity, network);

		switch (network) {
		case PlayerMessageUtility.NETWORK_WIFI:
			DoPlayer(url);
			break;
		case PlayerMessageUtility.NETWORK_OFFLINE:
			new AlertDialog.Builder(this.mActivity)
			.setTitle("消息")
			.setMessage(message)
			.setPositiveButton("确认", null).show();
			break;
		case PlayerMessageUtility.NETWORK_ALARM_OFF_MOVIE_OFF:
			new AlertDialog.Builder(this.mActivity)
			.setTitle("消息")
			.setMessage(message)
			.setPositiveButton("确认", null).show();
			break;
		case PlayerMessageUtility.NETWORK_ALARM_OFF_MOVIE_ON:
			DoPlayer(url);
			break;
		case PlayerMessageUtility.NETWORK_ALARM_ON_MOVIE_OFF:
			new AlertDialog.Builder(this.mActivity)
			.setTitle("消息")
			.setMessage(message)
			.setPositiveButton("确认", null).show();
			break;
		case PlayerMessageUtility.NETWORK_ALARM_ON_MOVIE_ON:
			new AlertDialog.Builder(this.mActivity)
			.setTitle("消息")
			.setMessage(message)
			.setPositiveButton("确认", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					DoPlayer(url);
				}
			}).show();
			break;
		}
		
//		int network = confirmMovNetwork();
//		if(network == 0 || network == 1)
//			DoPlayer(url);
//		else if(network == 2)
//		{
//			new AlertDialog.Builder(mActivity)
//			.setTitle("알림")
//			.setMessage(mActivity.getString(R.string.player_msg_data_confirm))
//			.setPositiveButton("승인", new DialogInterface.OnClickListener() {
//				@Override
//				public void onClick(DialogInterface dialog, int which) {
//					DoPlayer(url);
//				}
//			})
//			.setNegativeButton("취소", new DialogInterface.OnClickListener() {
//				@Override
//				public void onClick(DialogInterface dialog, int which) {
//					dialog.dismiss();
//				}
//			})
//			.show();
//		}
//		else if(network == 3)
//		{
//			new AlertDialog.Builder(mActivity)
//			.setTitle("알림")
//			.setMessage(mActivity.getString(R.string.player_msg_data_limit))
//			.setPositiveButton("확인", new DialogInterface.OnClickListener() {
//				@Override
//				public void onClick(DialogInterface dialog, int which) {
//					dialog.dismiss();
//				}
//			})
//			.show();
//		}
//		else
//		{
//			new AlertDialog.Builder(mActivity)
//			.setTitle("알림")
//			.setMessage(mActivity.getString(R.string.player_msg_none_network))
//			.setPositiveButton("확인", new DialogInterface.OnClickListener() {
//				@Override
//				public void onClick(DialogInterface dialog, int which) {
//					dialog.dismiss();
//				}
//			})
//			.show();
//		}
	}

//	private int confirmMovNetwork()
//	{
//		int rtnResult; // 0 : wifi 연결,  1: 3G연결허용,  2: 3G연결불가,  3: 네트워크 연결 안됨.
//		String[] rtnAccess;
//		Hunet_Sql_CRUD_HUNET_MLC_ACCESS_Cls AccessCls = new Hunet_Sql_CRUD_HUNET_MLC_ACCESS_Cls(mActivity, AppMain.getId());
//
//		rtnAccess = AccessCls.Select_Access_Data();
//
//		AccessCls.CloseDB();
//
//		if(HunetNetworkMethod.connectWifiConfirm(mActivity))
//		{
//			rtnResult = 0;
//		}
//		else if(HunetNetworkMethod.connect3GConfirm(mActivity) || HunetNetworkMethod.connectWimaxConfirm(mActivity))
//		{
//			if(!rtnAccess[2].equals("1"))
//				rtnResult = 3;
//			else
//			{
//				if(rtnAccess[1].equals("1"))
//					rtnResult = 2;
//				else
//					rtnResult = 1;
//			}
//		}
//		else
//			rtnResult = 4;
//
//		return rtnResult;
//	}

	public void CheckAction()
	{
		String actionNm = mActionNm;
		mActionNm		= "";


		if("refresh".equals(player.CheckAction()))
		{
			mWebView.reload();
			return;
		}

		if("".equals(actionNm))
			return;

		if("refresh".equals(actionNm))
		{
			mWebView.reload();
			return;
		}		

		mWebView.loadUrl(actionNm);
	}

	private void SangSangPlayer(String url)
	{
		String[] strTemp;
		strTemp = url.replace("sangsangplayer://", "").split("/");
		final Intent intent = new Intent(mActivity, HunetSangSangVideoActivity.class);
		intent.putExtra("contractNo", Integer.parseInt(strTemp[0]));
		intent.putExtra("contentsSeq", Integer.parseInt(strTemp[1]));
		intent.putExtra("goodsId", strTemp[2]);

		int network = PlayerMessageUtility.getNetworkSettings(mActivity);
		String message = PlayerMessageUtility.getPlayMessage(mActivity, network);

		switch (network) {
		case PlayerMessageUtility.NETWORK_WIFI:
			mActionNm = "refresh";
			mActivity.startActivity(intent);
			break;
		case PlayerMessageUtility.NETWORK_OFFLINE:
			new AlertDialog.Builder(this.mActivity)
			.setTitle("消息")
			.setMessage(message)
			.setPositiveButton("确认", null).show();
			break;
		case PlayerMessageUtility.NETWORK_ALARM_OFF_MOVIE_OFF:
			new AlertDialog.Builder(this.mActivity)
			.setTitle("消息")
			.setMessage(message)
			.setPositiveButton("确认", null).show();
			break;
		case PlayerMessageUtility.NETWORK_ALARM_OFF_MOVIE_ON:
			mActionNm = "refresh";
			mActivity.startActivity(intent);
			break;
		case PlayerMessageUtility.NETWORK_ALARM_ON_MOVIE_OFF:
			new AlertDialog.Builder(this.mActivity)
			.setTitle("消息")
			.setMessage(message)
			.setPositiveButton("确认", null).show();
			break;
		case PlayerMessageUtility.NETWORK_ALARM_ON_MOVIE_ON:
			new AlertDialog.Builder(this.mActivity)
			.setTitle("消息")
			.setMessage(message)
			.setPositiveButton("确认", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					mActionNm = "refresh";
					mActivity.startActivity(intent);
				}
			}).show();
			break;
		}
		
//		int network = confirmMovNetwork();// 0 : wifi 연결,  1: 3G연결허용 및 알람비허용, 2:3G연결허용 및 알람허용,  3: 3G연결불가,  4: 네트워크 연결 안됨.
//		if(network == 0 || network == 1)
//		{
//			mActionNm = "refresh";
//			mActivity.startActivity(intent);
//		}
//		else if(network == 2)
//		{
//			new AlertDialog.Builder(mActivity)
//			.setTitle("알림")
//			.setMessage(mActivity.getString(R.string.player_msg_data_confirm))
//			.setPositiveButton("확인", new DialogInterface.OnClickListener() {
//				@Override
//				public void onClick(DialogInterface dialog, int which) {
//					mActionNm = "refresh";
//					mActivity.startActivity(intent);
//				}
//			})
//			.setNegativeButton("취소", new DialogInterface.OnClickListener() {
//				@Override
//				public void onClick(DialogInterface dialog, int which) {
//				}
//			})
//			.show();
//		}
//		else if(network == 3)
//		{
//			new AlertDialog.Builder(mActivity)
//			.setTitle("알림")
//			.setMessage(mActivity.getString(R.string.player_msg_data_limit))
//			.setPositiveButton("확인", new DialogInterface.OnClickListener() {
//				@Override
//				public void onClick(DialogInterface dialog, int which) {
//				}
//			})
//			.show();
//		}
//		else
//		{
//			new AlertDialog.Builder(mActivity)
//			.setTitle("알림")
//			.setMessage(mActivity.getString(R.string.player_msg_none_network))
//			.setPositiveButton("확인", new DialogInterface.OnClickListener() {
//				@Override
//				public void onClick(DialogInterface dialog, int which) {
//				}
//			})
//			.show();
//		}
	}

	private void SangSangDrmPlayer(String url)
	{
		String[] strTemp;
		strTemp = url.replace("sangsangdrmplayer://", "").split("/");
		final Intent intent = new Intent(mActivity, hunet.player.stwplayer.HunetStwPlayerActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
		intent.putExtra("contractNo", Integer.parseInt(strTemp[0]));
		intent.putExtra("contentsSeq", Integer.parseInt(strTemp[1]));
		intent.putExtra("goodsId", strTemp[2]);
		intent.putExtra("userId", mLoginId);
		intent.putExtra("companySeq", mCompanySeq);
		intent.putExtra("playerType", "sangsangWeb");

		int network = PlayerMessageUtility.getNetworkSettings(mActivity);
		String message = PlayerMessageUtility.getPlayMessage(mActivity, network);

		switch (network) {
		case PlayerMessageUtility.NETWORK_WIFI:
			mActionNm = "refresh";
			mActivity.startActivity(intent);
			break;
		case PlayerMessageUtility.NETWORK_OFFLINE:
			new AlertDialog.Builder(this.mActivity)
			.setTitle("消息")
			.setMessage(message)
			.setPositiveButton("确认", null).show();
			break;
		case PlayerMessageUtility.NETWORK_ALARM_OFF_MOVIE_OFF:
			new AlertDialog.Builder(this.mActivity)
			.setTitle("消息")
			.setMessage(message)
			.setPositiveButton("确认", null).show();
			break;
		case PlayerMessageUtility.NETWORK_ALARM_OFF_MOVIE_ON:
			mActionNm = "refresh";
			mActivity.startActivity(intent);
			break;
		case PlayerMessageUtility.NETWORK_ALARM_ON_MOVIE_OFF:
			new AlertDialog.Builder(this.mActivity)
			.setTitle("消息")
			.setMessage(message)
			.setPositiveButton("确认", null).show();
			break;
		case PlayerMessageUtility.NETWORK_ALARM_ON_MOVIE_ON:
			new AlertDialog.Builder(this.mActivity)
			.setTitle("消息")
			.setMessage(message)
			.setPositiveButton("确认", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					mActionNm = "refresh";
					mActivity.startActivity(intent);
				}
			}).show();
			break;
		}
		
//		int network = confirmMovNetwork();// 0 : wifi 연결,  1: 3G연결허용 및 알람비허용, 2:3G연결허용 및 알람허용,  3: 3G연결불가,  4: 네트워크 연결 안됨.
//		if(network == 0 || network == 1)
//		{
//			mActionNm = "refresh";
//			mActivity.startActivity(intent);
//		}
//		else if(network == 2)
//		{
//			new AlertDialog.Builder(mActivity)
//			.setTitle("알림")
//			.setMessage(mActivity.getString(R.string.player_msg_data_confirm))
//			.setPositiveButton("확인", new DialogInterface.OnClickListener() {
//				@Override
//				public void onClick(DialogInterface dialog, int which) {
//					mActionNm = "refresh";
//					mActivity.startActivity(intent);
//				}
//			})
//			.setNegativeButton("취소", new DialogInterface.OnClickListener() {
//				@Override
//				public void onClick(DialogInterface dialog, int which) {
//				}
//			})
//			.show();
//		}
//		else if(network == 3)
//		{
//			new AlertDialog.Builder(mActivity)
//			.setTitle("알림")
//			.setMessage(mActivity.getString(R.string.player_msg_data_limit))
//			.setPositiveButton("확인", new DialogInterface.OnClickListener() {
//				@Override
//				public void onClick(DialogInterface dialog, int which) {
//				}
//			})
//			.show();
//		}
//		else
//		{
//			new AlertDialog.Builder(mActivity)
//			.setTitle("알림")
//			.setMessage(mActivity.getString(R.string.player_msg_none_network))
//			.setPositiveButton("확인", new DialogInterface.OnClickListener() {
//				@Override
//				public void onClick(DialogInterface dialog, int which) {
//				}
//			})
//			.show();
//		}
	}
}
