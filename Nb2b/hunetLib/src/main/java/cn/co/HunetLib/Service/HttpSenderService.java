package cn.co.HunetLib.Service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import cn.co.HunetLib.Common.HunetNetwork;
import cn.co.HunetLib.Http.HunetHttpRequester;
import cn.co.HunetLib.SQLite.AppEventModel;
import cn.co.HunetLib.SQLite.SQLiteManager;
import cn.co.HunetLib.Http.HunetHttpData;

public class HttpSenderService extends Service implements Runnable {
    private SQLiteManager m_SqlManager = null;
    private Thread m_HttpSender = null;
    private int m_State = 0;
    private int m_CheckNetworkCount = 0;
    private final int RUNNING = 0;
    private final int SUSPENDED = 1;
    private final int STOPPED = 2;

    @Override
    public void onCreate() {
        m_HttpSender = new Thread(this, "httpSenderService");
        m_SqlManager = new SQLiteManager(this);
        m_State = RUNNING;

        m_HttpSender.start();

        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    private synchronized int getState() {
        return m_State;
    }

    private synchronized void setState(int state) {
        m_State = state;

        if (m_State == RUNNING || m_State == STOPPED)
            notify();
    }

    private synchronized boolean checkState() {
        while (m_State == SUSPENDED) {
            try {
                wait();
            } catch (Exception e) {
            }
        }

        return m_State == STOPPED;
    }

    @Override
    public void run() {
        while (true) {
            if (checkState())
                break;

            if (HunetNetwork.GetCurrentUseNetwork(this) == 0) {
                if (m_CheckNetworkCount++ >= 12)
                    break;

                try {
                    Thread.sleep(10000);
                } catch (Exception e) {
                }

                continue;
            } else
                m_CheckNetworkCount = 0;

            AppEventModel model = m_SqlManager.GetAppEvent();

            if (model.event_seq == 0)
                break;

            if (model.try_count > 0) {
                try {
                    Thread.sleep(5000);
                } catch (Exception e) {
                }
            }

            String[] arUrl = model.url.split("\\?");
            String url = arUrl.length > 0 ? arUrl[0] : "";
            String param = arUrl.length > 1 ? arUrl[1] : "";

            HunetHttpData data = new HunetHttpData();
            data.m_nId = 1;
            data.m_strUrl = url;
            data.m_strParam = param;
            data.m_objTag = model;

            HunetHttpRequester.GetPostData(data);

            String isSuccess = data.GetJsonValue("IsSuccess", "None");
            String resultCode = data.GetJsonValue("ResultCode", "0");

            if ("YES".equalsIgnoreCase(isSuccess) || "1".equalsIgnoreCase(resultCode)) {
                m_SqlManager.DeleteAppEvent(model);
            } else {
                model.try_count++;

                if (model.try_count >= model.max_count)
                    m_SqlManager.DeleteAppEvent(model);
                else
                    m_SqlManager.UpdateAppEvent(model);
            }
        }

        stopSelf();
    }

    @Override
    public IBinder onBind(Intent intent) {

        return null;
    }

    @Override
    public void onDestroy() {
        setState(STOPPED);
        m_HttpSender = null;

        super.onDestroy();
    }
}
