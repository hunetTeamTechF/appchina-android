package cn.co.HunetLib;

import cn.co.HunetLib.Base.TabActivity;
import cn.co.HunetLib.Company.Company;
import cn.co.HunetLib.Interface.ITabControlEventListener;
import cn.co.HunetLib.SQLite.LoginModel;
import cn.co.HunetLib.SQLite.ScheduleModel;
import hunet.library.Utilities;
import cn.co.HunetLib.R;

import android.content.Intent;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MoreActivity extends TabActivity implements View.OnClickListener, ITabControlEventListener {

	private ImageView btn_logout;
	private TextView txt_schedule_type;
	private TextView txt_version;
	private TextView txt_title;

	private LinearLayout btn_notice;
	private LinearLayout btn_faq;
	private LinearLayout btn_qna;
	private LinearLayout btn_schedule;
	private LinearLayout btn_config;
	private LinearLayout btn_member;
	private LinearLayout btn_version;
	private LinearLayout btn_downcenter;
	private LinearLayout btn_hreminder;
	private LinearLayout btn_hstory;
	private LinearLayout btn_password;
	private LinearLayout btn_devoption;

	int TitleClickCount = 0;

	private FrameLayout	m_main_fl;

	private final int eMORE_CLICK_LOGOUT 		= 300;
	private final int eMORE_CLICK_NOTICE 		= 301;
	private final int eMORE_CLICK_QNA 			= 302;
	private final int eMORE_CLICK_MEMBERINFO 	= 303;
	private final int eMORE_CLICK_SCHEDULE 		= 304;
	private final int eMORE_CLICK_VERSION 		= 305;
	private final int eMORE_CLICK_DOWNCENTER 	= 306;
	private final int eMORE_CLICK_FAQ			= 307;
	private final int eMORE_CLICK_CONFIG		= 308;
	private final int eMORE_CLICK_HSTORY		= 309;
	private final int eMORE_CLICK_PASSWORD		= 310;
	private final int eMORE_CLICK_DEVOPTION		= 311;
	private final int eMORE_CLICK_TITLE			= 312;
	private final int eMORE_CLICK_HREMINDER		= 313;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.more_activity);

		Utilities.changeTitlebarColor(this, R.id.more_main_fl, R.id.header_text_color);

		ColorFilter filter = new LightingColorFilter(0x000000, getResources().getColor(R.color.activity_settings_arrow_color));

		btn_logout = (ImageView)findViewById(R.id.btn_logout);	    
		btn_notice = (LinearLayout) findViewById(R.id.btn_notice);
		btn_faq = (LinearLayout) findViewById(R.id.btn_faq);
		btn_qna = (LinearLayout) findViewById(R.id.btn_qna);
		btn_schedule = (LinearLayout) findViewById(R.id.btn_schedule);
		btn_config = (LinearLayout) findViewById(R.id.btn_config);
		btn_member = (LinearLayout) findViewById(R.id.btn_member);
		btn_version = (LinearLayout) findViewById(R.id.btn_version);
		btn_downcenter = (LinearLayout)findViewById(R.id.btn_downcenter);
		btn_hreminder = (LinearLayout)findViewById(R.id.btn_hreminder);
		btn_hstory = (LinearLayout)findViewById(R.id.btn_hstory);
		btn_password = (LinearLayout)findViewById(R.id.btn_password);
		btn_devoption = (LinearLayout)findViewById(R.id.btn_devoption);

		txt_schedule_type = (TextView) findViewById(R.id.txt_schedule_type);
		txt_version = (TextView) findViewById(R.id.txt_version);
		txt_title = (TextView) findViewById(R.id.header_text_color);
		
		((ImageView) findViewById(R.id.img_notice)).setColorFilter(filter);
		((ImageView) findViewById(R.id.img_faq)).setColorFilter(filter);
		((ImageView) findViewById(R.id.img_qna)).setColorFilter(filter);
		((ImageView) findViewById(R.id.img_schedule)).setColorFilter(filter);
		((ImageView) findViewById(R.id.img_config)).setColorFilter(filter);
		((ImageView) findViewById(R.id.img_member)).setColorFilter(filter);
		((ImageView) findViewById(R.id.img_version)).setColorFilter(filter);
		((ImageView) findViewById(R.id.img_downcenter)).setColorFilter(filter);
		((ImageView) findViewById(R.id.img_hreminder)).setColorFilter(filter);
		((ImageView) findViewById(R.id.img_hstory)).setColorFilter(filter);
		((ImageView) findViewById(R.id.img_password)).setColorFilter(filter);
		((ImageView) findViewById(R.id.img_devoption)).setColorFilter(filter);

		btn_logout.setOnClickListener(this); 	btn_logout.setTag(eMORE_CLICK_LOGOUT);
		btn_notice.setOnClickListener(this); 	btn_notice.setTag(eMORE_CLICK_NOTICE);
		btn_faq.setOnClickListener(this); 		btn_faq.setTag(eMORE_CLICK_FAQ);
		btn_qna.setOnClickListener(this); 		btn_qna.setTag(eMORE_CLICK_QNA);
		btn_schedule.setOnClickListener(this); 	btn_schedule.setTag(eMORE_CLICK_SCHEDULE);
		btn_config.setOnClickListener(this); 	btn_config.setTag(eMORE_CLICK_CONFIG);
		btn_member.setOnClickListener(this); 	btn_member.setTag(eMORE_CLICK_MEMBERINFO);
		btn_version.setOnClickListener(this); 	btn_version.setTag(eMORE_CLICK_VERSION);
		btn_downcenter.setOnClickListener(this); btn_downcenter.setTag(eMORE_CLICK_DOWNCENTER);
		btn_hreminder.setOnClickListener(this);  btn_hreminder.setTag(eMORE_CLICK_HREMINDER);
		btn_hstory.setOnClickListener(this);		btn_hstory.setTag(eMORE_CLICK_HSTORY);
		btn_password.setOnClickListener(this);	btn_password.setTag(eMORE_CLICK_PASSWORD);
		btn_devoption.setOnClickListener(this);  btn_devoption.setTag(eMORE_CLICK_DEVOPTION);
		txt_title.setOnClickListener(this); 		txt_title.setTag(eMORE_CLICK_TITLE);

		CreateTabControl((LinearLayout)findViewById(R.id.more_ll_more_tab));
		setSelectTab(Company.eTAB_MORE);

		if(getCompany().getViewModel(Company.eTAB_NOTICE) == null)
			btn_notice.setVisibility(View.GONE);

		if(getCompany().getViewModel(Company.eTAB_FAQ) == null)
			btn_faq.setVisibility(View.GONE);

		if(getCompany().getViewModel(Company.eTAB_QNA) == null)
			btn_qna.setVisibility(View.GONE);

		if(getCompany().getViewModel(Company.eTAB_SCHEDULE) == null)
			btn_schedule.setVisibility(View.GONE);

		if(getCompany().getViewModel(Company.eTAB_CONFIG) == null)
			btn_config.setVisibility(View.GONE);

		if(getCompany().getViewModel(Company.eTAB_MEMBERINFO) == null)
			btn_member.setVisibility(View.GONE);

		if(getCompany().getViewModel(Company.eTAB_VERSION) == null)
			btn_version.setVisibility(View.GONE);

		if(getCompany().getViewModel(Company.eTAB_DOWNLOAD_CENTER) == null || getCompany().getDrmUse() == false)
			btn_downcenter.setVisibility(View.GONE);

		if(getCompany().getViewModel(Company.eTAB_PASSWORD) == null)
			btn_password.setVisibility(View.GONE);

//		btn_hreminder.setVisibility("1".equals(getCompany().getHReminderUseType()) ? View.VISIBLE : View.GONE);
//		btn_hstory.setVisibility("1".equals(getCompany().getHStoryUseType()) ? View.VISIBLE : View.GONE);
		btn_hreminder.setVisibility(View.GONE);
		btn_hstory.setVisibility(View.GONE);
		initIntent(this.getIntent());
	}

	@Override 
	protected void onNewIntent(Intent intent)
	{
		super.onNewIntent(intent);
		initIntent(intent);
	}

	private void initIntent(Intent intent)
	{
		Bundle 	bundle 		= intent.getExtras();
		String 	nextUrl 	= this.getBundleValue(bundle, "NEXT_URL", "");
		String 	indexDetail = this.getBundleValue(bundle, "INDEX_DETAIL", "");

		if("".equals(indexDetail))
			return;

		if("공지사항".equals(indexDetail))
			getCompany().setStartActivity(this, Company.eACTIVITY_NOTICE, String.format("SHOW_HEADER=Y&SHOW_TAB=Y&TAB_NUMBER=%d&NEXT_URL=%s", Company.eTAB_NOTICE, nextUrl), false, 0);
		else if("H-스토리".equals(indexDetail))
			getCompany().setStartActivity(this, Company.eACTIVITY_HSTORY, String.format("SHOW_HEADER=N&SHOW_TAB=N&TAB_NUMBER=%d&NEXT_URL=%s", Company.eTAB_HSTORY, nextUrl), false, 0);
		else if("H-리마인더".equals(indexDetail))
			getCompany().setStartActivity(this, Company.eACTIVITY_HREMINDER, String.format("SHOW_HEADER=N&SHOW_TAB=N&TAB_NUMBER=%d&NEXT_URL=%s", Company.eTAB_HREMINDER, nextUrl), false, 0);
	}

	@Override
	protected void onFirstResume() 
	{

	}	

	@Override
	protected void onResume() 
	{
		super.onResume();
		TitleClickCount = 0;

		if(getCompany().ShowDevOption)
			btn_devoption.setVisibility(View.VISIBLE);		

		ScheduleModel schedule = getSQLiteManager().GetScheduleData();
		txt_schedule_type.setText("1".equals(schedule.use) ? "사용" : "사용안함");
		txt_version.setText(getPackageVersion());
	}	

	@Override
	public void onClick(View view) 
	{
		int tagId = (Integer)view.getTag();

		Intent intent = null;

		switch(tagId)
		{
		case eMORE_CLICK_NOTICE: 		getCompany().setStartActivity(this, Company.eACTIVITY_NOTICE, String.format("SHOW_HEADER=Y&SHOW_TAB=Y&TAB_NUMBER=%d", Company.eTAB_NOTICE), false, 0); break;
		case eMORE_CLICK_QNA: 			getCompany().setStartActivity(this, Company.eACTIVITY_QNA, String.format("SHOW_HEADER=Y&SHOW_TAB=Y&TAB_NUMBER=%d", Company.eTAB_QNA), false, 0); break;
		case eMORE_CLICK_MEMBERINFO: 	getCompany().setStartActivity(this, Company.eACTIVITY_MEMBERINFO, String.format("SHOW_HEADER=Y&SHOW_TAB=Y&TAB_NUMBER=%d", Company.eTAB_MEMBERINFO), false, 0); break;
		case eMORE_CLICK_FAQ: 			getCompany().setStartActivity(this, Company.eACTIVITY_FAQ, String.format("SHOW_HEADER=Y&SHOW_TAB=Y&TAB_NUMBER=%d", Company.eTAB_FAQ), false, 0); break;
		case eMORE_CLICK_SCHEDULE: 		getCompany().setStartActivity(this, Company.eACTIVITY_SCHEDULE, "", false, 0); break;
		case eMORE_CLICK_VERSION:		getCompany().setStartActivity(this, Company.eACTIVITY_VERSION, "", false, 0); break;
		case eMORE_CLICK_DOWNCENTER:	getCompany().setStartActivity(this, Company.eACTIVITY_DOWNLOAD_CENTER, "", false, 0); break;
		case eMORE_CLICK_CONFIG: 		getCompany().setStartActivity(this, Company.eACTIVITY_CONFIG, "", false, 0); break;
		case eMORE_CLICK_HREMINDER:		getCompany().setStartActivity(this, Company.eACTIVITY_HREMINDER, String.format("SHOW_HEADER=N&SHOW_TAB=N&TAB_NUMBER=%d", Company.eTAB_HREMINDER), false, 0); break; 
		case eMORE_CLICK_HSTORY:		getCompany().setStartActivity(this, Company.eACTIVITY_HSTORY, String.format("SHOW_HEADER=N&SHOW_TAB=N&TAB_NUMBER=%d", Company.eTAB_HSTORY), false, 0); break;
		case eMORE_CLICK_PASSWORD:		getCompany().setStartActivity(this, Company.eACTIVITY_PASSWORD, String.format("SHOW_HEADER=Y&SHOW_TAB=N&TAB_NUMBER=%d", Company.eTAB_PASSWORD), false, 0); break;
		case eMORE_CLICK_DEVOPTION: 	getCompany().setStartActivity(this, Company.eACTIVITY_DEVOPTION, "", false, 0); break;
		case eMORE_CLICK_TITLE:
		{
			if(btn_devoption.getVisibility() == View.VISIBLE)
				break;

			if(TitleClickCount < 10)
			{
				TitleClickCount++;
				break;
			}

			getCompany().ShowDevOption = true;
			btn_devoption.setVisibility(View.VISIBLE);

			this.ShowToast("개발자 옵션을 사용하실 수 있습니다.");
		}
		break;
		case eMORE_CLICK_LOGOUT:
			LoginModel model 	= getSQLiteManager().GetLoginData(getCompany().LoginModel.id);
			model.auto_login 	= "0";
			model.loginYn		= "N";
			getSQLiteManager().SetLoginData(model);
			getCompany().LoginModel = null;

			Company initLogin = new Company() {
				@Override
				public void SetFirstCheckX() {
					super.SetFirstCheckX();
				}
			};
			initLogin.FirstCheck = "x";

			intent = new Intent(MoreActivity.this, getCompany().getNextActivity(Company.eACTIVITY_LOGIN));
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			finish();
			break;
		}
	}


}
