package cn.co.HunetLib.Common;

import android.content.Context;
import android.os.Build;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;

public class DeviceUtility 
{
	private Context 			m_Context;
	private TelephonyManager 	m_TelManager;
	
	public DeviceUtility(Context context)
	{		
		m_Context 		= context;
		m_TelManager 	= (TelephonyManager)m_Context.getSystemService(Context.TELEPHONY_SERVICE);
	}
		
	public String getModel()
	{
		return Build.MODEL;
	}
	
	public String getDeviceId()
	{
		return m_TelManager.getDeviceId();
	}
	
	public String getAndroidId()
	{
		return Secure.getString(m_Context.getContentResolver(), Secure.ANDROID_ID);
	}

	@Override
	public String toString()
	{
		String info = String.format("MODEL : %s\nVERSION : %s", Build.MODEL, Build.VERSION.RELEASE);	
		
		return info;
	}
	
	public String getDeviceInfo()
	{
		return String.format("%s %s %s", getModel(), getDeviceId(), getAndroidId());
	}	
}
