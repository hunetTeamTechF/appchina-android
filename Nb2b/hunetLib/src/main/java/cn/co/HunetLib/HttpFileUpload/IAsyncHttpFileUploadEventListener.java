package cn.co.HunetLib.HttpFileUpload;

import java.util.List;

public interface IAsyncHttpFileUploadEventListener 
{
	public void OnFileUploadComplete(HttpFileInfo info);
	public void OnJobComplete(List<HttpFileInfo> infos);
}
