package cn.co.HunetLib;

import cn.co.HunetLib.Base.BaseActivity;
import cn.co.HunetLib.Common.HunetWebChromeClient;
import cn.co.HunetLib.Common.UrlAction;
import hunet.library.PlayerMessageUtility;
import cn.co.HunetLib.R;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class HtmlLanguageCourseActivity extends BaseActivity {
	private WebView wvHtml;
	private UrlAction m_UrlAction;
	private String firstUrl;
	private final int mp3 = 1;
	private final int mov = 2;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.html_language_course);
		wvHtml = (WebView) findViewById(R.id.wvHtml);

		InitWebView(wvHtml);
		getIntent(getIntent());

		m_UrlAction = getCompany().getUrlAction();
		m_UrlAction.Init(this, wvHtml);

//		Bundle bundle = getIntent().getExtras();
	}
	
	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		getIntent(intent); 
	}
	
	private void getIntent(Intent intent) {
		// 가로화면 요청시 가로화면으로 전환
		String tmpOrientation = getBundleValue(intent.getExtras(), "orientation", "");
		
		if (tmpOrientation != null) {
			if ("L".equalsIgnoreCase(tmpOrientation)) {
				setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
				wvHtml.getSettings().setLoadWithOverviewMode(true);
				wvHtml.getSettings().setUseWideViewPort(true);
			}
		}
		
		firstUrl = getBundleValue(intent.getExtras(), "url", firstUrl);
		wvHtml.loadUrl("http://" + firstUrl);
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// wvHtml.reload();
		wvHtml.loadUrl("javascript:window.main.fnMobileNext();");
		// wvHtml.loadUrl("javascript: $(document).ready(function () {alert('test');});");
	}

	@SuppressLint("SetJavaScriptEnabled")
	private void InitWebView(WebView webview)
	{	
		WebSettings setting = webview.getSettings();
		setting.setUserAgentString(setting.getUserAgentString() + " HunetAndroid ");
		setting.setJavaScriptEnabled(true);
		webview.setHorizontalScrollBarEnabled(false);
		webview.setVerticalScrollBarEnabled(false);
		webview.clearCache(true);

		webview.setWebChromeClient(new HunetWebChromeClient());
		webview.setWebViewClient(new WebViewClient() 
		{
			@Override
			public void onPageStarted(WebView view, String url, Bitmap favicon) 
			{
				ShowDialog(HtmlLanguageCourseActivity.this, "", true);
				super.onPageStarted(view, url, favicon);
			}

			@Override
			public void onPageFinished(WebView view, String url) 
			{
				HideDialog();
				super.onPageFinished(view, url);
			}

			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) 
			{					
				String tempUrl = url.toLowerCase();

				if (url.contains("mp3player://")) {
					//view.goBack();
					ConfirmPlayer(url,mp3);	
					return true;
				} else if (url.contains("htmlplayer://")) {
					//view.goBack();
					ConfirmPlayer(url,mov);
					return true;
				} else if (url.contains("close://")) {
					finish();
				}

				if(m_UrlAction != null && m_UrlAction.CheckUrl(url))
					return true;


				return false;
			}

			@Override
			public void onReceivedError(WebView view, int errorCode, String description, String failingUrl)
			{					

			}
		});
	}

	private void Play(final String url, final int type) {
		if (type == mp3) {
			String temp = url.replace("mp3player://", "");
			Intent intent = new Intent(Intent.ACTION_VIEW);
			intent.setDataAndType(Uri.parse("http://" + temp), "audio/*");
			startActivity(intent);
		} else {
			Intent intent = new Intent(HtmlLanguageCourseActivity.this, hunet.player.stwplayer.HunetStwPlayerActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
			intent.putExtra("url", "http://" + url.replace("htmlplayer://", ""));
			intent.putExtra("playerType", "htmlWeb");
			intent.putExtra("htmlLanguageType", true);
			startActivity(intent);
		}
	}

	private void ConfirmPlayer(final String url, final int type) {
		int network = PlayerMessageUtility.getNetworkSettings(this);
		String message = PlayerMessageUtility.getPlayMessage(this, network);

		switch (network) {
		case PlayerMessageUtility.NETWORK_WIFI:
			Play(url,type);
			break;
		case PlayerMessageUtility.NETWORK_OFFLINE:
			new AlertDialog.Builder(this)
			.setTitle("消息")
			.setMessage(message)
			.setPositiveButton("确认", null).show();
			break;
		case PlayerMessageUtility.NETWORK_ALARM_OFF_MOVIE_OFF:
			new AlertDialog.Builder(this)
			.setTitle("消息")
			.setMessage(message)
			.setPositiveButton("确认", null).show();
			break;
		case PlayerMessageUtility.NETWORK_ALARM_OFF_MOVIE_ON:
			Play(url,type);
			break;
		case PlayerMessageUtility.NETWORK_ALARM_ON_MOVIE_OFF:
			new AlertDialog.Builder(this)
			.setTitle("消息")
			.setMessage(message)
			.setPositiveButton("确认", null).show();
			break;
		case PlayerMessageUtility.NETWORK_ALARM_ON_MOVIE_ON:
			new AlertDialog.Builder(this)
			.setTitle("消息")
			.setMessage(message)
			.setPositiveButton("确认", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					Play(url,type);
				}
			}).show();
			break;
		}

//		int network = HunetNetwork.GetCurrentUseNetwork(this);
//		if (network == 0 || network == 1)
//			Play(url,type);
//		else if (network == 2) {
//			new AlertDialog.Builder(HtmlLanguageCourseActivity.this)
//			.setTitle("알림")
//			.setMessage(this.getString(R.string.player_msg_data_confirm))
//			.setPositiveButton("승인", new DialogInterface.OnClickListener() {
//				@Override
//				public void onClick(DialogInterface dialog, int which) {
//					Play(url,type);
//				}
//			})
//			.setNegativeButton("취소", new DialogInterface.OnClickListener() {
//				@Override
//				public void onClick(DialogInterface dialog, int which) {
//					dialog.dismiss();
//				}
//			}).show();
//		} else if (network == 3) {
//			new AlertDialog.Builder(HtmlLanguageCourseActivity.this)
//			.setTitle("알림")
//			.setMessage(this.getString(R.string.player_msg_data_limit))
//			.setPositiveButton("확인", new DialogInterface.OnClickListener() {
//				@Override
//				public void onClick(DialogInterface dialog, int which) {
//					dialog.dismiss();
//				}
//			}).show();
//		} else {
//			new AlertDialog.Builder(HtmlLanguageCourseActivity.this)
//			.setTitle("알림")
//			.setMessage(this.getString(R.string.player_msg_none_network))
//			.setPositiveButton("확인", new DialogInterface.OnClickListener() {
//				@Override
//				public void onClick(DialogInterface dialog, int which) {
//					dialog.dismiss();
//				}
//			}).show();
//		}
	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		switch (keyCode) {
		case KeyEvent.KEYCODE_BACK:
			if (wvHtml != null && wvHtml.canGoBack()) {
				wvHtml.goBack();
				return true;
			} else {
				finish();
				return false;
			}
		default:
			return false;
		}
	}

	@Override
	protected void onFirstResume() 
	{
		
	}	
}
