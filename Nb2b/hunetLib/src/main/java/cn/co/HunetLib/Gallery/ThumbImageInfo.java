package cn.co.HunetLib.Gallery;

public class ThumbImageInfo 
{
	private String id;
	private String data;
	private boolean checkdState;
	
	public String getId()
	{
		return id;
	}
	
	public void setId(String id)
	{
		this.id = id;
	}
	
	public String getData()
	{
		return data;
	}
	
	public void setData(String data)
	{
		this.data = data;
	}
	
	public boolean getCheckedState()
	{
		return checkdState;
	}
	
	public void setCheckedState(boolean checkdState)
	{
		this.checkdState = checkdState;
	}
}
