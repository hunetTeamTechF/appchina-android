package cn.co.HunetLib.OldPlayer;

import cn.co.HunetLib.Common.AppMain;
import hunet.domain.DomainAddress;

import java.io.InputStream;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Handler;


/**
 * 강의관련 HTTP 통신. 플레이정보
 * 작업자 : 장형일
 * 작업일 : 2011.11
 * mlc 플레이어 인앱 수정. 2011.11.11 
 */
public class Hunet_http_Contents_Cls extends Hunet_Http_Post_Cls {
	
	InputStream instream = null;
	Handler mHandler =  null;
	/*
	 * 기본 생성자.
	 */
	public Hunet_http_Contents_Cls() {
		
	}
	
	/***
	 * 컨텐츠 상세정보 조회
	 * @param course_cd
	 * @param chapter_no
	 * @param contents_no
	 * @return
	 * 
	 * 과목타입이 모바일 일 시. course_type = 3
	 */
	public ContentsData StudyContentView(String course_cd, String chapter_no, int contents_no)
	{
		ContentsData data = new ContentsData();
		JSONObject jobj = null;
		
		setUrl(DomainAddress.getUrlApps() + "/Hunet_Player/Android.aspx");
		setBlPlayer(true);
		setType("ContentDetailView");
		setParam("course_cd=" + course_cd + "&chapter_no=" + chapter_no + "&contents_no=" + String.valueOf(contents_no));
		InputStream instream = hunetHttpPostData();
		if(instream==null)
			return null;
		//Json데이터 변환
		jobj = HunetJsonMethod.convertJsonObject(instream);
		
		//ContentsData 데이터 변환
		try {
			if(jobj.getString("IsSuccess").equals("YES"))
			{
				data.setContents_url(jobj.getString("contents_url"));
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return data;
	}
	
	/***
	 * 컨텐츠 상세정보 조회
	 * @param course_cd
	 * @param chapter_no
	 * @param
	 * @return
	 * 
	 * 과목타입이 온라인 일 시. course_type = 1
	 */
	public ContentsData StudyFrameView(String course_cd, String chapter_no, int frame_no)
	{
		ContentsData data = new ContentsData();
		JSONObject jobj = null;
		
		setUrl(DomainAddress.getUrlApps() + "/Hunet_Player/Android.aspx");
		setBlPlayer(true);
		setType("FrameDetailView");
		setParam("course_cd=" + course_cd + "&chapter_no=" + chapter_no + "&frame_no=" + String.valueOf(frame_no));
		InputStream instream = hunetHttpPostData();
		if(instream==null)
			return null;
		//Json데이터 변환
		jobj = HunetJsonMethod.convertJsonObject(instream);
		
		//ContentsData 데이터 변환
		try {
			if(jobj.getString("IsSuccess").equals("YES"))
			{
				data.setMobile_mov_url(jobj.getString("contents_url"));
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return data;
	}
	
	/***
	 * 목차 리스트 조회
	 * @param course_cd
	 * @param chapter_no
	 * @param frame_no
	 * @return
	 */
	public ArrayList<MarkData> MarkList(String course_cd, String chapter_no, int frame_no, int take_course_seq, Activity activity)
	{
		ArrayList<MarkData> dataList = new ArrayList<MarkData>();
		JSONObject jobj = null;
		JSONArray listJobj = null;
		
		setUrl(DomainAddress.getUrlApps() + "/Hunet_Player/Android.aspx");
		setBlPlayer(true);
		setType("MarkList");
		setParam("course_cd=" + course_cd + "&chapter_no=" + chapter_no + "&frame_no=" + String.valueOf(frame_no) + "&take_course_seq=" + String.valueOf(take_course_seq));
		

		instream = hunetHttpPostData();
		if(instream==null)
			return null;
		//Json데이터 변환
		jobj = HunetJsonMethod.convertJsonObject(instream);
		
		//ContentsData 데이터 변환
		try {
			if(jobj.getString("IsSuccess").equals("YES"))
			{
				listJobj = jobj.getJSONArray("List");
				
				for(int i=0; i < listJobj.length(); i++)
				{
					MarkData data = new MarkData();
					JSONObject jb = listJobj.getJSONObject(i);
					
					data.setMark_no(jb.getInt("mark_no"));
					data.setMark_nm(jb.getString("mark_nm"));
					data.setMark_value(jb.getInt("mark_value"));
					data.setPpt_url(jb.getString("ppt_url"));
					data.setReal_frame_no(jb.getInt("real_frame_no"));
					data.setProgress_yn(jb.getString("progress_yn"));
					
					dataList.add(data);
				}
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return dataList;
	}
	
	/***
	 * 동영상 이어보기 정보
	 * @param course_cd
	 * @param
	 * @param progress_no
	 * @param frame_no
	 * @return
	 */
	public int MovProgress(String course_cd, int take_course_seq, int progress_no, int frame_no)
	{
		int last_mark_no = 0;
		JSONObject jobj = null;
		
		setUrl(DomainAddress.getUrlApps() + "/Hunet_Player/Android.aspx");
		setBlPlayer(true);
		setType("MovProgress");
		setParam("course_cd=" + course_cd + "&take_course_seq=" + String.valueOf(take_course_seq) + "&progress_no=" + String.valueOf(progress_no) + "&frame_no=" + String.valueOf(frame_no));
		InputStream instream = hunetHttpPostData();
		if(instream==null)
			return 0;
		//Json데이터 변환
		jobj = HunetJsonMethod.convertJsonObject(instream);
		
		//ContentsData 데이터 변환
		try {
			if(jobj.getString("IsSuccess").equals("YES"))
			{
				last_mark_no = jobj.getInt("last_mark_no");
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return last_mark_no;
	}
	
	/***
	 * 진도 저장
	 * @param user_id
	 * @param course_cd
	 * @param
	 * @param chapter_no
	 * @param progress_no
	 * @param frame_no
	 * @param mark_no
	 */
	public void ProgressUpdate(String user_id, String course_cd, int take_course_seq, String chapter_no, int progress_no, int frame_no, int mark_no, int studySec, int mobileStudySec)
	{
		setUrl(DomainAddress.getUrlApps() + "/Hunet_Player/Android.aspx");
		setBlPlayer(true);
		setType("ProgressUpdate");
		setParam("user_id=" + user_id + "&course_cd=" + course_cd + "&take_course_seq=" + String.valueOf(take_course_seq) + "&chapter_no=" + chapter_no + 
				"&progress_no=" + String.valueOf(progress_no) + "&frame_no=" + String.valueOf(frame_no) + "&mark_no=" + String.valueOf(mark_no) + "&study_sec=" + String.valueOf(studySec) + "&mobile_study_sec=" + String.valueOf(mobileStudySec));
		@SuppressWarnings("unused")
		InputStream instream = hunetHttpPostData();
		if(instream==null)
			return;
	}
	
	/***
	 * 과목정보 조회. 현재 과목구분값
	 * @param course_cd
	 * @return
	 */
	public CourseData SelectCourseInfo(String course_cd, int take_course_seq)
	{
		CourseData data = new CourseData();
		JSONObject jobj = null;
		
		setUrl(DomainAddress.getUrlApps() + "/Hunet_Player/Android.aspx");
		setBlPlayer(true);
		setType("SelectCourseInfo");
		setParam("course_cd=" + course_cd + "&take_course_seq=" + String.valueOf(take_course_seq));
		InputStream instream = hunetHttpPostData();
		if(instream==null)
			return null;
		//Json데이터 변환
		jobj = HunetJsonMethod.convertJsonObject(instream);
		
		//ContentsData 데이터 변환
		try {
			if(jobj.getString("IsSuccess").equals("YES"))
			{
				data.setReviewEndDate(jobj.getString("review_end_date"));
				data.setCourseType(jobj.getInt("course_type"));
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return data;
	}
	
	/***
	 * 진도정보 및 과목정보
	 * @param course_cd
	 * @param chapter_no
	 * @param take_course_seq
	 * @return
	 */
	public ProgressData SelectProgressInfo(String course_cd, String chapter_no, int frame_no, int take_course_seq)
	{
		ProgressData pdata = new ProgressData();
		CourseData data = new CourseData();
		JSONObject jobj = null;
		
		setUrl(DomainAddress.getUrlApps() + "/Hunet_Player/Android.aspx");
		setBlPlayer(true);
		setType("SelectProgressInfo");
		setParam("user_id=" + AppMain.getId() + "&course_cd=" + course_cd + "&chapter_no=" + chapter_no + "&frame_no=" + String.valueOf(frame_no) + "&take_course_seq=" + String.valueOf(take_course_seq));
		InputStream instream = hunetHttpPostData();
		if(instream==null)
			return null;
		//Json데이터 변환
		jobj = HunetJsonMethod.convertJsonObject(instream);
		
		//ContentsData 데이터 변환
		try {
			if(jobj.getString("IsSuccess").equals("YES"))
			{
				data.setReviewEndDate(jobj.getString("review_end_date"));
				data.setCourseType(jobj.getInt("course_type"));
				
				pdata.set_CourseData(data);
				
				pdata.setProgressNo(jobj.getInt("progress_no"));
				pdata.setLastMarkNo(jobj.getInt("last_mark_no"));
				
				if("Y".equals(jobj.getString("list_mov_yn")))
					pdata.setBlListMov(true);
				else
					pdata.setBlListMov(false);
				
				if("Y".equals(jobj.getString("progress_restriction_yn")))
					pdata.setBlProgressRestrict(true);
				else
					pdata.setBlProgressRestrict(false);
				
				pdata.setStudySec(jobj.getInt("study_sec"));
				pdata.setMobileStudySec(jobj.getInt("mobile_study_sec"));
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return pdata;
	}
}
