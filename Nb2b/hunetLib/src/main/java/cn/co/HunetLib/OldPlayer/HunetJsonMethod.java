package cn.co.HunetLib.OldPlayer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.json.JSONException;
import org.json.JSONObject;

/***
 * @author hyoungil
 * JSON 매소드 클래스
 * 작업자 : 장형일
 * 작업일 : 2011.06
 */
public class HunetJsonMethod {
	
	public HunetJsonMethod() {
		
	}
	
	/***
	 * String => JSONObject 객체로 변환
	 * @param str
	 * @return
	 */
	public static JSONObject convertJsonObject(InputStream instream)
	{
		JSONObject JO = null;
		
		if(instream != null)
		{
			BufferedReader br = null;
			StringBuilder json = new StringBuilder();
			String line = null;
		
			br = new BufferedReader(new InputStreamReader(instream));
		    
		    try {
				while ((line = br.readLine()) != null){
					
				    json.append(line);
				    JO = new JSONObject(json.toString());
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}		
		
		return JO;
	}
	
	
}
