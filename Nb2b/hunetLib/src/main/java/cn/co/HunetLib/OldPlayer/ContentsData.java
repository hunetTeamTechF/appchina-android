package cn.co.HunetLib.OldPlayer;

/**
 * 강의관련 HTTP 통신. 플레이정보
 * 작업자 : 장형일
 * 작업일 : 2011.11
 * mlc 플레이어 인앱 수정. 2011.11.11 
 */
public class ContentsData {
	
	private int contents_no = 0;
	private String contents_cd = "";
	private String contents_cd_nm = "";
	private String contents_nm = "";
	private String progress_yn = "";


	public int getContents_no() {
		return contents_no;
	}

	public void setContents_no(int contents_no) {
		this.contents_no = contents_no;
	}

	public String getContents_cd() {
		return contents_cd;
	}

	public void setContents_cd(String contents_cd) {
		this.contents_cd = contents_cd;
	}

	public String getContents_cd_nm() {
		return contents_cd_nm;
	}

	public void setContents_cd_nm(String contents_cd_nm) {
		this.contents_cd_nm = contents_cd_nm;
	}

	public String getContents_nm() {
		return contents_nm;
	}

	public void setContents_nm(String contents_nm) {
		this.contents_nm = contents_nm;
	}

	public String getProgress_yn() {
		return progress_yn;
	}

	public void setProgress_yn(String progress_yn) {
		this.progress_yn = progress_yn;
	}
	
	/* 컨텐츠 학습정보 관련 */
	
	private String contents = "";
	private String contents_time = "";
	private float contents_size = 0;
	private String contents_url = "";
	private String refer_mov_url = "";
	
	public String getContents() {
		return contents;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}

	public String getContents_time() {
		return contents_time;
	}

	public void setContents_time(String contents_time) {
		this.contents_time = contents_time;
	}

	public String getContents_url() {
		return contents_url;
	}

	public void setContents_url(String contents_url) {
		this.contents_url = contents_url;
	}

	public String getRefer_mov_url() {
		return refer_mov_url;
	}

	public void setRefer_mov_url(String refer_mov_url) {
		this.refer_mov_url = refer_mov_url;
	}
	
	public void setContents_size(float contents_size) {
		this.contents_size = contents_size;
	}

	public float getContents_size() {
		return contents_size;
	}
	
	
	private String mobile_contents_url = "";
	private String mobile_mov_url = "";
	private String mark_yn = "";
	
	public void setMobile_contents_url(String mobile_contents_url) {
		this.mobile_contents_url = mobile_contents_url;
	}

	public String getMobile_contents_url() {
		return mobile_contents_url;
	}

	public void setMobile_mov_url(String mobile_mov_url) {
		this.mobile_mov_url = mobile_mov_url;
	}

	public String getMobile_mov_url() {
		return mobile_mov_url;
	}

	public void setMark_yn(String mark_yn) {
		this.mark_yn = mark_yn;
	}

	public String getMark_yn() {
		return mark_yn;
	}

	
	public ContentsData()
	{
		
	}

}
