package cn.co.HunetLib.HttpFileDownload;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

import android.os.AsyncTask;

public class FileDownload {

	public void download(String fileURL, String downloadPath, downloadFileEventListener listener) {
        FileDownloadEntity param = new FileDownloadEntity();
        param.listener = listener;
        param.fileUrl = fileURL;
        param.downloadPath = downloadPath;
        
        File f = new File(downloadPath);
        int cnt = 1;
        
        while (f.exists()) {
        	String ext = downloadPath.substring(downloadPath.lastIndexOf("."));
        	String path = downloadPath.replace(ext, "");
        	path = String.format("%s(%s)%s", path, (cnt++), ext);
        	f = new File(path);
        }
        
        param.downloadPath = f.getPath();
        
        FileDownloadTask task = new FileDownloadTask();
        task.execute(param);
    }
    
    public interface downloadFileEventListener {
    	public void completed(File downloadFile);
    }
    
    private class FileDownloadEntity {
    	downloadFileEventListener listener;
    	String fileUrl;
    	String downloadPath;
    }
    
    private class FileDownloadTask extends AsyncTask<FileDownloadEntity, FileDownloadEntity, Void> {

		@Override
		protected Void doInBackground(FileDownloadEntity... params) {
			
			int count = 0;
			
			try {
				URL url = new URL(params[0].fileUrl);
				URLConnection connection = url.openConnection();
				connection.connect();
				
				int lengthOfFile = connection.getContentLength();
				
				InputStream input = new BufferedInputStream(url.openStream());
				OutputStream output = new FileOutputStream(params[0].downloadPath);
				byte data[] = new byte[1024];
				long total = 0;
				
				while ((count = input.read(data)) != -1) {
					total += count;
					output.write(data, 0, count);
				}
				
				output.flush();
				output.close();
				input.close();
//				
//	            FileOutputStream f = new FileOutputStream(params[0].downloadPath);
//	            HttpURLConnection c = (HttpURLConnection) url.openConnection();
////	            c.setRequestMethod("GET");
//	            c.setDoOutput(true);
//	            c.connect();
//
//	            InputStream in = c.getInputStream();
//
//	            byte[] buffer = new byte[1024];
//	            int len1 = 0;
//
//	            while ((len1 = in.read(buffer)) > 0)
//	                f.write(buffer, 0, len1);
//
//	            f.close();
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
			
			publishProgress(params);
			
			return null;
		}
    	
		@Override
		protected void onProgressUpdate(FileDownloadEntity... values) {

			if (values[0].listener != null)
				values[0].listener.completed(new File(values[0].downloadPath));
		}
    }
}
