package cn.co.HunetLib.GCM;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;

import java.io.BufferedInputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import cn.co.HunetLib.Common.AppMain;
import cn.co.HunetLib.Common.Define;
import cn.co.HunetLib.SQLite.AccessModel;
import cn.co.HunetLib.SQLite.AppEventModel;
import cn.co.HunetLib.SQLite.LoginModel;
import cn.co.HunetLib.SQLite.SQLiteManager;
import cn.co.HunetLib.Service.HttpSenderService;
import cn.co.HunetLib.Company.Company;
import cn.co.HunetLib.R;

/**
 * Created by zeple on 2015-07-06.
 */
public class GCMLibIntentService extends com.google.android.gms.gcm.GcmListenerService {

    private final String TAG = "GCMLibIntentService";

    final int GCM_NOTI_REGIST = 1;
    final int GCM_NOTI_CANCEL = 2;
    final int GCM_NOTI_CANCELALL = 3;
    final int GCM_BADGECOUNT = 4;

    /**
     * @param from SenderID 값을 받아온다.
     * @param data Set형태로 GCM으로 받은 데이터 payload이다.
     */
    @Override
    public void onMessageReceived(String from, Bundle data) {
        int push_type = getParseInt(getStringBundle(data, "type", "0"), 0);
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        switch (push_type) {
            case GCM_NOTI_REGIST: {

                // Step 1. Noti 설정
                SQLiteManager sqlManager = new SQLiteManager(getBaseContext());
                LoginModel loginModel = sqlManager.GetLoginData();
                AccessModel accessModel = sqlManager.GetAccessData();
                Company company = AppMain.CompanyInfo;
                boolean isLoggedIn = "Y".equalsIgnoreCase(loginModel.loginYn);
                boolean enabledPush = "1".equalsIgnoreCase(accessModel.push_type);

                int key = getParseInt(data.getString("key"), 0);
                int tabIndex = getParseInt(data.getString("tabIndex"), 0);
                String title = getStringBundle(data, "title", "").trim();
                String message = getStringBundle(data, "message", "");
                String nextUrl = getStringBundle(data, "nextUrl", "");
                String userSeq = getStringBundle(data, "userSeq", "");
                String indexDetail = getStringBundle(data, "indexDetail", "");
                String badgeCount = getStringBundle(data, "badgeCnt", "");
                String imageUrl = getStringBundle(data, "imageURL", "");
                Bitmap notiImage = null;

                if (isLoggedIn && enabledPush) {
                    if (TextUtils.isEmpty(title) && !TextUtils.isEmpty(company.getAppName()))
                        title = company.getAppName().trim();

                    if (!TextUtils.isEmpty(imageUrl)) {
                        // 푸시정보에 이미지 정보가 있다면  읽어들여서 BigPictureStyle로 뿌리기
                        try {
                            URL url = new URL(imageUrl);
                            URLConnection conn = url.openConnection();
                            conn.connect();
                            BufferedInputStream bis = new BufferedInputStream(conn.getInputStream());
                            notiImage = BitmapFactory.decodeStream(bis);
                            bis.close();
                        } catch (Exception e) {
                            notiImage = null;
                            e.printStackTrace();
                        }
                    }

                    // Step 2. Next Url 설정
                    Intent intent = new Intent(this, company.getNextActivity(Company.eACTIVITY_LOGIN));
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("DEFINE_NOTIFY", Define.NOTIFY_GCM_NOTI_REGIST.getValue());
                    intent.putExtra("TAB_INDEX", tabIndex);

                    if (nextUrl != null && !TextUtils.isEmpty(nextUrl))
                        intent.putExtra("NEXT_URL", nextUrl);
                    if (userSeq != null && !TextUtils.isEmpty(userSeq))
                        intent.putExtra("USER_SEQ", userSeq);
                    if (indexDetail != null && !TextUtils.isEmpty(indexDetail))
                        intent.putExtra("INDEX_DETAIL", indexDetail);

                    PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);

                    NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                            .setSmallIcon(R.drawable.icon_app)
                            .setTicker(title)
                            .setContentTitle(title)
                            .setContentText(message)
                            .setAutoCancel(true)
                            .setContentIntent(pendingIntent)
                            .setDefaults(Notification.DEFAULT_VIBRATE)
                            .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));

                    if (notiImage != null) {
                        NotificationCompat.BigPictureStyle style = new NotificationCompat.BigPictureStyle();
                        style.setBigContentTitle(title);
                        style.setSummaryText(message);
                        style.bigPicture(notiImage);

                        builder.setShowWhen(true);
                        builder.setStyle(style);
                    }

                    notificationManager.notify(key, builder.build());

                    // Step 3. Badge Count 설정
                    if (!TextUtils.isEmpty(badgeCount))
                        SetBadgeUpdate(Integer.parseInt(badgeCount));
                }

                // Step 4. Recv 이벤트
                if (userSeq != null && !TextUtils.isEmpty(userSeq)) {
                    String loginYn = isLoggedIn ? "Y" : "N";
                    String pushType = enabledPush ? "1" : "0";
                    String result = "";

                    try {
                        result = URLEncoder.encode("loginYn_" + loginYn + ",pushType_" + pushType, "utf-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }

                    AppEventModel model = new AppEventModel();
                    model.max_count = 10;
                    model.url = company.getUrl(Company.eURL_NOTIFY_EVENT) + "&eventType=recv&userSeq=" + userSeq + "&result=" + result;
                    sqlManager.InsertAppEvent(model);

                    startService(new Intent(this, HttpSenderService.class));
                }
            }
            break;
            case GCM_NOTI_CANCEL: {
                notificationManager.cancel(getParseInt(getStringBundle(data, "key", "0"), 0));
            }
            break;
            case GCM_NOTI_CANCELALL: {
                notificationManager.cancelAll();
            }
            break;
            case GCM_BADGECOUNT: {
                SetBadgeUpdate(getParseInt(getStringBundle(data, "badgeCnt", ""), 0));
            }
            break;
        }
    }

    private int getParseInt(String value, int defValue) {
        int retVal;

        try {
            retVal = Integer.parseInt(value);
        } catch (NumberFormatException e) {
            retVal = defValue;
        }

        return retVal;
    }

    private String getStringBundle(Bundle bundle, String key, String defValue) {
        String value = bundle.getString(key);

        if (value == null)
            value = defValue;

        return value;

    }

    private void SetBadgeUpdate(int a_nCount) {
        Intent badgeIntent = new Intent("android.intent.action.BADGE_COUNT_UPDATE");
        badgeIntent.putExtra("badge_count", a_nCount);
        badgeIntent.putExtra("badge_count_package_name", this.getPackageName());
        badgeIntent.putExtra("badge_count_class_name", this.getPackageName() + ".LoginActivity");

        sendBroadcast(badgeIntent);
    }
}