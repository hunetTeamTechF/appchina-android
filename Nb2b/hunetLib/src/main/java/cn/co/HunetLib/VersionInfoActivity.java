package cn.co.HunetLib;

import cn.co.HunetLib.Common.HunetNetwork;
import hunet.library.Utilities;
import cn.co.HunetLib.Base.TabActivity;
import cn.co.HunetLib.Company.Company;
import cn.co.HunetLib.Http.HunetHttpData;
import cn.co.HunetLib.Interface.ITabControlEventListener;
import cn.co.HunetLib.R;
import cn.co.HunetLib.Common.DownLoadManager;
import cn.co.HunetLib.Common.UpdataInfo;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.io.File;
import android.os.Message;
import android.os.Handler;
import android.widget.Toast;
import android.widget.Button;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.util.Log;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;


public class VersionInfoActivity extends TabActivity implements ITabControlEventListener, OnClickListener {

	LinearLayout ll_versioninfo_back;
	TextView tv_current_version;
	TextView tv_last_version;
	TextView tv_version_update;

	private final int eVERSION_UPDATE = 1;
	private final int eVERSION_FINISH = 2;

	/* download */
	private final String TAG = this.getClass().getName();
	private final int UPDATA_NONEED = 0;
	private final int UPDATA_CLIENT = 1;
	private final int GET_UNDATAINFO_ERROR = 2;
	private final int SDCARD_NOMOUNTED = 3;
	private final int DOWN_ERROR = 4;
	private Button getVersion;
	private UpdataInfo info;
	private String localVersion;
	private String DownUrl;
		
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);

	    setContentView(R.layout.versioninfo_activity);
	
	    Utilities.changeTitlebarColor(this, R.id.version_info_fl, R.id.header_text_color);
	    
	    ll_versioninfo_back = (LinearLayout) findViewById(R.id.more_ll_versioninfo_back);
		tv_current_version = (TextView) findViewById(R.id.more_tv_current_version);
		tv_last_version = (TextView) findViewById(R.id.more_tv_last_version);
		tv_version_update = (TextView) findViewById(R.id.more_tv_version_update);

		ll_versioninfo_back.setOnClickListener(this);ll_versioninfo_back.setTag(eVERSION_FINISH);
		tv_version_update.setOnClickListener(this);tv_version_update.setTag(eVERSION_UPDATE);

		CreateTabControl((LinearLayout)findViewById(R.id.more_ll_tab));
		setSelectTab(Company.eTAB_VERSION);
		
		tv_current_version.setText(getPackageVersion());
	}
	
	@Override
	protected void onResume() 
	{
		super.onResume();	
		tv_last_version.setText("");
		
		if(HunetNetwork.GetCurrentUseNetwork(this) == 0)
    	{
    		ShowToast(getString(R.string.app_msg_none_network));
			return;
    	}
		
		ShowDialog(this, "", true);
		String url = String.format(getCompany().getUrl(Company.eURL_APP_VERSION),  getCompany().getPackageName(this).replace("cn.co.hunet.MobileLearningCenterFor", ""));
    	AsyncExecute(0, url, null);
	}

	@Override
    public void AsyncRequestDataResult(HunetHttpData result) {
		super.AsyncRequestDataResult(result);
		HideDialog();

		DownUrl = result.GetJsonValue("DownloadUrl","");

		String updateVersion = result.GetJsonValue("Version", "0.0.0");
		tv_last_version.setText(updateVersion);
		
		if(CheckUpdateVersion(updateVersion))
			tv_version_update.setVisibility(View.VISIBLE);


	}

	@Override
	protected void onFirstResume() 
	{
	}

	@Override
	public void onClick(View view) 
	{
		int type = (Integer)view.getTag();
		UpdataInfo info = new UpdataInfo();
		switch(type)
		{
		case eVERSION_UPDATE:
//			String url = "market://details?id=" + getPackageName();
//			//String url = "http://appdown.xiunaichina.com/";
//			Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
//			startActivity(intent);
//
			downLoadApk();

			break;
		case eVERSION_FINISH:
			finish();
			break;
		}
	}


	Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			super.handleMessage(msg);
			switch (msg.what) {
				case UPDATA_NONEED:
					Toast.makeText(getApplicationContext(), "不需要更新",
							Toast.LENGTH_SHORT).show();
				case UPDATA_CLIENT:
					//对话框通知用户升级程序
					showUpdataDialog();
					break;
				case GET_UNDATAINFO_ERROR:
					//服务器超时
					Toast.makeText(getApplicationContext(), "获取服务器更新信息失败", Toast.LENGTH_LONG).show();
					break;
				case DOWN_ERROR:
					//下载apk失败
					Toast.makeText(getApplicationContext(), "下载新版本失败", Toast.LENGTH_LONG).show();
					break;
			}
		}
	};


	/*
	 *
	 * 弹出对话框通知用户更新程序
	 *
	 * 弹出对话框的步骤：
	 *  1.创建alertDialog的builder.
	 *  2.要给builder设置属性, 对话框的内容,样式,按钮
	 *  3.通过builder 创建一个对话框
	 *  4.对话框show()出来
	 */
	private void showUpdataDialog() {
		AlertDialog.Builder builer = new Builder(this);
		builer.setTitle("版本升级");
		builer.setMessage(info.getDescription());
		//当点确定按钮时从服务器上下载 新的apk 然后安装   װ
		builer.setPositiveButton("确定", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				//Log.i(TAG, "下载apk,更新");
				downLoadApk();
			}
		});
		builer.setNegativeButton("取消", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				//do sth
			}
		});
		AlertDialog dialog = builer.create();
		dialog.show();
	}

	/*
	 * 从服务器中下载APK
	 */
	private void downLoadApk() {
		final ProgressDialog pd;    //进度条对话框

		pd = new  ProgressDialog(this);
		pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		pd.setMessage("正在下载更新");
		pd.show();

		new Thread(){
			@Override
			public void run() {
				try {
					File file = DownLoadManager.getFileFromServer(DownUrl, pd);
					sleep(3000);
					installApk(file);
					pd.dismiss(); //结束掉进度条对话框
				} catch (Exception e) {
					Message msg = new Message();
					msg.what = DOWN_ERROR;
					handler.sendMessage(msg);
					e.printStackTrace();
				}
			}}.start();
	}

	//安装apk
	private void installApk(File file) {
		Intent intent = new Intent();
		//执行动作
		intent.setAction(Intent.ACTION_VIEW);
		//执行的数据类型
		intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
		startActivity(intent);
	}


}
