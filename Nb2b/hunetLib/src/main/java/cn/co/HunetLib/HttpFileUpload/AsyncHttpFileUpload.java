package cn.co.HunetLib.HttpFileUpload;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;

public class AsyncHttpFileUpload implements IHttpFileUploadEventListener
{	
	private IAsyncHttpFileUploadEventListener m_EventListener;
	private List<HttpFileInfo> 	m_httpFileInfoList;
	private ProgressDialog 		m_ProgressDialog;
	private Activity 			m_ParentActivity;
	private AsyncRequestData 	m_AyncReqData;
	private long m_totalFileSize 	= 0;
	private long m_sendProgress		= 0;
			
	public AsyncHttpFileUpload(Activity parentActivity)
	{
		m_ParentActivity = parentActivity;
		m_httpFileInfoList = new ArrayList<HttpFileInfo>();
	}
	
	public void SetEventListener(IAsyncHttpFileUploadEventListener a_EventListener)
	{
		m_EventListener = a_EventListener;
	}
	
	public void AddFileInfo(String filePath, String sendUrl)
	{
		File file 			= new File(filePath);
		HttpFileInfo info 	= new HttpFileInfo();
		info.filePath 		= filePath;
		info.fileName		= file.getName();
		info.sendUrl		= sendUrl;
		
		m_httpFileInfoList.add(info);
	}
	
	public void Clear()
	{
		for(int nIndex = m_httpFileInfoList.size() - 1; nIndex >= 0; nIndex--)
		{
			m_httpFileInfoList.remove(nIndex);
		}
	}
	
	public void DoUpload()
	{		
		m_totalFileSize = 0;
		m_sendProgress 	= 0;
		
		for(int nIndex = 0; nIndex < m_httpFileInfoList.size(); nIndex++)
		{
			HttpFileInfo info = m_httpFileInfoList.get(nIndex);
			
			File file = new File(info.filePath);
			
			m_totalFileSize += file.length();
		}
		
		if(m_ProgressDialog == null)
		{
			m_ProgressDialog = new ProgressDialog(m_ParentActivity);
			m_ProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			//m_ProgressDialog.setTitle("파일 업로드");
			//m_ProgressDialog.setMessage("파일 전송중");
			m_ProgressDialog.setCancelable(false);
			m_ProgressDialog.setCanceledOnTouchOutside(false);
			m_ProgressDialog.setMax(100);
			
			m_ProgressDialog.setButton("Cancel", new DialogInterface.OnClickListener() 
			{
				public void onClick(DialogInterface dialog, int which) 
				{
					CloseProgressDialog();
					
					m_AyncReqData.cancel(true);
					Clear();
				}
			});
		}
		
		m_ProgressDialog.setProgress(0);
		m_ProgressDialog.show();
		
		InternalUpload();
	}
	
	private void CloseProgressDialog()
	{
		if(m_ProgressDialog == null)
			return;
		
		m_ProgressDialog.dismiss();
		m_ProgressDialog = null;
	}
	
	private void InternalUpload()
	{
		HttpFileInfo info = GetNextUpload();
		
		if(info == null)
		{
			CloseProgressDialog();
			
			if(m_EventListener != null)
				m_EventListener.OnJobComplete(m_httpFileInfoList);
			
			Clear();
			return;
		}
				
		m_AyncReqData = new AsyncRequestData();
		m_AyncReqData.SetEventListener(this);
		m_AyncReqData.execute(info);
	}
	
	private HttpFileInfo GetNextUpload()
	{		
		for(int nIndex = 0; nIndex < m_httpFileInfoList.size(); nIndex++)
		{
			HttpFileInfo info = m_httpFileInfoList.get(nIndex);
			
			if(info.code == -1)
				return info;
		}
		
		return null;
	}
	
	@Override
	public void OnProgressUpdate(int bytes)
	{
		m_sendProgress += bytes;
		
		int nPercent = (int)(((double)m_sendProgress / (double)m_totalFileSize) * 100);
		
		m_ProgressDialog.setProgress(nPercent);
	}

	@Override
	public void OnUploadComplete(HttpFileInfo info) 
	{
		if(m_EventListener != null)
			m_EventListener.OnFileUploadComplete(info);
		
		InternalUpload();
	}
	
	private class AsyncRequestData extends AsyncTask<HttpFileInfo, Integer, HttpFileInfo> implements IHttpProgressUpdateEventListener
    {
		private HttpFileUpload m_httpFileUpload = new HttpFileUpload();
		private IHttpFileUploadEventListener m_EventListener;
		
		public void SetEventListener(IHttpFileUploadEventListener a_EventListener)
		{
			m_EventListener = a_EventListener;
		}
		
		@Override
		protected void onCancelled()
		{
			m_httpFileUpload.SetEventListener(null);
			m_httpFileUpload.Disconnect();
			
			super.onCancelled();
		}
		
		@Override 
		protected void onPreExecute()
		{
			m_httpFileUpload.SetEventListener(this);
			
			super.onPreExecute();
		}
		
    	@Override
    	protected HttpFileInfo doInBackground(HttpFileInfo... fileInfo)
    	{
    		return m_httpFileUpload.DoUpload(fileInfo[0]);
    	}
    	
    	@Override
    	protected void onProgressUpdate(Integer... progress)
    	{
    		if(m_EventListener != null)
    			m_EventListener.OnProgressUpdate(progress[0]);
    	}
    	
    	@Override
    	protected void onPostExecute(HttpFileInfo result)
    	{
    		if(m_EventListener != null)
    			m_EventListener.OnUploadComplete(result);
    	}

		@Override
		public void OnProgressUpdate(int bytes) 
		{
			publishProgress(bytes);
		}
    }	
}
