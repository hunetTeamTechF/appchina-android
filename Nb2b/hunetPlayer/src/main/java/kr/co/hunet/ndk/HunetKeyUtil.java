package kr.co.hunet.ndk;

public class HunetKeyUtil {
    public native String getEncryptKey();

    static {
        System.loadLibrary("hunet-key-util");
    }
}
