package hunet.library;

import hunet.data.SQLiteManager;
import hunet.drm.models.AccessModel;

import java.io.File;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

public class Utilities {

	public static String GetExternalDir()
	{
		return Environment.getExternalStorageDirectory() + "/hunet_mlc/";
	}

	public static String GetTimeString(double milliSec, boolean showHour) {
		int hours = (int) (milliSec / (3600000)); // 1000*60*60
		int minutes =0;
		if(showHour)
			minutes =(int) (milliSec % (3600000)) / (60000);
		else
			minutes =(int) (milliSec /60000);//1000*60
		int seconds = (int) ((milliSec % (3600000)) % (60000)) / 1000;

		if (showHour)
			return String.format("%02d:%02d:%02d", hours, minutes, seconds);
		else
			return String.format("%02d:%02d", minutes, seconds);
	}

//	public static Date GetFormattedDate(Date date, String pattern) {
//		SimpleDateFormat dateFormat = new SimpleDateFormat(pattern, Locale.KOREA);
//		Date retDate = null;
//
//		try {
//			retDate = dateFormat.parse(dateFormat.format(date));
//		} catch (ParseException e) {
//			retDate = new Date();
//		}
//
//		return retDate;
//	}
//
//	public static Date GetFormattedDate(String date, String pattern) {
//		SimpleDateFormat dateFormat = new SimpleDateFormat(pattern, Locale.KOREA);
//		Date retDate = null;
//
//		try {
//			retDate = dateFormat.parse(date);
//		} catch (ParseException e) {
//			retDate = new Date();
//		}
//
//		return retDate;
//	}
//
//	public static String GetFormattedDateString(String date, String pattern) {
//		SimpleDateFormat dateFormat = new SimpleDateFormat(pattern, Locale.KOREA);
//		String retDate = "";
//
//		try {
//			retDate = dateFormat.format(dateFormat.parse(date));
//		} catch (ParseException e) {
//			retDate = dateFormat.format(new Date());
//		}
//
//		return retDate;
//	}

	public static String GetFormattedDateString(Date date, String pattern) {
		return new SimpleDateFormat(pattern, Locale.KOREA).format(date);
	}

//	public static long GetDiffDay(Date startDate, Date endDate) {
//		return (endDate.getTime() - startDate.getTime()) / (1000 * 60 * 60 * 24);
//	}

	public static void changeTitlebarColor(Activity activity, int resId, int resHeaderColorId){
		FrameLayout fl;
		TextView textview;
		String ruleIdKey = "ruleIdKey";
		SharedPreferences mPref = PreferenceManager.getDefaultSharedPreferences(activity);
		String ruleId = mPref.getString(ruleIdKey, "");

		if(!TextUtils.isEmpty(ruleId)){
			fl = (FrameLayout)activity.findViewById(resId);
			textview = (TextView)activity.findViewById(resHeaderColorId);
			ruleId = ruleId.toLowerCase();
			int position = ruleId.lastIndexOf("_");
			if(position > 0){
				ruleId = ruleId.substring(0, position);
			}else if(position < 0){
				position = ruleId.lastIndexOf("-");
				ruleId = ruleId.substring(0, position);
			}

			String resourseId = "@drawable/"+ruleId+"_activity_header_color";
			String resourseColorId = ruleId+"_activity_header_text_color";
			String resourseStringId = "@string/"+ruleId+"_activity_header_text_color_string";


			if(getResourceId(activity, resourseId) > 0)
				fl.setBackgroundResource(getResourceId(activity, resourseId));

			int stringId =  getResourceStringId(activity, resourseStringId);
			if(stringId > 0){
				String resColorString = activity.getResources().getString(stringId);
				textview.setTextColor(Color.parseColor(resColorString));
			}


//		    if(getResourceColorId(activity, resourseColorId) > 0)
//		    	textview.setTextColor(getResourceColorId(activity, resourseId));

		}
	}

	private static int getResourceId(Activity activity, String name)
	{
		return activity.getResources().getIdentifier(name, "drawable", activity.getPackageName());
	}

//	public static int getResourceColorId(Activity activity, String name)
//	{
//		return activity.getResources().getIdentifier(name, "color", activity.getPackageName());
//	}

	private static int getResourceStringId(Activity activity, String name)
	{
		return activity.getResources().getIdentifier(name, "string", activity.getPackageName());
	}

	public static File getExternalMemoryFileHandle(final Context context) {
		File contentsDir = null;
		File compate[];

		compate = ContextCompat.getExternalFilesDirs(context, Environment.DIRECTORY_DOWNLOADS);
		if(compate[0] == null)
			return null;

		// 외장 메모리가 없으면 첫번째 외부 저장소에 대한 디렉토리에 저장 하자 --> //context.getFilesDir(): 내부 저장소용
		contentsDir = compate[0].getAbsoluteFile(); // 첫번째 외부 저장소에 대한 디렉토리   

		if(Build.VERSION.SDK_INT >= 19){ // KitKat
			if(getExternalMemoryCheck(context)){
				if(compate.length > 1) { // 외장 메모리
					if(compate[1] != null) // null이 기본으로 잡혀 2개인 경우가 있다. 갤럭시 S5
						contentsDir = compate[1].getAbsoluteFile();
				}
			}

		}else{
			if(getExternalMemoryCheck(context)){
				HashSet<String> out = getExternalMounts();
				int size = out.size();
				if(size > 2) {
					Activity activity = (Activity) context;
					activity.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							Toast.makeText(context, "마운트 정보가  정확하지 않습니다.", Toast.LENGTH_LONG).show();
						}
					});
					return null;
				}else if(size==2){
					String tempDir = contentsDir.getAbsolutePath();
					String[] arrayString = out.toArray(new String[size]);
					String root_dir;
					if(tempDir.contains(arrayString[0])){
						root_dir = arrayString[1];
					}else{
						root_dir = arrayString[0];
					}
					root_dir = root_dir + "/files/Download";
					contentsDir = new File(root_dir);
				}
			}
		}

		return contentsDir;
	}

	public static File getInternalMemoryFileHandle(final Context context) {
		File contentsDir = null;
		File compate[];

		compate = ContextCompat.getExternalFilesDirs(context, Environment.DIRECTORY_DOWNLOADS);

		if(compate[0] == null)
			return null;

		// 외장 메모리가 없으면 첫번째 외부 저장소에 대한 디렉토리에 저장 하자 --> //context.getFilesDir(): 내부 저장소용
		contentsDir = compate[0].getAbsoluteFile(); // 첫번째 외부 저장소에 대한 디렉토리   

		return contentsDir;
	}

	public static File getMemoryFileHandle(final Context context) {
		SQLiteManager sqlManager = new SQLiteManager(context);
		AccessModel accessModel = sqlManager.GetAccessData();
		File contentsDir = null;
		File compate[];

		compate = ContextCompat.getExternalFilesDirs(context, Environment.DIRECTORY_DOWNLOADS);

		if(compate[0] == null)
			return null;

		// 외장 메모리가 없으면 첫번째 외부 저장소에 대한 디렉토리에 저장 하자 --> //context.getFilesDir(): 내부 저장소용
		contentsDir = compate[0].getAbsoluteFile(); // 첫번째 외부 저장소에 대한 디렉토리   

		if(Build.VERSION.SDK_INT >= 19){ // KitKat
			if("1".equals(accessModel.external_memory)){ // 외부 메모리 사용
				if(getExternalMemoryCheck(context)){
					if(compate.length > 1) { // 외장 메모리
						if(compate[1] != null) // null이 기본으로 잡혀 2개인 경우가 있다. 갤럭시 S5
							contentsDir = compate[1].getAbsoluteFile();
					}
				}
			}

		}else{
			if("1".equals(accessModel.external_memory)){ // 외부 메모리 사용
				if(getExternalMemoryCheck(context)){
					HashSet<String> out = getExternalMounts();
					int size = out.size();
					if(size > 2) {
						Activity activity = (Activity) context;
						activity.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								Toast.makeText(context, "마운트 정보가  정확하지 않습니다.", Toast.LENGTH_LONG).show();
							}
						});
						return null;
					}else if(size==2){
						String tempDir = contentsDir.getAbsolutePath();
						String[] arrayString = out.toArray(new String[size]);
						String root_dir;
						if(tempDir.contains(arrayString[0])){
							root_dir = arrayString[1];
						}else{
							root_dir = arrayString[0];
						}
						root_dir = root_dir + "/files/Download";
						contentsDir = new File(root_dir);
					}
				}
			}
		}

		return contentsDir;
	}

	public static String getDownloadPullPath(final Context context, String seq, String id) {
		SQLiteManager sqlManager = new SQLiteManager(context);
		AccessModel accessModel = sqlManager.GetAccessData();
		File contentsDir = null;
		File compate[] = null;
		String root_dir = "";
		String video_dir = "";
		String video_fullpath = "";

		compate = ContextCompat.getExternalFilesDirs(context, Environment.DIRECTORY_DOWNLOADS);

		if(compate[0] == null)
			return "";

		// 외장 메모리가 없으면 첫번째 외부 저장소에 대한 디렉토리에 저장 하자 --> //context.getFilesDir(): 내부 저장소용
		contentsDir = compate[0].getAbsoluteFile(); // 첫번째 외부 저장소에 대한 디렉토리

		if(Build.VERSION.SDK_INT >= 19){ // KitKat
			if("1".equals(accessModel.external_memory)){ // 외부 메모리 사용
				if(getExternalMemoryCheck(context)){
					if(compate.length > 1) { // 외장 메모리
						if(compate[1] != null) // null이 기본으로 잡혀 2개인 경우가 있다. 갤럭시 S5
							contentsDir = compate[1].getAbsoluteFile();
					}
				}
			}
			root_dir = contentsDir.getAbsolutePath();

		}else{
			if("1".equals(accessModel.external_memory)){ // 외부 메모리 사용
				if(getExternalMemoryCheck(context)){
					HashSet<String> out = getExternalMounts();
					int size = out.size();
					if(size > 2) {
						Activity activity = (Activity) context;
						activity.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								Toast.makeText(context, "마운트 정보가  정확하지 않습니다.", Toast.LENGTH_LONG).show();
							}
						});
						return "";
					}else if(size == 2){
						String tempDir = contentsDir.getAbsolutePath();
						String[] arrayString = out.toArray(new String[size]);

						if(tempDir.contains(arrayString[0])){
							root_dir = arrayString[1];
						}else{
							root_dir = arrayString[0];
						}
						root_dir = root_dir + "/files/Download";
					}else{
						root_dir = contentsDir.getAbsolutePath();
					}
				}else{
					root_dir = contentsDir.getAbsolutePath();
				}
			}else{
				root_dir = contentsDir.getAbsolutePath();
			}

		}

//        root_dir = contentsDir.getAbsolutePath();

		video_dir = root_dir + "/" + seq + "/videos/";
		video_fullpath = video_dir + id + ".mp4";

		File file = new File(video_dir);

		if (file.exists() == false)
			file.mkdirs();

		return video_fullpath;
	}

	public static Boolean getExternalMemoryCheck(Context context) {
		File compate[];
		Boolean bExternalMemory = false;

		if(Build.VERSION.SDK_INT >= 19){ // KitKat
			compate = ContextCompat.getExternalFilesDirs(context, Environment.DIRECTORY_DOWNLOADS);
			if(compate[0] == null)
				return false;

			if(compate.length > 1) { // 외장 메모리
				if(compate[1] != null) // null이 기본으로 잡혀 2개인 경우가 있다. 갤럭시 S5
					bExternalMemory = true;
			}
		}else{
			HashSet<String> out = getExternalMounts();
			if(out.size() > 1){
				bExternalMemory = true;
			}
		}
		return bExternalMemory;
	}

	private static HashSet<String> getExternalMounts(){
		final HashSet<String> out = new HashSet<String>();
		String reg = "(?i).*vold.*(vfat|ntfs|exfat|fat32|ext3|ext4).*rw.*";

		String s = "";

		try {
			final Process process = new ProcessBuilder().command("mount")
					.redirectErrorStream(true).start();

			process.waitFor();

			final InputStream is = process.getInputStream();
			final byte[] buffer = new byte[1024];

			while (is.read(buffer) != -1) {
				s = s + new String(buffer);
			}
			is.close();
		} catch (final Exception e) {
			e.printStackTrace();
		}

		// parse output
		final String[] lines = s.split("\n");
		for (String line : lines) {
			if (!line.toLowerCase(Locale.US).contains("asec")) {
				if (line.matches(reg)) {
					String[] parts = line.split(" ");
					for (String part : parts) {
						if (part.startsWith("/"))
							if (!part.toLowerCase(Locale.US).contains("vold"))
								out.add(part);
					}
				}
			}
		}
		return out;

	}
}
