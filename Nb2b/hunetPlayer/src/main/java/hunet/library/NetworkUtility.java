package hunet.library;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.widget.Toast;

public class NetworkUtility {

	public final static int NETWORK_STATUS_OFFLINE = 0;
	private final static int NETWORK_STATUS_WIFI = 1;
	private final static int NETWORK_STATUS_MOBILE = 2;

	private int currentNetworkStatus = -1;
	private ConnectivityManager networkManager;
	private Timer networkTimer;
	private NetworkChangeEventListener listener;
	private Activity mActivity;
	private Context mContext;

	public interface NetworkChangeEventListener {
		public void onChangeNetworkStatus(int status, int prevStatus);
	}

	public NetworkUtility(Context context) {
		this.mContext = context;
		this.mActivity = (Activity)context;
		this.networkManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE); 
	}
	
	public void setNetworkChangeEventListener(NetworkChangeEventListener listener) {
		this.listener = listener;
	}
	
//	public void updateCurrentStatus() {
//		if (isConnectedWIFI())
//			currentNetworkStatus = NETWORK_STATUS_WIFI;
//		else if (isConnectedMobile())
//			currentNetworkStatus = NETWORK_STATUS_MOBILE;
//		else
//			currentNetworkStatus = NETWORK_STATUS_OFFLINE;
//	}

	public void startMonitoring() {

		if (this.networkTimer == null) {
			this.networkTimer = new Timer();
			this.networkTimer.schedule(new TimerTask() {
				
				@Override
				public void run() {
					mActivity.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							new NetworkTask().execute();
						}
					});
				}
			}, 0, 5000);
		}
	}

	public void stopMonitoring() {
		if (this.networkTimer != null) {
			this.networkTimer.cancel();
			this.networkTimer = null;
		}
	}
	
	/**
	 * WIFI망에 접속되었고 연결이 가능한지 여부를 가져옵니다.
	 * @return
	 */
	private boolean isConnectedWIFI() {
		NetworkInfo wifiInfo = this.networkManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

		return (wifiInfo != null && wifiInfo.isConnected());
	}
	
	/**
	 * 이동통신사망에 접속되었고 연결이 가능한지 여부를 가져옵니다.
	 * @return
	 */
	private boolean isConnectedMobile() {
		NetworkInfo mobileInfo = this.networkManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		NetworkInfo wimaxInfo = this.networkManager.getNetworkInfo(ConnectivityManager.TYPE_WIMAX);

		return (mobileInfo != null && mobileInfo.isConnected())
				|| (wimaxInfo != null && wimaxInfo.isConnected());
	}

	private class NetworkTask extends AsyncTask<Void, Integer, Void> {

		@Override
		protected Void doInBackground(Void... params) { 

			// 네트워크 상태변동이 있을 때 리스너 호출
			int prevNetworkStatus = currentNetworkStatus;

			if (isConnectedWIFI())
				currentNetworkStatus = NETWORK_STATUS_WIFI;
			else if (isConnectedMobile())
				currentNetworkStatus = NETWORK_STATUS_MOBILE;
			else
				currentNetworkStatus = NETWORK_STATUS_OFFLINE;

			if (prevNetworkStatus != -1 && prevNetworkStatus != currentNetworkStatus)
				publishProgress(currentNetworkStatus, prevNetworkStatus);

			return null;
		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);

			if (listener != null)
				listener.onChangeNetworkStatus(values[0], values[1]);
		}
	}
}
