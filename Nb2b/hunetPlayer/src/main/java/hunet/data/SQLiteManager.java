package hunet.data;

import hunet.drm.models.AccessModel;
import hunet.drm.models.AppSettingsModel;
import hunet.drm.models.DownloadCenterSettingsModel;
import hunet.drm.models.StudyIndexModel;
import hunet.drm.models.StudyProgressModel;
import hunet.drm.models.StudyProgressSyncFailedModel;
import hunet.drm.models.StudySangSangModel;
import hunet.drm.models.TakeCourseModel;
import hunet.drm.models.TakeSangSangModel;
import hunet.library.Utilities;
import hunet.core.EncryptUtility;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.text.TextUtils;

public class SQLiteManager 
{
	protected Context context;
	EncryptUtility ecdc = new EncryptUtility();

//	protected SQLite sQLite;
//	protected SQLiteDatabase db;
	
	private static SQLiteDatabase db;
	private static SQLite mInstance;
	
	
	private static SQLite initialize(Context context){
		if(mInstance == null){
			mInstance = new SQLite(context);
			OpenDB();
		}
		return mInstance;
	}
	
	public SQLiteManager(Context context)
	{	
		this.context = context;
		initialize(context);

//		OpenDB();
	}
	
	private static void OpenDB() {
		if (db == null)
			try{
				db = mInstance.getWritableDatabase();
			}catch(SQLiteException e){
				e.printStackTrace();
			}finally{

			}
	}
	
	public void CloseDB() {
		if (db != null) {
			db.close();
			db = null;
		}
	}
	
	public String GetCurrentTime()
	{
		Date 		curDate = new Date();
		Timestamp 	curTime = new Timestamp(curDate.getTime());
		
		return curTime.toString();
	}
	
	public AppSettingsModel GetAppSettings(int company_seq) {
		Cursor cursor = db.query(SQLite.eAPP_SETTINGS_TABLE_NM, new String[] { "company_seq", "enable_sangsang_player_speedbar" }, "company_seq = ?", new String[] { String.valueOf(company_seq) }, null, null, null);
		String[] arValues = GetCursorValues(cursor);
		cursor.close();
		
		AppSettingsModel model = new AppSettingsModel();
		model.company_seq = "".equals(arValues[0]) ? 0 : Integer.parseInt(arValues[0]);
		model.enable_sangsang_player_speedbar = "".equals(arValues[1]) ? "N" : arValues[1];
		
		return model;
	}
	
	public void SetAppSettings(AppSettingsModel model) {
		AppSettingsModel tempModel = GetAppSettings(model.company_seq);
		
		ContentValues cv = new ContentValues();
		cv.put("company_seq", model.company_seq);
		cv.put("enable_sangsang_player_speedbar", model.enable_sangsang_player_speedbar);

		if(tempModel.company_seq == 0)
			db.insert(SQLite.eAPP_SETTINGS_TABLE_NM, null, cv);
		else
			db.update(SQLite.eAPP_SETTINGS_TABLE_NM, cv, "company_seq = ?", new String[] { String.valueOf(model.company_seq) });
	}
	
	public void SetDownTakeCourseData(TakeCourseModel model)
	{
		TakeCourseModel tempModel 	= GetDownTakeCourseData(model.take_course_seq);
//		SQLiteDatabase 	db 				= sQLite.getWritableDatabase();	
		
		if(tempModel.take_course_seq == 0)
		{
			ContentValues cv = new ContentValues();
			
			cv.put("take_course_seq", model.take_course_seq);
			cv.put("course_cd", model.course_cd);
			cv.put("course_nm", model.course_nm);
			cv.put("study_end_date", model.study_end_date);
			
			db.insert(SQLite.eDOWN_TAKE_COURSE_TABLE_NM, null, cv);
		}
		
//		db.close();
	}
	
	public void SetDownTakeSangSangData(TakeSangSangModel model)
	{
		TakeSangSangModel tempModel 	= GetDownTakeSangSangData(model.goods_id);
//		SQLiteDatabase 	db 				= sQLite.getWritableDatabase();	
		
		if("".equals(tempModel.goods_id))
		{
			ContentValues cv = new ContentValues();
			
			cv.put("goods_id", model.goods_id);
			cv.put("goods_nm", model.goods_nm);			
			cv.put("study_end_date", model.study_end_date);
			
			db.insert(SQLite.eDOWN_TAKE_SANGSANG_TABLE_NM, null, cv);
		}
		
//		db.close();
	}
	
	public void SetDownloadCenterSettings(DownloadCenterSettingsModel model)
	{
		if(model.company_seq == 0)
			return;
		
		DownloadCenterSettingsModel tempModel 	= GetDownloadCenterSettings();
//		SQLiteDatabase 	db 				= sQLite.getWritableDatabase();	
		
		if(tempModel.company_seq == 0)
		{
			ContentValues cv = new ContentValues();
			
			cv.put("company_seq", model.company_seq);
			cv.put("tab1_text", model.tab1_text);
			cv.put("tab2_text", model.tab2_text);
			cv.put("only_imagine_yn", model.only_imagine_yn);		
			
			db.insert(SQLite.eDOWNLOAD_CENTER_SETTINGS, null, cv);
		}
		else
		{		
			
			String strQuery = "update " + SQLite.eDOWNLOAD_CENTER_SETTINGS
					+ " set tab1_text = '" + model.tab1_text
					+ "' , tab2_text = '" + model.tab2_text					
					+ "' , only_imagine_yn = '" + model.only_imagine_yn
					+ "' , company_seq = " + model.company_seq;					
			
			db.execSQL(strQuery);
		}
		
//		db.close();
	}
	
	public DownloadCenterSettingsModel GetDownloadCenterSettings()
	{		
//		SQLiteDatabase db 		= sQLite.getWritableDatabase();
		String[] 	arValues 	= {"", "", "", ""};
		String 		strQuery 	= "SELECT company_seq, tab1_text, tab2_text, only_imagine_yn"
								+ " FROM " + SQLite.eDOWNLOAD_CENTER_SETTINGS;
								
		 
		Cursor cursor 	= db.rawQuery(strQuery, null);
		arValues 		= GetCursorValues(cursor, arValues);
		
		cursor.close();
//		db.close();
		
		DownloadCenterSettingsModel model 	= new DownloadCenterSettingsModel();
		model.company_seq 		= "".equals(arValues[0]) ? 0 : Integer.parseInt(arValues[0]);
		model.tab1_text 			= arValues[1];
		model.tab2_text				= arValues[2];
		model.only_imagine_yn		= arValues[3];
		
		return model;
	}
	
	public TakeCourseModel GetDownTakeCourseData(int take_course_seq)
	{	
//		SQLiteDatabase db 		= sQLite.getWritableDatabase();
		String[] 	arValues 	= {"", "", "", ""};
		String 		strQuery 	= "SELECT take_course_seq, course_cd, course_nm, study_end_date"
								+ " FROM " + SQLite.eDOWN_TAKE_COURSE_TABLE_NM
								+ " WHERE take_course_seq = " + take_course_seq;
		 
		Cursor cursor 	= db.rawQuery(strQuery, null);
		arValues 		= GetCursorValues(cursor, arValues);
		
		cursor.close();
//		db.close();
		
		TakeCourseModel model 	= new TakeCourseModel();
		model.take_course_seq 		= "".equals(arValues[0]) ? 0 : Integer.parseInt(arValues[0]);
		model.course_cd 			= arValues[1];
		model.course_nm				= arValues[2];
		model.study_end_date		= arValues[3];
		
		return model;
	}
	
	public TakeSangSangModel GetDownTakeSangSangData(String goods_id)
	{	
//		SQLiteDatabase db 		= sQLite.getWritableDatabase();
		String[] 	arValues 	= {"", "", ""};
		String 		strQuery 	= "SELECT goods_id, goods_nm, study_end_date"
								+ " FROM " + SQLite.eDOWN_TAKE_SANGSANG_TABLE_NM
								+ " WHERE goods_id = '" + goods_id+"'";
		 
		Cursor cursor 	= db.rawQuery(strQuery, null);
		arValues 		= GetCursorValues(cursor, arValues);
		
		cursor.close();
//		db.close();
		
		TakeSangSangModel model 	= new TakeSangSangModel();
		model.goods_id 		= arValues[0];
		model.goods_nm 			= arValues[1];
		model.study_end_date				= arValues[2];
		
		return model;
	}
	
	public ArrayList<TakeCourseModel> GetDownTakeCourseDataList()
	{
//		SQLiteDatabase db 		= sQLite.getWritableDatabase();
		String 		strQuery 	= "SELECT take_course_seq, course_cd, course_nm, study_end_date"
								+ " FROM " + SQLite.eDOWN_TAKE_COURSE_TABLE_NM
								+ " ORDER BY course_nm ";
		 
		Cursor 				cursor 		= db.rawQuery(strQuery, null);
		ArrayList<String[]> listValue 	= GetCursorListValue(cursor, 4);
		
		cursor.close();
//		db.close();
		
		int nLength = listValue.size();
		
		ArrayList<TakeCourseModel> modelList = new ArrayList<TakeCourseModel>(); 
		
		for(int nIndex = 0; nIndex < nLength; nIndex++)
		{
			String[] arValues = listValue.get(nIndex);
			
			TakeCourseModel model 	= new TakeCourseModel();
			model.take_course_seq 		= "".equals(arValues[0]) ? 0 : Integer.parseInt(arValues[0]);
			model.course_cd 			= arValues[1];
			model.course_nm				= arValues[2];
			model.study_end_date		= arValues[3];
			
			modelList.add(model);
		}	
		
		return modelList;
	}
	
	public ArrayList<TakeSangSangModel> GetDownTakeSangSangDataList()
	{
//		SQLiteDatabase db 		= sQLite.getWritableDatabase();
		String 		strQuery 	= "SELECT goods_id, goods_nm, study_end_date"
								+ " FROM " + SQLite.eDOWN_TAKE_SANGSANG_TABLE_NM
								+ " ORDER BY goods_nm ";
		 
		Cursor 				cursor 		= db.rawQuery(strQuery, null);
		ArrayList<String[]> listValue 	= GetCursorListValue(cursor, 3);
		
		cursor.close();
//		db.close();
		
		int nLength = listValue.size();
		
		ArrayList<TakeSangSangModel> modelList = new ArrayList<TakeSangSangModel>(); 
		
		for(int nIndex = 0; nIndex < nLength; nIndex++)
		{
			String[] arValues = listValue.get(nIndex);
			
			TakeSangSangModel model 	= new TakeSangSangModel();
			model.goods_id 		= arValues[0];
			model.goods_nm 			= arValues[1];			
			model.study_end_date		= arValues[2];
			
			modelList.add(model);
		}	
		
		return modelList;
	}
	
	public void DeleteDownTakeCourseData(int take_course_seq)
	{		
//		SQLiteDatabase db = sQLite.getWritableDatabase();
		String strQuery = "DELETE FROM " + SQLite.eDOWN_TAKE_COURSE_TABLE_NM + " WHERE take_course_seq = " + take_course_seq;
		
		db.execSQL(strQuery);
//		db.close();
	}
	
	public void DeleteDownTakeSangSangData(String goods_id)
	{		
//		SQLiteDatabase db = sQLite.getWritableDatabase();
		String strQuery = "DELETE FROM " + SQLite.eDOWN_TAKE_SANGSANG_TABLE_NM + " WHERE goods_id = '" + goods_id+"'";
		
		db.execSQL(strQuery);
//		db.close();
	}
	
	public int GetDownStudyIndexDataCount(int take_course_seq)
	{
//		SQLiteDatabase db 		= sQLite.getWritableDatabase();
		String 		strQuery 	= "SELECT count(*)"
								+ " FROM " + SQLite.eDOWN_STUDY_INDEX_TABLE_NM
								+ " WHERE take_course_seq = " + take_course_seq;
		
		Cursor 	cursor = db.rawQuery(strQuery, null);
		int 	nCount = 0;
		
		if(cursor.moveToFirst())
			nCount = cursor.getInt(0);
		
		cursor.close();
//		db.close();
		
		return nCount;
	}
	
	public int GetDownStudySangSangDataCount(String goods_id)
	{
//		SQLiteDatabase db 		= sQLite.getWritableDatabase();
		String 		strQuery 	= "SELECT count(*)"
								+ " FROM " + SQLite.eDOWN_STUDY_SANGSANG_TABLE_NM
								+ " WHERE se_goods_id = '" + goods_id +"'";
		
		Cursor 	cursor = db.rawQuery(strQuery, null);
		int 	nCount = 0;
		
		if(cursor.moveToFirst())
			nCount = cursor.getInt(0);
		
		cursor.close();
//		db.close();
		
		return nCount;
	}
		
	public ArrayList<StudyIndexModel> GetDownStudyIndexDataList(int take_course_seq)
	{
//		SQLiteDatabase db 		= sQLite.getWritableDatabase();
		String 		strQuery 	= "SELECT take_course_seq, chapter_no, index_no, index_nm, max_mark_sec, max_view_sec, view_index, "
								+ "study_sec, last_view_sec, IFNULL(full_path, ''), IFNULL(external_memory, '')"
								+ " FROM " + SQLite.eDOWN_STUDY_INDEX_TABLE_NM
								+ " WHERE take_course_seq = " + take_course_seq
								+ " ORDER BY view_index ";
		 
		Cursor 				cursor 		= db.rawQuery(strQuery, null);
		ArrayList<String[]> listValue 	= GetCursorListValue(cursor, 11);
		
		cursor.close();
//		db.close();
		
		int nLength = listValue.size();
		
		ArrayList<StudyIndexModel> modelList = new ArrayList<StudyIndexModel>(); 
		
		for(int nIndex = 0; nIndex < nLength; nIndex++)
		{
			String[] arValues = listValue.get(nIndex);
			
			StudyIndexModel model 	= new StudyIndexModel();
			model.take_course_seq 	= "".equals(arValues[0]) ? 0 : Integer.parseInt(arValues[0]);
			model.chapter_no		= arValues[1];
			model.index_no 			= "".equals(arValues[2]) ? 0 : Integer.parseInt(arValues[2]);
			model.index_nm 			= arValues[3];
			model.max_mark_sec 		= "".equals(arValues[4]) ? 0 : Integer.parseInt(arValues[4]);
			model.max_view_sec		= "".equals(arValues[5]) ? 0 : Integer.parseInt(arValues[5]);
			model.view_index		= "".equals(arValues[6]) ? 0 : Integer.parseInt(arValues[6]);
			model.study_sec			= arValues[7] == null || "".equals(arValues[7]) ? 0 : Integer.parseInt(arValues[7]);
			model.last_view_sec		= arValues[8] == null || "".equals(arValues[8]) ? 0 : Integer.parseInt(arValues[8]);
			model.full_path 		= arValues[9];
			model.external_memory 	= arValues[10];
			
			modelList.add(model);
		}	
		
		return modelList;
	}
	
	public ArrayList<StudySangSangModel> GetDownStudySangSangDataList(String goods_id)
	{
//		SQLiteDatabase db 		= sQLite.getWritableDatabase();
		String 		strQuery 	= "SELECT company_seq, contract_no, user_id, goods_id, contents_seq, contents_nm, view_sec, view_sec_mobile, "
								+ "last_view_sec,view_no,contents_view_sec,se_goods_id, IFNULL(full_path, ''), IFNULL(external_memory, '')"
								+ " FROM " + SQLite.eDOWN_STUDY_SANGSANG_TABLE_NM
								+ " WHERE se_goods_id = '" + goods_id+"'"
								+ " ORDER BY contents_seq ";
		 
		Cursor 				cursor 		= db.rawQuery(strQuery, null);
		ArrayList<String[]> listValue 	= GetCursorListValue(cursor, 14);
		
		cursor.close();
//		db.close();
		
		int nLength = listValue.size();
		
		ArrayList<StudySangSangModel> modelList = new ArrayList<StudySangSangModel>(); 
		
		for(int nIndex = 0; nIndex < nLength; nIndex++)
		{
			String[] arValues = listValue.get(nIndex);
			
			StudySangSangModel model 	= new StudySangSangModel();
			model.company_seq			= "".equals(arValues[0]) ? 0 : Integer.parseInt(arValues[0]);
			model.contract_no			= "".equals(arValues[1]) ? 0 : Integer.parseInt(arValues[1]);
			model.user_id 				= ecdc.Dec(arValues[2]);
			model.goods_id				= arValues[3];
			model.contents_seq 			= "".equals(arValues[4]) ? 0 : Integer.parseInt(arValues[4]);
			model.contents_nm 			= arValues[5];
			model.view_sec 				= "".equals(arValues[6]) ? 0 : Integer.parseInt(arValues[6]);
			model.view_sec_mobile		= "".equals(arValues[7]) ? 0 : Integer.parseInt(arValues[7]);
			model.last_view_sec			= "".equals(arValues[8]) ? 0 : Integer.parseInt(arValues[8]);
			model.view_no				= "".equals(arValues[9]) ? 0 : Integer.parseInt(arValues[9]);
			model.contents_view_sec		= "".equals(arValues[10]) ? 0 : Integer.parseInt(arValues[10]);
			model.se_goods_id			= arValues[11];
			model.full_path				= arValues[12];
			model.external_memory		= arValues[13];
			
			
			modelList.add(model);
		}	
		
		return modelList;
	}
		
	public void SetDownStudyIndexData(StudyIndexModel model)
	{
		if(model.take_course_seq == 0)
			return;
		
		StudyIndexModel tempModel = null;
		
		if(TextUtils.isEmpty(model.external_memory)){
			tempModel 	= GetDownStudyIndexData(model.take_course_seq, model.chapter_no);
		}else{
			tempModel 	= GetDownStudyIndexData(model.take_course_seq, model.chapter_no, model.external_memory);
		}
//		StudyIndexModel tempModel 	= GetDownStudyIndexData(model.take_course_seq, model.chapter_no);
//		SQLiteDatabase 	db 				= sQLite.getWritableDatabase();	
		
		if(tempModel.take_course_seq == 0)
		{
			ContentValues cv = new ContentValues();
			
			cv.put("take_course_seq", model.take_course_seq);
			cv.put("chapter_no", model.chapter_no);
			cv.put("index_no", model.index_no);
			cv.put("index_nm", model.index_nm);
			cv.put("max_mark_sec", model.max_mark_sec);
			cv.put("max_view_sec", model.max_view_sec);
			cv.put("view_index", model.view_index);
			cv.put("study_sec", model.study_sec);
			cv.put("last_view_sec", model.last_view_sec);
			cv.put("full_path", model.full_path);
			cv.put("external_memory", model.external_memory);
			
			db.insert(SQLite.eDOWN_STUDY_INDEX_TABLE_NM, null, cv);
		}
		else
		{	
			int max_view_sec = model.max_view_sec > tempModel.max_view_sec ? model.max_view_sec : tempModel.max_view_sec;
			int last_view_sec = model.last_view_sec > 0 ? model.last_view_sec : tempModel.last_view_sec;
			
			String strQuery = "update " + SQLite.eDOWN_STUDY_INDEX_TABLE_NM
					+ " set max_view_sec = " + max_view_sec+", study_sec = " + model.study_sec + ", last_view_sec=" + last_view_sec
					+ " where take_course_seq = " + model.take_course_seq + " and chapter_no = '" + model.chapter_no + "'";
			
			db.execSQL(strQuery);
		}
		
//		db.close();
	}
	
	public void SetDownStudySangSangData(StudySangSangModel model)
	{
		if("".equals(model.goods_id))
			return;
		
		StudySangSangModel tempModel = null;
		
		if(TextUtils.isEmpty(model.external_memory)){
			tempModel 	= GetDownStudySangSangData(model.goods_id, model.contents_seq);
		}else{
			tempModel 	= GetDownStudySangSangData(model.goods_id, model.contents_seq, model.external_memory);
		}
		
//		SQLiteDatabase 	db 				= sQLite.getWritableDatabase();	
		
		if("".equals(tempModel.goods_id))
		{
			ContentValues cv = new ContentValues();
			cv.put("company_seq", model.company_seq);
			cv.put("contract_no", model.contract_no);
			cv.put("contents_seq", model.contents_seq);
			cv.put("user_id", ecdc.Enc(model.user_id));
			cv.put("goods_id", model.goods_id);
			cv.put("contents_nm", model.contents_nm);
			cv.put("view_sec", model.view_sec);
			cv.put("view_sec_mobile", model.view_sec_mobile);
			cv.put("last_view_sec", model.last_view_sec);
			cv.put("view_no", model.view_no);
			cv.put("contents_view_sec", model.contents_view_sec);
			cv.put("se_goods_id", model.se_goods_id);
			cv.put("full_path", model.full_path);
			cv.put("external_memory", model.external_memory);
			cv.put("last_position_sec",model.last_position_sec);
			db.insert(SQLite.eDOWN_STUDY_SANGSANG_TABLE_NM, null, cv);
		}
		else
		{	
			
			String strQuery = "update " + SQLite.eDOWN_STUDY_SANGSANG_TABLE_NM
					+ " set view_sec = " + model.view_sec+ " ,last_position_sec= "+ model.last_position_sec +", view_sec_mobile = "+model.view_sec_mobile+",last_view_sec= "+model.last_view_sec
					+ " where contents_seq = " + model.contents_seq + " and goods_id = '" + model.goods_id+ "'";
			
			db.execSQL(strQuery);
		}
		
//		db.close();
	}

///////////////////////////////////////	//////////////////////////////////////////////////////////////////////////////////////////	
	
	public void DeleteDownStudyIndexData(int take_course_seq, String chapter_no, String external_memory)
	{		
//		SQLiteDatabase db = sQLite.getWritableDatabase();
		String strQuery = "DELETE FROM " + SQLite.eDOWN_STUDY_INDEX_TABLE_NM 
						+ " WHERE take_course_seq = " + take_course_seq + " and chapter_no = '" + chapter_no + "'" + " and external_memory = '" + external_memory + "'";
		
		db.execSQL(strQuery);
//		db.close();
	}
	
	public void DeleteDownStudySangSangData(String goods_id, int contents_seq, String external_memory)
	{		
//		SQLiteDatabase db = sQLite.getWritableDatabase();
		String strQuery = "DELETE FROM " + SQLite.eDOWN_STUDY_SANGSANG_TABLE_NM 
						+ " WHERE contents_seq = " + contents_seq + " and goods_id = '" + goods_id + "'" + " and external_memory = '" + external_memory + "'";
		
		db.execSQL(strQuery);
//		db.close();
	}

	public StudyIndexModel GetDownStudyIndexData(int take_course_seq, String chapter_no)
	{
//		SQLiteDatabase db 		= sQLite.getWritableDatabase();
		String[] 	arValues 	= {"", "", "", "", "", "", "","", "", "", ""};
		String 		strQuery 	= "SELECT take_course_seq, chapter_no, index_no, index_nm, max_mark_sec, max_view_sec, view_index, "
								+ "study_sec, last_view_sec, IFNULL(full_path, ''), IFNULL(external_memory, '')"
								+ " FROM " + SQLite.eDOWN_STUDY_INDEX_TABLE_NM
								+ " WHERE take_course_seq = " + take_course_seq + " and chapter_no = '" + chapter_no + "'";
		 
		Cursor cursor 	= db.rawQuery(strQuery, null);
		arValues 		= GetCursorValues(cursor, arValues);
		
		cursor.close();
//		db.close();
		
		StudyIndexModel model 	= new StudyIndexModel();
		model.take_course_seq 		= "".equals(arValues[0]) ? 0 : Integer.parseInt(arValues[0]);
		model.chapter_no 			= arValues[1];
		model.index_no 				= "".equals(arValues[2]) ? 0 : Integer.parseInt(arValues[2]);
		model.index_nm 				= arValues[3];
		model.max_mark_sec 			= "".equals(arValues[4]) ? 0 : Integer.parseInt(arValues[4]);
		model.max_view_sec 			= "".equals(arValues[5]) ? 0 : Integer.parseInt(arValues[5]);
		model.view_index 			= "".equals(arValues[6]) ? 0 : Integer.parseInt(arValues[6]);
		model.study_sec 			= arValues[7] == null ||  "".equals(arValues[7]) ? 0 : Integer.parseInt(arValues[7]);
		model.last_view_sec 		= arValues[8] == null ||  "".equals(arValues[8]) ? 0 : Integer.parseInt(arValues[8]);
		model.full_path 			= arValues[9];
		model.external_memory 		= arValues[10];
		
		return model;
	}
	
	public StudyIndexModel GetDownStudyIndexData(int take_course_seq, String chapter_no, String external_memory)
	{
//		SQLiteDatabase db 		= sQLite.getWritableDatabase();
		String[] 	arValues 	= {"", "", "", "", "", "", "","", "", "", ""};
		String 		strQuery 	= "SELECT take_course_seq, chapter_no, index_no, index_nm, max_mark_sec, max_view_sec, view_index, "
								+ "study_sec, last_view_sec, IFNULL(full_path, ''), IFNULL(external_memory, '')"
								+ " FROM " + SQLite.eDOWN_STUDY_INDEX_TABLE_NM
								+ " WHERE take_course_seq = " + take_course_seq + " and chapter_no = '" + chapter_no + "'" + " and external_memory = '" + external_memory + "'";
		 
		Cursor cursor 	= db.rawQuery(strQuery, null);
		arValues 		= GetCursorValues(cursor, arValues);
		
		cursor.close();
//		db.close();
		
		StudyIndexModel model 	= new StudyIndexModel();
		model.take_course_seq 		= "".equals(arValues[0]) ? 0 : Integer.parseInt(arValues[0]);
		model.chapter_no 			= arValues[1];
		model.index_no 				= "".equals(arValues[2]) ? 0 : Integer.parseInt(arValues[2]);
		model.index_nm 				= arValues[3];
		model.max_mark_sec 			= "".equals(arValues[4]) ? 0 : Integer.parseInt(arValues[4]);
		model.max_view_sec 			= "".equals(arValues[5]) ? 0 : Integer.parseInt(arValues[5]);
		model.view_index 			= "".equals(arValues[6]) ? 0 : Integer.parseInt(arValues[6]);
		model.study_sec 			= arValues[7] == null ||  "".equals(arValues[7]) ? 0 : Integer.parseInt(arValues[7]);
		model.last_view_sec 		= arValues[8] == null ||  "".equals(arValues[8]) ? 0 : Integer.parseInt(arValues[8]);
		model.full_path 			= arValues[9];
		model.external_memory 		= arValues[10];
		
		return model;
	}
	
	public StudySangSangModel GetDownStudySangSangData(String goods_id,int contents_seq)
	{
//		SQLiteDatabase db 		= sQLite.getWritableDatabase();
		String[] 	arValues 	= {"", "", "", "", "", "", "","","","","","", "", ""};
		String 		strQuery 	= "SELECT company_seq,contract_no,user_id, goods_id, contents_seq, contents_nm, view_sec, view_sec_mobile,"
								+ " last_view_sec,view_no,contents_view_sec,se_goods_id, IFNULL(full_path, ''), IFNULL(external_memory, '')"
								+ " FROM " + SQLite.eDOWN_STUDY_SANGSANG_TABLE_NM
								+ " WHERE goods_id = '" + goods_id+ "' and contents_seq = " + contents_seq;
		 
		Cursor cursor 	= db.rawQuery(strQuery, null);
		arValues 		= GetCursorValues(cursor, arValues);
		
		cursor.close();
//		db.close();
		
		StudySangSangModel model 	= new StudySangSangModel();
		model.company_seq			= "".equals(arValues[0]) ? 0 : Integer.parseInt(arValues[0]);
		model.contract_no			= "".equals(arValues[1]) ? 0 : Integer.parseInt(arValues[1]);
		model.user_id 				= ecdc.Dec(arValues[2]);
		model.goods_id				= arValues[3];
		model.contents_seq 			= "".equals(arValues[4]) ? 0 : Integer.parseInt(arValues[4]);
		model.contents_nm 			= arValues[5];
		model.view_sec 				= "".equals(arValues[6]) ? 0 : Integer.parseInt(arValues[6]);
		model.view_sec_mobile		= "".equals(arValues[7]) ? 0 : Integer.parseInt(arValues[7]);
		model.last_view_sec			= "".equals(arValues[8]) ? 0 : Integer.parseInt(arValues[8]);
		model.view_no				= "".equals(arValues[9]) ? 0 : Integer.parseInt(arValues[9]);
		model.contents_view_sec		= "".equals(arValues[10]) ? 0 : Integer.parseInt(arValues[10]);
		model.se_goods_id			= arValues[11];
		model.full_path 			= arValues[12];
		model.external_memory 		= arValues[13];
		
		return model;
	}
	
	public StudySangSangModel GetDownStudySangSangData(String goods_id,int contents_seq, String external_memory)
	{
//		SQLiteDatabase db 		= sQLite.getWritableDatabase();
		String[] 	arValues 	= {"", "", "", "", "", "", "","","","","","", "", ""};
		String 		strQuery 	= "SELECT company_seq,contract_no,user_id, goods_id, contents_seq, contents_nm, view_sec, view_sec_mobile,"
								+ " last_view_sec,view_no,contents_view_sec,se_goods_id, IFNULL(full_path, ''), IFNULL(external_memory, '')"
								+ " FROM " + SQLite.eDOWN_STUDY_SANGSANG_TABLE_NM
								+ " WHERE goods_id = '" + goods_id+ "' and contents_seq = " + contents_seq + " and external_memory = '" + external_memory + "'";
		 
		Cursor cursor 	= db.rawQuery(strQuery, null);
		arValues 		= GetCursorValues(cursor, arValues);
		
		cursor.close();
//		db.close();
		
		StudySangSangModel model 	= new StudySangSangModel();
		model.company_seq			= "".equals(arValues[0]) ? 0 : Integer.parseInt(arValues[0]);
		model.contract_no			= "".equals(arValues[1]) ? 0 : Integer.parseInt(arValues[1]);
		model.user_id 				= ecdc.Dec(arValues[2]);
		model.goods_id				= arValues[3];
		model.contents_seq 			= "".equals(arValues[4]) ? 0 : Integer.parseInt(arValues[4]);
		model.contents_nm 			= arValues[5];
		model.view_sec 				= "".equals(arValues[6]) ? 0 : Integer.parseInt(arValues[6]);
		model.view_sec_mobile		= "".equals(arValues[7]) ? 0 : Integer.parseInt(arValues[7]);
		model.last_view_sec			= "".equals(arValues[8]) ? 0 : Integer.parseInt(arValues[8]);
		model.view_no				= "".equals(arValues[9]) ? 0 : Integer.parseInt(arValues[9]);
		model.contents_view_sec		= "".equals(arValues[10]) ? 0 : Integer.parseInt(arValues[10]);
		model.se_goods_id			= arValues[11];
		model.full_path 			= arValues[12];
		model.external_memory 		= arValues[13];
		
		return model;
	}
	
	public ArrayList<StudyIndexModel> GetDownStudyDataList()
	{
		String 		strQuery 	= "SELECT *"
								+ " FROM " + SQLite.eDOWN_STUDY_INDEX_TABLE_NM
								+ " WHERE study_sec > 0";

		 
		Cursor 				cursor 		= db.rawQuery(strQuery, null);
		ArrayList<String[]> listValue 	= GetCursorListValue(cursor, 11);
		
		cursor.close();
//		db.close();
		
		int nLength = listValue.size();
		
		ArrayList<StudyIndexModel> modelList = new ArrayList<StudyIndexModel>(); 
		
		for(int nIndex = 0; nIndex < nLength; nIndex++)
		{
			String[] arValues = listValue.get(nIndex);
			
			StudyIndexModel model 	= new StudyIndexModel();
			model.take_course_seq 	= "".equals(arValues[0]) ? 0 : Integer.parseInt(arValues[0]);
			model.chapter_no		= arValues[1];
			model.index_no 			= "".equals(arValues[2]) ? 0 : Integer.parseInt(arValues[2]);
			model.index_nm 			= arValues[3];
			model.max_mark_sec 		= "".equals(arValues[4]) ? 0 : Integer.parseInt(arValues[4]);
			model.max_view_sec		= "".equals(arValues[5]) ? 0 : Integer.parseInt(arValues[5]);
			model.view_index		= "".equals(arValues[6]) ? 0 : Integer.parseInt(arValues[6]);
			model.study_sec			= arValues[7] == null || "".equals(arValues[7]) ? 0 : Integer.parseInt(arValues[7]);
			model.last_view_sec 	= arValues[8] == null ||  "".equals(arValues[8]) ? 0 : Integer.parseInt(arValues[8]);
			model.full_path 		= arValues[9];
			model.external_memory 	= arValues[10];
			
			modelList.add(model);
		}	
		
		return modelList;
	}
	
	public StudyProgressModel GetStudyProgress()
	{
//		SQLiteDatabase db 		= sQLite.getWritableDatabase();
		String[] 	arValues 	= {"", "", "", "", ""};
		String 		strQuery 	= "SELECT study_progress_seq, url, try_count, reg_id, reg_date "
								+ " FROM " + SQLite.eSTUDYPROGRESS_TABLE_NM
								//+ " WHERE try_count < 10 "
								+ " ORDER BY study_progress_seq asc LIMIT 1";
		 
		Cursor cursor 	= db.rawQuery(strQuery, null);
		arValues 		= GetCursorValues(cursor, arValues);
		
		cursor.close();
//		db.close();
		
		StudyProgressModel model 	= new StudyProgressModel();
		model.study_progress_seq 	= "".equals(arValues[0]) ? 0 : Integer.parseInt(arValues[0]);
		model.url 					= arValues[1];
		model.try_count 			= "".equals(arValues[2]) ? 0 : Integer.parseInt(arValues[2]);
		model.reg_id 				= arValues[3];
		model.reg_date 				= arValues[4];
		
		return model; 
	}
	
	public void InsertStudyProgress(String url, String reg_id)
	{
//		SQLiteDatabase 	db = sQLite.getWritableDatabase();
		ContentValues 	cv = new ContentValues();
		
		cv.put("url", url);
		cv.put("try_count", "0");
		cv.put("reg_id", reg_id);
		cv.put("reg_date", GetCurrentTime());
		
		db.insert(SQLite.eSTUDYPROGRESS_TABLE_NM, null, cv);		
//		db.close();
	}
	
	public void UpdateStudyProgress(int study_progress_seq)
	{
//		SQLiteDatabase db = sQLite.getWritableDatabase();
		String strQuery = "update " + SQLite.eSTUDYPROGRESS_TABLE_NM
				+ " set try_count = try_count + 1 "
				+ " where study_progress_seq = " + study_progress_seq + " ;";
		
		db.execSQL(strQuery);
//		db.close();
	}
	
	public void DeleteStudyProgress(int study_progress_seq)
	{
//		SQLiteDatabase db = sQLite.getWritableDatabase();
		String strQuery = "DELETE FROM " + SQLite.eSTUDYPROGRESS_TABLE_NM + " WHERE study_progress_seq = " + study_progress_seq + " ";
		
		db.execSQL(strQuery);
//		db.close();
	}
	
	public ArrayList<StudyProgressSyncFailedModel> getStudyProgressFailedInfoList() {
		String[] columns = new String[] { "seq", "url", "params", "reg_date" };
		Cursor cursor = db.query(SQLite.TABLE_MLC_PROGRESS_SYNC_FAILED_LIST, columns, null, null, null, null, "seq asc");
		ArrayList<Hashtable<String, String>> arValues = getCursorListValue(cursor);
		cursor.close();
		
		ArrayList<StudyProgressSyncFailedModel> retList = new ArrayList<StudyProgressSyncFailedModel>();
		for (Hashtable<String, String> entity : arValues) {
			StudyProgressSyncFailedModel model = new StudyProgressSyncFailedModel();
			model.seq = getValue(entity, "seq", 0);
			model.url = getValue(entity, "url", "");
			model.params = getValue(entity, "params", "");
			model.reg_date = getValue(entity, "reg_date", "");
			retList.add(model);
		}
		
		return retList;
	}
	
	public StudyProgressSyncFailedModel getStudyProgressFailedInfo(int seq) {
		String[] columns = new String[] { "seq", "url", "params", "reg_date" };
		Cursor cursor = db.query(SQLite.TABLE_MLC_PROGRESS_SYNC_FAILED_LIST, columns, "seq=?", new String[] { String.valueOf(seq) }, null, null, "seq asc");
		Hashtable<String, String> arValues = getCursorValue(cursor);
		cursor.close();
		
		StudyProgressSyncFailedModel model = new StudyProgressSyncFailedModel();
		model.seq = getValue(arValues, "seq", 0);
		model.url = getValue(arValues, "url", "");
		model.params = getValue(arValues, "params", "");
		model.reg_date = getValue(arValues, "reg_date", "");
		
		return model;
	}
	
	public void setStudyProgressFailedInfo(StudyProgressSyncFailedModel model) {
		StudyProgressSyncFailedModel tmpModel = getStudyProgressFailedInfo(model.seq);

		ContentValues cv = new ContentValues();
		cv.put("url", model.url);
		cv.put("params", model.params);
		cv.put("reg_date", Utilities.GetFormattedDateString(new Date(), "yyyy-MM-dd HH:mm:ss"));
		
		if (tmpModel.seq == 0)
			db.insert(SQLite.TABLE_MLC_PROGRESS_SYNC_FAILED_LIST, null, cv);
		else
			db.update(SQLite.TABLE_MLC_PROGRESS_SYNC_FAILED_LIST, cv, "seq=?", new String[] { String.valueOf(tmpModel.seq) });
	}
	
	public void removeStudyProgressFailedInfo(int seq) {
		db.delete(SQLite.TABLE_MLC_PROGRESS_SYNC_FAILED_LIST, "seq=?", new String[] { String.valueOf(seq) });
	}
	
	/***
	 * 저장된 App 3G 접속 정보 조회.
	 * @return
	 */
	public String[] Select_Access_Data() {
		
//		SQLiteDatabase db = sQLite.getWritableDatabase();
		String[] rtnValues = {"", "", "", "", ""};
		//String[] colValues = {"id", "access_text", "access_mov"};
		
		Cursor cursor = db.rawQuery("SELECT id, access_text, access_mov, IFNULL(push_type, ''),"
						+ " IFNULL(external_memory, '') FROM " + SQLite.eACCESS_TABLE_NM + " WHERE id = '1'", null);
		
		while(cursor.moveToNext()) {
			rtnValues[0] = cursor.getString(0);
			rtnValues[1] = cursor.getString(1);
			rtnValues[2] = cursor.getString(2);
			rtnValues[3] = cursor.getString(3);
			rtnValues[4] = cursor.getString(4);
			break;
		}
		
//		db.close();
				
		return rtnValues;
	}
	
	/**
	 * 3G/4G 접속 사용여부 및 알리미사용 관련 환경설정 정보를 가져옵니다.
	 * @return
	 */
	public AccessModel GetAccessData()
	{		
//		SQLiteDatabase db 		= sQLite.getWritableDatabase();
		String[] 	arValues 	= { "", "", "", "", "" };
		String 		strQuery 	= "SELECT id, access_text, access_mov, IFNULL(push_type, ''), IFNULL(external_memory, '') ";
		strQuery				+= " FROM " + SQLite.eACCESS_TABLE_NM + " WHERE id = '1'";
		
		Cursor cursor 	= db.rawQuery(strQuery, null);
		arValues 		= GetCursorValues(cursor, arValues);
		
		cursor.close();
//		db.close();
		
		AccessModel model 		= new AccessModel();
		model.id 				= arValues[0];
		model.access_text 		= arValues[1];
		model.access_mov 		= arValues[2];
		model.push_type 		= arValues[3];
		model.external_memory 	= arValues[4];
		
		return model;
	}
	
	/**
	 * 3G/4G 접속 사용여부 및 알리미사용 관련 환경설정 정보를 적용합니다.
	 * @param model
	 */
	public void SetAccessData(AccessModel model)
	{
		AccessModel 	tempModel 	= GetAccessData();
//		SQLiteDatabase 	db 			= sQLite.getWritableDatabase();	
		
		if("".equals(tempModel.id))
		{
			ContentValues cv = new ContentValues();
			
			cv.put("id", "1");
			cv.put("access_text", model.access_text);
			cv.put("access_mov", model.access_mov);
			cv.put("push_type", model.push_type);
			cv.put("external_memory", model.external_memory);
			
			db.insert(SQLite.eACCESS_TABLE_NM, null, cv);
		}
		else
		{
			String strQuery = "update " + SQLite.eACCESS_TABLE_NM
					+ " set access_text = '" + model.access_text + "'"
					+ " , access_mov = '" + model.access_mov + "'"
					+ " , push_type = '" + model.push_type + "'"
					+ " , external_memory = '" + model.external_memory + "'"
					+ " where id = '1' ;";
			
			db.execSQL(strQuery);
		}
		
//		db.close();
	}

	protected ArrayList<String[]> GetCursorListValue(Cursor cursor, int nValueLen)
	{
		ArrayList<String[]> tempList = new ArrayList<String[]>();
				
		while(cursor.moveToNext()) 
		{
			String[] arTempValue = new String[nValueLen];
			
			for(int nIndex = 0; nIndex < nValueLen; nIndex++)
			{
				arTempValue[nIndex] = cursor.getString(nIndex);
				
			}
			
			tempList.add(arTempValue);
		}
		
		return tempList;
	}

	protected String[] GetCursorValues(Cursor cursor, String[] arValues)
	{
		int nLength = arValues.length;
		
		while(cursor.moveToNext()) 
		{
			for(int nIndex = 0; nIndex < nLength; nIndex++)
				arValues[nIndex] = cursor.getString(nIndex);
		}
		
		return arValues;
	}
	
	protected String[] GetCursorValues(Cursor cursor)
	{
		String[] arValues = new String[cursor.getColumnCount()];
		
		for (int i = 0; i < arValues.length; i++)
			arValues[i] = "";

		while(cursor.moveToNext()) 
		{
			for(int i = 0; i < cursor.getColumnCount(); i++)
				arValues[i] = cursor.getString(i);
		}
		
		return arValues;
	}
	
	protected String getValue(Hashtable<String, String> entity, String key, String defValue) {
		return getNullorEmptyValue(entity.get(key), defValue);
	}

	protected int getValue(Hashtable<String, String> entity, String key, int defValue) {
		return getNullorEmptyValue(entity.get(key), defValue);
	}
	
	private String getNullorEmptyValue(String value, String defValue) {
		if (null == value || "".equals(value.trim()))
			return defValue;

		return value;
	}

	private int getNullorEmptyValue(String value, int defValue) {
		if (null == value || "".equals(value.trim()))
			return defValue;

		return Integer.parseInt(value);

	}

	protected Hashtable<String, String> getCursorValue(Cursor cursor) {
		Hashtable<String, String> retVal = new Hashtable<String, String>();

		while(cursor.moveToNext()) {
			for (int i = 0; i < cursor.getColumnCount(); i++)
				retVal.put(cursor.getColumnName(i), cursor.getString(i));
		}

		return retVal;
	}

	protected ArrayList<Hashtable<String, String>> getCursorListValue(Cursor cursor) {
		ArrayList<Hashtable<String, String>> retVal = new ArrayList<Hashtable<String,String>>();

		while(cursor.moveToNext()) {
			Hashtable<String, String> entity = new Hashtable<String, String>();
			
			for (int i = 0; i < cursor.getColumnCount(); i++)
				entity.put(cursor.getColumnName(i), cursor.getString(i));
			
			retVal.add(entity);
		}

		return retVal;
	}
}
