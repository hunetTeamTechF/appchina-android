package hunet.net;

import android.os.AsyncTask;

public class AsyncHttpRequestData 
{
	private IAsyncHttpRequestData eventListener = null;
	
	public void SetEventListener(IAsyncHttpRequestData eventListener)
	{
		this.eventListener = eventListener;
	}
	
	public boolean Request(int nId, String strUrl, String strParam, Object objTag)
	{
		AsyncRequestData asynReq 	= new AsyncRequestData();
		HttpData data 			= new HttpData();
		data.id 					= nId;
		data.url 				= strUrl;
		data.param 			= strParam;
		data.tag				= objTag;
		android.os.Process.setThreadPriority(9);
		asynReq.execute(data);
		
		return true;
	}
	
	private class AsyncRequestData extends AsyncTask<HttpData, Void, HttpData>
    {
    	@Override
    	protected HttpData doInBackground(HttpData... datas)
    	{
    		HttpData httpData = datas[0];
    		    			
    		return HttpRequester.GetPostData(httpData);
    	}
    	
    	@Override
    	protected void onPostExecute(HttpData result)
    	{
    		AsyncRequestDataResult(result);
    	}
    }

	private void AsyncRequestDataResult(HttpData result)
	{
		if(eventListener == null)
			return;
		
		eventListener.AsyncRequestDataResult(result);
	}	
}
