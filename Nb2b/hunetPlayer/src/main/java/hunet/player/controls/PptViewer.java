package hunet.player.controls;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import hunet.library.hunetplayer.R;
import hunet.player.interfaces.IPptViewerEventListener;

public class PptViewer extends LinearLayout implements View.OnClickListener {
	private IPptViewerEventListener pptEventListener;
	private ImageButton btnLeft;
	private ImageButton btnRight;
	private ImageButton btnPptPlay;
	private ImageButton btnClose;
	private LinearLayout swipe;
	private WebView pptWebView;

	public PptViewer(Context context, AttributeSet attrs) {
		super(context, attrs);

		LayoutInflater li = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		li.inflate(R.layout.library_player_ppt_viewer, this, true);
		btnLeft = (ImageButton) findViewById(R.id.btnLeft);
		btnRight = (ImageButton) findViewById(R.id.btnRight);
		btnPptPlay = (ImageButton) findViewById(R.id.btnPptPlay);
		pptWebView = (WebView) findViewById(R.id.pptWebView);
		swipe = (LinearLayout)findViewById(R.id.swipe);
		btnClose = (ImageButton) findViewById(R.id.btnClose);
		btnLeft.setOnClickListener(this);
		btnRight.setOnClickListener(this);
		btnPptPlay.setOnClickListener(this);
		btnClose.setOnClickListener(this);
		/*
		swipe.setOnTouchListener(new OnSwipeTouchListener() {
			public void onSwipeRight(float delta) {
				pptEventListener.OnPptLeftMove();
			}

			public void onSwipeLeft(float delta) {
				pptEventListener.OnPptRightMove();
			}
		});
		*/

	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	}

	private void InitView() {

	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		InitView();
	}

	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		super.onLayout(changed, l, t, r, b);
	}

	public void SetLoadWebView(String strImgUrl) {
		if ("".equals(strImgUrl) || "notPt".equals(strImgUrl))
			return;

		String strLoadUrl = "http://study.hunet.co.kr/mobile/mobilePtGate.aspx?imgPtUrl=";

		try {
			strLoadUrl += URLEncoder.encode(strImgUrl, "EUC_KR");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		if (strLoadUrl.equals(pptWebView.getUrl()))
			return;

		pptWebView.loadUrl(strLoadUrl);
	}

	public void SetEventListener(IPptViewerEventListener eventListener) {
		this.pptEventListener = eventListener;
	}

	@Override
	public void onClick(View view) {
		if (R.id.btnLeft == view.getId())
			pptEventListener.OnPptLeftMove();
		else if (R.id.btnRight == view.getId())
			pptEventListener.OnPptRightMove();
		else if (R.id.btnPptPlay == view.getId())
			pptEventListener.OnPptPlay();
		else if (R.id.btnClose == view.getId())
			pptEventListener.OnPptClose();

	}

}
