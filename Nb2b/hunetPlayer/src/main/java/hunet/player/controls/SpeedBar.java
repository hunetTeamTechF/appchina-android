package hunet.player.controls;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import hunet.library.hunetplayer.R;
import hunet.player.interfaces.ISpeedEventListener;

public class SpeedBar extends LinearLayout implements View.OnClickListener
{	
	private ISpeedEventListener speedEventListener = null;
	private ImageButton	btnMediaSpeedDown = null;
	private ImageButton btnMediaSpeedUp = null;	
	private LinearLayout speedLayout = null;
	private Rect rctLayout	= new Rect();
	private int volumeWidth = 0;
	
	public SpeedBar(Context context, AttributeSet attrs) 
	{
		super(context, attrs);
		
		LayoutInflater li = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		li.inflate(R.layout.library_player_speed_bar, this, true);
		
		btnMediaSpeedDown		= (ImageButton)findViewById(R.id.btnMediaSpeedDown);
		btnMediaSpeedUp 	= (ImageButton)findViewById(R.id.btnMediaSpeedUp);
		
		speedLayout = (LinearLayout)findViewById(R.id.speedLayout);
		btnMediaSpeedDown.setOnClickListener(this);
		btnMediaSpeedUp.setOnClickListener(this);
	}
	
	void SetEventListener(ISpeedEventListener speedEventListener)
	{
		this.speedEventListener = speedEventListener;
	}
	
	private void InitView()
	{
		speedLayout.getGlobalVisibleRect(rctLayout);		
	}
	
	@Override
	public void onWindowFocusChanged(boolean hasFocus)
	{
		InitView();
	}

//	public int GetValue()
//	{
//		if(volumeWidth == 0)
//			return 0;
//		return 0;
//
//	}
//
//	public void SetValue(int nValue)
//	{
//
//	}
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) 
	{
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);	
	}
	
	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b)
	{
		super.onLayout(changed, l, t, r, b);
	}

//	public boolean onTouch(View view, MotionEvent event) 
//	{		
//		if(R.id.btnMediaSpeedDown == view.getId())
//			speedEventListener.OnSpeedDown();
//		else if(R.id.btnMediaSpeedUp == view.getId())
//			speedEventListener.OnSpeedUp();				
//		return true;
//	}

	@Override
	public void onClick(View view) {
		if(R.id.btnMediaSpeedDown == view.getId())
			speedEventListener.OnSpeedDown();
		else if(R.id.btnMediaSpeedUp == view.getId())
			speedEventListener.OnSpeedUp();	
		
	}
}
