package hunet.player.controls;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import hunet.library.hunetplayer.R;
import hunet.player.interfaces.IVolumeEventListener;

public class VolumeBar extends LinearLayout implements View.OnTouchListener
{
	private IVolumeEventListener m_VolumeEventListener;
	
	private LinearLayout 	volumeMain			= null;
	private LinearLayout 	volumeBackground 		= null;
	private ImageView 		volumePoint			= null;
	private Rect 			rctVolumeBackgound 	= new Rect();
	private int 			volumeWidth			= 0;
	
	public VolumeBar(Context context, AttributeSet attrs) 
	{
		super(context, attrs);
		
		LayoutInflater li = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		li.inflate(R.layout.library_player_volume_bar, this, true);
		
		volumeMain		= (LinearLayout)findViewById(R.id.volume_main);
		volumeBackground 	= (LinearLayout)findViewById(R.id.volume_bg_bar);
		volumePoint 		= (ImageView)findViewById(R.id.volume_point);
		
		volumeMain.setOnTouchListener(this);		
		volumePoint.setOnTouchListener(this);
	}
	
	void SetEventListener(IVolumeEventListener volumeEventListener)
	{
		m_VolumeEventListener = volumeEventListener;
	}
	
	private void InitView()
	{
		volumeBackground.getGlobalVisibleRect(rctVolumeBackgound);
		rctVolumeBackgound.bottom -= volumePoint.getHeight();
		volumeWidth = rctVolumeBackgound.bottom - rctVolumeBackgound.top;
	}
	
	@Override
	public void onWindowFocusChanged(boolean hasFocus)
	{
		InitView();
	}

//	public int GetValue()
//	{
//		if(volumeWidth == 0)
//			return 0;
//
//		LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)volumePoint.getLayoutParams();
//		return (int)Math.round(((double)(params.topMargin) / (double)(volumeWidth)) * 100.0);
//	}

	void SetValue(int nValue)
	{
		LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)volumePoint.getLayoutParams();
		params.topMargin = volumeWidth - (int)Math.round(((double)volumeWidth * (double)(nValue)) / 100);
		volumePoint.setLayoutParams(params);
	}
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) 
	{
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);	
	}
	
	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b)
	{
		super.onLayout(changed, l, t, r, b);
	}

	@Override
	public boolean onTouch(View view, MotionEvent event) 
	{				
		if(m_VolumeEventListener != null)
			m_VolumeEventListener.onTouch(this, event);
				
		if(R.id.volume_main == view.getId()) return true;
		else if(R.id.volume_point == view.getId()) 
		{				
			InitView();
			
			int nPosition = (int)event.getRawY();
			
			if(nPosition < rctVolumeBackgound.top)
				nPosition = 0;
			else if(nPosition > rctVolumeBackgound.bottom)
				nPosition = volumeWidth;
			else
				nPosition -= rctVolumeBackgound.top;
			
			int nPostionPercent = (int)Math.round(((double)(volumeWidth - nPosition) / (double)volumeWidth) * 100.0);
			
			LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)view.getLayoutParams();
			params.topMargin = nPosition;
			view.setLayoutParams(params);
			
			if(m_VolumeEventListener != null)
				m_VolumeEventListener.OnChangeVolume(this, nPostionPercent);
		}
			return true;		
	}
}
