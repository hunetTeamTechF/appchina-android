package hunet.player.controls;


import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import java.util.ArrayList;

import hunet.library.Utilities;
import hunet.library.hunetplayer.R;
import hunet.player.interfaces.IIndexViewerEventListener;
import hunet.player.model.MarkDataModel;

public class IndexViewer extends LinearLayout implements View.OnClickListener{
	private IIndexViewerEventListener indexEventListener;
	private ImageButton btnClose;
	private LinearLayout layoutIndexContents;
	
	public IndexViewer(Context context, AttributeSet attrs) 
	{
		super(context, attrs);
		
		LayoutInflater li = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		li.inflate(R.layout.library_player_index_viewer, this, true);		
		btnClose = (ImageButton)findViewById(R.id.btnClose);	
		layoutIndexContents = (LinearLayout)findViewById(R.id.layoutIndexContents);
		
		btnClose.setOnClickListener(this);
		
	}
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) 
	{
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);	
	}
	private void InitView()
	{
		
	}
	
	@Override
	public void onWindowFocusChanged(boolean hasFocus)
	{
		InitView();
	}
	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b)
	{
		super.onLayout(changed, l, t, r, b);
	}
	public void SetIndexContetns(ArrayList<MarkDataModel> markList)
	{
		if (markList != null
				&& markList.size() > 0) {
			int nLength = markList.size();

			for (int nIndex = 0; nIndex < nLength; nIndex++) {
				MarkDataModel data = markList.get(nIndex);
				CreateIndexItem(data);				
			}
		}		
	}
	private void CreateIndexItem(final MarkDataModel data) {
		/*
		LinearLayout ll_Index = new LinearLayout(this.getContext());
		final ImageView imgIndexView = new ImageView(this.getContext()); // 목차 봤는지, 안 봤는지 구분
															// 이미지
		TextView tvIndex = new TextView(this.getContext()); // 목차 텍스트뷰		
		imgIndexView.setLayoutParams(new Gallery.LayoutParams(27, 27));		
		imgIndexView.setBackgroundResource(R.drawable.l_btn_index_next);
	

		// 목차 텍스 트 설정
		tvIndex.setLayoutParams(new Gallery.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
		tvIndex.setTextSize(18);
		tvIndex.setPadding(5, 0, 0, 0);
		tvIndex.setTag(data.markValue);

		if ("".equals(data.markNm) == false) {
			String strTime = "[" + Utilities.GetTimeString(data.markValue, false) + "]";
			tvIndex.setText(strTime + " " + data.markNm);
			tvIndex.setTextColor(Color.WHITE);
		}
		

		ll_Index.setLayoutParams(new Gallery.LayoutParams(
				LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
		ll_Index.setOrientation(LinearLayout.HORIZONTAL);
		ll_Index.addView(imgIndexView);
		ll_Index.addView(tvIndex);
		ll_Index.setPadding(0, data.index == 0 ? 14 : 32, 0, 0);
		*/
		IndexViewerContents temp = new IndexViewerContents(this.getContext(),null);
		String strTime = "[" + Utilities.GetTimeString(data.markValue, false) + "]";
		temp.SetView(strTime,data.markNm);
		//temp.setLayoutParams(new Gallery.LayoutParams(LayoutParams.FILL_PARENT,DPFromPixel(45)));
		temp.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				indexEventListener.OnIndexChnaged(data.markValue);
				
			}
		});
		layoutIndexContents.addView(temp);		
	}
//	public int DPFromPixel(int pixel)
//	  {
//	    float scale = this.getResources().getDisplayMetrics().density;
//
//	    return (int)(pixel / 1.5f * scale);
//	  }
	public void SetEventListener(IIndexViewerEventListener eventListener)
	{
		this.indexEventListener = eventListener;
	}
	

	@Override
	public void onClick(View view) {		
		if(R.id.btnClose == view.getId())
			indexEventListener.OnIndexClose();
		
	}

}
