package hunet.player.model;

import java.util.ArrayList;



import hunet.data.*;
import hunet.player.interfaces.IPlayerStudyDataEventListener;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;

public abstract class PlayerModelBase 
{
	protected IPlayerStudyDataEventListener m_EventListener = null;
	protected SQLiteManager	m_SqlManager;
	protected Intent				m_StudyProgressService;
	protected Activity				m_ParentActivity;	
	public MarkDataModel  m_CurMarkData;	
	public ArrayList<MarkDataModel> m_arMarkList;	
	public String title = "";
	public PlayerModelBase(Activity activity)
	{
		//Log.d("PlayerStudyData","PlayerStudyData");
		m_ParentActivity		= activity;
		m_SqlManager			= new SQLiteManager(activity);
		m_StudyProgressService 	= new Intent(activity, hunet.player.service.SyncProgressService.class);
	}
	
	public void SetEventListener(IPlayerStudyDataEventListener listner)
	{
		this.m_EventListener = listner;		
	}
	
	public abstract boolean CheckProgressRestriction();
	
	public abstract void SaveProgress(int lastViewSec, int studySec);
	
	public abstract int GetLastViewSec();

	public abstract int GetLastPositionSec();
	
	public abstract String GetPlayerUrl();

	public abstract String GetQuizUrl();

	public abstract String GetQuizYn();
	
	public abstract boolean CheckSendProgress(int lastViewSec, int studySec);
	
	public abstract boolean HasPpt();
	
	public abstract boolean UseSpeeedControl();
}
