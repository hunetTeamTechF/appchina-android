package hunet.player.model;


import hunet.drm.models.StudySangSangModel;

import android.app.Activity;
import android.os.Bundle;

public class PlayerOSModel extends PlayerModelBase 
{
	
	private StudySangSangModel m_DownStudySangSangModel;
	private String 				m_strFilePath 	= "";
	private String 				userId = "";
	private String 				goodsId 		= "";
	private int 				contentsSeq	= 0;
	private String				m_external_memory = "";
	private int 				lastPositionSec = 0;
	
	private boolean useSpeedBar = false;
	
	public PlayerOSModel(Activity activity) 
	{
		super(activity);
		
		Bundle bundle = activity.getIntent().getExtras();
        
        if(bundle == null)
        	return;
        
        userId 					= bundle.getString("userId");
        contentsSeq 			= bundle.getInt("contentsSeq",0);
        goodsId 				= bundle.getString("goodsId");
//        m_strFilePath 			= String.format("%s%s_%d.mp4", Utilities.GetExternalDir(), userId, contentsSeq);
        m_external_memory		= bundle.getString("external_memory");
        m_strFilePath 			= bundle.getString("full_path");
        m_DownStudySangSangModel 	= m_SqlManager.GetDownStudySangSangData(goodsId, contentsSeq, m_external_memory);
		lastPositionSec = bundle.getInt("lastPositionSec");
        int company_seq = m_DownStudySangSangModel.company_seq;
        useSpeedBar = "Y".equalsIgnoreCase(m_SqlManager.GetAppSettings(company_seq).enable_sangsang_player_speedbar);
        
        super.title = m_DownStudySangSangModel.contents_nm;
       
	}

	@Override
	public boolean CheckProgressRestriction() 
	{
		return m_DownStudySangSangModel.view_sec >= m_DownStudySangSangModel.contents_view_sec;
	}
	
	@Override
	public boolean CheckSendProgress(int lastViewSec, int studySec)
	{
		if(studySec % 60 > 0)
			return false;
		
		return true;
	}

	@Override
	public void SaveProgress(int lastViewSec, int studySec) 
	{			
		m_DownStudySangSangModel.view_sec_mobile += studySec;
		m_DownStudySangSangModel.view_sec += studySec;
		m_DownStudySangSangModel.last_view_sec = lastViewSec;
		
		m_SqlManager.SetDownStudySangSangData(m_DownStudySangSangModel);
	}

	@Override
	public int GetLastViewSec() 
	{
		return m_DownStudySangSangModel.last_view_sec;
	}

	@Override
	public int GetLastPositionSec() {
		return m_DownStudySangSangModel.last_view_sec;
		//return lastPositionSec;
	}

	@Override
	public String GetPlayerUrl() 
	{
		return m_strFilePath;
	}

	@Override
	public String GetQuizUrl() {return "";}

	@Override
	public String GetQuizYn() {
		return "n";
	}

	@Override
	public boolean HasPpt() {
		return false;
	}
	
	@Override
	public boolean UseSpeeedControl() {
		return useSpeedBar;
	}
}
