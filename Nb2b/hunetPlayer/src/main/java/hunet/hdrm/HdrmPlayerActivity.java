package hunet.hdrm;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.text.Html;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import hunet.core.HRunnable;
import hunet.core.IHunetRunnableEventListener;
import hunet.core.JsonParse;
import hunet.domain.DomainAddress;
import hunet.library.PlayerMessageUtility;
import hunet.library.Utilities;
import hunet.library.hunetplayer.R;
import hunet.net.Network;
import hunet.player.controls.ControlBar;
import hunet.player.controls.IndexViewer;
import hunet.player.controls.PptViewer;
import hunet.player.interfaces.IControlBarEventListener;
import hunet.player.interfaces.IIndexViewerEventListener;
import hunet.player.interfaces.IPlayerStudyDataEventListener;
import hunet.player.interfaces.IPptViewerEventListener;
import hunet.player.model.MarkDataModel;
import hunet.player.model.PlayerModelBase;
import hunet.player.model.PlayerModelFactory;
import io.vov.vitamio.LibsChecker;
import io.vov.vitamio.MediaPlayer;
import io.vov.vitamio.MediaPlayer.OnBufferingUpdateListener;
import io.vov.vitamio.MediaPlayer.OnCompletionListener;
import io.vov.vitamio.MediaPlayer.OnPreparedListener;
import io.vov.vitamio.MediaPlayer.OnVideoSizeChangedListener;

public class HdrmPlayerActivity extends Activity implements
IControlBarEventListener, IHunetRunnableEventListener,
DialogInterface.OnClickListener, IPlayerStudyDataEventListener,
IPptViewerEventListener, IIndexViewerEventListener,
// HdrmPlayer Impl...
OnBufferingUpdateListener, OnCompletionListener, OnPreparedListener, OnVideoSizeChangedListener, SurfaceHolder.Callback
{
	private static final String TAG = "HdrmPlayerActivity";
	private MarkDataModel pptMarkDataModel;
	private ProgressBar pgbBuffering = null;
	private FrameLayout mRootLayout = null;
	private ControlBar playerControlbar;
	private PptViewer pptViewer = null;
	private IndexViewer indexViewer = null;
	private AudioManager audioManager;
	private PowerManager powerManager;
	private HRunnable runnableBuffering;
	private PlayerModelBase playerModelBase;
	private TextView txtSubTitle;
	private Timer timer;
	private TimerTask timerTask;
	private Toast toast = null;
	private int maxVolume = 0;
	private long actualViewingTime = 0;
	private int milliseconds = 0;
	private int useNetwork = 0;
	private boolean isProgressBarDrag = false;
	// 받아야하는 파라미터
	String userId = "";
	String playerType = "";
	long startTime = 0;
	long endTime = 0;
	boolean repeat =false;
	private float mediaRateMax = (float)2.0;
	private float mediaRateMin = (float)0.8;
	private long mLastViewSec = 0;
	private long mLastPositionSec = 0;
	private long CheckLastSec = 0;
	private long mLastMinute = 0;

	// HdrmPlayer variables
	private int mVideoWidth = 0;
	private int mVideoHeight = 0;
	private MediaPlayer mMediaPlayer;
	private SurfaceView mPreview;
	private SurfaceHolder holder;
	private String mPath;
	private boolean mIsVideoSizeKnown = false;
	private boolean mIsVideoReadyToBePlayed = false;
	private boolean isServerStarted = false;
	private HdrmWebServer httpServer;
	private float mPlsyBackSpeed = 1.0f;
	private int contentSeq = 0;
	private String Quizyn = "n";
	private MediaPlayer.OnTimedTextListener mOnTimedTextListener;
	private String TouChCheckOx = "x";
	private ImageButton subtitle_button;
	private String caption_kr = "";
	private String caption_cn = "";
	private String caption_en = "";
	private String CaptionYn = "";
	public String CheckFirstPlay = "o";
	Handler handler = new Handler();


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.library_player);
		getWindow().addFlags(
				WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
						| WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
						| WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
						| WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);

		InflateControl();
		SetValiableFromIntent();
		SetEvent();
		InitControl();

		if(TextUtils.equals(playerType, "localSangSang") || TextUtils.equals(playerType, "sangsangWeb")){
			mediaRateMax = (float)1.5;
		}

		// HdrmPlayer initialize
		if (!LibsChecker.checkVitamioLibs(this))
			return;

		txtSubTitle = (TextView) findViewById(R.id.txtSubTitle);
		mPreview = (SurfaceView) findViewById(R.id.surface);
		holder = mPreview.getHolder();
		holder.addCallback(this);
		holder.setFormat(PixelFormat.RGBA_8888);
		// HdrmPlayer Play Movie
		ShowBuffering(true);
		mPath = playerModelBase.GetPlayerUrl();

		if (mPath.contains("wbt_hl"))
		{
			mPath = mPath.replace("wbt_hlsc","WBT_HLSC");
			mPath = mPath.replace("mobile","Mobile");
			mPath = mPath.replace("pc","PC");
			mPath = mPath.replace("pc_help","PC_Help");
		}

		mLastViewSec = playerModelBase.GetLastViewSec();
		mLastPositionSec = playerModelBase.GetLastPositionSec();
		Quizyn = playerModelBase.GetQuizYn();
//		if (mLastViewSec>0) {
//			playerControlbar.Pause();
//			new AlertDialog.Builder(HdrmPlayerActivity.this)
//					.setMessage(
//							"从最后收看的位置开始收看?")
//					.setCancelable(false)
//					.setPositiveButton("是",
//							new DialogInterface.OnClickListener() {
//								@Override
//								public void onClick(
//										DialogInterface dialog,
//										int which) {playerControlbar.Play();
//								}
//							})
//					.setNegativeButton("否",
//							new DialogInterface.OnClickListener() {
//								@Override
//								public void onClick(
//										DialogInterface dialog,
//										int which) {
//									setCurrentTime(0);
//									mMediaPlayer.start();
//									playerControlbar.Play();
//								}
//							}).show();
//		}
		checkMovieUrl();
		startServer();
		playVideo();



		//Log.i(TAG,"onCreate");
	}


	public void subtitleButtonClicked(View button){
		//Log.d("test", "subtitleClicked");

		Context wrapper = new ContextThemeWrapper(HdrmPlayerActivity.this,R.style.Theme_InCallScreen);

		if (Build.VERSION.SDK_INT > 10) {
			PopupMenu popup = new PopupMenu(wrapper, button);
			//popup.getMenu().add("titleRes");
			popup.getMenuInflater().inflate(R.menu.subtitle_menu, popup.getMenu());


			// menu 설정
			if (caption_cn.equals(null) || caption_cn.equals("null")) {
				popup.getMenu().removeItem((R.id.subtitle_cn));
			}

			if (caption_kr.equals(null) || caption_kr.equals("null")) {
				popup.getMenu().removeItem((R.id.subtitle_kr));
			}

			//registering popup with OnMenuItemClickListener
			popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
				public boolean onMenuItemClick(MenuItem item) {

					if (item.getTitle().equals("不用字幕")) {
						//Log.d("test","Clicked 不用字幕");
						mMediaPlayer.addTimedTextSource("");
						mMediaPlayer.setTimedTextShown(true);
						txtSubTitle.setVisibility(View.GONE);
					} else if (item.getTitle().equals("汉语字幕")) {
						//Log.d("test","Clicked 汉语字幕");
						mMediaPlayer.addTimedTextSource(caption_cn);
						mMediaPlayer.setTimedTextShown(true);
						txtSubTitle.setVisibility(View.VISIBLE);
					} else if (item.getTitle().equals("한국어자막")) {
						//Log.d("test","Clicked 한국어자막");
						mMediaPlayer.addTimedTextSource(caption_kr);
						mMediaPlayer.setTimedTextShown(true);
						txtSubTitle.setVisibility(View.VISIBLE);
					}
//				Toast.makeText(
//						HdrmPlayerActivity.this,
//						"You Clicked : " + item.getTitle(),
//						Toast.LENGTH_SHORT
//				).show();
					return true;
				}
			});

			popup.show(); //showing popup menu
		}
	}

	private void playVideo() {
		doCleanUp();
		try {
			// Create a new media player and set the listeners
			mMediaPlayer = new MediaPlayer(this);
			mMediaPlayer.setDataSource(this, Uri.parse(mPath));
			mMediaPlayer.setDisplay(holder);
			mMediaPlayer.setScreenOnWhilePlaying(true);
			mMediaPlayer.setAdaptiveStream(true);
			mMediaPlayer.prepareAsync();
			mMediaPlayer.setOnBufferingUpdateListener(this);
			mMediaPlayer.setOnCompletionListener(this);
			mMediaPlayer.setOnPreparedListener(this);
			mMediaPlayer.setOnVideoSizeChangedListener(this);
			mMediaPlayer.setOnTimedTextListener(mTimedTextListener);

			setVolumeControlStream(AudioManager.STREAM_MUSIC);


		} catch (Exception e) {
			//Log.e(TAG, "error: " + e.getMessage(), e);
		}
	}

	@Override
	public void onBufferingUpdate(MediaPlayer arg0, int percent) {
//		 Log.d(TAG, "onBufferingUpdate percent:" + percent);
	}

	@Override
	public void onCompletion(MediaPlayer arg0) {
		//Log.d(TAG, "onCompletion called ");

		String QuizUrl = playerModelBase.GetQuizUrl();
		//Log.d(TAG,"The QuizUrl is  "+QuizUrl);
		// Custom
		if (playerType.equals("htmlWeb")) {
			Intent result = new Intent();
			result.putExtra("completed", true);
			setResult(Activity.RESULT_OK, result);
		}

		String quiz = Quizyn;
		//Intent intent = new Intent(this, getCompany().getNextActivity(getCompany().eNEXT_ACTIVITY_LOGIN));
		if (Quizyn.equals("y") &&  playerModelBase.CheckProgressRestriction() == false)  // Quiz 가 있을때만 퀴즈페이지로 이동
		{
			Intent intent = new Intent(this, QuizViewActivity.class);
			intent.putExtra("WebUrl",QuizUrl);
			intent.putExtra("subTitle","Quiz");
			//Intent intent = new Intent(this, HomeActivity.class);
			startActivity(intent);
		}
		finish();
	}

	@Override
	public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
		//Log.v(TAG, "onVideoSizeChanged called");
		if (width == 0 || height == 0) {
			//Log.e(TAG, "invalid video width(" + width + ") or height(" + height + ")");
			return;
		}

		if (!mIsVideoSizeKnown) {
			mIsVideoSizeKnown = true;
			mVideoWidth = width;
			mVideoHeight = height;
			return;
		}

		if (mIsVideoReadyToBePlayed && mIsVideoSizeKnown && !mp.isPlaying()) {
			startVideoPlayback();
			setFixedVideoSize(mVideoWidth, mVideoHeight, false);
		}
	}

	@Override
	public void onPrepared(MediaPlayer mediaplayer) {
		//Log.d(TAG, "onPrepared called");
		mediaplayer.pause();
		playerControlbar.Pause();
		mIsVideoReadyToBePlayed = true;

		mMediaPlayer.addTimedTextSource(caption_cn);
		mMediaPlayer.setTimedTextShown(true);
	}

	@Override
	public void surfaceChanged(SurfaceHolder surfaceholder, int i, int j, int k) {
		//Log.d(TAG, "surfaceChanged called");

	}

	public void surfaceDestroyed(SurfaceHolder surfaceholder) {
		//Log.d(TAG, "surfaceDestroyed called");
	}

	public void surfaceCreated(SurfaceHolder holder) {
		//Log.d(TAG, "surfaceCreated called");
	}

	private void releaseMediaPlayer() {
		if (mMediaPlayer != null) {
			mMediaPlayer.release();
			mMediaPlayer = null;
		}
	}

	private void doCleanUp() {
		mVideoWidth = 0;
		mVideoHeight = 0;
		mIsVideoReadyToBePlayed = false;
		mIsVideoSizeKnown = false;
	}

	private void startVideoPlayback() {
		//Log.v(TAG, "startVideoPlayback");
		ShowBuffering(false);
		long totalViewSec = getDurationTime();
		mLastViewSec = mLastViewSec < totalViewSec ? mLastViewSec : 0;

		playerControlbar.SetSeekBarMax((int) totalViewSec);
		playerControlbar.SetTotalTimeText(Utilities.GetTimeString(totalViewSec * 1000, false));
		playerControlbar.SetMediaRateText(mPlsyBackSpeed);
		playerControlbar.SetEventListener(this);

//		if (mLastViewSec > 0) {
//			//setCurrentTime(mLastViewSec);
//			//mMediaPlayer.start();
//		}

		if (mLastViewSec>0 && CheckFirstPlay=="o") {
			playerControlbar.Pause();
			new AlertDialog.Builder(HdrmPlayerActivity.this)
					.setMessage(
							getString(R.string.continuWhatch))
					.setCancelable(false)
					.setPositiveButton(getString(R.string.yes),
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(
										DialogInterface dialog,
										int which) {
									setCurrentTime(mLastViewSec);
									mMediaPlayer.start();
									playerControlbar.Play();
								}
							})
					.setNegativeButton(getString(R.string.no),
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(
										DialogInterface dialog,
										int which) {
									setCurrentTime(0);
									mMediaPlayer.start();
									playerControlbar.Play();
								}
							}).show();
			CheckFirstPlay = "x";
		} else if (mLastViewSec>0 && CheckFirstPlay=="x") {
			setCurrentTime(mLastViewSec);
			mMediaPlayer.start();
			playerControlbar.Play();
		} else {
			playerControlbar.Play();
		}

	}

	private void setFixedVideoSize(int videoWidth, int videoHeight, boolean fit) {
		float videoProportion = (float) videoWidth / (float) videoHeight;

		// Get the width of the screen
		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		int screenWidth = metrics.widthPixels;
		int screenHeight = metrics.heightPixels;
//		float screenProportion = (float) screenWidth / (float) screenHeight;

		// Get the SurfaceView layout parameters
		android.view.ViewGroup.LayoutParams lp = mPreview.getLayoutParams();
//		if (videoProportion > screenProportion) {
		if (fit) {
			lp.width = screenWidth;
			lp.height = (int) ((float) screenWidth / videoProportion);
		} else {
			lp.width = (int) (videoProportion * (float) screenHeight);
			lp.height = screenHeight;
		}
		// Commit the layout parameters
		mPreview.setLayoutParams(lp);
	}



	/**
	 * 소켓 연결할 스레드 정의 자막코드 존재여부 체크
	 */
	class ConnectThread extends Thread {
		String urlStr;

		public ConnectThread(String inStr) {
			urlStr = inStr;
		}

		@Override
		public void run() {

			try {
				final String output = request(urlStr);
				handler.post(new Runnable() {
					public void run() {
						// txtMsg.setText(output);
					}
				});

			} catch(Exception ex) {
				ex.printStackTrace();
			}

		}


		private String request(String urlStr) {
			StringBuilder output = new StringBuilder();
			try {
				URL url = new URL(urlStr);
				HttpURLConnection conn = (HttpURLConnection)url.openConnection();
				if (conn != null) {
					conn.setConnectTimeout(10000);
					conn.setRequestMethod("GET");
					conn.setDoInput(true);
					conn.setDoOutput(true);

					int resCode = conn.getResponseCode();
					if (resCode == HttpURLConnection.HTTP_OK) {
						BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream())) ;
						String line = null;
						while(true) {
							line = reader.readLine();

							if (line == null) {
								break;
							} else {
								JsonParse jsonParse = new JsonParse();
								jsonParse.SetData(line);
								CaptionYn = jsonParse.GetJsonValue("caption_yn");
								caption_cn = jsonParse.GetJsonValue("caption_cn").replace(".vtt", ".srt");
								caption_kr = jsonParse.GetJsonValue("caption_kr").replace(".vtt",".srt");
								caption_en = jsonParse.GetJsonValue("caption_en").replace(".vtt", ".srt");
							}
							output.append(line + "\n");
						}

						reader.close();
						conn.disconnect();
					}
				}
			} catch(Exception ex) {
				//Log.e("SampleHTTP", "Exception in processing response.", ex);
				ex.printStackTrace();
			}

			return output.toString();
		}

	}



	private void startServer() {
		if (isServerStarted || mPath == null || mPath == "") {
			return;
		}

		if (!mPath.startsWith("http://") && !mPath.startsWith("https://")) {
			try {
				File movieFile = new File(mPath);
				String wwwroot = movieFile.getParentFile().getAbsolutePath();
				httpServer = new HdrmWebServer("localhost", 8085, new File(wwwroot).getAbsoluteFile(), true);
				httpServer.start();
				isServerStarted = true;

				if (!mPath.startsWith("http://")) {
					HdrmManager.isEqualDeviceUUID(this, movieFile);
					mPath = "http://localhost:8085/" + movieFile.getName();
				}
			} catch (Exception e) {
				e.printStackTrace();
				isServerStarted = false;
				finish();
			}
		}
	}

	private void stopServer() {
		if (isServerStarted == false) {
			return;
		}

		if (httpServer != null) {
			httpServer.stop();
			httpServer = null;
		}
		isServerStarted = false;
	}



	private void InitControl() {
		playerControlbar.InitControlBar(playerModelBase.HasPpt(),
				playerModelBase.title);
		if(playerModelBase.title.length() < 28)//padding
		{
			for(int i=28-playerModelBase.title.length();i > 0;i-- )
				playerModelBase.title += "&nbsp";
		}
		indexViewer.SetIndexContetns(playerModelBase.m_arMarkList);
		
		if (!playerModelBase.UseSpeeedControl())
			playerControlbar.HideSpeedLayout();
	}

	private void SetEvent() {
		runnableBuffering.SetEventListener(this);	
		playerModelBase.SetEventListener(this);
		pptViewer.SetEventListener(this);
		indexViewer.SetEventListener(this);
	}


	private void InflateControl() {
		mRootLayout = (FrameLayout) findViewById(R.id.rootLayout);
		playerControlbar = (ControlBar) findViewById(R.id.playerControlbar);
		pptViewer = (PptViewer) findViewById(R.id.pptViewer);
		indexViewer = (IndexViewer) findViewById(R.id.indexViewer);
		pgbBuffering = (ProgressBar) findViewById(R.id.pgbBuffering);

		playerControlbar.setVisibility(View.GONE);
	}


	private void SetValiableFromIntent() {
		audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);		
		timer = new Timer();
		runnableBuffering = new HRunnable(1);	
		maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
		//		useNetwork = Network.GetCurrentUseNetwork(this);
//		useNetwork = PlayerMessageUtility.getNetworkSettings(HdrmPlayerActivity.this);
		
		if (isConnectedWifi())
			useNetwork = 0;
		else if (isConnectedMobile())
			useNetwork = 1;
		else
			useNetwork = -1;
		
		toast = toast.makeText(this, "", toast.LENGTH_SHORT);
		Bundle bundle = getIntent().getExtras();
		playerType = "web";

		if (bundle != null && bundle.containsKey("userId"))
			userId = bundle.getString("userId");
		if (bundle != null && bundle.containsKey("playerType"))
			playerType = bundle.getString("playerType");
		playerModelBase = PlayerModelFactory.Instance().Get(this,playerType);
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);

		if (hasFocus == false)
			return;

		int curVolumeValue = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
		int nPercent = (int) Math.round((curVolumeValue / (double) maxVolume) * 100);

		playerControlbar.onWindowFocusChanged(hasFocus);
		playerControlbar.SetVolume(nPercent);
	}

	@Override
	public void onResume() {
		super.onResume();
		playerControlbar.setVisibility(View.VISIBLE);
		if (mMediaPlayer == null) {
			playVideo();
		}
	}

	private MediaPlayer.OnTimedTextListener mTimedTextListener = new MediaPlayer.OnTimedTextListener() {
		@Override
		public void onTimedTextUpdate(byte[] pixels, int width, int height) {
//			io.vov.vitamio.utils.Log.i("onSubtitleUpdate: bitmap subtitle, %dx%d", width, height);
//			if (mOnTimedTextListener != null)
//				mOnTimedTextListener.onTimedTextUpdate(pixels, width, height);
			txtSubTitle.setWidth(width);
			txtSubTitle.setHeight(height);
		}

		@Override
		public void onTimedText(String text) {
			if (txtSubTitle != null) {
				if (!TextUtils.isEmpty(text)) {
					txtSubTitle.setVisibility(View.VISIBLE);
					txtSubTitle.setText(text);
				} else {
					txtSubTitle.setVisibility(View.GONE);
				}
			}
		}
	};

	@Override
	public void onPause() {
		super.onPause();
		playerControlbar.Pause();
		StartServiceStudyProgress();
		releaseMediaPlayer();
		doCleanUp();
	}
	
	@Override
	public void finish() {
		//Log.i(TAG, "finish ");
		mRootLayout.setVisibility(View.INVISIBLE);

		super.finish();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		releaseMediaPlayer();
		doCleanUp();
		stopServer();
	}

	private void checkMovieUrl() {
//		if (TextUtils.isEmpty(mPath)) {
//			Toast.makeText(getBaseContext(), getString(R.string.noVideo), Toast.LENGTH_LONG).show();
//			finish();
//			return;
//		}
//		mPath = mPath.replace("http://m2.", "http://w2.").replace("http://m.", "http://w.");
		if (TextUtils.isEmpty(mPath)) {
			Toast.makeText(getBaseContext(), "视频文件不存在！请跟管理员联系", Toast.LENGTH_LONG).show();
			finish();
			return;
		}
		if (mPath.contains("wbt_hl"))
		{

		}
		else
		{

			// 자막 Replace
			String[] arrays = mPath.split("cdn/");
			String CheckDrmYn = "N";
			if (arrays.length > 1) {
				String[] arrCourse = arrays[1].split(("_"));

				caption_cn = mPath.replace("http://cdn.xiunaichina.com/cdn/","http://study.xiunaichina.com/Study/Contents/Captions/"+arrCourse[0]+"/");
				caption_cn = caption_cn.replace("400K.mp4","Chinese.srt").replace("700K.mp4", "Chinese.srt");

				if (TextUtils.isEmpty(caption_cn)) {
					caption_cn = "";
				}

				caption_kr = mPath.replace("http://cdn.xiunaichina.com/cdn/","http://study.xiunaichina.com/Study/Contents/Captions/"+arrCourse[0]+"/");
				caption_kr = caption_kr.replace("400K.mp4","Korean.srt").replace("700K.mp4","Korean.srt");

				if (TextUtils.isEmpty(caption_kr)) {
					caption_kr = "";
				}


				String FullUrl = DomainAddress.getUrlApps() + "/JLog?"+String.format("type=50&movUrl=%s", mPath);
				ConnectThread thread = new ConnectThread(FullUrl);
				thread.start();
			}

			mPath = mPath.replace("http://m2.", "http://w2.").replace("http://m.", "http://w.");

		}

	}

	private long getCurrentTime()
	{
		if (mMediaPlayer != null) {
			return mMediaPlayer.getCurrentPosition() / 1000;
		}
		else {
			return 0;
		}
	}

	private void setCurrentTime(long time)
	{
		long position = time < getDurationTime() ? time * 1000 : 0;
		mMediaPlayer.seekTo(position);
	}

	private long getDurationTime()
	{
		if (mMediaPlayer != null) {
			return mMediaPlayer.getDuration() / 1000;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		switch (keyCode) {
		case KeyEvent.KEYCODE_VOLUME_UP:
		case KeyEvent.KEYCODE_VOLUME_DOWN: {
			int curVolumeValue = audioManager
					.getStreamVolume(AudioManager.STREAM_MUSIC);
			int nPercent = (int) Math
					.round((curVolumeValue / (double) maxVolume) * 100);

			playerControlbar.SetVolume(nPercent);
		}
		break;
		}

		return super.onKeyUp(keyCode, event);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		switch (keyCode) {
		case KeyEvent.KEYCODE_BACK: {			
			playerControlbar.Pause();			
			new AlertDialog.Builder(HdrmPlayerActivity.this)
			.setMessage(
					getString(R.string.realEnd))
					.setCancelable(false)
					.setPositiveButton(getString(R.string.end),
							new DialogInterface.OnClickListener() {
						@Override
						public void onClick(
								DialogInterface dialog,
								int which) {
							if (playerType.equals("htmlWeb")) {
								Intent result = new Intent();
								result.putExtra("completed", true);
								setResult(Activity.RESULT_OK, result);
							}
							finish();					
						}
					})
					.setNegativeButton(getString(R.string.btn_cancel),
							new DialogInterface.OnClickListener() {
						@Override
						public void onClick(
								DialogInterface dialog,
								int which) {							
							playerControlbar.Play();
						}
					}).show();
		}
		return true;
		}

		return super.onKeyDown(keyCode, event);
	}

	private void StartTimer() {
		StopTimer();

		timerTask = new TimerTask() {
			@Override
			public void run() {
				runOnUiThread(OnTimerViewUpdate);
			}
		};
		timer.schedule(timerTask, 0, 500);
	}

	private void StopTimer() {
		if (timerTask == null)
			return;

		timerTask.cancel();
		timerTask = null;
	}

//	private boolean CheckScreenOn() {
//		boolean isScreenOn = false;
//
//		try {
//			isScreenOn = powerManager.isScreenOn();
//		} catch (Exception e) {
//			isScreenOn = true;
//		}
//
//		return isScreenOn;
//	}
	
	private boolean isConnectedWifi() {
		return Network.connectWifiConfirm(HdrmPlayerActivity.this);
	}
	
	private boolean isConnectedMobile() {
		return Network.connect3GConfirm(HdrmPlayerActivity.this) || Network.connectWimaxConfirm(HdrmPlayerActivity.this);
	}

	private Runnable OnTimerViewUpdate = new Runnable() {
		public void run() {

			if (mMediaPlayer == null || !mMediaPlayer.isPlaying()){
				return;
			}

			long currentSec = getCurrentTime();
			if (currentSec != mLastViewSec) {
				actualViewingTime++;
			}
			mLastViewSec = currentSec;

			int nTime = (int)mLastViewSec;
			if( repeat && startTime > 0 && endTime >0 && (int)endTime <= nTime ){
				setCurrentTime(startTime);
			}

			if (isProgressBarDrag == false)
				playerControlbar.SetProgress(nTime);

			long uptime = System.currentTimeMillis();
			long minute = TimeUnit.MILLISECONDS.toMinutes(uptime);
			if(minute != mLastMinute && playerControlbar.txtIndexNm != null)
			{
				mLastMinute = minute;
				SimpleDateFormat dateFormat = new SimpleDateFormat("a h:mm", Locale.CHINESE);
				String currentTime = dateFormat.format(new Date());
				playerControlbar.txtIndexNm.setText(Html.fromHtml(String.format("<b>%s %s</b>", playerModelBase.title, currentTime)));
			}

			milliseconds += 500;
			if (milliseconds < 1000) {
				return;
			}
			else {
				milliseconds = 0;
			}

			//Log.i(TAG, "OnTimerViewUpdate milliseconds:" + milliseconds + ", actualViewingTimess:" + actualViewingTime + ", mLastViewSec:" + mLastViewSec);


			if (actualViewingTime > 5 && actualViewingTime % 3 == 0) {

				int curUseNetwork = 0;

				if (isConnectedWifi())			// 와이파이
					curUseNetwork = 0;
				else if (isConnectedMobile()) 	// 이동통신사 망
					curUseNetwork = 1;
				else							// 오프라인
					curUseNetwork = -1;

				int currentNetworkConfig = PlayerMessageUtility.getNetworkSettings(HdrmPlayerActivity.this);
				String message = PlayerMessageUtility.getNetworkChangeMessage(HdrmPlayerActivity.this, currentNetworkConfig);

//				int nCurUseNetwork = Network.GetCurrentUseNetwork(getApplicationContext());

				if (curUseNetwork != useNetwork && !("local".equals(playerType) || "localSangSang".equals(playerType))) {
					useNetwork = curUseNetwork;

					if (currentNetworkConfig != PlayerMessageUtility.NETWORK_ALARM_OFF_MOVIE_ON) {

						switch (currentNetworkConfig) {
							case PlayerMessageUtility.NETWORK_ALARM_OFF_MOVIE_OFF:
								playerControlbar.Pause();

								new AlertDialog.Builder(HdrmPlayerActivity.this)
										.setTitle(getString(R.string.message))
										.setMessage(message)
										.setPositiveButton(getString(R.string.btn_confirm), new DialogInterface.OnClickListener() {
											@Override
											public void onClick(DialogInterface dialog, int which) {
												PlayerMessageUtility.setOnMovieStatus(HdrmPlayerActivity.this);
												playerControlbar.Play();
											}
										}).setNegativeButton(getString(R.string.btn_cancel), new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										finish();
									}
								}).show();
								break;
							case PlayerMessageUtility.NETWORK_ALARM_ON_MOVIE_OFF:
								playerControlbar.Pause();

								new AlertDialog.Builder(HdrmPlayerActivity.this)
										.setTitle(getString(R.string.message))
										.setMessage(message)
										.setPositiveButton(getString(R.string.btn_confirm), new DialogInterface.OnClickListener() {
											@Override
											public void onClick(DialogInterface dialog, int which) {
												PlayerMessageUtility.setOnMovieStatus(HdrmPlayerActivity.this);
												playerControlbar.Play();
											}
										}).setNegativeButton(getString(R.string.btn_cancel), new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										finish();
									}
								}).show();
								break;
							case PlayerMessageUtility.NETWORK_ALARM_ON_MOVIE_ON:
								playerControlbar.Pause();

								new AlertDialog.Builder(HdrmPlayerActivity.this)
										.setTitle(getString(R.string.message))
										.setMessage(message)
										.setPositiveButton(getString(R.string.btn_confirm), new DialogInterface.OnClickListener() {
											@Override
											public void onClick(DialogInterface dialog, int which) {
												PlayerMessageUtility.setOnMovieStatus(HdrmPlayerActivity.this);
												playerControlbar.Play();
											}
										}).show();
								break;
						}
					}
//					if (nCurUseNetwork == 1) {
//
//
//						new AlertDialog.Builder(HdrmPlayerActivity.this)
//						.setTitle("알림")
//						.setMessage("네트워크 환경이 변경되었습니다.")
//						.setCancelable(false)
//						.setPositiveButton("확인",
//								new DialogInterface.OnClickListener() {
//							@Override
//							public void onClick(
//									DialogInterface dialog,
//									int which) {
//								playerControlbar.Play();
//								player.play();
//							}
//						}).show();
//
//					} else if (nCurUseNetwork == 2) {
//						new AlertDialog.Builder(HdrmPlayerActivity.this)
//						.setTitle("알림")
//						.setMessage(
//								"현재 3G/4G망에 접속중으로 별도의 무선데이터 이용료가 발생할 수 있습니다.")
//								.setCancelable(false)
//								.setPositiveButton("승인",
//										new DialogInterface.OnClickListener() {
//									@Override
//									public void onClick(
//											DialogInterface dialog,
//											int which) {
//										playerControlbar.Play();
//										player.play();
//									}
//								})
//								.setNegativeButton("취소",
//										new DialogInterface.OnClickListener() {
//									@Override
//									public void onClick(
//											DialogInterface dialog,
//											int which) {
//										dialog.dismiss();
//										finish();
//									}
//								}).show();
//					}
				}
			}

//			Log.i(TAG,"currentTime : (int) (player.currentTime() : " + player.currentTime() + " actualViewingTime : " + actualViewingTime);
			if (playerModelBase.CheckSendProgress((int)mLastViewSec, (int)actualViewingTime) == false){
				/*
				if(nTime == duration)
				{
					StopTimer();
					if (playerType.equals("htmlWeb")) {
						Intent result = new Intent();
						result.putExtra("completed", true);
						setResult(Activity.RESULT_OK, result);
					}
					finish();
				}*/
				return;
			}

			StartServiceStudyProgress();
		}
	};

	private void StartServiceStudyProgress() {
		//Log.i(TAG, "StartServiceStudyProgress currentTime:" + mLastViewSec + ", actualViewingTime:" + actualViewingTime);
		playerModelBase.SaveProgress((int) mLastViewSec, (int) actualViewingTime);
		if("localSangSang".equals(playerType)) {
			actualViewingTime = 0;
		}
	}

	@Override
	public void OnChangeVolume(ControlBar controlBar, int nValue) {
		int nVolumeValue = (int) Math
				.round(((double) nValue * (double) maxVolume) / 100.0);
		audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, nVolumeValue,
				AudioManager.FLAG_SHOW_UI);
	}

	@Override
	public boolean OnClickPlay(ControlBar controlBar) {
		StartTimer();

		if (mMediaPlayer == null) {
			return false;
		}
		if (mMediaPlayer.isPlaying()){
			return true;
		}

		mMediaPlayer.start();

		return true;
	}

	@Override
	public boolean OnClickPause(ControlBar controlBar) {
		StopTimer();

		if (mMediaPlayer == null || !mMediaPlayer.isPlaying())
			return true;

		mMediaPlayer.pause();

		return true;
	}

	@Override
	public boolean OnClickBack(ControlBar controlBar) {

		try {
			long currentTime = mLastViewSec;
			long backward = currentTime -10;
			if(backward > 0)
			{
				setCurrentTime(backward);
			}
			else {
				setCurrentTime(0);
			}			
		} catch (Exception e) {
			e.printStackTrace();
		}

		return true;
	}

	@Override
	public boolean OnClickForward(ControlBar controlBar) {
		if (playerModelBase.CheckProgressRestriction() == false)
			return true;

		try {
			long totalTime = getDurationTime();
			long currentTime = mLastViewSec;
			long forward = currentTime +10;
			if(totalTime <= forward)
			{
				setCurrentTime(totalTime);
			}
			else{
				setCurrentTime(forward);
			}			
		} catch (Exception e) {
			e.printStackTrace();
		}

		return true;
	}

	@Override
	public boolean OnClickSpeedUp(ControlBar controlBar) {
		mPlsyBackSpeed += 0.1;

		if (mPlsyBackSpeed > mediaRateMax)
			mPlsyBackSpeed = mediaRateMax;

		playerControlbar.SetMediaRateText(mPlsyBackSpeed);
		mMediaPlayer.setPlaybackSpeed(mPlsyBackSpeed);

		StartTimer();
		return true;
	}

	@Override
	public boolean OnClickSpeedDown(ControlBar controlBar) {
		mPlsyBackSpeed -= 0.1;

		if (mPlsyBackSpeed < mediaRateMin)
			mPlsyBackSpeed = mediaRateMin;

		playerControlbar.SetMediaRateText(mPlsyBackSpeed);
		mMediaPlayer.setPlaybackSpeed(mPlsyBackSpeed);

		StartTimer();
		return true;
	}

	public boolean OnClickSound(ControlBar controlBar) {
		return true;
	}

	public void OnStartTrackingTouch(ControlBar controlBar, int progress) {
		isProgressBarDrag = true;
	}

	public void OnStopTrackingTouch(ControlBar controlBar, int progress) {
		int nTime = (int)mLastPositionSec;

		if (playerModelBase.CheckProgressRestriction() == false && nTime<progress) {
			toast.setText(getString(R.string.firstStudyMessage));
			toast.show();
		}  else {
			try {
				setCurrentTime(progress);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		isProgressBarDrag = false;
	}

	public void OnProgressChanged(ControlBar controlBar, int progress) {
		// no use
	}

	@Override
	public void OnRun(int nId, Object tag) {
		switch (nId) {
		case 1:
			ShowBuffering((Boolean) tag);
			break;
		case 2:
			finish();
			break;
		}
	}

	@Override
	public void onClick(DialogInterface dialog, int which) {
		if (which != DialogInterface.BUTTON_POSITIVE)
			return;

		dialog.dismiss();
		if (playerType.equals("htmlWeb")) {
			Intent result = new Intent();
			result.putExtra("completed", true);
			setResult(Activity.RESULT_OK, result);
		}

		finish();

	}

	private boolean SetMediaPlayerSeek(int nMillisec) {
		if (playerModelBase.CheckProgressRestriction() == false)
			return true;
		try {			
			setCurrentTime((int)(nMillisec/1000.));

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

		if (!mMediaPlayer.isPlaying())
			playerControlbar.Play();

		return true;
	}

	private void ShowIndexList(boolean bShow) {
		if (bShow) {
			Animation animation = new TranslateAnimation(
					-indexViewer.getWidth(), 0, 0, 0);
			animation.setDuration(500);
			indexViewer.setVisibility(View.VISIBLE);
			indexViewer.startAnimation(animation);
			playerControlbar.Hide();
			/*
			 * layoutIndexBG.setVisibility(View.VISIBLE);
			 * layoutIndexBG.startAnimation(animation);
			 */
		} else {
			Animation animation = new TranslateAnimation(0,
					-indexViewer.getWidth(), 0, 0);
			animation.setDuration(500);
			indexViewer.setVisibility(View.INVISIBLE);
			indexViewer.startAnimation(animation);
			/*
			 * layoutIndexBG.setVisibility(View.INVISIBLE);
			 * layoutIndexBG.startAnimation(animation);
			 */
		}
	}

	private void ShowPptViewer(boolean bShow) {
		if (bShow) {
			Animation animation = new TranslateAnimation(-pptViewer.getWidth(),
					0, 0, 0);
			animation.setDuration(500);
			pptViewer.setVisibility(View.VISIBLE);
			pptViewer.startAnimation(animation);
			pptMarkDataModel = playerModelBase.m_CurMarkData;
			pptViewer.SetLoadWebView(playerModelBase.m_CurMarkData.pptUrl);
			playerControlbar.Hide();

		} else {
			Animation animation = new TranslateAnimation(0,
					-pptViewer.getWidth(), 0, 0);
			animation.setDuration(500);
			pptViewer.setVisibility(View.INVISIBLE);
			pptViewer.startAnimation(animation);
		}
	}

	@Override
	public boolean OnClickIndexFull(ControlBar controlBar, boolean bIsFull) {
		if (playerModelBase.HasPpt()) {
			ShowIndexList(true);
		}
		return true;
	}

	@Override
	public boolean OnClickPptFull(ControlBar controlBar, boolean bIsFull) {
		if (playerModelBase.HasPpt()) {
			// SetScreen(layoutWebView, bIsFull);
			ShowPptViewer(true);
		}
		return true;
	}

	@Override
	public boolean OnPptLeftMove() {
		// TODO Auto-generated method stub
		if (playerModelBase.m_arMarkList != null && pptMarkDataModel != null
				&& playerModelBase.m_arMarkList.size() > 0) {
			MarkDataModel data = pptMarkDataModel;
			int index = playerModelBase.m_arMarkList.indexOf(data);

			for (int nIndex = index; 0 <= nIndex; nIndex--) {
				MarkDataModel temp = playerModelBase.m_arMarkList.get(nIndex);
				if (temp.pptUrl != null && !temp.pptUrl.equals("notPt")
						&& data.markNo != temp.markNo
						&& !data.pptUrl.equals(temp.pptUrl)) {
					pptMarkDataModel = temp;
					pptViewer.SetLoadWebView(temp.pptUrl);
					break;
				}
			}
		}
		return false;
	}

	@Override
	public boolean OnPptRightMove() {
		// TODO Auto-generated method stub
		if (playerModelBase.m_arMarkList != null && pptMarkDataModel != null
				&& playerModelBase.m_arMarkList.size() > 0) {
			MarkDataModel data = pptMarkDataModel;
			int index = playerModelBase.m_arMarkList.indexOf(data);
			int size = playerModelBase.m_arMarkList.size();

			for (int nIndex = index; nIndex < size; nIndex++) {
				MarkDataModel temp = playerModelBase.m_arMarkList.get(nIndex);
				if (temp.pptUrl != null && !temp.pptUrl.equals("notPt")
						&& data.markNo != temp.markNo
						&& !data.pptUrl.equals(temp.pptUrl)) {
					pptMarkDataModel = temp;
					pptViewer.SetLoadWebView(temp.pptUrl);
					break;
				}
			}
		}
		return false;
	}

	@Override
	public boolean OnPptPlay() {
		// TODO Auto-generated method stub
		if (pptMarkDataModel != null) {
			SetMediaPlayerSeek(pptMarkDataModel.markValue);
			ShowPptViewer(false);
		}
		return false;
	}

	@Override
	public boolean OnPptClose() {
		// TODO Auto-generated method stub
		ShowPptViewer(false);
		return false;
	}

	@Override
	public boolean OnIndexClose() {
		// TODO Auto-generated method stub
		ShowIndexList(false);
		return false;
	}

	@Override
	public boolean OnIndexChnaged(int markValue) {
		// TODO Auto-generated method stub
		SetMediaPlayerSeek(markValue);
		ShowIndexList(false);
		return false;
	}

	@Override
	public void OnClickComplete(ControlBar controlBar) {
		// TODO Auto-generated method stub
		if (playerType.equals("htmlWeb")) {
			Intent result = new Intent();
			result.putExtra("completed", true);
			setResult(Activity.RESULT_OK, result);
		}
		this.finish();
	}

	@Override
	public void OnClickScreenFit(boolean fit) {
		setFixedVideoSize(mVideoWidth, mVideoHeight, fit);
	}

	private void ShowBuffering(boolean isShow) {
		if (isShow) {
			pgbBuffering.setVisibility(View.VISIBLE);
		}
		else {
			pgbBuffering.setVisibility(View.INVISIBLE);
		}
	}
	@Override
	public void OnClickRepeat(String status) {
		// TODO Auto-generated method stub
		if(status == "1")
		{
			startTime = mLastViewSec;
		}
		else if(status == "2")
		{
			endTime = mLastViewSec;
			repeat =true;
			if( repeat && startTime > 0 && endTime >0 ){
				setCurrentTime(startTime);
			}
		}
		else if(status == "0")
		{			
			startTime = 0;
			endTime = 0;
			repeat =false;			
		}
	}
}