package hunet.hdrm;

import android.content.Context;
import android.os.Build;
import android.telephony.TelephonyManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import hunet.hdrm.httpd.CryptoProvider;
import kr.co.hunet.ndk.HunetKeyUtil;

public class HdrmManager {
//	private static final byte[] DefaultKeyBytes = new HunetKeyUtil().stringFromJNI().getBytes();
	private static final String DRM_CHARSET_NAME = "UTF-8";
	private static final String DRM_PREFIX = "HunetDRM";
	private static final int DRM_PREFIX_BYTES_SIZE = 8;
	private static final int DRM_INFO_BYTES_LENGTH_SIZE = 5;
	private static final int DRM_INFO_BYTES_MAX_SIZE = 1000;
	private static final int DRM_ENCRYPT_BYTES_SIZE = 1000;
	private static String currentLocalFilepath = "";
	private static JSONObject currentDrmInfo = null;
	private final static int JELLY_BEAN_4_2 = 17;

	private static byte[] encrypt(byte[] clareText) throws Exception {
		byte[] rawKey = getRawKey(new HunetKeyUtil().getEncryptKey().getBytes());
		return encrypt(rawKey, clareText);
	}

	private static byte[] decrypt(byte[] encrypted) throws Exception {
		byte[] rawKey = getRawKey(new HunetKeyUtil().getEncryptKey().getBytes());
		return decrypt(rawKey, encrypted);
	}

	private static byte[] getRawKey(byte[] seed) throws Exception {
		KeyGenerator kgen = KeyGenerator.getInstance("AES");
		SecureRandom sr = null;
		// AndroidN 부터 SecureRandom에서 "Crypto" provider가 deprecated 됨
		// https://android-developers.googleblog.com/2016/06/security-crypto-provider-deprecated-in.html
		// 따라서 CryptoProvier 클래스를 생성하여 넣어주는 형태로 변경
		// http://stackoverflow.com/questions/39097099/security-crypto-provider-deprecated-in-android-n/42337802#42337802
		if(android.os.Build.VERSION.SDK_INT >= 24){
			sr = SecureRandom.getInstance("SHA1PRNG", new CryptoProvider());
		}
		if (android.os.Build.VERSION.SDK_INT >= JELLY_BEAN_4_2) {
			sr = SecureRandom.getInstance("SHA1PRNG", "Crypto");
		} else {
			sr = SecureRandom.getInstance("SHA1PRNG");
		}
		sr.setSeed(seed);
		try {
			kgen.init(256, sr);
			// kgen.init(128, sr);
		} catch (Exception e) {
			// Log.w(LOG, "This device doesn't suppor 256bits, trying 192bits.");
			try {
				kgen.init(192, sr);
			} catch (Exception e1) {
				// Log.w(LOG, "This device doesn't suppor 192bits, trying 128bits.");
				kgen.init(128, sr);
			}
		}
		SecretKey skey = kgen.generateKey();
		byte[] raw = skey.getEncoded();
		return raw;
	}

	private static byte[] encrypt(byte[] raw, byte[] clear) throws Exception {
		SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
		Cipher cipher = Cipher.getInstance("AES");
		cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
		byte[] encrypted = cipher.doFinal(clear);
		return encrypted;
	}

	private static byte[] decrypt(byte[] raw, byte[] encrypted) throws Exception {
		SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
		Cipher cipher = Cipher.getInstance("AES");
		cipher.init(Cipher.DECRYPT_MODE, skeySpec);
		byte[] decrypted = cipher.doFinal(encrypted);
		return decrypted;
	}

	public static long getHdrmContinueDownloadPosition(String localFilepath) {
		File localFile = new File(localFilepath);

		if (localFile.exists() && localFile.length() > 0) {
			if (localFile.length() < (HdrmManager.DRM_ENCRYPT_BYTES_SIZE + HdrmManager.DRM_INFO_BYTES_MAX_SIZE)) {
				localFile.delete();
			}
		}

		if (!localFile.exists()) {
			try {
				localFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		long position = localFile.length();
		if (position > 0) {
			currentLocalFilepath = localFilepath;
			position = position - getHdrmMetadataLengthWithOverEncryptedBytesLength();
		}
		return position;
	}

	public static int getHdrmMetadataLengthWithOverEncryptedBytesLength() {
		int hdrmInfoSize = 0;
		JSONObject json = readDrmInfo(currentLocalFilepath);
		if (json != null) {
			try {
				hdrmInfoSize = json.getInt("hdrm_metadata_length_with_over_encrypted_bytes_length");
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		return hdrmInfoSize;
	}

	public static int getHdrmMetadataLengthWithEncryptedBytesLength() {
		int hdrmInfoSize = 0;
		JSONObject json = readDrmInfo(currentLocalFilepath);
		if (json != null) {
			try {
				hdrmInfoSize = json.getInt("encrypted_bytes_length") + json.getInt("hdrm_metadata_length");
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		return hdrmInfoSize;
	}

	public static long getRawFileLen(File f) {
		long fileLen = f.length();
		JSONObject json = readDrmInfo(f.getAbsolutePath());
		if (json != null) {
			try {
				fileLen = json.getLong("raw_file_bytes_length");
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		return fileLen;
	}

	private static String getUDID(Context context) {
		final TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

		final String tmDevice, tmSerial, androidId;
		tmDevice = "" + tm.getDeviceId();
		tmSerial = "" + tm.getSimSerialNumber();
		androidId = ""
				+ android.provider.Settings.Secure.getString(
						context.getContentResolver(),
						android.provider.Settings.Secure.ANDROID_ID);

		UUID deviceUuid = new UUID(androidId.hashCode(),
				((long) tmDevice.hashCode() << 32) | tmSerial.hashCode());
		
		return deviceUuid.toString();
	}

	private static JSONObject makeDrmInfo(Context context) {
		JSONObject drmInfo = new JSONObject();
		try {
			drmInfo.put("company", "hunet");
			drmInfo.put("version", "1.0");
			drmInfo.put("create_datetime", getDateTimeStringNow());
			drmInfo.put("du_datetime", ""); // 만료일시
			drmInfo.put("edit_datetime", "");

			final TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
			final String tmDevice, tmSerial, androidId;
			tmDevice = "" + tm.getDeviceId();
			tmSerial = "" + tm.getSimSerialNumber();
			androidId = "" + android.provider.Settings.Secure.getString(
							context.getContentResolver(),
							android.provider.Settings.Secure.ANDROID_ID);
			drmInfo.put("device_id", tmDevice);
			drmInfo.put("sim_serial_number", tmSerial);
			drmInfo.put("android_id", androidId);
			UUID udid = new UUID(androidId.hashCode(), ((long) tmDevice.hashCode() << 32) | tmSerial.hashCode());
			drmInfo.put("udid", udid.toString()); // 기기 고유식별키
			
			drmInfo.put("crypt_type", "AES256");
			drmInfo.put("encrypted_bytes_length", 0); // 암호화된 바이트 길이
			drmInfo.put("decrypted_bytes_length", DRM_ENCRYPT_BYTES_SIZE); // 암호화하기전 바이트 길이
			drmInfo.put("raw_file_bytes_length", 0L); // 원본파일 바이트 길이
			
			drmInfo.put("title", "");
			drmInfo.put("mime_type", "mp4");
			drmInfo.put("device_os", "android");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return drmInfo;
	}

	private static JSONObject readDrmInfo(String localFilePath) {
		if (localFilePath.equals(currentLocalFilepath) && currentDrmInfo != null) {
			return currentDrmInfo;
		}

		JSONObject drmInfo = null;
		byte[] drmPrefixBytes = new byte[DRM_PREFIX_BYTES_SIZE];
		FileInputStream fis = null;
		try {
				fis = new FileInputStream(new File(localFilePath));
			fis.read(drmPrefixBytes, 0, DRM_PREFIX_BYTES_SIZE);
			
			String drmPrefixString = new String(drmPrefixBytes, DRM_CHARSET_NAME);
			if (drmPrefixString.equalsIgnoreCase(DRM_PREFIX) == false) {
				return null;
			}

			byte[] drmInfoBytesLengthBytes = new byte[DRM_INFO_BYTES_LENGTH_SIZE];
			fis.read(drmInfoBytesLengthBytes, 0, drmInfoBytesLengthBytes.length);
			String drmInfoBytesLength = new String(drmInfoBytesLengthBytes, DRM_CHARSET_NAME);

			byte[] drmInfoBytes = new byte[Integer.parseInt(drmInfoBytesLength)];
			fis.read(drmInfoBytes, 0, drmInfoBytes.length);
			
			String drmInfoString = new String(drmInfoBytes, DRM_CHARSET_NAME);
			drmInfo = new JSONObject(drmInfoString.trim());
			drmInfo.put("drminfo_bytes_length", drmInfoBytes.length);

			int hdrmMetadataLength = HdrmManager.DRM_PREFIX_BYTES_SIZE + HdrmManager.DRM_INFO_BYTES_LENGTH_SIZE + drmInfoBytes.length;
			drmInfo.put("hdrm_metadata_length", hdrmMetadataLength);

			int encryptedBytesLength = drmInfo.getInt("encrypted_bytes_length");
			int decryptedBytesLength = drmInfo.getInt("decrypted_bytes_length");
			int overEncryptedBytesLength = encryptedBytesLength - decryptedBytesLength;
			drmInfo.put("hdrm_over_encrypted_bytes_length", overEncryptedBytesLength);
			int hdrmMetadataWithOverBytesLength = hdrmMetadataLength + overEncryptedBytesLength;
			drmInfo.put("hdrm_metadata_length_with_over_encrypted_bytes_length", hdrmMetadataWithOverBytesLength);

			currentLocalFilepath = localFilePath;
			currentDrmInfo = drmInfo;
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		} finally {
			if (fis != null) {
				try {
					fis.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return drmInfo;
	}

	public static boolean isEqualDeviceUUID(Context context, File file) {
		boolean isEqual = true;
		try {
			JSONObject drmInfo = HdrmManager.readDrmInfo(file.getAbsolutePath());
			if (drmInfo == null) {
				return true;
			}

			isEqual = drmInfo.getString("udid").equalsIgnoreCase(HdrmManager.getUDID(context));

		} catch (Exception e) {
			e.printStackTrace();
		}

		return isEqual;
	}

	public static void writeCurrentHdrmEncryptedBytes(byte[] bytes, int readBytesLength, RandomAccessFile output, Context context, long remorteFileSize) {
		try {
			byte[] firstBytes = new byte[HdrmManager.DRM_ENCRYPT_BYTES_SIZE];
			System.arraycopy(bytes, 0, firstBytes, 0, firstBytes.length);
			byte[] rawBytes = new byte[readBytesLength - firstBytes.length];
			System.arraycopy(bytes, firstBytes.length, rawBytes, 0, rawBytes.length);
			byte[] encryptedBytes = HdrmManager.encrypt(firstBytes);

			JSONObject drmInfo = HdrmManager.makeDrmInfo(context);
			drmInfo.put("du_datetime", ""); // 만료일시
			drmInfo.put("edit_datetime", ""); // 수정일시
			drmInfo.put("decrypted_bytes_length", firstBytes.length);
			drmInfo.put("encrypted_bytes_length", encryptedBytes.length);
			drmInfo.put("raw_file_bytes_length", remorteFileSize);
			String drmInfoString = drmInfo.toString();

			byte[] drmPrefixBytes = HdrmManager.DRM_PREFIX.getBytes(HdrmManager.DRM_CHARSET_NAME);
			byte[] drmInfoBytes = drmInfoString.getBytes(HdrmManager.DRM_CHARSET_NAME);
			String drmInfoBytesLength = HdrmManager.padLeftZero(drmInfoBytes.length, HdrmManager.DRM_INFO_BYTES_LENGTH_SIZE);
			byte[] drmInfoBytesLengthBytes = drmInfoBytesLength.getBytes(HdrmManager.DRM_CHARSET_NAME);

			output.write(drmPrefixBytes, 0, drmPrefixBytes.length);
			output.write(drmInfoBytesLengthBytes, 0, drmInfoBytesLengthBytes.length);
			output.write(drmInfoBytes, 0, drmInfoBytes.length);
			output.write(encryptedBytes, 0, encryptedBytes.length);
			output.write(rawBytes, 0, rawBytes.length);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static byte[] readCurrentHdrmDecryptedBytes() {
		byte[] decryptedDataBuff = null;

		try {
			JSONObject drmInfo = HdrmManager.readDrmInfo(currentLocalFilepath);
			if (drmInfo == null) {
				return null;
			}

			FileInputStream fis = new FileInputStream(new File(currentLocalFilepath));

			int hdrmMetadataLength = drmInfo.getInt("hdrm_metadata_length");
			fis.skip(hdrmMetadataLength);

			int encryptedBytesLength = drmInfo.getInt("encrypted_bytes_length");
			byte[] encryptedDataBuff = new byte[encryptedBytesLength];
			fis.read(encryptedDataBuff, 0, encryptedDataBuff.length);
			decryptedDataBuff = HdrmManager.decrypt(encryptedDataBuff);

			HdrmManager.threadSleep(200L);

			fis.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return decryptedDataBuff;
	}

//	public static String padRight(String s, int n) {
//	     return String.format("%1$-" + n + "s", s);
//	}

	private static String padLeftZero(int val, int cnt) { return String.format("%0" + cnt + "d", val); }

	private static Date getDateTimeNow() {
		return new Date(System.currentTimeMillis());
	}

	private static String getDateTimeStringNow() {
		return getDateTimeString(getDateTimeNow());
	}

	private static String getDateTimeString(Date datetime) {
		SimpleDateFormat formatter = new SimpleDateFormat ( "yyyy-MM-dd HH:mm:ss", Locale.KOREA );
		String dateTimeNowString = formatter.format ( datetime );

		return dateTimeNowString;
	}

	private static void threadSleep(long time) {
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
