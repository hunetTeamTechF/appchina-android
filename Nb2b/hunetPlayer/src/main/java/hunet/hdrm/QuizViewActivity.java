package hunet.hdrm;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.content.Intent;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.webkit.WebChromeClient;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.view.View;
import android.view.View.OnClickListener;
import android.app.ProgressDialog;
import hunet.library.hunetplayer.R;

/**
 * Created by albert on 2016. 1. 8..
 */
public class QuizViewActivity extends Activity {
    private WebView mWebView;
    private ProgressDialog m_LoadingDialog = null;
    private boolean m_IsFinish = false;
    LinearLayout backBtn;
    TextView header_text;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.library_web_view);
        Intent intent = getIntent();
        String WebUrl = intent.getStringExtra("WebUrl");
        String HeaderText = intent.getStringExtra("subTitle");
        backBtn = (LinearLayout) findViewById(R.id.more_ll_versioninfo_back);

        backBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        header_text = (TextView) findViewById(R.id.header_text_color);
        header_text.setText(HeaderText);

        mWebView = (WebView) findViewById(R.id.main_webView);
        mWebView.getSettings().setJavaScriptEnabled(true);
        //mWebView.getSettings().setLoadsImagesAutomatically(true);
        mWebView.loadUrl(WebUrl);

        mWebView.setWebChromeClient(new HunetWebChromeClient());
        mWebView.setWebViewClient(new WebViewClient() {

            public void onPageStarted(WebView view, String url, Bitmap favicon)
            {
                ShowDialog(QuizViewActivity.this, "", true);
            }

            public void onPageFinished(WebView view, String url) {
                HideDialog();
            }

        });

    }

//    private class WebViewClientClass extends WebViewClient {
//        @Override
//        public void onPageStarted(WebView view, String url, Bitmap favicon)
//        {
//            ShowDialog(QuizViewActivity.this, "", true);
//        }
//
//        @Override
//        public void onPageFinished(WebView view, String url)
//        {
//            HideDialog();
//        }
//
//        @Override
//        public boolean shouldOverrideUrlLoading(WebView view, String url) {
//            view.loadUrl(url);
//            return true;
//        }
//    }

    private void ShowDialog(Context context, String msg, boolean cancelable)
    {
        if(m_IsFinish || context == null || m_LoadingDialog != null)
            return;

        m_LoadingDialog = new ProgressDialog(context);
        m_LoadingDialog.setMessage("".equals(msg) ? "加载中..." : msg);
        m_LoadingDialog.setIndeterminate(true);
        m_LoadingDialog.setCancelable(cancelable);
        m_LoadingDialog.show();
    }

    private void HideDialog()
    {
        if(m_LoadingDialog == null)
            return;

        try
        {
            m_LoadingDialog.dismiss();
            m_LoadingDialog = null;
        }
        catch(Exception e) { }
    }

}
