package hunet.drm.download;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import hunet.data.SQLiteManager;
import hunet.domain.DomainAddress;
import hunet.drm.models.StudySangSangModel;
import hunet.drm.models.TakeSangSangModel;
import hunet.library.Utilities;
import hunet.library.hunetplayer.R;
import hunet.net.AsyncHttpRequestData;
import hunet.net.HttpData;
import hunet.net.IAsyncHttpRequestData;

public class DownloadSangSangContentsActivity extends Activity implements
        OnClickListener
        , ICourseIndexEventListener
        , IAsyncHttpRequestData {
    private static String userId = "";
    private ProgressDialog loadingDialog = null;
    //	private String externalDir = Environment.getExternalStorageDirectory() + "/hunet_mlc/";
    private AsyncHttpRequestData asyncReqData;
    private SQLiteManager sqlManager;
    private ImageButton btnPre;
    private LinearLayout llCourseIndexList;
    private TextView tvCourseTitle;
    private TextView tvCourseDescription;
    private String goodsId;
    private int contentsSeq = 0;

    private StudySangSangModel selectedModel;
    private TakeSangSangModel downTakeCourseModel;
    private boolean bDownloadAfterPlayerStart = false;
    private boolean isPlay = true;
    private String strStudyDate = "";

    private final int eSYNC_PLAYER_BEFORE_START = 1;
    private final int eSYNC_PLAYER_ENDED = 2;
    private final int eSYNC_PROGRESS = 3;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.library_download_course_index_activity);

        Utilities.changeTitlebarColor(this, R.id.download_course_index_fl, R.id.header_text_color);

        btnPre = (ImageButton) findViewById(R.id.btnPre);
        llCourseIndexList = (LinearLayout) findViewById(R.id.llCourseIndexList);
        tvCourseTitle = (TextView) findViewById(R.id.tvCourseTitle);
        tvCourseDescription = (TextView) findViewById(R.id.tvCourseDescription);
        sqlManager = new SQLiteManager(this);
        selectedModel = new StudySangSangModel();
        asyncReqData = new AsyncHttpRequestData();

        btnPre.setOnClickListener(this);
        asyncReqData.SetEventListener(this);
        //currentAct = this;

        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            goodsId = bundle.getString("goodsId");
            if (bundle.containsKey("contentsSeq"))
                contentsSeq = bundle.getInt("contentsSeq");

        }

        downTakeCourseModel = sqlManager.GetDownTakeSangSangData(goodsId);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    private long GetRemainStudyDate(String studyEndDate) {
        String[] arDate = studyEndDate.split("-");

        if (arDate.length != 3)
            return 0;

        int year = Integer.parseInt(arDate[0]);
        int month = Integer.parseInt(arDate[1]) - 1;
        int day = Integer.parseInt(arDate[2]);

        Calendar curCal = Calendar.getInstance();
        Calendar studyEndCal = Calendar.getInstance();
        studyEndCal.set(year, month, day);

        long remainDay = ((studyEndCal.getTime().getTime() - curCal.getTime().getTime()) / 86400000); // 1000 * 60 * 60 * 24

        return remainDay;
    }

    private void Refresh() {
        tvCourseTitle.setText(downTakeCourseModel.goods_nm);

        long day = GetRemainStudyDate(downTakeCourseModel.study_end_date);
        String strText = String.format("&middot; 학습기간 : %s까지&nbsp;%s"
                , downTakeCourseModel.study_end_date, day <= 0 ? "<font color=#ee0000>(학습 기간 종료)</font>" : "<font color=#b37e4d>(" + day + "일 남음)</font>");

        tvCourseDescription.setText(Html.fromHtml(strText));

        int nLength = llCourseIndexList.getChildCount();

        for (int nIndex = nLength - 1; nIndex >= 0; nIndex--) {
            CourseIndexLayout courseIndexLayout = (CourseIndexLayout) llCourseIndexList.getChildAt(nIndex);
            courseIndexLayout.SetEventListener(null);
            llCourseIndexList.removeViewAt(nIndex);
        }

        TakeSangSangModel takeCourseModel = sqlManager.GetDownTakeSangSangData(goodsId);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String strCurTime = sdf.format(new Date());
        strStudyDate = takeCourseModel == null ? "" : takeCourseModel.study_end_date;

        if ("".equals(strStudyDate) || strCurTime.compareTo(takeCourseModel.study_end_date) >= 0)
            isPlay = false;

        ArrayList<StudySangSangModel> modelList = sqlManager.GetDownStudySangSangDataList(goodsId);
        nLength = modelList.size();

        if (!Utilities.getExternalMemoryCheck(getApplicationContext())) {
            int nIndex = 0;
            int nSize = modelList.size();
            if (nSize > 0) {
                while (true) {
                    if (nIndex > (nSize - 1)) {
                        break;
                    }
                    StudySangSangModel model = modelList.get(nIndex);
                    if (TextUtils.equals(model.external_memory, "2")) {
                        modelList.remove(nIndex);
                        nSize--;
                        continue;
                    }
                    nIndex++;

                }
                nLength = modelList.size();
            }
        }

        for (int nIndex = 0; nIndex < nLength; nIndex++) {
            CourseIndexLayout courseIndexLayout = new CourseIndexLayout(this);
            StudySangSangModel model = modelList.get(nIndex);
            //courseIndexLayout.index.setText("");
            courseIndexLayout.index.setVisibility(View.GONE);
            courseIndexLayout.SetDownSangSangInfo(model);
            courseIndexLayout.SetEventListener(this);
            if (contentsSeq == 0)
                llCourseIndexList.addView(courseIndexLayout);
            else {
                if (contentsSeq == model.contents_seq) {
                    llCourseIndexList.addView(courseIndexLayout);
                    selectedModel = model;
                }
            }
        }

        if (nLength == 0) {
            Toast.makeText(this, "차시 목록이 존재하지 않습니다.", Toast.LENGTH_LONG).show();
            DoBack();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        //appMain.setTabNo(4);
        //AddTab(R.id.more_ll_down_course_index_tab);

//		AccessModel accessModel = sqlManager.GetAccessData();
//		if(TextUtils.equals(accessModel.external_memory, "1")){
//			if(Utilities.getExternalMemoryCheck(getApplicationContext())){
//				accessModel.external_memory = "1";
//			}else{
//				accessModel.external_memory = "0";
//			}
//		}
//		sqlManager.SetAccessData(accessModel);

        Refresh();

        if (bDownloadAfterPlayerStart == false)
            return;

        bDownloadAfterPlayerStart = false;

        if (TextUtils.isEmpty(selectedModel.external_memory)) {
            selectedModel = sqlManager.GetDownStudySangSangData(selectedModel.goods_id, selectedModel.contents_seq);
        } else {
            selectedModel = sqlManager.GetDownStudySangSangData(selectedModel.goods_id, selectedModel.contents_seq, selectedModel.external_memory);
        }

        SendProgressSync(eSYNC_PLAYER_ENDED, selectedModel);
    }

    @Override
    protected void onPause() {
        super.onResume();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                DoBack();
                break;
        }

        return super.onKeyDown(keyCode, event);
    }

    private void SendProgressSync(int eSendType, StudySangSangModel model) {
        StudySangSangModel temp = null;

        if (TextUtils.isEmpty(model.external_memory)) {
            temp = sqlManager.GetDownStudySangSangData(model.goods_id, model.contents_seq);
        } else {
            temp = sqlManager.GetDownStudySangSangData(model.goods_id, model.contents_seq);
        }

        temp = sqlManager.GetDownStudySangSangData(model.goods_id, model.contents_seq);

        String strReqUrl = DomainAddress.getUrlApps() + "/Hunet_Player/Android.aspx";
        String strReqParam = String.format("action=SangSangProgressUpdateV2&userId=%s&goodsId=%s&contentsSeq=%d&viewSec=%d&viewSecMobile=%d&lastViewSec=%d&viewNo=%d"
                , model.user_id, model.goods_id
                , model.contents_seq, model.view_sec, model.view_sec_mobile, model.last_view_sec, model.view_no);
        if (eSendType == eSYNC_PLAYER_BEFORE_START) {
            strReqParam = strReqParam + "&mode=S";
        }
        if (eSendType == eSYNC_PROGRESS) {
            strReqParam = strReqParam + "&mode=A";
        }
        asyncReqData.Request(eSendType, strReqUrl, strReqParam, null);
    }

    private void UpdateDownStudySangSangData(StudySangSangModel model) {
        //DownStudySangSangModel model = new DownStudySangSangModel();
        //model.goods_id 	= goodsId;
        //model.contents_seq 			= contentsSeq;
        //model.max_view_sec 		= max_view_sec;

        sqlManager.SetDownStudySangSangData(model);
    }

    private void DoBack() {
        /*Intent intent = new Intent(DownloadCourseIndexActivity.this, DownloadCourseActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);*/
        finish();
    }

    @Override
    public void onClick(View view) {
        Intent intent = null;

        if (view.getId() == R.id.btnPre)
            DoBack();

        if (intent == null)
            return;

        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
        finish();
    }

    @Override
    public void OnClickPlay(CourseIndexLayout object) {
        StudySangSangModel model = object.GetDownStudySangSangModel();

        if (isPlay == false) {
            Toast.makeText(this, "학습 기간이 종료되어 학습을 할 수 없습니다. 삭제 후 다시 다운 받아주세요.", Toast.LENGTH_LONG).show();
            return;
        }

//		String 	downFileName = "";
//		if(model.external_memory.equalsIgnoreCase("0")){
//			String externalDir = Environment.getExternalStorageDirectory() + "/hunet_mlc/";
//			downFileName = String.format("%s%s_%d.mp4", externalDir, model.user_id, model.contents_seq);
//		}else{
//			downFileName = Utilities.getDownloadPullPath(getApplicationContext(), 
//							model.user_id, String.valueOf(model.contents_seq));
//		}

        String downFileName = model.full_path;

        File file = new File(downFileName);

        if (file.exists() == false) {
            Toast.makeText(this, "파일이 존재 하지 않습니다. 확인 후 다시 시도해 주세요.", Toast.LENGTH_LONG).show();
            return;
        }

        selectedModel.user_id = model.user_id;
        selectedModel.goods_id = model.goods_id;
        selectedModel.contents_seq = model.contents_seq;
        selectedModel.contents_nm = model.contents_nm;
        selectedModel.last_view_sec = model.last_view_sec;
        selectedModel.view_sec = model.view_sec;
        selectedModel.view_sec_mobile = model.view_sec_mobile;
        selectedModel.view_no = model.view_no;
        selectedModel.contents_view_sec = model.contents_view_sec;
        selectedModel.full_path = model.full_path;
        selectedModel.external_memory = model.external_memory;

        ShowDialog(this, "정보 구성중입니다.\n잠시만 기다려 주세요.", false);
        SendProgressSync(eSYNC_PLAYER_BEFORE_START, model);
    }

    @Override
    public void OnClickSync(CourseIndexLayout object) {
        final StudySangSangModel model = object.GetDownStudySangSangModel();

        new AlertDialog.Builder(this).setTitle("동기화 확인").setMessage(String.format("%s\n동기화를 진행 하시겠습니까?", model.contents_nm))
                .setPositiveButton("확인", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ShowDialog(DownloadSangSangContentsActivity.this, "동기화 진행중 입니다.\n\n잠시만 기다려주세요.", false);
                        SendProgressSync(eSYNC_PROGRESS, model);
                    }
                })
                .setNegativeButton("취소", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                }).show();
    }

    public void ShowDialog(Context context, String msg, boolean cancelable) {
        if (loadingDialog != null)
            return;

        loadingDialog = new ProgressDialog(context);
        loadingDialog.setMessage("".equals(msg) ? "로딩하는 중입니다..." : msg);
        loadingDialog.setIndeterminate(true);
        loadingDialog.setCancelable(cancelable);
        loadingDialog.show();
    }

    public void HideDialog() {
        if (loadingDialog != null) {
            try {
                loadingDialog.dismiss();
                loadingDialog = null;
            } catch (Exception e) {
            }
        }
    }

    @Override
    public void OnClickRemove(CourseIndexLayout object) {
        final StudySangSangModel model = object.GetDownStudySangSangModel();

        new AlertDialog.Builder(this).setTitle("삭제 확인").setMessage(String.format("%s\n동영상을 삭제 하시겠습니까?", model.contents_nm))
                .setPositiveButton("삭제", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
//	    			String 	strFilePath = "";
//	    			if(model.external_memory.equalsIgnoreCase("0")){
//	    				String externalDir = Environment.getExternalStorageDirectory() + "/hunet_mlc/";
//	    				strFilePath = String.format("%s%s_%d.mp4", externalDir, model.user_id, model.contents_seq);
//	    			}else{
//	    				strFilePath = Utilities.getDownloadPullPath(getApplicationContext(), 
//		    					model.user_id, String.valueOf(model.contents_seq));
//	    			}

                            String strFilePath = model.full_path;

                            File file = new File(strFilePath);
                            if (file.exists())
                                file.delete();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        sqlManager.DeleteDownStudySangSangData(model.goods_id, model.contents_seq, model.external_memory);
                        int nCount = sqlManager.GetDownStudySangSangDataCount(model.se_goods_id);

                        Toast.makeText(DownloadSangSangContentsActivity.this, "삭제가 완료 되었습니다.", Toast.LENGTH_LONG).show();

                        if (nCount > 0) {
                            Refresh();
                            return;
                        }

                        sqlManager.DeleteDownTakeSangSangData(model.se_goods_id);
                        DoBack();
                    }
                })
                .setNegativeButton("취소", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                }).show();
    }

    @Override
    public void AsyncRequestDataResult(HttpData result) {
        // TODO Auto-generated method stub
        if ("YES".equals(result.GetJsonValue("IsSuccess"))) {
            selectedModel.last_view_sec = Integer.parseInt(result.GetJsonValue("MaxViewSec"));
            UpdateDownStudySangSangData(selectedModel);
        }

        switch (result.id) {
            case eSYNC_PROGRESS: {
                HideDialog();
                if ("YES".equals(result.GetJsonValue("IsSuccess")))
                    Toast.makeText(this, "동기화가 완료 되었습니다.", Toast.LENGTH_LONG).show();
                else
                    Toast.makeText(this, "동기화가 실패 하였습니다.\n잠시 후 다시 시도해 주세요.", Toast.LENGTH_LONG).show();
            }
            break;
            case eSYNC_PLAYER_BEFORE_START: {
                bDownloadAfterPlayerStart = true;
                HideDialog();
                Intent intent = new Intent(this, hunet.player.stwplayer.HunetStwPlayerActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                intent.putExtra("playerType", "localSangSang");
                intent.putExtra("companySeq", selectedModel.company_seq);
                intent.putExtra("contractNo", selectedModel.contract_no);
                intent.putExtra("goodsId", selectedModel.goods_id);
                intent.putExtra("userId", selectedModel.user_id);
                intent.putExtra("contentsSeq", selectedModel.contents_seq);
                intent.putExtra("contents_view_sec", selectedModel.contents_view_sec);
                intent.putExtra("full_path", selectedModel.full_path);
                intent.putExtra("external_memory", selectedModel.external_memory);

                startActivity(intent);
            }
            break;
            case eSYNC_PLAYER_ENDED: {

            }
            break;
        }
    }

}
