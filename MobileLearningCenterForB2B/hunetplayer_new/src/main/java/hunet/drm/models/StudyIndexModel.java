package hunet.drm.models;

public class StudyIndexModel 
{
	public int take_course_seq = 0;
	public String chapter_no = "";
	public int index_no = 0;
	public String index_nm = "";
	public int max_mark_sec = 0;
	public int max_view_sec = 0;	
	public int view_index = 0;	
	public int study_sec = 0;
	public int last_view_sec = 0;
	public String full_path = "";
	public String external_memory = "";
}
