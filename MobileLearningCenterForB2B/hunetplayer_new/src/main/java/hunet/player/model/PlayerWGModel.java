package hunet.player.model;

import hunet.core.JsonParse;
import hunet.domain.DomainAddress;
import hunet.net.HttpData;
import hunet.player.model.restriction.ItemType;
import hunet.player.model.restriction.RestrictionEntity;
import hunet.player.model.restriction.RestrictionType;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

public class PlayerWGModel extends PlayerModelBase {
	
	private static final String TAG = "PlayerActivity";
	
	// default info
	private String courseCd = "";
	private int takeCourseSeq = 0;
	private String chapterNo = "";
	private int frameNo = 0;
	private String userId = "";

	// progress info
	private String reviewEndDate = "";
	private int courseType = 0;
	private String parentCd = "";
	private String seamlessYn = "";
	private int progressNo = 0;
	private int lastMarkNo = 0;
	private String listMovYn = "";
	private String progressRestrictionYn = "";
	private int studySec = 0;
	private int mobileStudySec = 0;
	private boolean hasPpt = false;

	// contents url
	private String contentsUrl = "";

	private int lastViewSec = 0;

	public PlayerWGModel(Activity activity) {
		super(activity);
		Log.d("PlayerStudyDataWeb", "PlayerStudyDataWeb");
		Bundle bundle = activity.getIntent().getExtras();
		m_arMarkList = new ArrayList<MarkDataModel>();
		m_CurMarkData = new MarkDataModel();

		if (bundle == null)
			return;

		ParseDefaultInfo(bundle.getString("defaultInfo"));
		ParseProgressInfo(bundle.getString("jsonProgressInfo"));
		ParsePlayUrl(bundle.getString("jsonPlayUrl"));
		ParseMarkData(bundle.getString("jsonMarkList"));
		GetTitle();
		int nTempLastViewSec = m_CurMarkData.markValue / 1000;

		if (m_arMarkList.size() > 0) {
			if (m_arMarkList.get(m_arMarkList.size() - 1).markNo == m_CurMarkData.markNo)
				nTempLastViewSec = 0;
		}

		lastViewSec = nTempLastViewSec;
	}

	private void GetTitle() {
		if (title == "") {
			HttpData data = new HttpData();
			data.url = DomainAddress.getUrlApps() + "/Hunet_Player/Android.aspx";
			data.param = String.format("action=GetIndexNm&courseCd=%s&chapterNo=%s&frameNo=%s", courseCd, chapterNo, frameNo);
			hunet.net.HttpRequester.GetPostData(data);
			super.title = data.GetJsonValue("Title");
		}
	}

	private void ParseDefaultInfo(String strData) {
		String[] arValue = strData.split("/");

		if (arValue.length != 5)
			return;

		courseCd = arValue[0];
		takeCourseSeq = Integer.parseInt(arValue[1]);
		chapterNo = arValue[2];
		frameNo = Integer.parseInt(arValue[3]);
		userId = arValue[4];
	}

	private void ParseProgressInfo(String strData) {
		JsonParse jsonParse = new JsonParse();
		jsonParse.SetData(strData);

		reviewEndDate = jsonParse.GetJsonValue("review_end_date");
		courseType = Integer.parseInt(jsonParse.GetJsonValue("course_type", "0"));
		progressNo = Integer.parseInt(jsonParse.GetJsonValue("progress_no", "0"));
		lastMarkNo = Integer.parseInt(jsonParse.GetJsonValue("last_mark_no","0"));
		listMovYn = jsonParse.GetJsonValue("list_mov_yn");
		progressRestrictionYn = jsonParse.GetJsonValue("progress_restriction_yn");
		studySec = Integer.parseInt(jsonParse.GetJsonValue("study_sec", "0"));
		mobileStudySec = Integer.parseInt(jsonParse.GetJsonValue("mobile_study_sec", "0"));
		contentsUrl = jsonParse.GetJsonValue("contents_url").replace("http://hunet2.hscdn.com/hunetvod/_definst_/mp4:M_learning/", "http://m.hunet.hscdn.com/hunet/M_learning/").replace("/playlist.m3u8", "");
		title = jsonParse.GetJsonValue("title");
	}

	private void ParsePlayUrl(String strData) {
		if (contentsUrl == "") {
			JsonParse jsonParse = new JsonParse();
			jsonParse.SetData(strData);

			contentsUrl = jsonParse.GetJsonValue("contents_url");
		}
	}

	private void ParseMarkData(String strData) {
		JsonParse jsonParse = new JsonParse();
		jsonParse.SetData(strData);
		JSONArray arJSONList = jsonParse.GetJsonArray("List");

		if (arJSONList == null)
			return;

		int nLength = arJSONList.length();

		for (int nIndex = 0; nIndex < nLength; nIndex++) {
			try {
				JSONObject jsonObj = arJSONList.getJSONObject(nIndex);
				MarkDataModel data = new MarkDataModel();
				data.index = nIndex;
				data.markNo = Integer.parseInt(jsonParse.GetJsonValue(jsonObj, "mark_no", "0"));
				data.markNm = jsonParse.GetJsonValue(jsonObj, "mark_nm", "");
				data.markValue = Integer.parseInt(jsonParse.GetJsonValue(jsonObj, "mark_value", "0"));
				data.pptUrl = jsonParse.GetJsonValue(jsonObj, "ppt_url", "");
				data.realFrameNo = Integer.parseInt(jsonParse.GetJsonValue(jsonObj, "real_frame_no", "0"));
				data.progressYn = jsonParse.GetJsonValue(jsonObj, "progress_yn", "");

				m_arMarkList.add(data);
				if (!hasPpt && data.pptUrl != null && !data.pptUrl.equals("notPt"))
					hasPpt = true;

				if (lastMarkNo == data.markNo)
					m_CurMarkData = data;
			} catch (Exception e) { }
		}
	}

	@Override
	public boolean CheckProgressRestriction() {
		return "Y".equals(progressRestrictionYn);
	}

	@Override
	public boolean CheckSendProgress(int lastViewSec, int studySec) {
		this.lastViewSec = lastViewSec;
		MarkDataModel tempData = null;
		
//		Log.i(TAG,"CheckSendProgress current time: " + lastViewSec + " playstudysec : " +  studySec);

		for (int nIndex = 0; nIndex < m_arMarkList.size(); nIndex++) {
			int nTempMarkValue = m_arMarkList.get(nIndex).markValue / 1000;

			if (nTempMarkValue <= lastViewSec){
//				Log.i(TAG,"tempData insert index : " + nIndex);
				tempData = m_arMarkList.get(nIndex);
			}
		}

		if (tempData != null && m_CurMarkData != tempData) {
//			Log.i(TAG,"curMarkData Change : " + m_CurMarkData.index + " tempData Index : " + tempData.index);
			m_CurMarkData = tempData;
			return true;
		}

		return false;
	}

	@Override
	public void SaveProgress(int lastViewSec, int studySec) {
		Log.i(TAG,"WG SaveProgress lastViewSec : " + lastViewSec + " studySec : " +  studySec);
		String strUrl = GetProgressUrl(lastViewSec, studySec);
		m_SqlManager.InsertStudyProgress(strUrl, userId);
		super.m_ParentActivity.startService(m_StudyProgressService);
	}

	@Override
	public int GetLastViewSec() {
		return lastViewSec;
	}

	@Override
	public String GetPlayerUrl() {
		return contentsUrl;
	}

	private String GetProgressUrl(int lastViewSec, int studySec) {
		String strUrl = DomainAddress.getUrlApps() + "/Hunet_Player/Android.aspx?action=ProgressUpdate"
				+ "&user_id=" + userId
				+ "&course_cd=" + courseCd
				+ "&take_course_seq=" + takeCourseSeq
				+ "&chapter_no=" + chapterNo
				+ "&progress_no=" + progressNo
				+ "&frame_no=" + frameNo
				+ "&mark_no=" + m_CurMarkData.markNo
				+ "&study_sec=" + (this.studySec + studySec)
				+ "&mobile_study_sec=" + (mobileStudySec + studySec);

		return strUrl;
	}

	@Override
	public boolean HasPpt() {
		return hasPpt;
	}

	@Override
	public boolean ShowSpeedControl() {
		RestrictionEntity entity = getRestrictionEntity(courseCd, ItemType.Online);

		if (entity != null) {
			RestrictionType restrictionType = entity.restrictionType;

			if (restrictionType == RestrictionType.Hide_SpeedBar
					|| restrictionType == RestrictionType.Hide_SpeedBar_CompletedButton)
				return false;
		}

		return true;
	}

	@Override
	public boolean ShowCompletedButton() {
		RestrictionEntity entity = getRestrictionEntity(courseCd, ItemType.Online);

		if (entity != null) {
			RestrictionType restrictionType = entity.restrictionType;

			if (restrictionType == RestrictionType.Hide_CompletedButton
					|| restrictionType == RestrictionType.Hide_SpeedBar_CompletedButton)
				return false;
		}

		return true;
	}

	@Override
	public boolean CheckOverlapStudyRestriction() {
		return false;
	}

	@Override
	public boolean IsOverlapStudy() {
		return false;
	}
}
