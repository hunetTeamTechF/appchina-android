package hunet.player.model.restriction;

/**
 * Created by zeple on 2015. 9. 16..
 */
public enum RestrictionType {
    Hide_SpeedBar,
    Hide_CompletedButton,
    Hide_SpeedBar_CompletedButton
}
