package hunet.player.model;

import hunet.core.JsonParse;
import hunet.domain.DomainAddress;
import hunet.drm.models.AppSettingsModel;
import hunet.net.AsyncHttpRequestData;
import hunet.net.HttpData;
import hunet.net.IAsyncHttpRequestData;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import org.json.JSONObject;

public class PlayerWSModel extends PlayerModelBase implements IAsyncHttpRequestData {
	private AsyncHttpRequestData asyncHttpRequestData;
	private AppSettingsModel appSettingsModel;

	private String userId = "";
	private String progressRestrictionYn = "";
	private boolean hasPpt = false;
	private String companySeq = "";
	private String contentsUrl = "";
	private int contractNo; // 계약번호
	private int contentsSeq; // 컨텐츠 번호
	private String goodsId; // 상품번호
	
	private boolean useSpeedBar = false;

	private int lastViewSec = 0;
	private int viewSec = 0;
	private int viewSecMobile = 0;
	private String guid = "";
	private boolean isOverlapStudy = false;

	private HttpData data;

	public PlayerWSModel(Activity activity) {
		super(activity);
		Bundle bundle = activity.getIntent().getExtras();
		// 상상마루 필수 파라미터
		if (bundle != null && bundle.containsKey("companySeq")) {
			companySeq = bundle.getString("companySeq");
			appSettingsModel = m_SqlManager.GetAppSettings(Integer.parseInt(companySeq));
			useSpeedBar = "Y".equalsIgnoreCase(appSettingsModel.enable_sangsang_player_speedbar);
		}
        
		if (bundle != null && bundle.containsKey("jsonDataLoaded")
				&& bundle.getString("jsonDataLoaded") != "")
			ParseInfo(bundle.getString("jsonDataLoaded"));
		else {
			if (bundle != null && bundle.containsKey("contractNo"))
				contractNo = bundle.getInt("contractNo");
			if (bundle != null && bundle.containsKey("contentsSeq"))
				contentsSeq = bundle.getInt("contentsSeq");
			if (bundle != null && bundle.containsKey("goodsId"))
				goodsId = bundle.getString("goodsId");
			if (bundle != null && bundle.containsKey("userId"))
				userId = bundle.getString("userId");
			
			data = new HttpData();
			// data.EncodingString = "EUC-KR";
			data.url = DomainAddress.getUrlMLC() + "/App/JLog.aspx";
			data.param = String
					.format("type=21&company_seq=%s&uid=%s&contract_no=%s&contents_seq=%s&goods_id=%s",
							companySeq, userId, contractNo, contentsSeq,
							goodsId);
			hunet.net.HttpRequester.GetPostData(data);
			contentsUrl = data
					.GetJsonValue("url")
					.replace(
							"http://hunet2.hscdn.com/hunetvod/_definst_/mp4:Mvod/",
							"http://m.hunet.hscdn.com/hunet/Mvod/")
					.replace("/playlist.m3u8", "");
			lastViewSec = Integer.parseInt(data.GetJsonValue("last_view_sec"));
			viewSec = Integer.parseInt(data.GetJsonValue("view_sec"));
			viewSecMobile = Integer.parseInt(data
					.GetJsonValue("view_sec_mobile"));
			progressRestrictionYn = data.GetJsonValue("pass_yn");
			super.title = data.GetJsonValue("contents_nm");
		}

		asyncHttpRequestData = new AsyncHttpRequestData();
		asyncHttpRequestData.SetEventListener(this);
		CheckSSOverlapInfo();
	}

	public void ParseInfo(String input) {
		JsonParse jsonParse = new JsonParse();
		jsonParse.SetData(input);		
		contractNo = Integer.parseInt(jsonParse.GetJsonValue("contract_no"));		
		contentsSeq =Integer.parseInt(jsonParse.GetJsonValue("contents_seq"));		
		goodsId = jsonParse.GetJsonValue("goods_id");	
		userId = jsonParse.GetJsonValue("user_id");		
		contentsUrl = jsonParse
				.GetJsonValue("url")
				.replace(
						"http://hunet2.hscdn.com/hunetvod/_definst_/mp4:Mvod/",
						"http://m.hunet.hscdn.com/hunet/Mvod/")
				.replace("/playlist.m3u8", "");
		lastViewSec = Integer.parseInt(jsonParse.GetJsonValue("last_view_sec"));
		viewSec = Integer.parseInt(jsonParse.GetJsonValue("view_sec"));
		viewSecMobile = Integer.parseInt(jsonParse
				.GetJsonValue("view_sec_mobile"));
		progressRestrictionYn = jsonParse.GetJsonValue("pass_yn");
		super.title = jsonParse.GetJsonValue("contents_nm");
	}

	@Override
	public boolean CheckProgressRestriction() {
		return "Y".equals(progressRestrictionYn);
	}

	@Override
	public boolean CheckSendProgress(int lastViewSec, int studySec) {
		boolean result = false;
		this.lastViewSec = lastViewSec;
		if (studySec > 0 && studySec % 60 == 0)
			result = true;
		return result;
	}

	@Override
	public void SaveProgress(int lastViewSec, int studySec) {
		String strUrl = GetProgressUrl(lastViewSec, studySec);
		Log.d("PlayerStudyDataWeb", strUrl);
		m_SqlManager.InsertStudyProgress(strUrl, userId);
		super.m_ParentActivity.startService(m_StudyProgressService);

		CheckSSOverlapInfo();
	}

	@Override
	public int GetLastViewSec() {
		return lastViewSec;
	}

	@Override
	public String GetPlayerUrl() {
		return contentsUrl;
	}

	private String GetProgressUrl(int lastViewSec, int studySec) {
		String strUrl = String
				.format(DomainAddress.getUrlMLC() + "/App/JLog.aspx?type=22&company_seq=%s&uid=%s&contract_no=%s&contents_seq=%s&goods_id=%s&view_sec=%d&view_sec_mobile=%d&last_view_sec=%d",
						companySeq, userId, contractNo, contentsSeq, goodsId,
						viewSec + studySec, viewSecMobile + studySec,
						lastViewSec);

		Log.d("test", String.format("viewSec : %d / viewSecMobile : %d / url : %s", viewSec, viewSecMobile, strUrl));

		return strUrl;
	}

	@Override
	public boolean HasPpt() {
		return hasPpt;
	}

	@Override
	public boolean ShowSpeedControl() {
		return useSpeedBar;
	}

	@Override
	public boolean ShowCompletedButton() {
		return true;
	}

	@Override
	public boolean CheckOverlapStudyRestriction() {
		return "N".equalsIgnoreCase(appSettingsModel.enable_sangsang_player_overlap_study);
	}

	@Override
	public boolean IsOverlapStudy() {
		return isOverlapStudy;
	}

	private void CheckSSOverlapInfo() {
		if (!DomainAddress.IS_STAGING) {
			if ("N".equalsIgnoreCase(appSettingsModel.enable_sangsang_player_overlap_study)) {
				String url = DomainAddress.getUrlApps() + "/api.aspx";
				String params = String.format("action=SangSangPlayerOverlapCheck&user_id=%s&guid=%s", userId, guid);
				asyncHttpRequestData.Request(0, url, params, null);
			}
		}
	}

	@Override
	public void AsyncRequestDataResult(HttpData result) {
		if (result.id == 0) {
			try {
				JSONObject json = new JSONObject(result.GetData());
				isOverlapStudy = (json.getInt("ResultCode") == 0);
				guid = json.getString("Data");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
