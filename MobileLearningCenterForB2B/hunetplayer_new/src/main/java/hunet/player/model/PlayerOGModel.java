package hunet.player.model;


import hunet.drm.models.StudyIndexModel;
import hunet.drm.models.TakeCourseModel;
import hunet.library.Utilities;
import hunet.player.model.restriction.ItemType;
import hunet.player.model.restriction.RestrictionEntity;
import hunet.player.model.restriction.RestrictionType;

import android.app.Activity;
import android.content.RestrictionEntry;
import android.os.Bundle;
import android.util.Log;

public class PlayerOGModel extends PlayerModelBase {

    private static final String TAG = "PlayerActivity";

    private StudyIndexModel m_DownStudyIndexModel;
    private String m_strFilePath = "";
    private int take_course_seq = 0;
    private String chapter_no = "";
    private int max_mark_sec = 0;
    private String m_external_memory = "";

    public PlayerOGModel(Activity activity) {
        super(activity);

        Bundle bundle = activity.getIntent().getExtras();

        if (bundle == null)
            return;

        take_course_seq = bundle.getInt("take_course_seq", 0);
        chapter_no = bundle.getString("chapter_no");
        max_mark_sec = bundle.getInt("max_mark_sec", 0);
//		m_strFilePath 			= String.format("%s%d_%s.mp4", Utilities.GetExternalDir(), take_course_seq, chapter_no);
        m_strFilePath = bundle.getString("full_path");
        m_external_memory = bundle.getString("external_memory");
        m_DownStudyIndexModel = m_SqlManager.GetDownStudyIndexData(take_course_seq, chapter_no, m_external_memory);
        super.title = bundle.getString("indexNm");
        m_DownStudyIndexModel.take_course_seq = take_course_seq;
        m_DownStudyIndexModel.chapter_no = chapter_no;
    }


    @Override
    public boolean CheckProgressRestriction() {
        return m_DownStudyIndexModel.max_view_sec >= max_mark_sec;
    }

    @Override
    public boolean CheckSendProgress(int lastViewSec, int studySec) {
        if (studySec % 60 > 0)
            return false;

        return true;
    }

    @Override
    public void SaveProgress(int lastViewSec, int studySec) {
        //if(lastViewSec > m_DownStudyIndexModel.max_view_sec)
        Log.i(TAG, "OG SaveProgress lastViewSec : " + lastViewSec + " studySec : " + studySec);
        m_DownStudyIndexModel.last_view_sec = lastViewSec;
        m_DownStudyIndexModel.study_sec = studySec;
        m_SqlManager.SetDownStudyIndexData(m_DownStudyIndexModel);
    }

    @Override
    public int GetLastViewSec() {
        return m_DownStudyIndexModel.last_view_sec;
    }

    @Override
    public String GetPlayerUrl() {
        return m_strFilePath;
    }

    @Override
    public boolean HasPpt() {
        return false;
    }

    @Override
    public boolean ShowSpeedControl() {
        TakeCourseModel takeCourseModel = m_SqlManager.GetDownTakeCourseData(take_course_seq);
        RestrictionEntity entity = getRestrictionEntity(takeCourseModel.course_cd, ItemType.Online);

        if (entity != null) {
            RestrictionType restrictionType = entity.restrictionType;

            if (restrictionType == RestrictionType.Hide_SpeedBar
                    || restrictionType == RestrictionType.Hide_SpeedBar_CompletedButton)
                return false;
        }

        return true;
    }

    @Override
    public boolean ShowCompletedButton() {
        TakeCourseModel takeCourseModel = m_SqlManager.GetDownTakeCourseData(take_course_seq);
        RestrictionEntity entity = getRestrictionEntity(takeCourseModel.course_cd, ItemType.Online);

        if (entity != null) {
            RestrictionType restrictionType = entity.restrictionType;

            if (restrictionType == RestrictionType.Hide_CompletedButton
                    || restrictionType == RestrictionType.Hide_SpeedBar_CompletedButton)
                return false;
        }

        return true;
    }

    @Override
    public boolean CheckOverlapStudyRestriction() {
        return false;
    }

    @Override
    public boolean IsOverlapStudy() {
        return false;
    }
}
