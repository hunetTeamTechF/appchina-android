package hunet.player.interfaces;

import android.view.MotionEvent;
import android.view.View;

public interface ISpeedEventListener 
{
	public boolean OnSpeedDown();
	public boolean OnSpeedUp();
}
