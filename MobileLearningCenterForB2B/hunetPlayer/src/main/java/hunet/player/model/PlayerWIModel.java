package hunet.player.model;

import hunet.domain.DomainAddress;
import hunet.net.HttpData;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

public class PlayerWIModel extends PlayerModelBase
{	
	// default info
	private String 	courseCd		= "";
	private int 	takeCourseSeq	= 0;
	private String 	chapterNo		= "";
	private int 	frameNo			= 0;
	private String 	userId			= "";
	
	// progress info
	private String 	reviewEndDate 			= "";
	private int 	courseType 				= 0;
	private String 	parentCd 				= "";
	private String 	seamlessYn 				= "";
	private int 	progressNo 				= 0;
	private int 	lastMarkNo 				= 0;
	private String 	listMovYn 				= "";
	private String 	progressRestrictionYn 	= "";
	private int 	studySec 				= 0;
	private int 	mobileStudySec 			= 0;
	private boolean  hasPpt = false;
	
	// contents url
	private String 	contentsUrl = "";
	

	private int lastViewSec	= 0;
	
	public PlayerWIModel(Activity activity)
	{
		super(activity);		
		Bundle bundle 	= activity.getIntent().getExtras();
		m_arMarkList 	= new ArrayList<MarkDataModel>();
		m_CurMarkData	= new MarkDataModel();
        
        if(bundle == null)
        	return;
        
        ParseDefaultInfo(bundle);
        ParseProgressInfo();
        ParsePlayUrl();
        ParseMarkData();
        GetTitle();
        int nTempLastViewSec = m_CurMarkData.markValue / 1000;
        
        if(m_arMarkList.size() > 0)
        {
        	if(m_arMarkList.get(m_arMarkList.size() - 1).markNo == m_CurMarkData.markNo)
        		nTempLastViewSec = 0;
        }
        
        lastViewSec = nTempLastViewSec;
	}
	
	private void GetTitle()
	{
		HttpData data = new HttpData();
		data.url = DomainAddress.getUrlApps() + "/Hunet_Player/Android.aspx";
		data.param = String.format("action=GetIndexNm&courseCd=%s&chapterNo=%s&frameNo=%d",courseCd,chapterNo,1 );		
        hunet.net.HttpRequester.GetPostData(data);   
        super.title = data.GetJsonValue("Title");
        		
	}
	private void ParseDefaultInfo(Bundle bundle)
	{
		courseCd =bundle.getString("course_cd");
		takeCourseSeq =Integer.parseInt(bundle.getString("take_course_seq"));
		chapterNo = bundle.getString("chapter_no");		
		frameNo = bundle.getInt("frame_no");
		userId = bundle.getString("user_id");	
	}
	
	private void ParseProgressInfo()
	{
		HttpData data = new HttpData();
		data.url = DomainAddress.getUrlApps() + "/Hunet_Player/Android.aspx";
		data.param = String.format("action=SelectProgressInfoByInmun&user_id=%s&course_cd=%s&chapter_no=%s&frame_no=%d&take_course_seq=%d"
				, "", courseCd, chapterNo, frameNo, takeCourseSeq);
		hunet.net.HttpRequester.GetPostData(data);		
		
		reviewEndDate 			= data.GetJsonValue("review_end_date");
		courseType 				= Integer.parseInt(data.GetJsonValue("course_type", "0"));
		progressNo 				= Integer.parseInt(data.GetJsonValue("progress_no", "0"));
		lastMarkNo 				= Integer.parseInt(data.GetJsonValue("last_mark_no", "0"));
		listMovYn 				= data.GetJsonValue("list_mov_yn");
		progressRestrictionYn 	= data.GetJsonValue("progress_restriction_yn");
		studySec 				= Integer.parseInt(data.GetJsonValue("study_sec", "0"));
		mobileStudySec 			= Integer.parseInt(data.GetJsonValue("mobile_study_sec", "0"));
	}
	
	private void ParsePlayUrl()
	{
		HttpData data = new HttpData();
		data.url = DomainAddress.getUrlApps() + "/Hunet_Player/Android.aspx";
		data.param = String.format("action=FrameDetailViewByTakeCourseSeq&course_cd=%s&chapter_no=%s&frame_no=%d&take_course_seq=%d"
				, courseCd, chapterNo, frameNo, takeCourseSeq);
		hunet.net.HttpRequester.GetPostData(data);
		
		contentsUrl = data.GetJsonValue("contents_url").replace("http://hunet2.hscdn.com/hunetvod/_definst_/mp4:M_Learning/", "http://w.hunet.hscdn.com/hunet/M_Learning/").replace("/playlist.m3u8", "");
	}
	
	private void ParseMarkData()
	{
		HttpData data = new HttpData();
		data.url = DomainAddress.getUrlApps() + "/Hunet_Player/Android.aspx";
		data.param = String.format("action=MarkList&course_cd=%s&chapter_no=%s&frame_no=%d&take_course_seq=%d"
				,  courseCd, chapterNo, frameNo, takeCourseSeq);	
		hunet.net.HttpRequester.GetPostData(data);
		JSONArray arJSONList = data.GetJsonArray("List");
		
		if(arJSONList == null)
			return;
		
		int nLength = arJSONList.length();
		
		for(int nIndex = 0; nIndex < nLength; nIndex++)
		{
			try 
			{
				JSONObject	jsonObj	= arJSONList.getJSONObject(nIndex);
				MarkDataModel 	model 	= new MarkDataModel();
				model.index			= nIndex;
				model.markNo 		= Integer.parseInt(data.GetJsonValue(jsonObj, "mark_no", "0"));
				model.markNm 		= data.GetJsonValue(jsonObj, "mark_nm", "");
				model.markValue 		= Integer.parseInt(data.GetJsonValue(jsonObj, "mark_value", "0"));
				model.pptUrl 		= data.GetJsonValue(jsonObj, "ppt_url", "");
				model.realFrameNo 	= Integer.parseInt(data.GetJsonValue(jsonObj, "real_frame_no", "0"));
				model.progressYn 	= data.GetJsonValue(jsonObj, "progress_yn", "");
				
				m_arMarkList.add(model);
				if(!hasPpt && model.pptUrl != null && !model.pptUrl.equals("notPt"))
					hasPpt = true;
					
								
				if(lastMarkNo == model.markNo)
					m_CurMarkData = model;
			} 
			catch (Exception e) {} 
		}
	}
	
	@Override
	public boolean CheckProgressRestriction() 
	{
		return "Y".equals(progressRestrictionYn);
	}
	
	@Override
	public boolean CheckSendProgress(int lastViewSec, int studySec)
	{
		this.lastViewSec 	= lastViewSec;
		MarkDataModel tempData 	= null;
		
		for(int nIndex = 0; nIndex < m_arMarkList.size(); nIndex++)
		{
			int nTempMarkValue = m_arMarkList.get(nIndex).markValue / 1000;
			
			if(nTempMarkValue <= lastViewSec)
				tempData = m_arMarkList.get(nIndex);
		}
						
		if(tempData == null || m_CurMarkData.markNo == tempData.markNo)
			return false;
		if(m_CurMarkData.markNo != tempData.markNo)
		{				
			MarkDataModel preData 	= m_CurMarkData;
			m_CurMarkData 			= tempData;		
			
			return true;
		}	
		m_CurMarkData = tempData;
		
		return true;
	}
	
	@Override
	public void SaveProgress(int lastViewSec, int studySec)
	{
		String strUrl = GetProgressUrl(lastViewSec, studySec);
		Log.d("PlayerStudyDataWeb",strUrl);
		m_SqlManager.InsertStudyProgress(strUrl, userId);		
		super.m_ParentActivity.startService(m_StudyProgressService);		
	}
	
	@Override
	public int GetLastViewSec()
	{
		return lastViewSec;
	}

	@Override
	public int GetLastPositionSec() {
		return 0;
	}
	
	@Override
	public String GetPlayerUrl()
	{		
		return contentsUrl;
	}
	
	private String GetProgressUrl(int lastViewSec, int studySec)
	{		
		String strUrl = DomainAddress.getUrlApps() + "/Hunet_Player/Android.aspx?action=NewProgressUpdate" +
				"&user_id=" + "" +
				"&course_cd=" + courseCd + 
				"&take_course_seq=" + takeCourseSeq + 
				"&chapter_no=" + chapterNo + 
				"&progress_no=" + progressNo +
				"&frame_no=" + m_CurMarkData.realFrameNo +
				"&mark_no=" + m_CurMarkData.markNo +
				"&study_sec=" + (mobileStudySec + studySec) +
				"&mobile_study_sec=" + (mobileStudySec + studySec);
		
		return strUrl;
	}


	@Override
	public String GetQuizUrl() {return "";}

	@Override
	public String GetQuizYn() {
		return "n";
	}

	@Override
	public boolean HasPpt() {
		// TODO Auto-generated method stub
		return hasPpt;
	}	
	
	@Override
	public boolean UseSpeeedControl() {
		return true;
	}	
}
