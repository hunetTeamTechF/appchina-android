package hunet.player.model;

import hunet.domain.DomainAddress;
import hunet.net.HttpData;
import android.app.Activity;
import android.os.Bundle;

public class PlayerWEModel extends PlayerModelBase {
	private String contentsUrl = "";

	public PlayerWEModel(Activity activity) {
		super(activity);
		Bundle bundle = activity.getIntent().getExtras();

		if (bundle == null)
			return;

		contentsUrl = bundle.getString("url").replace(
				"rtsp://hunet2.hscdn.com/hunetvod/_definst_/mp4:M_Learning/",
				"http://w.hunet.hscdn.com/hunet/M_Learning/");
		if (bundle.containsKey("userId") && bundle.containsKey("courseCd")) {
			UpdateHistory(bundle.getString("userId"),
					bundle.getString("courseCd"),
					bundle.getString("takeCourseSeq"),
					bundle.getString("chapterNo"));
		}

	}

	private void UpdateHistory(String userId, String courseCd,
			String takeCourseSeq, String chapterNo) {
		// TODO Auto-generated method stub

		HttpData data = new HttpData();
		data.url = DomainAddress.getUrlApps() + "/Hunet_Player/Android.aspx";
		data.param = String
				.format("action=EdubankProcessHistory&userId=%s&courseCd=%s&takeCourseSeq=%s&chapterNo=%s",
						userId, courseCd, takeCourseSeq, chapterNo);
		hunet.net.HttpRequester.GetPostData(data);
	}

	@Override
	public boolean CheckProgressRestriction() {
		return true;
	}

	@Override
	public boolean CheckSendProgress(int lastViewSec, int studySec) {
		return true;
	}

	@Override
	public void SaveProgress(int lastViewSec, int studySec) {

	}

	@Override
	public int GetLastViewSec() {
		return 0;
	}

	@Override
	public int GetLastPositionSec() {
		return 0;
	}

	@Override
	public String GetQuizUrl() {return "";}

	@Override
	public String GetPlayerUrl() {
		return contentsUrl;
	}

	private String GetProgressUrl(int lastViewSec, int studySec) {
		return "";
	}

	@Override
	public String GetQuizYn() {
		return "n";
	}

	@Override
	public boolean HasPpt() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean UseSpeeedControl() {
		return true;
	}
}
