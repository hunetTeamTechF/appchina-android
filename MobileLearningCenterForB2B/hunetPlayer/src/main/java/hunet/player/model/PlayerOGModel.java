package hunet.player.model;


import hunet.drm.models.StudyIndexModel;
import hunet.drm.models.TakeCourseModel;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

public class PlayerOGModel extends PlayerModelBase {

    private static final String TAG = "HdrmPlayerActivity";

    private StudyIndexModel m_DownStudyIndexModel;
    private String m_strFilePath = "";
    private int take_course_seq = 0;
    private String chapter_no = "";
    private int max_mark_sec = 0;
    private String m_external_memory = "";
    private int lastPositionSec = 0;

    public PlayerOGModel(Activity activity) {
        super(activity);

        Bundle bundle = activity.getIntent().getExtras();

        if (bundle == null)
            return;

        take_course_seq = bundle.getInt("take_course_seq", 0);
        chapter_no = bundle.getString("chapter_no");
        max_mark_sec = bundle.getInt("max_mark_sec", 0);
//		m_strFilePath 			= String.format("%s%d_%s.mp4", Utilities.GetExternalDir(), take_course_seq, chapter_no);
        m_strFilePath = bundle.getString("full_path");
        m_external_memory = bundle.getString("external_memory");
        m_DownStudyIndexModel = m_SqlManager.GetDownStudyIndexData(take_course_seq, chapter_no, m_external_memory);
        super.title = bundle.getString("indexNm");
        m_DownStudyIndexModel.take_course_seq = take_course_seq;
        m_DownStudyIndexModel.chapter_no = chapter_no;
    }


    @Override
    public boolean CheckProgressRestriction() {
        return m_DownStudyIndexModel.max_view_sec >= max_mark_sec;
    }

    @Override
    public boolean CheckSendProgress(int lastViewSec, int studySec) {
        if (studySec % 60 > 0)
            return false;

        return true;
    }

    @Override
    public void SaveProgress(int lastViewSec, int studySec) {
        //if(lastViewSec > m_DownStudyIndexModel.max_view_sec)
        Log.i(TAG, "OG SaveProgress lastViewSec : " + lastViewSec + " studySec : " + studySec);
        m_DownStudyIndexModel.last_view_sec = lastViewSec;
        m_DownStudyIndexModel.study_sec = studySec;
        m_SqlManager.SetDownStudyIndexData(m_DownStudyIndexModel);
    }

    @Override
    public int GetLastViewSec() {
        return m_DownStudyIndexModel.last_view_sec;
    }

    @Override
    public int GetLastPositionSec() {
        return lastPositionSec;
    }

    @Override
    public String GetPlayerUrl() {
        return m_strFilePath;
    }

    @Override
    public String GetQuizUrl() {return "";}


    @Override
    public boolean HasPpt() {
        return false;
    }


    @Override
    public String GetQuizYn() {
        return "n";
    }

    @Override
    public boolean UseSpeeedControl() {
        // "[강원랜드] 베이직" 과목은 배속기능 제거
        TakeCourseModel takeCourseModel = m_SqlManager.GetDownTakeCourseData(take_course_seq);

        return !takeCourseModel.course_cd.equalsIgnoreCase("HLSC09777");
    }
}
