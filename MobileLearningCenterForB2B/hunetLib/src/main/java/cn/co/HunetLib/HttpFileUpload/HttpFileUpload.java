package cn.co.HunetLib.HttpFileUpload;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpFileUpload 
{
	private final String lineEnd = "\r\n";
	private final String twoHyphens = "--";
	private final String boundary = "*****";
	private IHttpProgressUpdateEventListener m_eventListener;
	private HttpURLConnection m_conn;
	
	public HttpFileUpload()
	{
		
	}
	
	public void Disconnect()
	{
		try
		{
			if(m_conn != null)
				m_conn.disconnect();
		}catch(Exception e){ e.printStackTrace(); }
	}
	
	public void SetEventListener(IHttpProgressUpdateEventListener a_eventListener)
	{
		m_eventListener = a_eventListener;
	}
	
	public HttpFileInfo DoUpload(HttpFileInfo fileInfo)
	{
		FileInputStream stream 	= null;
		URL 			sendUrl = null;
		String 			result 	= "";
		fileInfo.code			= 0;
		
		try 
		{
			stream 	= new FileInputStream(fileInfo.filePath);
			sendUrl = new URL(fileInfo.sendUrl);
			
			m_conn = (HttpURLConnection)sendUrl.openConnection();
			m_conn.setConnectTimeout(5000);
			m_conn.setReadTimeout(5000);
			m_conn.setDoInput(true);
			m_conn.setDoOutput(true);
			m_conn.setUseCaches(false);
			m_conn.setRequestMethod("POST");
			m_conn.setRequestProperty("Connection", "Keep-Alive");
			m_conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
			
			DataOutputStream dos = new DataOutputStream(m_conn.getOutputStream());
			
			String[] arParam = fileInfo.params.split("&");
			
			for(int nIndex = 0; nIndex < arParam.length; nIndex++)
			{
				String[] arValue = arParam[nIndex].split("=");
				
				if(arValue.length != 2)
					continue;
				
				dos.writeBytes(twoHyphens + boundary + lineEnd);
				dos.writeBytes("Content-Disposition: form-data; name=\"" + arValue[0] + "\"" + lineEnd);
				dos.writeBytes(lineEnd + arValue[1] + lineEnd);
			}
			
			dos.writeBytes(twoHyphens + boundary + lineEnd);
			dos.writeBytes("Content-Disposition: form-data; name=\"uploadfile\"; filename=\"" + fileInfo.fileName + "\"" + lineEnd);
			dos.writeBytes(lineEnd);
			
			int bytesAvailable = stream.available();
			int maxBufferSize = 1024;
			int bufferSize = Math.min(bytesAvailable, maxBufferSize);
			
			byte[] buffer = new byte[bufferSize];
			
			int bytesRead = stream.read(buffer, 0, bufferSize);
			
			while(bytesRead > 0)
			{
				if(m_eventListener != null)
					m_eventListener.OnProgressUpdate(bytesRead);
				
				dos.write(buffer, 0, bufferSize);
				
				bytesAvailable = stream.available();
				bufferSize = Math.min(bytesAvailable, maxBufferSize);
				bytesRead = stream.read(buffer, 0, bufferSize);
			}
			
			dos.writeBytes(lineEnd);
			dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
			
			stream.close();
			dos.flush();
			
			InputStream inputStream = m_conn.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(inputStream, "utf-8"), 1024);
			StringBuffer strBuffer = new StringBuffer();
			int word;
			
			while((word = br.read()) != -1)
			{
				strBuffer.append((char)word);
			}
			
			result = strBuffer.toString();
			dos.close();
		} 
		catch(Exception e) 
		{
			e.printStackTrace();
			result 			= e.getMessage();
			fileInfo.code 	= 1;
		}
		
		fileInfo.message = result;
		
		return fileInfo;
	}
}
