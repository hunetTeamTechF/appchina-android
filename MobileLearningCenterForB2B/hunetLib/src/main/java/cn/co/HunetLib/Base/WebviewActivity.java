package cn.co.HunetLib.Base;

import cn.co.HunetLib.R;
import cn.co.HunetLib.Common.HunetWebChromeClient;
import cn.co.HunetLib.Interface.IWebViewEventListener;
import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.KeyEvent;
import android.webkit.*;

public abstract class WebviewActivity extends BaseActivity
{
	private IWebViewEventListener m_WebViewListener;
	protected WebView m_WebView;
	protected int m_SelectTab;
	
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.webview_layout);
	    m_WebView = (WebView)findViewById(R.id.main_webView);
	    
	    InitWebView(m_WebView);
	    
	    m_SelectTab = savedInstanceState.getInt("TAB_NUMBER", 0);
	    
	    if(m_SelectTab > 0)
	    	return;
	    
	    finish();
	}
	
	@Override
	protected void onFirstResume()
	{
		String url = getCompany().getUrl(getCompany().getTabUrlType(0));
		
		if("".equals(url))
			return;
		
		setLoadUrl(url);
	}
	
	public void setWebViewEventListener(IWebViewEventListener webViewListener)
	{
		m_WebViewListener = webViewListener;
	}
	
	protected void setLoadUrl(String url)
	{
		m_WebView.loadUrl(url);
	}
	
	@SuppressLint("SetJavaScriptEnabled")
	protected void InitWebView(WebView webview)
	{	
		WebSettings setting = webview.getSettings();		
		setting.setUserAgentString(setting.getUserAgentString() + " HunetAndroid ");
		setting.setJavaScriptEnabled(true);
		
		webview.setHorizontalScrollBarEnabled(false);
		webview.setVerticalScrollBarEnabled(false);
		webview.setWebChromeClient(new HunetWebChromeClient());
		webview.setWebViewClient(new WebViewClient() 
		{
			@Override
			public void onPageStarted(WebView view, String url, Bitmap favicon) 
			{
				ShowDialog(WebviewActivity.this, "", true);
				
				if(m_WebViewListener != null)
					m_WebViewListener.OnPageStarted(view, url, favicon);
				
				super.onPageStarted(view, url, favicon);
			}
			
			@Override
			public void onPageFinished(WebView view, String url) 
			{
				HideDialog();
				
				if(m_WebViewListener != null)
					m_WebViewListener.OnPageFinished(view, url);
							
				super.onPageFinished(view, url);				
			}
			
			@Override
		    public boolean shouldOverrideUrlLoading(WebView view, String url) 
			{					
				String strTempUrl = url.toLowerCase();
				
				if(m_WebViewListener != null && m_WebViewListener.OnShouldOverrideUrlLoading(view, strTempUrl))
					return true;
				
		        return false;
	    	}
			
			@Override
			public void onReceivedError(WebView view, int errorCode, String description, String failingUrl)
			{					
				if(m_WebViewListener != null)
					m_WebViewListener.OnReceivedError(view, errorCode, description, failingUrl);				
			}
		});
	}
	
	@Override 
	public boolean onKeyDown(int keyCode, KeyEvent event) 
	{
		switch(keyCode)
		{
		case KeyEvent.KEYCODE_BACK:
			{
				if(m_WebView != null && m_WebView.canGoBack())
				{
					m_WebView.goBack();
					return true;
				}
			}
			break;
		}
		
		return super.onKeyDown(keyCode, event);
	}
	
	@Override
	protected void onDestroy()
	{
		super.onDestroy();
	}
}
