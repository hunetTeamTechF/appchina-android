package cn.co.HunetLib.Base;

import hunet.data.SQLiteManager;
import cn.co.HunetLib.Common.ErrorReporter;
import android.app.Application;

public class BaseApplication extends Application
{
	@Override
	public void onCreate() {
		super.onCreate();
		
		ErrorReporter errorReporter = ErrorReporter.getInstance();
		errorReporter.Init(getApplicationContext());
		errorReporter.CheckErrorAndSendLog(getApplicationContext());
		Thread.setDefaultUncaughtExceptionHandler(errorReporter);
	}
	
	@Override
	public void onTerminate() {
		SQLiteManager sqlManager = new SQLiteManager(getApplicationContext());
		sqlManager.CloseDB();
		super.onTerminate();
	}
}
