package cn.co.HunetLib.OldPlayer;

import hunet.data.SQLite;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/***
 * 3G 접속 정보 관련 SQLite DB 관련 클래스
 * @author hyoungil
 * 2011.05
 */
public class Hunet_Sql_CRUD_HUNET_MLC_ACCESS_Cls {
	
	SQLiteOpenHelper dbHelper;
    SQLiteDatabase db;
    
	public String id = "";
	
	public static String tableName = "MLC_ACCESS";
	
	public Hunet_Sql_CRUD_HUNET_MLC_ACCESS_Cls()
	{
		
	}
	
	public Hunet_Sql_CRUD_HUNET_MLC_ACCESS_Cls(Context context, String id)
	{
		dbHelper = new SQLite(context);
		db = dbHelper.getWritableDatabase();
		
		this.id = id;
	}
	
	/***
	 * 저장된 App 3G 접속 정보 조회.
	 * @return
	 */
	public String[] Select_Access_Data() {
		String[] rtnValues = {"", "", "", ""};
		String[] colValues = {"id", "access_text", "access_mov, push_type"};
		
		Cursor cursor = db.rawQuery("SELECT * FROM " + tableName + " WHERE id = '1'", null);
		
		while(cursor.moveToNext()) {
			rtnValues[0] = cursor.getString(0);
			rtnValues[1] = cursor.getString(1);
			rtnValues[2] = cursor.getString(2);
			rtnValues[3] = cursor.getString(3);
			break;
		}
				
		return rtnValues;
	}

	/***
	 * App 3G 접속 정보 저장.
	 * @param id
	 * @param access
	 */
	public void Save_Access_Data (int access) 
	{
		String[] accessData = Select_Access_Data();
		
		if("1".equals(accessData[0]))
		{
			Update_Access_Text_Data(access);
		}
		else
		{
			ContentValues cv = new ContentValues();
			cv.put("id", "1");
			cv.put("access_text", access);
			cv.put("access_mov", access);
			
			db.insert(tableName, null, cv);
		}
	}
	
	/***
	 * App 3G 텍스트/일반 데이터 접속  정보 업데이트.
	 * @param access_text
	 */
	public void Update_Access_Text_Data (int access_text) {
		String sql = "update " + tableName + " set access_text = '" + String.valueOf(access_text) + "' where id = '1'";
		
		db.execSQL(sql);
	}
	
	/***
	 * App 3G 동영상 접속 정보 업데이트.
	 * @param access_mov
	 */
	public void Update_Access_Mov_Data (int access_mov) {
		String sql = "update " + tableName + " set access_mov = '" + String.valueOf(access_mov) + "' where id = '1'";
		
		db.execSQL(sql);
	}
	
	/***
	 * App 3G 동영상 접속 아이디 정보 업데이트.
	 * @param access_mov
	 */
	public void Update_Access_Id_Data (int access_mov) {
		String sql = "update " + tableName + " set id = '" + this.id + "' where id = '1'";
		
		db.execSQL(sql);
	}
	
	/***
	 * App 3G 정보 삭제.
	 * @param id
	 */
	public void Delete_Access_Data() {
		String[] whereArgs = {"1"};
		db.delete(tableName, "id=?", whereArgs);
	}
	
	public void CloseDB()
	{
		if(db != null)
		{
			db.close();
			db = null;
		}
	}
}
