package cn.co.HunetLib.GCM;

import android.content.Intent;

/**
 * Created by zeple on 2015-07-06.
 */
public class GCMInstanceIDListenerService extends com.google.android.gms.iid.InstanceIDListenerService {

    private static final String TAG = "InstanceIDLS";

    @Override
    public void onTokenRefresh() {
        startService(new Intent(this, GCMRegistrationIntentService.class));
    }
}
