package cn.co.HunetLib.Gallery;

import android.graphics.Bitmap;
import android.support.v4.util.LruCache;

public class LruMemoryCache 
{
	private LruCache<String, Bitmap> mMemoryCache;

	public LruMemoryCache()
	{
		final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);

	    // Use 1/8th of the available memory for this memory cache.
	    final int cacheSize = maxMemory / 8;

	    mMemoryCache = new LruCache<String, Bitmap>(cacheSize) 
	    {
	        @Override
	        protected int sizeOf(String key, Bitmap bitmap) 
	        {
	            return (bitmap.getRowBytes() * bitmap.getHeight())/ 1024;
	        }
	    };
	}
	
	public void addBitmapToMemoryCache(String key, Bitmap bitmap) {
	    if (getBitmapFromMemCache(key) == null) {
	        mMemoryCache.put(key, bitmap);
	    }
	}

	public Bitmap getBitmapFromMemCache(String key) {
	    return mMemoryCache.get(key);
	}
}
