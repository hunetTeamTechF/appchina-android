package cn.co.HunetLib.OldPlayer;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.VideoView;

/**
 * 강의관련 HTTP 통신. 플레이정보
 * 작업자 : 장형일
 * 작업일 : 2011.11
 * mlc 플레이어 인앱 수정. 2011.11.04
 */
public class VideoViewPlayer extends VideoView {
	
	public VideoViewPlayer(Context context, AttributeSet attrs){
		super(context, attrs);
	}
	int wVideo;
	int hVideo;
	
	@SuppressLint("WrongCall")
	public void setVideoAspect(int w,int h)
	{
		wVideo=w;
		hVideo=h;
		onMeasure(w, h);
	}
	
	@Override
	protected void onMeasure (int widthMeasureSpec, int heightMeasureSpec)  
	{
		 super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		 if(wVideo!=0 && hVideo!=0)
		 {
			 setMeasuredDimension(wVideo,hVideo);
			 getHolder().setFixedSize(wVideo, hVideo);
		 }
	}
}
