package cn.co.HunetLib;

import hunet.library.Utilities;
import cn.co.HunetLib.Base.TabActivity;
import cn.co.HunetLib.Company.Company;
import cn.co.HunetLib.Interface.ITabControlEventListener;
import cn.co.HunetLib.R;
import cn.co.HunetLib.SQLite.AccessModel;
import cn.co.HunetLib.SQLite.LoginModel;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Toast;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class ConfigActivity extends TabActivity implements ITabControlEventListener, OnCheckedChangeListener
{
	LinearLayout	ll_tab;
	LinearLayout 	ll_config_back;	
	CheckBox 		cb_auto_login;               //자동 로그인 체크
	CheckBox 		cb_3g_textdata;              //텍스트/일반 데이터 3G 체크
	CheckBox 		cb_3g_mov;                   //동영상 강의 시청 3G 체크
	CheckBox 		config_cb_push_msg;			 //푸쉬 알림 메시지
	CheckBox 		config_cb_external_memory;	 //외장 메모리 선택
	ImageView 		img_volume_point;            //볼륨 포인터 
	LinearLayout 	ll_volume;                   //볼륨 레이아웃
	LinearLayout 	ll_volume_min;               //볼륨최소(음소거) 레이아웃
	LinearLayout 	ll_volume_max;               //볼륨최대 레이아웃
	LinearLayout 	ll_external_memory_download; //외장메모리
	AccessModel 	m_AccessModel;
	LoginModel 		m_LoginModel;
	boolean 		m_GCMSendManageEnabled;
	private final int eAUTO_LOGIN 		= 1;
	private final int eACCESS_TEXT 		= 2;
	private final int eACCESS_MOV 		= 3;
	private final int ePUSH_MSG 		= 4;
	private final int eEXTERNAL_MEMORY 	= 5;
	
	private final int eGCM_INFO 		= 10;
	private final int eGCM_VIEW_UPDATE 	= 11;

	
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	
	    setContentView(R.layout.config_activity);
	    
	    Utilities.changeTitlebarColor(this, R.id.mainHeader, R.id.header_text_color);
	    
	    ll_config_back = (LinearLayout)findViewById(R.id.more_ll_config_back);
	    cb_auto_login = (CheckBox)findViewById(R.id.config_cb_auto_login);                    //자동 로그인 체크
	    cb_3g_textdata = (CheckBox)findViewById(R.id.config_cb_3g_textdata);               //텍스트/일반 데이터 3G 체크
		cb_3g_mov = (CheckBox)findViewById(R.id.config_cb_3g_mov);                         //동영상 강의 시청 3G 체크
		config_cb_push_msg = (CheckBox)findViewById(R.id.config_cb_push_msg);
		config_cb_external_memory = (CheckBox)findViewById(R.id.config_cb_external_memory_download);
		LinearLayout ll_gcm_manage = (LinearLayout)findViewById(R.id.ll_gcm_manage);
		ll_external_memory_download = (LinearLayout)findViewById(R.id.ll_external_memory_download);
		m_GCMSendManageEnabled = getCompany().getGCMSendManageEnabled();
		
		CreateTabControl((LinearLayout)findViewById(R.id.more_ll_config_tab));
		setSelectTab(Company.eTAB_CONFIG);
		
		//if(m_GCMSendManageEnabled == false)
			ll_gcm_manage.setVisibility(View.GONE);
		
		cb_auto_login.setOnCheckedChangeListener(this);cb_auto_login.setTag(eAUTO_LOGIN);
		cb_3g_textdata.setOnCheckedChangeListener(this);cb_3g_textdata.setTag(eACCESS_TEXT);
		cb_3g_mov.setOnCheckedChangeListener(this);cb_3g_mov.setTag(eACCESS_MOV);
		//config_cb_push_msg.setOnCheckedChangeListener(this);config_cb_push_msg.setTag(ePUSH_MSG);
		config_cb_external_memory.setOnCheckedChangeListener(this);config_cb_external_memory.setTag(eEXTERNAL_MEMORY);

		ll_config_back.setOnClickListener(new OnClickListener() { @Override
		public void onClick(View v) 
		{
	    	finish();
		}});
	}
	
	@Override
	protected void onFirstResume() 
	{
		
	}
	
	@Override
	protected void onResume()
	{
		super.onResume();
		
		if(getCompany().LoginModel == null)
			return;
		
		m_LoginModel 	= this.getSQLiteManager().GetLoginData(getCompany().LoginModel.id);
		m_AccessModel 	= this.getSQLiteManager().GetAccessData();
		
//// 이동식 메모리 검수가 완료되지 않아 외장메모리 선택 되지 않도록 주석 처리		
		if(TextUtils.equals(m_AccessModel.external_memory, "1")){
			if(!Utilities.getExternalMemoryCheck(getApplicationContext())){
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						Toast.makeText(getApplicationContext(), "이동식 메모리가 제거 되었습니다..", Toast.LENGTH_LONG).show();
					}
				});
			}
		}
		
		// 외부 메모리가 존재 하지 않으면 메뉴 선택 되지 않도록..
		if(Utilities.getExternalMemoryCheck(getApplicationContext())){
			if(getCompany().getDrmUse() == true)
				ll_external_memory_download.setVisibility(View.VISIBLE);
		}
		
		cb_auto_login.setChecked("1".equals(m_LoginModel.auto_login));
		cb_3g_textdata.setChecked("1".equals(m_AccessModel.access_text));
		cb_3g_mov.setChecked("1".equals(m_AccessModel.access_mov));
		config_cb_push_msg.setChecked("1".equals(m_AccessModel.push_type));
		config_cb_external_memory.setChecked("1".equals(m_AccessModel.external_memory));
	}
	
	@Override
	protected void onPause()
	{
		super.onPause();
	}
	
	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) 
	{
		int type = (Integer)buttonView.getTag();
		
		switch(type)
		{
		case eAUTO_LOGIN:		m_LoginModel.auto_login 		= isChecked ? "1" : "0"; break;
		case eACCESS_TEXT: 		m_AccessModel.access_text 		= isChecked ? "1" : "0"; break;
		case eACCESS_MOV: 		m_AccessModel.access_mov 		= isChecked ? "1" : "0"; break;
		case ePUSH_MSG:			m_AccessModel.push_type 		= isChecked ? "1" : "0"; break;
		case eEXTERNAL_MEMORY:
			if(Utilities.getExternalMemoryCheck(getApplicationContext())){
				m_AccessModel.external_memory = isChecked ? "1" : "0";
			}else{
				config_cb_external_memory.setChecked(false);
			}
			break;
		}
		
		this.getSQLiteManager().SetLoginData(m_LoginModel);
		this.getSQLiteManager().SetAccessData(m_AccessModel);
	}
}
