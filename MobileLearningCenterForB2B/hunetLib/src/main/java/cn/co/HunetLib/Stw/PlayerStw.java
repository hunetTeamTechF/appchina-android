package cn.co.HunetLib.Stw;

import hunet.data.SQLiteManager;
import hunet.drm.download.library.OnlineDrmDownload;
import hunet.drm.models.StudyIndexModel;
import hunet.drm.models.TakeCourseModel;
import cn.co.HunetLib.Http.AsyncHttpRequestData;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Environment;
import android.webkit.WebView;
import android.widget.Toast;

public class PlayerStw 
{
	private ProgressDialog 			mLoadingDialog = null;
	private Activity 				mActivity;
	private WebView					mWebView;
	private class CommonData
	{
		public String courseCd		= "";
		public String takeCourseSeq	= "";
		public String chapterNo		= "";
		public String frameNo		= "";
		public String view_index	= "";
		
		// player
		public String defaultInfo 	= "";
		public String progressInfo	= "";
		public String markList 		= "";
		public String playUrl 		= "";
	}
	
	private final int ePLAYER_STEP1_GET_PROGRESSINFO 	= 1; // 진도 정보 가져오기
	private final int ePLAYER_STEP2_GET_PLAYURL			= 2; // 플레이 url 가져 오기
	private final int ePLAYER_STEP3_GET_MARKLIST		= 3; // 마크 정보 가져 오기
	private final int eDOWN_STEP1_GET_DOWNLOAD_INFO		= 4; // 다운로드 정보 가져 오기
	private final int eDOWN_STEP2_SET_DOWNLOAD_COMPLETE	= 5; // 다운로드 정보 저장하기
	
	private AsyncHttpRequestData 	m_AsyncReqData;
	private CommonData				m_CommonData;
	private ProgressDialog			m_ProgressDialog;
	private TakeCourseModel 		m_DownTakeCourseModel;
	private StudyIndexModel 		m_DownStudyIndexModel;
	hunet.library.player player;
	OnlineDrmDownload downloader;
	private SQLiteManager			m_SqlManager;
	private String 					mCompanySeq			= "";
	private String 					mLoginId			= "";
	private String					mExternalDir		= ""; 		
	private String 					mActionNm 			= "";
	private boolean 				mIsFinish			= false;
	
	public PlayerStw(Activity activity, WebView webView, int companySeq, String loginId)
	{
		this(activity, webView, companySeq, loginId, new OnlineDrmDownload(activity, webView, loginId));
	}
	
	public PlayerStw(Activity activity, WebView webView, int companySeq, String loginId, OnlineDrmDownload downloader)
	{
		this.mActivity				= activity;
		this.mWebView				= webView;
		this.mExternalDir			= Environment.getExternalStorageDirectory() + "/hunet_mlc/";
		this.mCompanySeq				= String.valueOf(companySeq);
		this.mLoginId				= loginId;
		this.m_AsyncReqData 			= new AsyncHttpRequestData();
		this.m_CommonData			= new CommonData();
	    this.m_DownTakeCourseModel 	= new TakeCourseModel();
	    this.m_DownStudyIndexModel 	= new StudyIndexModel();
	    this.m_SqlManager			= new SQLiteManager(mActivity);
	    this.player = new hunet.library.player(activity, loginId);
		this.downloader = downloader;
	}
	
	public void Destroy()
	{
		mActivity 	= null;
		mWebView 	= null;
		mIsFinish	= true;
	}
	
	public boolean HasProtocal(String url)
	{
		if(url.contains("drmplay://"))
		{
			player.ConfirmDrmPlayer(url);
			return true;
		}
		else if(url.contains("download://"))
		{
			downloader.ConfirmDownload(url);
			return true;
		}
		else if(url.contains("downloadplay://"))
		{
			downloader.DownloadPlay(url);
			return true;
		}
		
		return false;
	}
	
	public void CheckAction()
	{
		String actionNm = mActionNm;
		mActionNm		= "";		

		if("refresh".equals(player.CheckAction()))
		{
			mWebView.reload();
			return;
		}
		
		if("".equals(actionNm))
			return;
		
		if("refresh".equals(actionNm))
		{
			mWebView.reload();
			return;
		}
		
		mWebView.loadUrl(actionNm);
	}
	
	public void ShowToast(String msg)
	{
		Toast.makeText(mActivity, msg, Toast.LENGTH_LONG).show();
	}
	
	public void ShowDialog(Context context, String msg, boolean cancelable)
	{
		if(mIsFinish || context == null || mLoadingDialog != null)
			return;
		
		mLoadingDialog = new ProgressDialog(context);
		mLoadingDialog.setMessage("".equals(msg) ? "加载中..." : msg);
		mLoadingDialog.setIndeterminate(true);
		mLoadingDialog.setCancelable(cancelable);
		mLoadingDialog.show();
	}
	
	public void HideDialog()
	{
		if(mLoadingDialog == null)
			return;
		
		try
		{
			mLoadingDialog.dismiss();
			mLoadingDialog = null;
		}
		catch(Exception e) { }
	}
}
