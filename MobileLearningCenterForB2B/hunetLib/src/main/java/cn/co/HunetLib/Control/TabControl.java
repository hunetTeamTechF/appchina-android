package cn.co.HunetLib.Control;

import cn.co.HunetLib.Common.AppMain;
import cn.co.HunetLib.R;
import cn.co.HunetLib.Company.Company;
import cn.co.HunetLib.Interface.ITabControlEventListener;

import android.content.res.Resources;
import android.graphics.Color;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.provider.Settings;
import android.view.View;
import android.content.Context;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageButton;
import android.widget.LinearLayout;

public class TabControl implements View.OnClickListener
{
	private int background_color = 0;
	private int text_color = 0;
	private int text_selected_color = 0;
	private int icon_color = 0;
	private int icon_selected_color = 0;

	protected int selected_tab = 0;
	protected LinearLayout tab_container;
	protected ITabControlEventListener listener;

	public TabControl(String background_color, String icon_color, String text_color, String icon_selected_color, String text_selected_color){
		this(Color.parseColor(background_color)
				, Color.parseColor(icon_color)
				, Color.parseColor(text_color)
				, Color.parseColor(icon_selected_color)
				, Color.parseColor(text_selected_color));
	}
	
	public TabControl(int background_color, int icon_color, int text_color, int icon_selected_color, int text_selected_color) {
		this.background_color = background_color;
		this.icon_color = icon_color;
		this.text_color = text_color;
		this.icon_selected_color = icon_selected_color;
		this.text_selected_color = text_selected_color;
	}

	public int getSelectTab() {
		return selected_tab;
	}

	public void setTabContainer(LinearLayout tabContainer)
	{
		tab_container = tabContainer;
	}

	public LinearLayout getTabContainer()
	{
		return tab_container;
	}

	public void setEventListener(ITabControlEventListener eventListener)
	{
		listener = eventListener;
	}

	public int GetChildTabId(int index)
	{
		int id = -1;

		if(index >= tab_container.getChildCount())
			return id;			

		return (Integer)tab_container.getChildAt(index).getTag();
	}

	protected void InitTabContainer() {
		tab_container.setBackgroundColor(background_color);

		if(tab_container.getChildCount() > 0)
			tab_container.removeAllViews();
	}
	
	protected TabItem createTabItem() {

			return new TabItem(tab_container.getContext(), icon_color, text_color, icon_selected_color, text_selected_color);
	}

	public void InitTab(int nSelectTab)
	{
		selected_tab = nSelectTab;

		if(nSelectTab == Company.eTAB_SANGSANG)
		{
			CreateSangSangTab();
			return;
		}
		else if(nSelectTab == Company.eTAB_H_LEADERSHIP_CLASSROOM)
		{
			CreateHLeadership();
			return;
		}

		InitTabContainer();

		/* 탭 메뉴 */
		TabItem tab_1 = createTabItem();
		TabItem tab_2 = createTabItem();
		TabItem tab_3 = createTabItem();
		TabItem tab_4 = createTabItem();
		TabItem tab_5 = createTabItem();

		tab_1.tabLayout.setTag(Company.eTAB_HOME);
		tab_2.tabLayout.setTag(nSelectTab == Company.eTAB_LECTURE_INMUN ? Company.eTAB_LECTURE_INMUN : Company.eTAB_LECTURE);
		tab_5.tabLayout.setTag(nSelectTab >= Company.eTAB_MORE ? nSelectTab : Company.eTAB_MORE);

		switch(AppMain.CompanyInfo.getEduType())
		{
		case Company.eIMAGINE_CONTRACT_LEARNINGANDIMAGINE: {
			tab_1.init(R.drawable.tab_home, tab_container.getContext().getString(R.string.tab_my));
			tab_2.init(R.drawable.tab_process, tab_container.getContext().getString(R.string.tab_my));
			tab_3.init(R.drawable.tab_classroom, tab_container.getContext().getString(R.string.tab_my));
			tab_4.init(R.drawable.tab_download, tab_container.getContext().getString(R.string.tab_download));
			tab_5.init(R.drawable.tab_setting, tab_container.getContext().getString(R.string.tab_setting));

			if (nSelectTab == Company.eTAB_HOME)
				tab_1.selected();
			else if (nSelectTab == Company.eTAB_LECTURE || nSelectTab == Company.eTAB_LECTURE_INMUN)
				tab_2.selected();
			else if (nSelectTab == Company.eTAB_CLASSROOM)
				tab_3.selected();
			else if (nSelectTab == Company.eTAB_DOWNLOAD_CENTER)
				tab_4.selected();
			else if (nSelectTab >= Company.eTAB_MORE)
				tab_5.selected();

			tab_3.tabLayout.setTag(Company.eTAB_CLASSROOM);
			tab_4.tabLayout.setTag(Company.eTAB_DOWNLOAD_CENTER);
		}
		break;
		case Company.eIMAGINE_CONTRACT_ONLYLEARNING: {

			tab_1.init(R.drawable.tab_home, tab_container.getContext().getString(R.string.tab_home));
			tab_2.init(R.drawable.tab_process, tab_container.getContext().getString(R.string.tab_my));
			tab_3.init(R.drawable.tab_classroom, tab_container.getContext().getString(R.string.tab_my));
			tab_4.init(R.drawable.tab_download, tab_container.getContext().getString(R.string.tab_download));
			tab_5.init(R.drawable.tab_setting, tab_container.getContext().getString(R.string.tab_setting));

			if (nSelectTab == Company.eTAB_HOME)
				tab_1.selected();
			else if (nSelectTab == Company.eTAB_LECTURE || nSelectTab == Company.eTAB_LECTURE_INMUN)
				tab_2.selected();
			else if (nSelectTab == Company.eTAB_CLASSROOM)
				tab_3.selected();
			else if (nSelectTab == Company.eTAB_DOWNLOAD_CENTER)
				tab_4.selected();
			else if (nSelectTab >= Company.eTAB_MORE)
				tab_5.selected();

			tab_3.tabLayout.setTag(Company.eTAB_CLASSROOM);
			tab_4.tabLayout.setTag(Company.eTAB_DOWNLOAD_CENTER);
		}
		break;
		case Company.eIMAGINE_CONTRACT_ONLYIMAGINE: {



			tab_1.init(R.drawable.tab_home, tab_container.getContext().getString(R.string.tab_home));
			tab_2.init(R.drawable.tab_process, tab_container.getContext().getString(R.string.tab_my));
			tab_3.init(R.drawable.tab_classroom, tab_container.getContext().getString(R.string.tab_my));
			tab_4.init(R.drawable.tab_download, tab_container.getContext().getString(R.string.tab_download));
			tab_5.init(R.drawable.tab_setting, tab_container.getContext().getString(R.string.tab_setting));

			if (nSelectTab == Company.eTAB_HOME)
				tab_1.selected();
			else if (nSelectTab == Company.eTAB_LECTURE || nSelectTab == Company.eTAB_LECTURE_INMUN)
				tab_2.selected();
			else if (nSelectTab == Company.eTAB_CLASSROOM)
				tab_3.selected();
			else if (nSelectTab == Company.eTAB_DOWNLOAD_CENTER)
				tab_4.selected();
			else if (nSelectTab >= Company.eTAB_MORE)
				tab_5.selected();

			tab_3.tabLayout.setTag(Company.eTAB_CLASSROOM);
			tab_4.tabLayout.setTag(Company.eTAB_DOWNLOAD_CENTER);
		}
		break;
		case Company.eIMAGINE_CONTRACT_NONE: {
			tab_1.init(R.drawable.tab_home,tab_container.getContext().getString(R.string.tab_home));
			tab_2.init(R.drawable.tab_process, tab_container.getContext().getString(R.string.tab_my));
			tab_3.init(R.drawable.tab_classroom, tab_container.getContext().getString(R.string.tab_my));
			tab_4.init(R.drawable.tab_counseling, tab_container.getContext().getString(R.string.tab_download));
			tab_5.init(R.drawable.tab_more, tab_container.getContext().getString(R.string.tab_setting));

			if (nSelectTab == Company.eTAB_HOME)
				tab_1.selected();
			else if (nSelectTab == Company.eTAB_LECTURE || nSelectTab == Company.eTAB_LECTURE_INMUN)
				tab_2.selected();
			else if (nSelectTab == Company.eTAB_CLASSROOM)
				tab_3.selected();
			else if (nSelectTab == Company.eTAB_CLASSROOM)
				tab_4.selected();
			else if (nSelectTab >= Company.eTAB_MORE)
				tab_5.selected();

			tab_3.tabLayout.setTag(Company.eTAB_CLASSROOM);
			tab_4.tabLayout.setTag(Company.eTAB_CLASSROOM);
		}
		break;
		}

		tab_1.tabLayout.setOnClickListener(this);
		tab_2.tabLayout.setOnClickListener(this);
		tab_3.tabLayout.setOnClickListener(this);
		tab_4.tabLayout.setOnClickListener(this);
		tab_5.tabLayout.setOnClickListener(this);

		tab_container.addView(tab_1.tabLayout);
		//tab_container.addView(tab_2.tabLayout);
		tab_container.addView(tab_3.tabLayout);
		tab_container.addView(tab_4.tabLayout);
		tab_container.addView(tab_5.tabLayout);
	}

	private ImageButton m_SangSangHome;
	private ImageButton m_SangSangBack;
	private ImageButton m_SangSangForward;
	private ImageButton m_SangSangRefresh;





	public void CreateSangSangTab()
	{		
		InitTabContainer();

		/*탭메뉴*/
		m_SangSangHome 		= new ImageButton(tab_container.getContext());
		m_SangSangBack 		= new ImageButton(tab_container.getContext());
		m_SangSangForward 	= new ImageButton(tab_container.getContext());
		m_SangSangRefresh 	= new ImageButton(tab_container.getContext());
		/*탭메뉴 끝*/

		/*탭메뉴 설정*/
		m_SangSangHome.setLayoutParams(new LinearLayout.LayoutParams(0, LayoutParams.FILL_PARENT, 1));
		m_SangSangBack.setLayoutParams(new LinearLayout.LayoutParams(0, LayoutParams.FILL_PARENT, 1));
		m_SangSangForward.setLayoutParams(new LinearLayout.LayoutParams(0, LayoutParams.FILL_PARENT, 1));
		m_SangSangRefresh.setLayoutParams(new LinearLayout.LayoutParams(0, LayoutParams.FILL_PARENT, 1));

		m_SangSangHome.setBackgroundResource(R.drawable.sangsangtab_home);
		m_SangSangBack.setBackgroundResource(R.drawable.sangsangtab_back_off);
		m_SangSangForward.setBackgroundResource(R.drawable.sangsangtab_next_off);
		m_SangSangRefresh.setBackgroundResource(R.drawable.sangsangtab_reflesh);

		m_SangSangHome.setOnClickListener(this);m_SangSangHome.setTag(Company.eTAB_SANGSANG_HOME);
		m_SangSangBack.setOnClickListener(this);m_SangSangBack.setTag(Company.eTAB_SANGSANG_BACK);
		m_SangSangForward.setOnClickListener(this);m_SangSangForward.setTag(Company.eTAB_SANGSANG_FORWARD);
		m_SangSangRefresh.setOnClickListener(this);m_SangSangRefresh.setTag(Company.eTAB_SANGSANG_REFRESH);

		tab_container.addView(m_SangSangHome);
		tab_container.addView(m_SangSangBack);
		tab_container.addView(m_SangSangForward);
		tab_container.addView(m_SangSangRefresh);
	}

	public void OnChangeSangSangTab(boolean goBack, boolean goForward)
	{
		m_SangSangBack.setBackgroundResource(goBack 		? R.drawable.sangsangtab_back : R.drawable.sangsangtab_back_off);
		m_SangSangForward.setBackgroundResource(goForward 	? R.drawable.sangsangtab_next : R.drawable.sangsangtab_next_off);
	}

	public void CreateHLeadership()
	{
		InitTabContainer();

		ImageButton[] hLeaderTabList = new ImageButton[5];

		for(int index = 0; index < 5; index++)
		{
			hLeaderTabList[index] = new ImageButton(tab_container.getContext());

			ImageButton hLeaderTab = hLeaderTabList[index];  	
			hLeaderTab.setLayoutParams(new LinearLayout.LayoutParams(0, LayoutParams.FILL_PARENT, 1));
			hLeaderTab.setOnClickListener(this);

			switch(index)
			{
			case 0: hLeaderTab.setBackgroundResource(R.drawable.tabmenu_home); 						hLeaderTab.setTag(Company.eTAB_HOME); 					break;
			case 1: hLeaderTab.setBackgroundResource(R.drawable.tabmenu_hleadership_classroom_on); 	hLeaderTab.setTag(Company.eTAB_H_LEADERSHIP_CLASSROOM); break;
			case 2: hLeaderTab.setBackgroundResource(R.drawable.tabmenu_hleadership_lecture); 		hLeaderTab.setTag(Company.eTAB_H_LEADERSHIP_LECTURE); 	break;
			case 3: hLeaderTab.setBackgroundResource(R.drawable.tabmenu_hleadership_mentor); 		hLeaderTab.setTag(Company.eTAB_H_LEADERSHIP_MENTOR); 	break;
			case 4: hLeaderTab.setBackgroundResource(R.drawable.tabmenu_hleadership_down); 			hLeaderTab.setTag(Company.eTAB_H_LEADERSHIP_DOWN); 		break;
			}

			tab_container.addView(hLeaderTab);
		}		
	}

	public void ChangeHLeaderShipTab(int tabId)
	{
		tab_container.getChildAt(1).setBackgroundResource(tabId == Company.eTAB_H_LEADERSHIP_CLASSROOM ? R.drawable.tabmenu_hleadership_classroom_on : R.drawable.tabmenu_hleadership_classroom);
		tab_container.getChildAt(2).setBackgroundResource(tabId == Company.eTAB_H_LEADERSHIP_LECTURE 	? R.drawable.tabmenu_hleadership_lecture_on : R.drawable.tabmenu_hleadership_lecture);
		tab_container.getChildAt(3).setBackgroundResource(tabId == Company.eTAB_H_LEADERSHIP_MENTOR 	? R.drawable.tabmenu_hleadership_mentor_on : R.drawable.tabmenu_hleadership_mentor);
	}

	@Override
	public void onClick(View v) 
	{		
		if(listener != null)
			listener.onClickTab((Integer)v.getTag(), selected_tab);
	}
}
