package cn.co.HunetLib.Common;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.net.Uri;
import android.webkit.ValueCallback;
import android.content.Intent;
import java.io.File;
import android.provider.MediaStore;
import android.os.Environment;
import android.os.Parcelable;




public class HunetWebChromeClient extends WebChromeClient 
{
	//Javascript alert 호출 시 실행
	@Override
	public boolean onJsAlert(WebView view, String url, String message, final JsResult result)
	{	   
		//AlertDialog 생성
		new AlertDialog.Builder(view.getContext()).setMessage(message).setPositiveButton("确认",
				new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which) 
			{
				result.confirm();  
			}
		}).setCancelable(false).create().show();
		return true;
	}

	@Override
	public boolean onJsConfirm(WebView view, String url, String message, final JsResult result) {
		//AlertDialog 생성
		new AlertDialog.Builder(view.getContext()).setMessage(message).setPositiveButton("确认",
				new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which) 
			{
				result.confirm();  
			}
		})
		.setNegativeButton("取消", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				result.cancel();
			}
		})
		.setCancelable(false).create().show();
		return true;
	}

//	private ValueCallback<Uri> filePathCallbackNormal;
//	private ValueCallback<Uri[]> filePathCallbackLollipop;
//	private Uri mCapturedImageURI;
//
//	// For Android < 3.0
//	public void openFileChooser(ValueCallback<Uri> uploadMsg) {
//		openFileChooser(uploadMsg, "");
//	}
//
//	// For Android 3.0+
//	public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType) {
//		filePathCallbackNormal = uploadMsg;
//		Intent i = new Intent(Intent.ACTION_GET_CONTENT);
//		i.addCategory(Intent.CATEGORY_OPENABLE);
//		i.setType("image/*");
//		startActivityForResult(Intent.createChooser(i, "File Chooser"), ESavingConstants.FILECHOOSER_NORMAL_REQ_CODE);
//	}
//
//	// For Android 4.1+
//	public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture) {
//		openFileChooser(uploadMsg, acceptType);
//	}
//
//
//	// For Android 5.0+
//	public boolean onShowFileChooser(
//			WebView webView, ValueCallback<Uri[]> filePathCallback,
//			WebChromeClient.FileChooserParams fileChooserParams) {
//		if (filePathCallbackLollipop != null) {
////                    filePathCallbackLollipop.onReceiveValue(null);
//			filePathCallbackLollipop = null;
//		}
//		filePathCallbackLollipop = filePathCallback;
//
//
//		// Create AndroidExampleFolder at sdcard
//		File imageStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "AndroidExampleFolder");
//		if (!imageStorageDir.exists()) {
//			// Create AndroidExampleFolder at sdcard
//			imageStorageDir.mkdirs();
//		}
//
//		// Create camera captured image file path and name
//		File file = new File(imageStorageDir + File.separator + "IMG_" + String.valueOf(System.currentTimeMillis()) + ".jpg");
//		mCapturedImageURI = Uri.fromFile(file);
//
//		Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//		captureIntent.putExtra(MediaStore.EXTRA_OUTPUT, mCapturedImageURI);
//
//		Intent i = new Intent(Intent.ACTION_GET_CONTENT);
//		i.addCategory(Intent.CATEGORY_OPENABLE);
//		i.setType("image/*");
//
//		// Create file chooser intent
//		Intent chooserIntent = Intent.createChooser(i, "Image Chooser");
//		// Set camera intent to file chooser
//		chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Parcelable[]{captureIntent});
//
//		// On select image call onActivityResult method of activity
//		startActivityForResult(chooserIntent, ESavingConstants.FILECHOOSER_LOLLIPOP_REQ_CODE);
//		return true;
//
//	}



} 
