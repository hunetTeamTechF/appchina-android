package cn.co.HunetLib.Common;

public enum Define {
	NONE							(0, "empty", "")
	
	, ACTIVITY_HOME					(10, "홈", "")
	, ACTIVITY_LECTURE				(11, "교육과정", "")
	, ACTIVITY_CLASSROOM			(12, "나의상의실", "")
	, ACTIVITY_SANGSANG				(13, "상상마루", "")
	, ACTIVITY_MORE					(14, "설정", "")
	
	, ACTIVITY_NOTICE				(20, "공지사항", "")
	, ACTIVITY_QNA					(21, "1:1친절상담", "")
	, ACTIVITY_FAQ					(22, "", "")
	, ACTIVITY_PASSWORD_CHANGE		(23, "修改密码", "")
	, ACTIVITY_MEMBER_MANAGE		(24, "회원정보관리", "")
	, ACTIVITY_SCHEDULE_ALARM		(25, "스케줄 알리미", "")
	, ACTIVITY_CONFIG				(26, "환경 설정", "")
	, ACTIVITY_VERSION				(26, "버전 정보", "")
	, ACTIVITY_DOWNLOAD_CENTER		(27, "다운로드 센터", "")
	, ACTIVITY_H_REMINDER			(28, "H-리마인더", "")
	, ACTIVITY_H_STORY				(29, "H-스토리", "")
	
	, NOTIFY_GCM_NOTI_REGIST 		(1000, "", "GCM 푸쉬 알림")
	;
	
	private int _value;
	private String _text;
	private String _desc;
	
	private Define(int value, String text, String desc) { this._value = value; this._text = text; this._desc = desc; }
	
	public int getValue() { return _value; }
	public String getText() { return _text; }
	public String getDesc() { return _desc; }
	
	public static Define getFindValue(int value)
	{
		Define define = Define.NONE;
		
		for(Define d : Define.values())
		{
			if(d.getValue() == value)
			{
				define = d;
				break;
			}
		}
		
		return define;
	}
}

