package cn.co.HunetLib;

import hunet.domain.DomainAddress;
import cn.co.HunetLib.Base.TabWebviewActivity;
import cn.co.HunetLib.Company.Company;
import android.content.Intent;
import android.os.Bundle;

public class ClassroomActivity extends TabWebviewActivity
{	
	String type = "";
	String processCd = "";
	String processYear = "";
	String processTerm = "";
	String courseCd = "";
	String takeCourseSeq = "";
	String companySeq = "";
	String userId = "";
	
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
	    super.onCreate(savedInstanceState);
	    setSelectTab(Company.eTAB_CLASSROOM);
	    
	    getIntent(getIntent());
	}
	
	@Override 
	protected void onNewIntent(Intent intent)
	{
		getIntent(intent);
		super.onNewIntent(intent);
	}
	
	private void getIntent(Intent intent) {
		if (intent != null) {
			Bundle bundle = intent.getExtras();
			type 			= this.getBundleValue(bundle, "type", "");
	    	processCd 		= this.getBundleValue(bundle, "processCd", "");
	    	processYear 	= this.getBundleValue(bundle, "processYear", "");
	    	processTerm 	= this.getBundleValue(bundle, "processTerm", "");
	    	courseCd 		= this.getBundleValue(bundle, "courseCd", "");
	    	takeCourseSeq 	= this.getBundleValue(bundle, "takeCourseSeq", "");
	    	companySeq		= this.getBundleValue(bundle, "cSeq", "");
	    	userId			= this.getBundleValue(bundle, "userId", "");
			setFirstResume(true);
		}
	}
	
	@Override
	protected void onFirstResume()
	{
		String url = "";
		
		if("userMenuInmun".equals(type)) {
			//인문학 사용자 메뉴 이동시.
			url = DomainAddress.getUrlMLC() + "/My/MyCourse_Inmun.aspx?processCd=" + processCd + "&processYear=" + processYear + "&processTerm=" + processTerm + "&courseCd=" + courseCd;
		}
		else if("userMenu".equals(type)) {
			//일반 과목 사용자 메뉴 이동시.
			url = DomainAddress.getUrlMLC() + "/My/MyCourseHome.aspx?processCd=" + processCd + "&processYear=" + processYear + "&processTerm=" + processTerm + "&courseCd=" + courseCd + "&takeCourseSeq=" + takeCourseSeq;
		}
		else if ("userCourseRoom".equals(type)) {
			// 해당 과목의 학습 페이지로 바로 이동시.
			url = String.format(DomainAddress.getUrlMLC() + "/SSO/StudyGate.aspx?processCd=%s&processYear=%s&processTerm=%s&cSeq=%s&userId=%s"
								, processCd, processYear, processTerm, companySeq, userId);
		}
		
		if("".equals(url))
			super.onFirstResume();
		else
			setLoadUrl(url);
	}
}
