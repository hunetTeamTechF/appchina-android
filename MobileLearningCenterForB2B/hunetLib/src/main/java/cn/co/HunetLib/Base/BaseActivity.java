package cn.co.HunetLib.Base;

import cn.co.HunetLib.Common.AppMain;
import cn.co.HunetLib.Common.Define;
import cn.co.HunetLib.Http.AsyncHttpRequestData;
import cn.co.HunetLib.Http.IAsyncHttpRequestData;
import cn.co.HunetLib.R;
import cn.co.HunetLib.SQLite.SQLiteManager;
import cn.co.HunetLib.Company.Company;
import cn.co.HunetLib.Http.HunetHttpData;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.Toast;

public abstract class BaseActivity extends Activity implements IAsyncHttpRequestData
{	
	private AsyncHttpRequestData m_AsyncReqData = null;
	private ProgressDialog m_LoadingDialog = null;
	private SQLiteManager m_SQLiteManager = null;
	private boolean m_IsFirstResume = true;
	private boolean m_IsFinish = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
                | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
		
		m_AsyncReqData = new AsyncHttpRequestData();
		m_AsyncReqData.SetEventListener(this);
		
		InitActivity(this.getIntent());
	}
	
	@Override 
	protected void onNewIntent(Intent intent)
	{
		super.onNewIntent(intent);
		InitActivity(intent);
	}
	
	private void InitActivity(Intent intent)
	{
		
	}
	
	public Company getCompany()
	{
		return AppMain.CompanyInfo;
	}

	public static Company getCompany2()
	{
		return AppMain.CompanyInfo;
	}
	
	protected abstract void onFirstResume();
	
	protected void setFirstResume(boolean call)
	{
		m_IsFirstResume = call;
	}
	
	@Override
	protected void onResume()
	{
		super.onResume();
		
		if(m_IsFirstResume)
		{
			m_IsFirstResume = false;
			onFirstResume();
		}
	}
	
	public SQLiteManager getSQLiteManager()
	{
		if(m_SQLiteManager == null)
			m_SQLiteManager = new SQLiteManager(this);
		
		return m_SQLiteManager;
	}
		
	public void ShowToast(String msg)
	{
		if(m_IsFinish)
			return;
		
		Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
	}
	
	public void ShowToast(String msg, int gravity, int xOffset, int yOffset)
	{
		if(m_IsFinish)
			return;
		
		Toast toast = Toast.makeText(this, msg, Toast.LENGTH_LONG);
		toast.setGravity(gravity, xOffset, yOffset);
		toast.show();
	}
	
	public void ShowDialog(Context context, String msg, boolean cancelable)
	{
		if(m_IsFinish || context == null || m_LoadingDialog != null)
			return;
		
		m_LoadingDialog = new ProgressDialog(context);
		m_LoadingDialog.setMessage("".equals(msg) ?  getString(R.string.loading) : msg);
		m_LoadingDialog.setIndeterminate(true);
		m_LoadingDialog.setCancelable(cancelable);
		m_LoadingDialog.show();
	}
	
	public void HideDialog()
	{
		if(m_LoadingDialog == null)
			return;
		
		try
		{
			m_LoadingDialog.dismiss();
			m_LoadingDialog = null;
		}
		catch(Exception e) { }
	}
	
	@Override
	protected void onDestroy()
	{
		m_IsFinish = true;
		HideDialog();
		
		super.onDestroy();		
	}
	
	protected void AsyncExecute(int nId, String strUrl, Object objTag)
	{
		String[] arUrl 	= strUrl.split("\\?");
		String url 		= arUrl.length > 0 ? arUrl[0] : "";
		String param 	= arUrl.length > 1 ? arUrl[1] : "";
		
		m_AsyncReqData.Request(nId, url, param, objTag);
	}
	
	protected void AsyncExecute(int nId, String strUrl, String strParam, Object objTag)
	{
		m_AsyncReqData.Request(nId, strUrl, strParam, objTag);
	}	
	
	@Override
	public void AsyncRequestDataResult(HunetHttpData result) {}
	
	protected boolean CheckUpdateVersion(String updateVersion)
	{
		String[] splitCurrent = getPackageVersion().split("[.]");
		String[] splitUpdate = updateVersion.split("[.]");

		// version 정보를 Insert하지 않을 경우
		if(splitUpdate.length <= 1 || splitCurrent.length <= 1)
			return false;
		
		// 앞자리부터 (메이저 -> 마이너 -> 빌드넘버 순) 업데이트 여부 확인
		int updMajor = Integer.parseInt(splitUpdate[0]);
		int updMinor = Integer.parseInt(splitUpdate[1]);
		int updBuild = Integer.parseInt(splitUpdate[2]);
		
		int appMajor = Integer.parseInt(splitCurrent[0]);
		int appMinor = Integer.parseInt(splitCurrent[1]);
		int appBuild = Integer.parseInt(splitCurrent[2]);
		
		// 메이저 버전 비교 (버전이 같으면 다음 버전 비교)
		if (appMajor < updMajor)
			return true;
		else if (appMajor > updMajor)
			return false;

		// 마이너 버전 비교 (버전이 같으면 다음 버전 비교)
		if (appMinor < updMinor)
			return true;
		else if (appMinor > updMinor)
			return false;

		// 빌드넘버 버전 비교
		if (appBuild < updBuild)
			return true;

		return false;
	}
	
	protected String getPackageVersion()
	{
		String curVersion = "";
    	
		try {
			curVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
		} 
		catch (Exception e) 
		{ 
			e.printStackTrace(); curVersion = ""; 
		}
		
		return curVersion;
	}
	
	protected Define getBundleValue(Bundle bundle, String key, Define defaultVal)
	{		
		if(bundle == null)
			return defaultVal;
		
		if(bundle.containsKey(key) == false)
			return defaultVal;
				
		return Define.getFindValue(bundle.getInt(key, 0));
	}
	
	protected int getBundleValue(Bundle bundle, String key, int defaultVal)
	{		
		if(bundle == null)
			return defaultVal;
		
		if(bundle.containsKey(key) == false)
			return defaultVal;
		
		return bundle.getInt(key);
	}
	
	protected String getBundleValue(Bundle bundle, String key, String defaultVal)
	{		
		if(bundle == null)
			return defaultVal;
		
		if(bundle.containsKey(key) == false)
			return defaultVal;
		
		return bundle.getString(key);
	}
	
	protected boolean getBundleValue(Bundle bundle, String key, boolean defaultVal)
	{		
		if(bundle == null)
			return defaultVal;
		
		if(bundle.containsKey(key) == false)
			return defaultVal;
		
		return bundle.getBoolean(key);
	}
	
	protected int getResourceId(Activity activity, String name)
	{
		return activity.getResources().getIdentifier(name, "id", this.getPackageName());
	}
}
