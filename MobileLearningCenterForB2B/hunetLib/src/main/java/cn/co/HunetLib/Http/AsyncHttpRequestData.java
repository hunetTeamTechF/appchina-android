package cn.co.HunetLib.Http;

import android.os.AsyncTask;

public class AsyncHttpRequestData 
{
	private IAsyncHttpRequestData m_EventListener = null;
	private boolean m_bIsCancel = false;
	
	public void SetEventListener(IAsyncHttpRequestData a_EventListener)
	{
		m_EventListener = a_EventListener;
	}
	
	public boolean Request(int nId, String strUrl, String strParam, Object objTag)
	{
		if(m_bIsCancel)
			return false;
		
		AsyncRequestData asynReq 	= new AsyncRequestData();
		HunetHttpData data 			= new HunetHttpData();
		data.m_nId 					= nId;
		data.m_strUrl 				= strUrl;
		data.m_strParam 			= strParam;
		data.m_objTag				= objTag;
				
		asynReq.execute(data);
		
		return true;
	}
	
	public void Cancel() { m_bIsCancel = true; }
	public boolean isCancel() { return m_bIsCancel; }
	
	private class AsyncRequestData extends AsyncTask<HunetHttpData, Void, HunetHttpData>
    {
    	@Override
    	protected HunetHttpData doInBackground(HunetHttpData... datas)
    	{
    		HunetHttpData httpData = datas[0];
    		    			
    		return HunetHttpRequester.GetPostData(httpData);
    	}
    	
    	@Override
    	protected void onPostExecute(HunetHttpData result)
    	{
    		AsyncRequestDataResult(result);
    	}
    }
	
	protected void AsyncRequestDataResult(HunetHttpData result) 
	{
		if(m_EventListener == null)
			return;
		
		m_EventListener.AsyncRequestDataResult(result);
	}	
}
