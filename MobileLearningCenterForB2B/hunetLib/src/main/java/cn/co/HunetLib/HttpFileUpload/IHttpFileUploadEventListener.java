package cn.co.HunetLib.HttpFileUpload;

public interface IHttpFileUploadEventListener extends IHttpProgressUpdateEventListener
{
	public void OnUploadComplete(HttpFileInfo info);
}
