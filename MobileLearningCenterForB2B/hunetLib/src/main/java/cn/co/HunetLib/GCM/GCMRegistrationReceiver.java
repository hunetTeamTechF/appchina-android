package cn.co.HunetLib.GCM;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import hunet.domain.DomainAddress;
import cn.co.HunetLib.Common.AppMain;
import cn.co.HunetLib.Common.DeviceUtility;
import cn.co.HunetLib.SQLite.AppEventModel;
import cn.co.HunetLib.SQLite.SQLiteManager;
import cn.co.HunetLib.Service.HttpSenderService;

/**
 * Created by zeple on 2015-08-25.
 */
public class GCMRegistrationReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();

        if (action.equalsIgnoreCase(GCMRegistUtil.GCM_BROADCAST_RECEIVER)) {
            DeviceUtility deviceUtility = new DeviceUtility(context);
            String type = intent.getStringExtra("type");
            String packageNm = "";
            String modelName = "";
            String androidID = "";
            String loginID = "";

            try {
                packageNm = URLEncoder.encode(context.getPackageName(), "utf-8");
                modelName = URLEncoder.encode(deviceUtility.getModel(), "utf-8");
                androidID = URLEncoder.encode(deviceUtility.getAndroidId(), "utf-8");
                loginID = URLEncoder.encode(AppMain.CompanyInfo.LoginModel.id, "utf-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            if (type.equalsIgnoreCase(GCMRegistUtil.REGISTRATION_COMPLETE)) {
                String token = intent.getStringExtra("token");
                String encToken = "";

                try {
                    encToken = URLEncoder.encode(token, "utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

                AppEventModel model = new AppEventModel();
                model.url = String.format(DomainAddress.getUrlMLC() + "/App/JLog.aspx?type=1002&userId=%s&deviceModel=%s&androidId=%s&gcmId=%s&packageNm=%s&companySeq=%d"
                        , loginID, modelName, androidID, encToken, packageNm, AppMain.getCompany_seq());

                new SQLiteManager(context).InsertAppEvent(model);
                context.startService(new Intent(context, HttpSenderService.class));
            } else if (type.equalsIgnoreCase(GCMRegistUtil.REGISTRATION_NOT_AVAILABLE)) {
                AppEventModel model = new AppEventModel();
                model.url = String.format(DomainAddress.getUrlMLC() + "/App/JLog.aspx?type=1002&userId=%s&deviceModel=%s&androidId=%s&gcmId=%s&packageNm=%s&companySeq=%d"
                        , loginID, modelName, androidID, "none", packageNm, AppMain.getCompany_seq());

                new SQLiteManager(context).InsertAppEvent(model);
                context.startService(new Intent(context, HttpSenderService.class));
            }
        }
    }
}
