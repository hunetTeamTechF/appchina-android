package cn.co.HunetLib;

import hunet.drm.download.DownloadCourseActivity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.webkit.WebView;
import cn.co.HunetLib.Base.TabWebviewActivity;
import cn.co.HunetLib.Company.Company;
import cn.co.HunetLib.Interface.IWebViewEventListener;

public class HLeaderShipActivity extends TabWebviewActivity implements IWebViewEventListener 
{
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
	    super.onCreate(savedInstanceState);
	    setSelectTab(Company.eTAB_H_LEADERSHIP_CLASSROOM);
	    this.setWebViewEventListener(this);
	}
	
	@Override
	public void onClickTab(int selectTab, int beforeSelectTab) 
	{
		switch(selectTab)
		{
		case Company.eTAB_HOME: this.finish(); break;
		case Company.eTAB_H_LEADERSHIP_CLASSROOM: 	setLoadUrl("http://h-leadership.hunet.co.kr/Study/MClassRoom"); break;
		case Company.eTAB_H_LEADERSHIP_LECTURE: 	setLoadUrl("http://h-leadership.hunet.co.kr/Study/MLecture"); break;
		case Company.eTAB_H_LEADERSHIP_MENTOR: 		setLoadUrl("http://h-leadership.hunet.co.kr/Mentoring/MMentor"); break;
		case Company.eTAB_H_LEADERSHIP_DOWN:
			Intent intent = new Intent(this, DownloadCourseActivity.class);
			startActivity(intent);
			return;
		}
	}

	@Override
	public void OnPageFinished(WebView view, String url) 
	{

	}

	@Override
	public void OnPageStarted(WebView view, String url, Bitmap favicon) 
	{
		String tempUrl = url.toLowerCase();
		
		if(tempUrl.contains("/study/mclassroom"))
			this.m_TabControl.ChangeHLeaderShipTab(Company.eTAB_H_LEADERSHIP_CLASSROOM);
		else if(tempUrl.contains("/study/mlecture"))
			this.m_TabControl.ChangeHLeaderShipTab(Company.eTAB_H_LEADERSHIP_LECTURE);
		else if(tempUrl.contains("/mentoring/mmentor"))
			this.m_TabControl.ChangeHLeaderShipTab(Company.eTAB_H_LEADERSHIP_MENTOR);
		else if(tempUrl.contains("/intro/magora"))
		{	
		}		
	}

	@Override
	public boolean OnShouldOverrideUrlLoading(WebView view, String url) 
	{		
		return false;
	}

	@Override
	public void OnReceivedError(WebView view, int errorCode, String description, String failingUrl) 
	{
		
	}
}
