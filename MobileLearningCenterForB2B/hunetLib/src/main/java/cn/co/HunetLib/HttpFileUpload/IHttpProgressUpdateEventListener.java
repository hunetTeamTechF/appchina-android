package cn.co.HunetLib.HttpFileUpload;

public interface IHttpProgressUpdateEventListener 
{
	public void OnProgressUpdate(int bytes);
}
