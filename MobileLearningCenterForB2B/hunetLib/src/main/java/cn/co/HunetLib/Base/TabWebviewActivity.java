package cn.co.HunetLib.Base;

import cn.co.HunetLib.Common.UrlAction;
import cn.co.HunetLib.HttpFileUpload.AsyncHttpFileUpload;
import cn.co.HunetLib.HttpFileUpload.IAsyncHttpFileUploadEventListener;
import cn.co.HunetLib.SQLite.LoginModel;
import hunet.domain.DomainAddress;

import java.net.URLDecoder;
import java.util.List;

import cn.co.HunetLib.LoginActivity;
import cn.co.HunetLib.R;
import cn.co.HunetLib.Common.HunetWebChromeClient;
import cn.co.HunetLib.Company.Company;
import cn.co.HunetLib.HttpFileUpload.HttpFileInfo;
import cn.co.HunetLib.Interface.ITabControlEventListener;
import cn.co.HunetLib.Interface.IWebViewEventListener;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.webkit.WebChromeClient;
import android.widget.LinearLayout;
import android.net.Uri;
import android.webkit.ValueCallback;

import java.io.File;


import android.os.Parcelable;
import android.os.Environment;
import android.provider.MediaStore;



public abstract class TabWebviewActivity extends TabActivity implements ITabControlEventListener, IAsyncHttpFileUploadEventListener
{
	private IWebViewEventListener m_WebViewListener;
//	private LinearLayout m_TabMenu;
	protected WebView m_WebView;
	protected UrlAction m_UrlAction;
	private AsyncHttpFileUpload m_AsynFileUpload = null;
	protected String m_NextUrl = "";
	private ValueCallback<Uri> filePathCallbackNormal;
	private ValueCallback<Uri[]> filePathCallbackLollipop;
	private Uri mCapturedImageURI;



	@Override
	public void onCreate(Bundle savedInstanceState)
	{
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.webview_layout);
	    m_WebView 	= (WebView)findViewById(R.id.main_webView);
	    
	    if(getCompany().LoginModel == null)
	    {
	    	finish();
	    	return;
	    }	    
	    
	    InitWebView(m_WebView);
	    m_UrlAction = getCompany().getUrlAction();
	    m_UrlAction.Init(this, m_WebView);
	    
	    CreateTabControl((LinearLayout)findViewById(R.id.main_tabMenu));
	    CheckNextUrl(this.getIntent());
	}
	
	@Override 
	protected void onNewIntent(Intent intent)
	{
		super.onNewIntent(intent);
		CheckNextUrl(intent);
	}
	
	protected void CheckNextUrl(Intent intent)
	{
		Bundle bundle = intent.getExtras();
		m_NextUrl = URLDecoder.decode(this.getBundleValue(bundle, "NEXT_URL", ""));
		
		if("".equals(m_NextUrl))
			return;
		
		setFirstResume(true);
	    m_WebView.setVisibility(View.INVISIBLE);
	}
	
	@Override
	protected void setSelectTab(int selectTab)
	{
		super.setSelectTab(selectTab);
		
		if(m_UrlAction != null)
			m_UrlAction.setTabId(selectTab);
	}
	
	@Override
	protected void onFirstResume()
	{
		String url = getCompany().getUrl(getCompany().getTabUrlType(m_TabControl.getSelectTab()));
	
		if("".equals(url))
			return;
		
		setLoadUrl(url);
	}
	
	@Override
	protected void onResume()
	{
		super.onResume();
				
		if(m_UrlAction != null)
			m_UrlAction.CheckAction();
	}
	
	public void setWebViewEventListener(IWebViewEventListener webViewListener)
	{
		m_WebViewListener = webViewListener;
	}
	
	protected void setLoadUrl(String url)
	{
		m_WebView.loadUrl(url);
	}
	
	@SuppressLint("SetJavaScriptEnabled")
	protected void InitWebView(WebView webview)
	{	
		WebSettings setting = webview.getSettings();
		setting.setUserAgentString(setting.getUserAgentString() + " HunetAndroid ");
		setting.setJavaScriptEnabled(true);
		webview.addJavascriptInterface(this, "ScriptBridge");
		
		webview.setHorizontalScrollBarEnabled(false);
		webview.setVerticalScrollBarEnabled(false);
		webview.setWebChromeClient(new HunetWebChromeClient()
		{
			// For Android < 3.0
			public void openFileChooser(ValueCallback<Uri> uploadMsg) {
				openFileChooser(uploadMsg, "");
			}

			// For Android 3.0+
			public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType) {
				filePathCallbackNormal = uploadMsg;
				Intent i = new Intent(Intent.ACTION_GET_CONTENT);
				i.addCategory(Intent.CATEGORY_OPENABLE);
				i.setType("image/*");
				startActivityForResult(Intent.createChooser(i, "File Chooser"), 1);
			}

			// For Android 4.1+
			public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture) {
				openFileChooser(uploadMsg, acceptType);
			}


			// For Android 5.0+
			public boolean onShowFileChooser(
					WebView webView, ValueCallback<Uri[]> filePathCallback,
					WebChromeClient.FileChooserParams fileChooserParams) {
				if (filePathCallbackLollipop != null) {
//                    filePathCallbackLollipop.onReceiveValue(null);
					filePathCallbackLollipop = null;
				}
				filePathCallbackLollipop = filePathCallback;


				// Create AndroidExampleFolder at sdcard
				File imageStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "AndroidExampleFolder");
				if (!imageStorageDir.exists()) {
					// Create AndroidExampleFolder at sdcard
					imageStorageDir.mkdirs();
				}

				// Create camera captured image file path and name
				File file = new File(imageStorageDir + File.separator + "IMG_" + String.valueOf(System.currentTimeMillis()) + ".jpg");
				mCapturedImageURI = Uri.fromFile(file);

				Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
				captureIntent.putExtra(MediaStore.EXTRA_OUTPUT, mCapturedImageURI);

				Intent i = new Intent(Intent.ACTION_GET_CONTENT);
				i.addCategory(Intent.CATEGORY_OPENABLE);
				i.setType("image/*");

				// Create file chooser intent
				Intent chooserIntent = Intent.createChooser(i, "Image Chooser");
				// Set camera intent to file chooser
				chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Parcelable[]{captureIntent});

				// On select image call onActivityResult method of activity
				startActivityForResult(chooserIntent, 2);
				return true;

			}
		});

		webview.setWebViewClient(new WebViewClient() 
		{
			@Override
			public void onPageStarted(WebView view, String url, Bitmap favicon) 
			{
				ShowDialog(TabWebviewActivity.this, "", true);
				
				if(m_WebViewListener != null)
					m_WebViewListener.OnPageStarted(view, url, favicon);

				super.onPageStarted(view, url, favicon);
			}
			
			@Override
			public void onPageFinished(WebView view, String url) 
			{
				HideDialog();
				
				if(m_WebViewListener != null)
					m_WebViewListener.OnPageFinished(view, url);
							
				super.onPageFinished(view, url);
				
				if("".equals(m_NextUrl))
				{
					m_WebView.setVisibility(View.VISIBLE);
					return;
				}
				
				m_WebView.clearHistory();
				m_WebView.loadUrl(m_NextUrl);
				m_NextUrl = "";
			}
			
			@Override
		    public boolean shouldOverrideUrlLoading(WebView view, String url) 
			{					
				//String tempUrl = url.toLowerCase();


				if (url.contains("httpweb://")) {
					url = url.replace("httpweb", "http");
					view.getContext().startActivity(
							new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
					return true;
				} else {
					if(m_WebViewListener != null && m_WebViewListener.OnShouldOverrideUrlLoading(view, url))
						return true;

					if(m_UrlAction != null && m_UrlAction.CheckUrl(url)){
						return true;
					}

				}

		        return false;
	    	}
			
			@Override
			public void onReceivedError(WebView view, int errorCode, String description, String failingUrl)
			{					
				if(m_WebViewListener != null)
					m_WebViewListener.OnReceivedError(view, errorCode, description, failingUrl);
			}
		});
	}
	
	public void goToLogin(final String returnUrl) {
		LoginModel model 	= getSQLiteManager().GetLoginData(getCompany().LoginModel.id);
		model.auto_login 	= "0";
		model.loginYn 		= "N";
		getSQLiteManager().SetLoginData(model);
		getCompany().LoginModel = null;
		
		Intent intent = new Intent(this, LoginActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
		finish();
    }
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == 1) {

			if (filePathCallbackNormal == null) return;
			Uri result = (data == null || resultCode != RESULT_OK) ? null : data.getData();
			filePathCallbackNormal.onReceiveValue(result);
			filePathCallbackNormal = null;
		} else if (requestCode == 2) {
			Uri[] result = new Uri[0];
			if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
				if(resultCode == RESULT_OK){
					result = (data == null) ? new Uri[]{mCapturedImageURI} : WebChromeClient.FileChooserParams.parseResult(resultCode, data);
				}
				filePathCallbackLollipop.onReceiveValue(result);
			}
		}


		if(resultCode != RESULT_OK)
			return;
		
		switch(requestCode)
		{
		case Company.eACTIVITY_RESULT_GALLERY_IMG:
			
			if(m_AsynFileUpload == null)
				m_AsynFileUpload = new AsyncHttpFileUpload(this);
			
			String 		checkData 	= data.getStringExtra("checkData");
			String[] 	arCheckData = checkData.split(",");
			
			for(int nIndex = 0; nIndex < arCheckData.length; nIndex++)
			{
				m_AsynFileUpload.AddFileInfo(arCheckData[nIndex], getCompany().getGalleryUploadUrl());
			}
			
			m_AsynFileUpload.SetEventListener(this);
			m_AsynFileUpload.DoUpload();
			break;
		}
	}
	
	@Override
	public void OnFileUploadComplete(HttpFileInfo info) 
	{
		if(info.code != 0)
			return;
		
//		m_WebView.loadUrl(String.format("javascript:ReadingPhoto('|%s');", info.message));
	}

	@Override
	public void OnJobComplete(List<HttpFileInfo> infos) 
	{		
		String strFileNames = "";
		
		for(int nIndex = 0; nIndex < infos.size(); nIndex++)
		{
			HttpFileInfo info = infos.get(nIndex);
			
			if(info.code != 0)
				continue;
			
			strFileNames += "|" + info.message;
		}
		
		//Log.i("hunet", String.format("OnFileUploadComplete >= javascript:ReadingPhoto('%s');", strFileNames));
		
		if(strFileNames.length() > 0)
			m_WebView.loadUrl(String.format("javascript:ReadingPhoto('%s');", strFileNames));
		
		m_AsynFileUpload.SetEventListener(null);
		m_AsynFileUpload = null;
	}
	
	@Override 
	public boolean onKeyDown(int keyCode, KeyEvent event) 
	{
		switch(keyCode)
		{
		case KeyEvent.KEYCODE_BACK:
			{
				if(m_WebView != null && m_WebView.canGoBack())
				{
					m_WebView.goBack();
					return true;
				}
			}
			break;
		}
		
		return super.onKeyDown(keyCode, event);
	}
	
	@Override
	protected void onDestroy()
	{
		super.onDestroy();
		
		if(m_UrlAction == null)
			return;
		
		m_UrlAction.Clear();
		m_UrlAction = null;
	}
	
	/**
	 * 앱 설치 여부를 확인합니다.
	 * @param packageName 패키지 명
	 * @return 설치 여부
	 */
	public boolean existsInstalledApp(String packageName) {
		return getPackageManager().getLaunchIntentForPackage(packageName) != null;
	}
	
	/**
	 * 스테이징 접속 여부를 반환합니다.
	 * @return 스테이징 접속 여부
	 */
	public boolean isStaging() {
		return DomainAddress.IS_STAGING;
	}
}
