package cn.co.hunet.MobileLearningCenterForAvene.TabClassLoader;

import cn.co.HunetLib.Common.AppMain;
import cn.co.HunetLib.Company.Company;
import cn.co.HunetLib.Control.TabControl;
import cn.co.HunetLib.Control.TabItem;
//import TabItem;
public class TabControl_c extends TabControl
{
	public TabControl_c() {
		// Background, Icon Color, Text Color, Selected Icon Color, Selected Text Color
		super("#4A4B4D", "#7A7A7B", "#7A7A7B", "#FFFFFF", "#FFFFFF");
	}


	@Override
	public void InitTab(int nSelectTab) {
		selected_tab = nSelectTab;

		if(nSelectTab == Company.eTAB_SANGSANG)
		{
			CreateSangSangTab();
			return;
		}
		else if(nSelectTab == Company.eTAB_H_LEADERSHIP_CLASSROOM)
		{
			CreateHLeadership();
			return;
		}

		InitTabContainer();

		/* 탭 메뉴 */
		TabItem tab_1 = createTabItem();
		TabItem tab_2 = createTabItem();
		TabItem tab_3 = createTabItem();
		TabItem tab_4 = createTabItem();
		TabItem tab_5 = createTabItem();

		tab_1.tabLayout.setTag(Company.eTAB_HOME);
		tab_2.tabLayout.setTag(nSelectTab == Company.eTAB_LECTURE_INMUN ? Company.eTAB_LECTURE_INMUN : Company.eTAB_LECTURE);
		tab_5.tabLayout.setTag(nSelectTab >= Company.eTAB_MORE ? nSelectTab : Company.eTAB_MORE);


		tab_1.init(cn.co.HunetLib.R.drawable.tab_home, "首页");
		tab_2.init(cn.co.HunetLib.R.drawable.tab_process, "课程信息");
		tab_3.init(cn.co.HunetLib.R.drawable.tab_classroom, "我的课堂");
		tab_4.init(cn.co.HunetLib.R.drawable.tab_download, "下载");
		tab_5.init(cn.co.HunetLib.R.drawable.tab_setting, "设置");

		if (nSelectTab == Company.eTAB_HOME)
			tab_1.selected();
		else if (nSelectTab == Company.eTAB_LECTURE || nSelectTab == Company.eTAB_LECTURE_INMUN)
			tab_2.selected();
		else if (nSelectTab == Company.eTAB_CLASSROOM)
			tab_3.selected();
		else if (nSelectTab == Company.eTAB_DOWNLOAD_CENTER)
			tab_4.selected();
		else if (nSelectTab >= Company.eTAB_MORE)
			tab_5.selected();

		tab_3.tabLayout.setTag(Company.eTAB_CLASSROOM);
		tab_4.tabLayout.setTag(Company.eTAB_DOWNLOAD_CENTER);

		tab_1.tabLayout.setOnClickListener(this);
		tab_2.tabLayout.setOnClickListener(this);
		tab_3.tabLayout.setOnClickListener(this);
		tab_4.tabLayout.setOnClickListener(this);
		tab_5.tabLayout.setOnClickListener(this);

		tab_container.addView(tab_1.tabLayout);
		//tab_container.addView(tab_2.tabLayout);
		tab_container.addView(tab_3.tabLayout);
		tab_container.addView(tab_4.tabLayout);
		tab_container.addView(tab_5.tabLayout);
	}
}
