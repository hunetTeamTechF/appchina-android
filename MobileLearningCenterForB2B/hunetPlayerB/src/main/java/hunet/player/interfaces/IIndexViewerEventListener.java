package hunet.player.interfaces;

import hunet.player.controls.VolumeBar;
import android.view.MotionEvent;
import android.view.View;

public interface IIndexViewerEventListener 
{
	public boolean OnIndexClose();
	public boolean OnIndexChnaged(int markValue);
}
